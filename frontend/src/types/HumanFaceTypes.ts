import { DistanceTaskResponseDto } from './AnalyticalTaskTypes';
import { FaceInfo } from './FileTypes';

export type HumanFace = {
	id?: number;
	isAverageFace?: boolean;
	isPrimaryFace?: boolean;
	hasFeaturePoints?: boolean;
	hasSymmetryPlane?: boolean;
	info?: FaceInfo;
	texture?: string;
	distances?: DistanceTaskResponseDto;
	samples?: HumanFaceSamples;
	geometry?: HumanFaceGeometry;
	symmetryPlane?: HumanFaceSymmetryPlaneDto;
	featurePoints?: HumanFaceFeaturePointsDto;
	colorsOfHeatmap?: number[];
	boundingBox?: HumanFaceBoundingBox;
};

export type CurvatureTaskDto = {
	taskId: number;
	curvatureMethod: CurvatureMethod;
};

export enum CurvatureMethod {
	Min = 'MIN',
	Max = 'MAX',
	Mean = 'MEAN',
	Gaussian = 'GAUSSIAN'
}

export type CurvatureTaskResponseDto = {
	faceId: number;
	heatmap: number[];
};

export type TransformationDto = {
	translation?: Point;
	rotation?: Point;
};

export type HumanFaceGeometry = {
	indices: number[];
	positions: number[];
	normals: number[];
	uvs: number[];
	numberOfFacets: number;
};

export type HumanFaceSamples = {
	humanFaceId?: number;
	positions: number[];
};

export type HumanFaceFeaturePointsDto = {
	featurePointDtos: FeaturePointDto[];
};

export type HumanFaceBoundingBox = {
	min: Point;
	max: Point;
	mid: Point;
	diagonalLength: number;
};

export type FeaturePointDto = {
	point: Point;
	featurePointType: number;
	relationToMesh?: RelationToMeshDto;
	name: string;
	description: string;
	color?: string;
	size: number;
	weight?: number;
	selected?: boolean;
	hovered?: boolean;
};

export enum FeaturePointCategory {
	STANDARD,
	CUSTOM,
	UNDEFINED
}

export type RelationToMeshDto = {
	nearestPoint: Point;
	distance: number;
};

export type HumanFaceSymmetryPlaneDto = {
	positions: number[];
	normals: number[];
	precision: number;
};

export type Point = {
	x: number;
	y: number;
	z: number;
};

export type HumanFaceUpdate = {
	taskId?: number;
	transformationDto?: TransformationDto;
	featurePointsUpdate?: HumanFaceFeaturePointsUpdate;
	symmetryPlaneUpdate?: HumanFaceSymmetryPlaneUpdate;
};

export type HumanFaceFeaturePointsUpdate = {
	positions: number[];
	nearestPointsPositions?: number[];
};

export type HumanFaceSymmetryPlaneUpdate = {
	symmetryTransformationDto: TransformationDto;
};

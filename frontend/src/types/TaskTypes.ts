export enum TaskType {
	SingleFaceAnalysis = 'SINGLE_FACE_ANALYSIS',
	PairComparison = 'PAIR_COMPARISON',
	BatchProcessing = 'BATCH_PROCESSING'
}

export type Task = {
	id: number;
	projectId: number;
	taskParametersId: number;
	name: string;
	taskType: TaskType;
};

export type TaskCreateDto = {
	fileUploadIds: number[];
	projectId: number;
};

export type TaskSimple = {
	id: number;
	name: string;
};

import React from 'react';

export type HeatmapRowData = {
	faceName: string;
	faceIndex: number;
	columns: { faceName: string; faceIndex: number; distance: number }[];
};

export type Transform = {
	scale: number;
	translateX: number;
	translateY: number;
};

export type SerializableClusterNode = {
	name: string;
	children: SerializableClusterNode[];
	leafNames: string[];
	distance: number;
};

export type PairStep = {
	pair: [string, string];
	resultName: string;
	distance: number;
};

export enum LinkageStrategy {
	SINGLE = 'SINGLE',
	COMPLETE = 'COMPLETE',
	AVERAGE = 'AVERAGE'
}

export type RowState = {
	event: React.MouseEvent<HTMLCanvasElement> | null;
	draggedIndex: number | null;
	targetIndex: number | null;
	clickedName: string | null;
	lastClickedName: string | null;
	lastClickedIndex: number | null;
};

export type ColumnState = {
	event: React.MouseEvent<HTMLCanvasElement> | null;
	draggedIndex: number | null;
	targetIndex: number | null;
	clickedName: string | null;
	lastClickedName: string | null;
	lastClickedIndex: number | null;
};

export type HiddenItems = {
	faces: string[];
	selectedFaces: string[];
};

export type ContextMenuState = {
	mouseX: number;
	mouseY: number;
	type: 'row' | 'column' | 'cell';
	rowName: string | null;
	rowIndex: number | null;
	columnName: string | null;
	columnIndex: number | null;
	pairDistance: number | null;
};

export type DistanceState = {
	min: number;
	max: number;
	pairDistance: number | null;
};

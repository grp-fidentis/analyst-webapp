import { HumanFaceSymmetryPlaneDto, TransformationDto } from './HumanFaceTypes';

export type SamplingTaskDto = {
	taskId?: number;
	samplingStrategy: PointSamplingStrategy;
	samplingStrength: number;
};

export enum PointSamplingStrategy {
	None = 'NONE',
	RandomSampling = 'RANDOM_SAMPLING',
	ShapePoissonDiskSubSampling = 'SHAPE_POISSON_DISK_SUB_SAMPLING',
	NormalPoissonDiskSubSampling = 'NORMAL_POISSON_DISK_SUB_SAMPLING',
	GaussianCurvature = 'GAUSSIAN_CURVATURE',
	MeanCurvature = 'MEAN_CURVATURE',
	MaxCurvature = 'MAX_CURVATURE',
	MinCurvature = 'MIN_CURVATURE',
	UniformSpaceSampling = 'UNIFORM_SPACE_SAMPLING',
	UniformMeshSampling = 'UNIFORM_MESH_SAMPLING'
}

export type RegistrationTaskDto = {
	taskId: number;
	registrationMethod: RegistrationMethod;
	scale?: boolean;
	icpMaxIter?: number;
	icpMinError?: number;
	icpAutoCrop?: boolean;
	samplingTaskDto: SamplingTaskDto;
	distanceTaskDto: DistanceTaskDto;
};

export enum RegistrationMethod {
	Icp = 'ICP',
	FeaturePoints = 'FEATURE_POINTS',
	Symmetry = 'SYMMETRY'
}

export type DistanceTaskDto = {
	taskId: number;
	distanceStrategy?: DistanceStrategy;
	crop?: boolean;
	useRelativeDistance?: boolean;
	heatmapRenderMode?: HeatmapRenderMode;
	transformationDto?: TransformationDto;
	exportType?: ExportType;
	featurePointSizes?: FeaturePointSize[];
	recomputeDistances?: boolean;
};

export type FeaturePointSize = {
	name: string;
	size: number;
};

export enum HeatmapRenderMode {
	Standard = 'STANDARD',
	Weighted = 'WEIGHTED',
	None = 'NONE'
}

export enum ExportType {
	DISTANCE,
	WEIGHTED_DISTANCE
}

export enum DistanceStrategy {
	PointToPoint = 'POINT_TO_POINT_NEAREST_NEIGHBORS',
	PointToTriangle = 'POINT_TO_TRIANGLE_NEAREST_NEIGHBORS',
	RayCasting = 'RAY_CASTING'
}

export type DistanceTaskResponseDto = {
	meanDistance: number;
	weightedMeanDistance: number;
	featurePointWeights: FeaturePointWeight[];
	colorsOfHeatmap?: number[];
};

export type FeaturePointWeight = {
	featurePointName: string;
	weight: number;
};

export enum LoadingOperation {
	None,
	Registration,
	Alignment,
	Samples,
	Heatmap,
	Distance,
	ExportDistance,
	ExportWeightedDistance,
	SelectSignificant,
	Symmetry,
	Files
}

export enum ShowSpheres {
	HideAll,
	ShowSelected,
	ShowAll
}

export enum SymmetryMethod {
	FastMeshSurface = 'FAST_MESH_SURFACE',
	RobustMeshSurface = 'ROBUST_MESH_SURFACE',
	RobustMeshVertices = 'ROBUST_MESH_VERTICES',
	FeaturePoints = 'FEATURE_POINTS'
}

export type SymmetryResponseTaskDto = {
	humanFaceId: number;
	symmetryPlane: HumanFaceSymmetryPlaneDto;
};

export type SymmetryTaskDto = {
	taskId: number;
	symmetryMethod: SymmetryMethod;
	candidateSearchSampling: SamplingTaskDto;
	candidatePruningSampling: SamplingTaskDto;
};

export type Point = {
	x: number;
	y: number;
	z: number;
};

export enum CuttingPlanesDirection {
	DirectionVertical = 'DIRECTION_VERTICAL',
	DirectionHorizontal = 'DIRECTION_HORIZONTAL',
	DirectionFrontBack = 'DIRECTION_FRONT_BACK'
}

export enum CuttingPlanesType {
	SymmetryPlane = 'SYMMETRY_PLANE',
	BoundingBoxPlane = 'BOUNDING_BOX_PLANE'
}

export type CuttingPlaneDto = {
	cuttingPlaneId: string;
	type: CuttingPlanesType;
	direction: CuttingPlanesDirection;
	order: number;
	positions: number[];
	normals: number[];
};

export type CuttingPlaneIntersection = {
	cuttingPlaneId: string;
	isPrimaryFace: boolean;
	order: number;
	curveSegments: Point2D[][];
	featurePoints: Point2D[];
	normals: Line2D[];
};

export type Line2D = {
	start: Point2D;
	end: Point2D;
};

export type Point2D = {
	x: number;
	y: number;
};

export type IntersectionDto = {
	cuttingPlanesIntersections: CuttingPlaneIntersection[];
};

export type HausdorffDistance = {
	order: number;
	distance: number;
};

export type CuttingPlanes2DProjectionResponseDto = {
	intersectionPointsDtos: IntersectionDto[];
	hausdorffDistances: HausdorffDistance[];
	boundingBoxDto: Bounding2DBox;
};

export type CuttingPlanes2DProjectionDto = {
	taskId: number;
	direction: CuttingPlanesDirection;
	cuttingPlaneStates: CuttingPlaneFrontendState[];
	samplingStrength: number;
	reducedVertices: number;
	canvasWidth?: number;
	canvasHeight?: number;
};

export type Bounding2DBox = {
	offsetX: number;
	offsetY: number;
	scale: number;
};

export type CuttingPlanesTaskDto = {
	state: CuttingPlanesState;
	projectionResponse?: CuttingPlanes2DProjectionResponseDto;
};

export type CuttingPlanesState = {
	taskId: number;
	currentDirection: CuttingPlanesDirection;
	samplingStrength: number;
	reducedVertices: number;
	showNormals: boolean;
	normalVectorLength: number;
	cuttingPlanes?: CuttingPlaneFrontendState[];
	canvasWidth: number;
	canvasHeight: number;
};

export type CuttingPlaneFrontendState = {
	cuttingPlaneId: string;
	isVisible: boolean;
	isSelected: boolean;
	isHover: boolean;
	sliderValue: number;
	type: CuttingPlanesType;
	direction: CuttingPlanesDirection;
	order: number;
	transformationDto: TransformationDto;
	positions: number[];
	normals: number[];
};

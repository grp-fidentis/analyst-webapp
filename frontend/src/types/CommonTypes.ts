export type ErrorResponse = {
	error: string;
	exception: string;
	message: string;
	status: string;
	timestamp: string;
};

export type Point3d = {
	x: number;
	y: number;
	z: number;
};

export type NonNullableFields<T> = {
	[P in keyof T]: NonNullable<T[P]>;
};

export type NonNullableField<T, K extends keyof T> = T &
	NonNullableFields<Pick<T, K>>;

export type TabInfo = {
	label: string;
	icon: JSX.Element;
};

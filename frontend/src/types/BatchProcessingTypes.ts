import { FileUpload } from './FileTypes';
import { LinkageStrategy } from './BatchVisualizationTypes';
import { HumanFace } from './HumanFaceTypes';

export enum AveragingMethod {
	NearestNeighbours = 'NEAREST_NEIGHBOURS',
	RayCasting = 'RAY_CASTING',
	Skip = 'SKIP'
}

export enum RegistrationMethod {
	MeshBasedICP = 'MESH_BASED_ICP',
	FeaturePoints = 'FEATURE_POINTS',
	SkipRegistration = 'SKIP_REGISTRATION'
}

export enum SimilarityMethod {
	IndirectDistance = 'INDIRECT_DISTANCE',
	IndirectVectors = 'INDIRECT_VECTORS',
	IndirectCombined = 'INDIRECT_COMBINED',
	IndirectRayCasting = 'INDIRECT_RAY_CASTING',
	PairwiseTwoWay = 'PAIRWISE_TWO_WAY',
	PairwiseTwoWayCropped = 'PAIRWISE_TWO_WAY_CROPPED'
}

export type BatchRegistrationTaskDto = {
	taskId: number;
	projectId: number;
	selectedFile: FileUpload | null;
	fileIds: number[];
	averagingTaskDto: AveragingTaskDto;
	registrationTaskDto: RegistrationTaskDto;
	calculationWebSocketSessionId: string | unknown;
};

export type BatchDistanceTaskDto = {
	taskId: number;
	selectedFile: FileUpload | null;
	fileIds: number[];
	similarityMethod: SimilarityMethod;
	calculationWebSocketSessionId: string | unknown;
};

export type FaceDistancesDto = {
	faceName: string;
	faceDistances: number[];
	faceDeviations: number[];
};

export type BatchDistancesDto = {
	faceDistances: FaceDistancesDto[];
	taskId: number;
	linkageStrategy: LinkageStrategy;
};

export type AveragingTaskDto = {
	taskId: number;
	averagingMethod: AveragingMethod;
	maxRegistrationIterations: number;
	stabilizationThreshold: number;
};

export type RegistrationTaskDto = {
	taskId: number;
	registrationMethod: RegistrationMethod;
	registrationSubsampling: number;
	scaleFacesDuringRegistration: boolean;
};

export type AddAverageFaceToProjectDto = {
	humanFaceEntityId: number;
	taskId: number;
	projectId: number;
	fileName: string;
};

export type RegistrationResponseDto = {
	humanFace: HumanFace;
	completedSuccessfully: boolean;
};

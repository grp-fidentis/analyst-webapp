import {
	AveragingMethod,
	RegistrationMethod,
	SimilarityMethod
} from './BatchProcessingTypes';

export type TaskParameters = {
	templateFaceForRegistration: string;
	templateFaceForDistance: string;
	averagingMethod: AveragingMethod;
	maxRegistrationIterations: number;
	stabilizationThreshold: number;
	registrationMethod: RegistrationMethod;
	registrationSubsampling: number;
	scaleFacesDuringRegistration: boolean;
	similarityMethod: SimilarityMethod;
	deviations: string | null;
	distances: string | null;
};

export type TaskParametersRegistrationUpdateDto = {
	templateFaceForRegistration: string;
	averagingMethod: AveragingMethod;
	maxRegistrationIterations: number;
	stabilizationThreshold: number;
	registrationMethod: RegistrationMethod;
	registrationSubsampling: number;
	scaleFacesDuringRegistration: boolean;
};

export type TaskParametersDistanceUpdateDto = {
	templateFaceForDistance: string | null;
	similarityMethod: SimilarityMethod | null;
	deviations: string | null;
	distances: string | null;
};

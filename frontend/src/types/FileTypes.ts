export type FaceInfo = {
	id: number;
	geometryFileName: string;
	geometryFileSize: number;
	featurePointsFileName: string | null;
	numberOfFacets: number | null;
	numberOfVertices: number | null;
	previewPhotoFileName: string | null;
	previewPhotoData: string | null;
	textureFileName: string | null;
	featurePointsNames: string[] | null;
};

export type FileUpload = {
	id: number;
	name: string;
	extension: string;
	smallPreviewPhotoData: string | null;
	faceInfoId: number;
	isAverageFace: boolean;
};

export type AdditionalFileDelete = {
	name: string;
	extension: string;
	fileInfoType: string;
};

export type TaskFile = {
	id: number;
	name: string;
	fileSize: number;
	stringData: string;
	taskId: number;
};

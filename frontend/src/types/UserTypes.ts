type Authority = {
	authority: string;
};

export type Role = {
	id: number;
	name: string;
};

export type UserAuth = {
	username: string;
	password: string;
};

export type UserDetail = {
	id: number | null;
	username: string;
	firstName: string;
	lastName: string;
	email: string;
	roles: Role[];
	enabled: boolean;
	locked: boolean;
	loginType: string;
};

export type User = {
	accountNonExpired: boolean;
	accountNonLocked: boolean;
	authorities: Authority[];
	credentialsNonExpired: boolean;
	dateCreated: string;
	dateEnabled: string | null;
} & UserDetail;

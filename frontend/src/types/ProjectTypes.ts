import { TaskSimple } from "./TaskTypes";

export type Project = {
	id: number;
	name: string;
	isOpened: boolean;
	tasks: TaskSimple[];
};

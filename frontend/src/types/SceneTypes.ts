export type RenderMode = 'TEXTURE' | 'SMOOTH' | 'WIREFRAME' | 'POINTS';

export enum AlignmentOperations {
	TranslateXLeft = 'TranslateXLeft',
	TranslateXRight = 'TranslateXRight',
	TranslateYUp = 'TranslateYUp',
	TranslateYDown = 'TranslateYDown',
	TranslateZBack = 'TranslateZBack',
	TranslateZFront = 'TranslateZFront',

	RotateXUp = 'RotateXUp',
	RotateXDown = 'RotateXDown',
	RotateYLeft = 'RotateYLeft',
	RotateYRight = 'RotateYRight',
	RotateZLeft = 'RotateZLeft',
	RotateZRight = 'RotateZRight'
}

export type Alignment = {
	isActive: boolean;
	operation?: AlignmentOperations;
};

export type SceneFaceVisibility = {
	showFace: boolean;
	showFeaturesPoints: boolean;
	showSymmetryPlane: boolean;
	opacity: number;
};

import { ThemeProvider } from '@mui/material';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { ConfirmProvider } from 'material-ui-confirm';

import 'react-toastify/dist/ReactToastify.min.css';

import { theme } from './utils/Theme';
import AppRoutes from './components/Routes';
import { AuthContextProvider } from './providers/AuthContextProvider';

const App = () => (
	<ThemeProvider theme={theme}>
		<AuthContextProvider>
			<ConfirmProvider>
				<BrowserRouter>
					<AppRoutes />
					<ToastContainer position="bottom-right" />
				</BrowserRouter>
			</ConfirmProvider>
		</AuthContextProvider>
	</ThemeProvider>
);

export default App;

import { AdditionalFileDelete, FaceInfo, FileUpload } from '../types/FileTypes';

import { httpRequest } from './AxiosConfig';

const URL = '/api/file-uploads';

export const FileUploadService = {
	saveFile: async (data: FormData, projectId: number) =>
		(
			await httpRequest.post(`${URL}/${projectId}`, data, {
				headers: { 'Content-Type': 'multipart/form-data' }
			})
		).data,

	deleteFiles: async (ids: number[]) =>
		await httpRequest.delete(URL, { data: ids }),

	getUserFilesOnProject: async (projectId: number): Promise<FileUpload[]> =>
		(await httpRequest.get(`${URL}/projects/${projectId}`)).data,

	uploadAdditionalFile: async (
		data: FormData,
		projectId: number,
		faceInfoId: number
	): Promise<FaceInfo> =>
		(
			await httpRequest.post(
				`${URL}/${projectId}/face-info/${faceInfoId}`,
				data,
				{
					headers: { 'Content-Type': 'multipart/form-data' }
				}
			)
		).data,

	uploadSmallPreviewPhoto: async (
		data: FormData,
		fileUploadId: number
	): Promise<FileUpload> =>
		(await httpRequest.post(`${URL}/${fileUploadId}/preview-photo`, data)).data,

	deleteFileByFaceInfoId: async (
		faceInfoId: number,
		dto: AdditionalFileDelete
	) =>
		await httpRequest.delete(`${URL}/face-info/${faceInfoId}`, {
			data: dto
		}),

	getFilesOfTask: async (taskId: number): Promise<FileUpload[]> =>
		(await httpRequest.get(`${URL}/tasks/${taskId}`)).data
};

import { Task, TaskCreateDto } from '../types/TaskTypes';

import { httpRequest } from './AxiosConfig';

const URL = '/api/tasks';

export const TaskService = {
	createTask: async (task: TaskCreateDto): Promise<Task> =>
		(await httpRequest.post(URL, task)).data,

	deleteTask: async (taskId: number) => {
		await httpRequest.delete(`${URL}/${taskId}`);
	},

	getTaskById: async (taskId: number): Promise<Task> =>
		(await httpRequest.get(`${URL}/${taskId}`)).data,

	getTasksOnProject: async (projectId: number): Promise<Task[]> =>
		(await httpRequest.get(URL, { params: { projectId } })).data,

	updateTaskName: async (taskId: number, name: string): Promise<Task> =>
		(await httpRequest.put(`${URL}/${taskId}`, { name })).data,

	getTaskByName: async (taskName: string): Promise<Task> =>
		(await httpRequest.get(`${URL}/check`, { params: { taskName } })).data
};

/* eslint-disable prefer-arrow/prefer-arrow-functions */

import { HumanFace, HumanFaceUpdate } from '../types/HumanFaceTypes';

import { compressionConfig, httpRequest } from './AxiosConfig';

const URL = '/api/human-faces';

export const HumanFaceService = {
	getHumanFaceByFileUploadIdAndTaskId: async (
		fileUploadId: number | null,
		taskId: number | undefined
	): Promise<HumanFace> =>
		(await httpRequest.get(`${URL}/task/${taskId}/file-upload/${fileUploadId}`))
			.data,

	getHumanFacesOfTask: async (taskId: number): Promise<HumanFace[]> =>
		(await httpRequest.get(`${URL}/task/${taskId}`, compressionConfig)).data,

	saveGeometry: async (
		id: number,
		humanFaceUpdate: HumanFaceUpdate
	): Promise<void> => {
		(await httpRequest.put(`${URL}/${id}/geometry`, humanFaceUpdate)).data;
	},

	getHumanFacesPair: async (
		taskId: number,
		rowFaceName: string,
		columnFaceName: string | null
	): Promise<HumanFace[]> =>
		(
			await httpRequest.get(`${URL}/task/${taskId}/get-pair`, {
				params: { rowFaceName, columnFaceName }
			})
		).data
};

import axios, { AxiosRequestConfig, AxiosResponseHeaders } from 'axios';
import { toast } from 'react-toastify';

import { TokensUtils } from '../utils/TokensUtils';
import { ROUTES } from '../utils/RoutesUrls';
import { CompressionUtils } from '../utils/CompressionUtils';

export const httpRequest = axios.create({
	headers: {
		Authorization: `Bearer ${TokensUtils.getAccessToken()}`
	}
});

export const compressionConfig: AxiosRequestConfig = {
	headers: { 'accept-custom-encoding': 'true' },
	responseType: 'arraybuffer'
};

export const createAxiosResponseInterceptor = () => {
	const interceptor = httpRequest.interceptors.response.use(
		response => {
			const acceptEncoding = response.headers['custom-content-encoding'];
			if (acceptEncoding?.includes('true')) {
				response.data = CompressionUtils.decompress(response.data);
			}
			return response;
		},
		error => {
			// Reject promise if usual error
			if (
				error.response.status !== 401 ||
				error.request.responseURL.includes(ROUTES.chooseLogin)
			) {
				const message = error.response?.data?.message;
				toast.error(
					message ?? `Server error with status: ${error.response.status}`
				);
				return Promise.reject(error);
			}

			/*
			 * When response code is 401, try to refresh the token.
			 * Eject the interceptor, so it doesn't loop in case
			 * token refresh causes the 401 response.
			 *
			 * Must be re-attached later on or the token refresh will only happen once
			 */
			httpRequest.interceptors.response.eject(interceptor);

			return axios
				.get('/api/auth/refresh-token', {
					headers: {
						Authorization: `Bearer ${TokensUtils.getRefreshToken()}`
					}
				})
				.then(response => {
					TokensUtils.setNewTokens(response.headers as AxiosResponseHeaders);
					httpRequest.defaults.headers.Authorization = `Bearer ${TokensUtils.getAccessToken()}`;
					// Retry the initial call, but with the updated token in the headers.
					error.response.config.headers.Authorization = `Bearer ${TokensUtils.getAccessToken()}`;
					// Resolves the promise if successful
					return axios(error.response.config);
				})
				.catch(error2 => {
					// Retry failed, clean up and reject the promise
					toast.error(error2.response?.data.message, {
						autoClose: 5000,
						// when in development server this triggers on the start of notification instead at the end
						onClose: () => (window.location.href = '/choose-login')
					});

					TokensUtils.removeTokens();
					TokensUtils.removeCurrentUser();
					return Promise.reject(error2);
				})
				.finally(createAxiosResponseInterceptor); // Re-attach the interceptor by running the method
		}
	);
};

createAxiosResponseInterceptor();

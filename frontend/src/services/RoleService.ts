import { httpRequest } from './AxiosConfig';

const URL = '/api/roles';

export const RoleService = {
	getRoles: async () => (await httpRequest.get(URL)).data
};

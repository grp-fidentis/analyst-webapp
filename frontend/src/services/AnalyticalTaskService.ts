import { AxiosResponse } from 'axios';

import {
	CuttingPlaneDto,
	CuttingPlanes2DProjectionDto,
	CuttingPlanes2DProjectionResponseDto,
	DistanceTaskDto,
	DistanceTaskResponseDto,
	RegistrationTaskDto,
	SamplingTaskDto,
	SymmetryResponseTaskDto,
	SymmetryTaskDto
} from '../types/AnalyticalTaskTypes';
import {
	CurvatureTaskDto,
	CurvatureTaskResponseDto,
	HumanFace,
	HumanFaceSamples
} from '../types/HumanFaceTypes';

import { compressionConfig, httpRequest } from './AxiosConfig';

const URL = '/api/analytical-tasks';

export const AnalyticalTaskService = {
	executeRegistration: async (
		dto: RegistrationTaskDto
	): Promise<HumanFace | null> => {
		const response: AxiosResponse = await httpRequest.post(
			`${URL}/registration/execute`,
			dto,
			compressionConfig
		);
		// @ts-ignore
		const length = Number(response?.headers?.getContentLength());
		return length === 0 ? null : response.data;
	},

	calculateSamplePoints: async (
		dto: SamplingTaskDto
	): Promise<HumanFaceSamples> =>
		(await httpRequest.post(`${URL}/sampling/calculate`, dto)).data,

	calculateDistance: async (
		dto: DistanceTaskDto
	): Promise<DistanceTaskResponseDto> =>
		(await httpRequest.post(`${URL}/distance/calculate-distance`, dto)).data,

	calculateDistanceHeatmap: async (
		dto: DistanceTaskDto
	): Promise<DistanceTaskResponseDto> =>
		(await httpRequest.post(`${URL}/distance/calculate-heatmap`, dto)).data,

	exportDistance: async (dto: DistanceTaskDto): Promise<Blob> =>
		(
			await httpRequest.post(`${URL}/distance/export-distance`, dto, {
				responseType: 'blob'
			})
		).data,

	calculateSymmetryPlane: async (
		dto: SymmetryTaskDto
	): Promise<SymmetryResponseTaskDto[]> =>
		(await httpRequest.post(`${URL}/symmetry/calculate`, dto)).data,

	createCuttingPlanes: async (taskId: number): Promise<CuttingPlaneDto[]> =>
		(await httpRequest.post(`${URL}/cutting-planes/task/${taskId}`)).data,

	calculate2DProjection: async (
		dto: CuttingPlanes2DProjectionDto
	): Promise<CuttingPlanes2DProjectionResponseDto> =>
		(
			await httpRequest.post(
				`${URL}/cutting-planes/calculate-2D-projection`,
				dto
			)
		).data,

	exportProjectionsAsZip: async (
		dto: CuttingPlanes2DProjectionDto
	): Promise<AxiosResponse<Blob>> =>
		await httpRequest.post(`${URL}/cutting-planes/export`, dto, {
			responseType: 'blob'
		}),

	calculateCurvatureHeatmap: async (
		dto: CurvatureTaskDto
	): Promise<CurvatureTaskResponseDto> =>
		(await httpRequest.post(`${URL}/curvature/calculate-heatmap`, dto)).data
};

import { Project } from '../types/ProjectTypes';

import { httpRequest } from './AxiosConfig';

const URL = '/api/projects';

export const ProjectService = {
	getUserProjects: async (isOpen?: boolean): Promise<Project[]> =>
		(await httpRequest.get(URL, { params: { isOpen } })).data,

	createNewProject: async (name: string): Promise<Project> =>
		(await httpRequest.post(URL, null, { params: { name } })).data,

	setProjectOpenState: async (id: number, open: boolean) => {
		await httpRequest.put(`${URL}/${id}/set-open-status`, null, {
			params: { open }
		});
	},

	deleteProject: async (id: number) => {
		await httpRequest.delete(`${URL}/${id}`);
	}
};

import { AxiosResponse } from 'axios';

import {
	AddAverageFaceToProjectDto,
	BatchDistancesDto,
	BatchDistanceTaskDto,
	BatchRegistrationTaskDto,
	RegistrationResponseDto
} from '../types/BatchProcessingTypes';
import {
	HeatmapRowData,
	SerializableClusterNode
} from '../types/BatchVisualizationTypes';

import { compressionConfig, httpRequest } from './AxiosConfig';

const URL = '/api/batch-processing';

export const BatchProcessingTaskService = {
	executeRegistration: async (
		dto: BatchRegistrationTaskDto
	): Promise<RegistrationResponseDto> => {
		const response: AxiosResponse = await httpRequest.post(
			`${URL}/registration/execute`,
			dto,
			compressionConfig
		);
		return response.data;
	},

	addAverageFaceToProject: async (
		dto: AddAverageFaceToProjectDto
	): Promise<void> => {
		await httpRequest.post(
			`${URL}/registration/add-average-face-to-project`,
			dto,
			compressionConfig
		);
	},

	exportAverageFace: async (
		humanFaceEntityId: number | undefined
	): Promise<Blob> =>
		(
			await httpRequest.post(
				`${URL}/registration/export-average-face/${humanFaceEntityId}`,
				{},
				{
					responseType: 'blob'
				}
			)
		).data,

	computeDistance: async (
		dto: BatchDistanceTaskDto
	): Promise<BatchDistancesDto | null> => {
		const response: AxiosResponse = await httpRequest.post(
			`${URL}/distance/calculate`,
			dto,
			compressionConfig
		);
		if (response.data.byteLength === 0) {
			return null;
		}
		if (response.data.id !== null) {
			return response.data;
		} else {
			return null;
		}
	},

	exportDistanceResults: async (dto: BatchDistancesDto): Promise<Blob> =>
		(
			await httpRequest.post(`${URL}/distance/export`, dto, {
				headers: {
					'Content-Type': 'application/json'
				},
				responseType: 'blob'
			})
		).data,

	prepareDataForHeatmap: async (
		dto: BatchDistancesDto
	): Promise<HeatmapRowData[]> => {
		const response: AxiosResponse = await httpRequest.post(
			`${URL}/visualization/prepare-heatmap`,
			dto,
			compressionConfig
		);
		return response.data;
	},

	prepareDataForDendrogram: async (
		dto: BatchDistancesDto
	): Promise<SerializableClusterNode> => {
		const response: AxiosResponse = await httpRequest.post(
			`${URL}/visualization/prepare-dendrogram`,
			dto,
			compressionConfig
		);
		return response.data;
	}
};

import { FaceInfo } from '../types/FileTypes';

import { httpRequest } from './AxiosConfig';

const URL = '/api/face-info';

export const FaceInfoService = {
	getFaceInfoById: async (faceInfoId: number): Promise<FaceInfo> =>
		(await httpRequest.get(`${URL}/${faceInfoId}`)).data,

	computeFaceInfoGeometry: async (
		faceInfoId: number,
		fileUploadId: number
	): Promise<FaceInfo> =>
		(
			await httpRequest.post(
				`${URL}/${faceInfoId}/compute-geometry-info/${fileUploadId}`
			)
		).data
};

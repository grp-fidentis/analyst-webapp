import axios, { AxiosResponseHeaders } from 'axios';

import { TokensUtils } from '../utils/TokensUtils';
import { User, UserAuth, UserDetail } from '../types/UserTypes';

import { httpRequest } from './AxiosConfig';

const URL = '/api/auth';

export const AuthService = {
	login: async (user: UserAuth): Promise<User> => {
		const res = await axios.post(
			`${URL}/login`,
			{
				username: user.username,
				password: user.password
			},
			{ headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
		);
		TokensUtils.setNewTokens(res.headers as AxiosResponseHeaders);
		TokensUtils.setCurrentUser(res.data as User);
		httpRequest.defaults.headers.Authorization = `Bearer ${TokensUtils.getAccessToken()}`;

		return res.data;
	},

	resetPassword: async (dto: { password: string; token: string }) =>
		(await axios.post(`${URL}/reset-password`, dto)).data,

	register: async (user: UserDetail) =>
		(await httpRequest.post(`${URL}/register`, user)).data,

	forgotPassword: async (email: string) =>
		(await axios.post(`${URL}/forgot-password`, { email })).data,

	logout: async () => {
		TokensUtils.removeTokens();
		TokensUtils.removeCurrentUser();
		await axios.post(`${URL}/logout`);
	}
};

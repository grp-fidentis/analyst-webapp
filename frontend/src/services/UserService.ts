import { User, UserDetail } from '../types/UserTypes';
import { PasswordsFormData } from '../components/auth-view/PasswordsCompare';

import { httpRequest } from './AxiosConfig';
import { Project } from '../types/ProjectTypes';

const URL = '/api/users';

export const UserService = {
	getAllUsers: async (): Promise<User[]> => (await httpRequest.get(URL)).data,

	deleteUser: async (id: number) => await httpRequest.delete(`${URL}/${id}`),

	updateUser: async (user: UserDetail): Promise<User> =>
		(await httpRequest.put(`${URL}/${user.id}`, user)).data,

	changePassword: async (data: PasswordsFormData) =>
		await httpRequest.post(`${URL}/reset-password`, data),

	enableUser: async (id: number, enabled: boolean): Promise<User> =>
		(await httpRequest.patch(`${URL}/enable/${id}`, { enabled: enabled })).data,

	getProjectsByUser: async (userId: number): Promise<Project[]> =>
		(await httpRequest.get(`${URL}/${userId}/projects`)).data,
};

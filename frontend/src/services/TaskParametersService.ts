import {
	TaskParameters,
	TaskParametersDistanceUpdateDto,
	TaskParametersRegistrationUpdateDto
} from '../types/TaskParametersTypes';

import { httpRequest } from './AxiosConfig';

const URL = '/api/task-parameters';

export const TaskParametersService = {
	getTaskParametersByTaskId: async (
		taskParametersId: number
	): Promise<TaskParameters> =>
		(await httpRequest.get(`${URL}/${taskParametersId}`)).data,

	updateTaskParametersForRegistration: (
		taskId: number,
		taskParametersDto: TaskParametersRegistrationUpdateDto
	): void => {
		httpRequest.put(`${URL}/${taskId}/registration-update`, taskParametersDto);
	},

	updateTaskParametersForDistance: (
		taskId: number,
		taskParametersDto: TaskParametersDistanceUpdateDto
	): void => {
		httpRequest.put(`${URL}/${taskId}/distance-update`, taskParametersDto);
	}
};

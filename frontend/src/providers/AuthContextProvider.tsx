import {
	createContext,
	Dispatch,
	FC,
	SetStateAction,
	useEffect,
	useMemo,
	useState
} from 'react';

import { AuthService } from '../services/AuthService';
import { Roles } from '../utils/Enums';
import { User } from '../types/UserTypes';
import { TokensUtils } from '../utils/TokensUtils';

type Props = {
	children: JSX.Element;
};

type AuthContextValue = {
	currentUser?: User;
	handleLogin: Dispatch<SetStateAction<User | undefined>>;
	logout: () => void;
	hasAdminRole?: boolean;
	isUserEnabled?: boolean;
	hasRole: (role: Roles) => boolean;
	getCurrentUser: () => User | undefined;
};

export const AuthContextProvider: FC<Props> = ({ children }) => {
	const [currentUser, setCurrentUser] = useState<User | undefined>();
	const [currentUserLoaded, setCurrentUserLoaded] = useState(false);

	const hasAdminRole = useMemo(
		() =>
			currentUser?.authorities.some(
				authority =>
					authority.authority === Roles.ROLE_ADMIN ||
					authority.authority === Roles.ROLE_SUPER_ADMIN
			),
		[currentUser]
	);

	const isUserEnabled = useMemo(() => currentUser?.enabled, [currentUser]);

	const getCurrentUser = () => {
		if (currentUser) {
			return currentUser;
		}

		const savedCurrentUser = TokensUtils.getCurrentUser();
		if (savedCurrentUser) {
			setCurrentUser(savedCurrentUser);
		}
		return savedCurrentUser;
	};

	const hasRole = (role: Roles) =>
		currentUser?.authorities.some(authority => authority.authority === role) ??
		false;

	useEffect(() => {
		setCurrentUser(undefined);
		setCurrentUserLoaded(true);
	}, []);

	const value = useMemo<AuthContextValue>(
		() => ({
			currentUser,
			hasAdminRole,
			hasRole,
			isUserEnabled,
			getCurrentUser,
			handleLogin: setCurrentUser,
			logout: () => AuthService.logout().then(() => setCurrentUser(undefined))
		}),
		[currentUser, hasAdminRole, hasRole, isUserEnabled, getCurrentUser]
	);

	return (
		<AuthContext.Provider value={value}>
			{currentUserLoaded && children}
		</AuthContext.Provider>
	);
};

export const AuthContext = createContext<AuthContextValue>(undefined as never);

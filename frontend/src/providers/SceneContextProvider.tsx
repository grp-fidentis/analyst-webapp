import {
	createContext,
	Dispatch,
	FC,
	SetStateAction,
	useMemo,
	useState
} from 'react';

import { HumanFace } from '../types/HumanFaceTypes';
import {
	Alignment,
	RenderMode,
	SceneFaceVisibility
} from '../types/SceneTypes';
import { HumanFaceService } from '../services/HumanFaceService';

type Props = {
	children: JSX.Element;
};

type SceneContextValue = {
	faces: HumanFace[];
	setFaces: Dispatch<SetStateAction<HumanFace[]>>;
	setFace: (
		face: HumanFace | null,
		fileUploadId: number | null,
		taskId: number | undefined
	) => void;
	renderMode: RenderMode;
	setRenderMode: Dispatch<SetStateAction<RenderMode>>;
	updateFace: (face: HumanFace, currentFaces?: HumanFace[]) => void;
	alignment: Alignment;
	setAlignment: Dispatch<SetStateAction<Alignment>>;
	backgroundColor: string;
	setBackgroundColor: Dispatch<SetStateAction<string>>;
	enableLightReflections: boolean;
	setEnableLightReflections: Dispatch<SetStateAction<boolean>>;
	sceneRendered: boolean;
	setSceneRendered: Dispatch<SetStateAction<boolean>>;
	featurePointsHighlightThreshold: number;
	setFeaturePointsHighlightThreshold: Dispatch<SetStateAction<number>>;
	primaryFaceVisibility: SceneFaceVisibility;
	secondaryFaceVisibility: SceneFaceVisibility;
	setPrimaryFaceVisibility: Dispatch<SetStateAction<SceneFaceVisibility>>;
	setSecondaryFaceVisibility: Dispatch<SetStateAction<SceneFaceVisibility>>;
	locked: boolean;
	setLocked: Dispatch<SetStateAction<boolean>>;
};

export const SceneContextProvider: FC<Props> = ({ children }) => {
	const [sceneRendered, setSceneRendered] = useState<boolean>(false);
	const [faces, setFaces] = useState<HumanFace[]>([]);
	const [renderMode, setRenderMode] = useState<RenderMode>('TEXTURE');
	const [locked, setLocked] = useState<boolean>(false);
	const [primaryFaceVisibility, setPrimaryFaceVisibility] =
		useState<SceneFaceVisibility>({
			showFace: true,
			showFeaturesPoints: false,
			showSymmetryPlane: false,
			opacity: 100
		});
	const [secondaryFaceVisibility, setSecondaryFaceVisibility] =
		useState<SceneFaceVisibility>({
			showFace: true,
			showFeaturesPoints: false,
			showSymmetryPlane: false,
			opacity: 100
		});
	const [backgroundColor, setBackgroundColor] = useState<string>('#323232');
	const [enableLightReflections, setEnableLightReflections] =
		useState<boolean>(false);
	const [alignment, setAlignment] = useState<Alignment>({
		isActive: false
	});
	const [featurePointsHighlightThreshold, setFeaturePointsHighlightThreshold] =
		useState<number>(5.0);

	const updateFace = (face: HumanFace, currentFaces?: HumanFace[]) => {
		currentFaces = currentFaces ? currentFaces : faces;

		const newFaces = currentFaces.filter(f => f.id !== face.id);
		const toUpdate = currentFaces.find(f => f.id === face.id);
		if (toUpdate) {
			if (toUpdate?.geometry && face.geometry?.positions) {
				toUpdate.geometry.positions = face.geometry.positions;
				toUpdate.geometry.normals = face.geometry.normals;
			}
			if (face.featurePoints) {
				toUpdate.featurePoints = face.featurePoints;
			}
			if (face.symmetryPlane) {
				toUpdate.symmetryPlane = face.symmetryPlane;
				toUpdate.hasSymmetryPlane = true;
			}
			toUpdate.samples = face.samples;
			toUpdate.colorsOfHeatmap = face.colorsOfHeatmap;

			newFaces.push({ ...toUpdate });
		}

		setFaces(newFaces);
	};

	const setFace = async (
		face: HumanFace | null,
		fileUploadId: number | null,
		taskId: number | undefined
	) => {
		if (face) {
			setFaces([face]);
			return;
		}
		const res = await HumanFaceService.getHumanFaceByFileUploadIdAndTaskId(
			fileUploadId,
			taskId
		);
		setFaces([res]);
	};

	const value = useMemo<SceneContextValue>(
		() => ({
			faces,
			setFaces,
			setFace,
			renderMode,
			setRenderMode,
			updateFace,
			alignment,
			setAlignment,
			backgroundColor,
			setBackgroundColor,
			enableLightReflections,
			setEnableLightReflections,
			setPrimaryFaceVisibility,
			setSecondaryFaceVisibility,
			primaryFaceVisibility,
			secondaryFaceVisibility,
			featurePointsHighlightThreshold,
			setFeaturePointsHighlightThreshold,
			setSceneRendered,
			sceneRendered,
			locked,
			setLocked
		}),
		[
			faces,
			setFaces,
			setFace,
			renderMode,
			setRenderMode,
			alignment,
			updateFace,
			setAlignment,
			backgroundColor,
			setBackgroundColor,
			enableLightReflections,
			setEnableLightReflections,
			setPrimaryFaceVisibility,
			setSecondaryFaceVisibility,
			primaryFaceVisibility,
			secondaryFaceVisibility,
			featurePointsHighlightThreshold,
			setFeaturePointsHighlightThreshold,
			setSceneRendered,
			sceneRendered,
			locked,
			setLocked
		]
	);

	return (
		<SceneContext.Provider value={value}>{children}</SceneContext.Provider>
	);
};

export const SceneContext = createContext<SceneContextValue>(
	undefined as never
);

import {
	createContext,
	Dispatch,
	FC,
	SetStateAction,
	useContext,
	useMemo,
	useState
} from 'react';
import { toast } from 'react-toastify';
import { GridSelectionModel } from '@mui/x-data-grid';

import { FaceInfo, FileUpload } from '../types/FileTypes';
import { FileUploadService } from '../services/FileUploadService';

import { ProjectsContext } from './ProjectContextProvider';

type Props = {
	children: JSX.Element;
};

type FilesContextValue = {
	files: FileUpload[];
	setFiles: Dispatch<SetStateAction<FileUpload[]>>;
	loading: boolean;
	getProjectFiles: () => void;
	faceInfo: FaceInfo | undefined;
	setFaceInfo: Dispatch<SetStateAction<FaceInfo | undefined>>;
	saveFiles: (formData: FormData) => void;
	fileSelection: GridSelectionModel;
	getFaceInfoIdByUploadId: (uploadId: number) => number;
	setFileSelection: Dispatch<SetStateAction<GridSelectionModel>>;
	deleteFilesFromProject: (ids: number[]) => void;
	uploadSmallPreviewPhoto: (data: FormData, fileUploadId: number) => void;
	uploadAdditionalFile: (
		data: FormData,
		projectId: number,
		faceInfoId: number
	) => void;
};

export const FileContextProvider: FC<Props> = ({ children }) => {
	const [loading, setLoading] = useState(false);
	const [files, setFiles] = useState<FileUpload[]>([]);
	const [faceInfo, setFaceInfo] = useState<FaceInfo | undefined>(undefined);
	const [fileSelection, setFileSelection] = useState<GridSelectionModel>([]);
	const { selectedProjectId } = useContext(ProjectsContext);

	const getProjectFiles = async () => {
		setLoading(true);
		try {
			if (!selectedProjectId) {
				return null;
			}
			if (files.length) setFiles([]);
			const res = await FileUploadService.getUserFilesOnProject(
				selectedProjectId
			);
			setFiles(res);
		} finally {
			setLoading(false);
		}
	};

	const saveFiles = async (formData: FormData) => {
		setLoading(true);
		try {
			if (!selectedProjectId) {
				return null;
			}
			if (files.length) setFiles([]);
			await FileUploadService.saveFile(formData, selectedProjectId);
			await getProjectFiles();
			toast.success('Files uploaded');
		} finally {
			setLoading(false);
		}
	};

	const getFaceInfoIdByUploadId = (uploadId: number) =>
		files.find(file => file.id === uploadId)?.faceInfoId ?? -1;

	const uploadAdditionalFile = async (
		formData: FormData,
		projectId: number,
		faceInfoId: number
	) => {
		const res = await toast.promise(
			FileUploadService.uploadAdditionalFile(formData, projectId, faceInfoId),
			{
				pending: 'Uploading file',
				error: {
					render: () => 'Uploading failed try again'
				},
				success: 'File uploaded'
			}
		);
		setFaceInfo(res);
	};

	const uploadSmallPreviewPhoto = async (
		data: FormData,
		fileUploadId: number
	) => {
		const res = await toast.promise(
			FileUploadService.uploadSmallPreviewPhoto(data, fileUploadId),
			{
				pending: 'Uploading file',
				error: {
					render: () => 'Uploading failed try again'
				},
				success: 'File preview successfully uploaded'
			}
		);
		setFiles(files.map(file => (file.id === res.id ? res : file)));
	};

	const deleteFilesFromProject = async (ids: number[]) => {
		setLoading(true);
		try {
			await FileUploadService.deleteFiles(ids);
			toast.success('Files deleted');
		} finally {
			setLoading(false);
		}
	};

	const value = useMemo<FilesContextValue>(
		() => ({
			files,
			setFiles,
			loading,
			getProjectFiles,
			saveFiles,
			deleteFilesFromProject,
			uploadAdditionalFile,
			fileSelection,
			setFileSelection,
			setFaceInfo,
			faceInfo,
			uploadSmallPreviewPhoto,
			getFaceInfoIdByUploadId
		}),
		[
			files,
			setFiles,
			loading,
			getProjectFiles,
			saveFiles,
			deleteFilesFromProject,
			fileSelection,
			setFileSelection,
			setFaceInfo,
			faceInfo,
			uploadSmallPreviewPhoto,
			getFaceInfoIdByUploadId
		]
	);

	return (
		<FilesContext.Provider value={value}>{children}</FilesContext.Provider>
	);
};

export const FilesContext = createContext<FilesContextValue>(
	undefined as never
);

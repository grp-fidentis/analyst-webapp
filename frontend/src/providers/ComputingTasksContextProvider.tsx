import { createContext, FC, useMemo, useState } from 'react';

type Props = {
	children: JSX.Element;
};

type ComputingTasksContextProviderValue = {
	isComputingTaskId: (taskId: number) => boolean;
	addComputingTaskId: (taskId: number | null) => void;
	removeComputingTaskId: (taskId: number | null) => void;
};

export const ComputingTasksContextProvider: FC<Props> = ({ children }) => {
	const [computingTaskIds, setComputingTaskIds] = useState<number[]>([]);

	const addComputingTaskId = (taskId: number | null) => {
		if (taskId === null) {
			return;
		}
		setComputingTaskIds([...computingTaskIds, taskId]);
	};

	const removeComputingTaskId = (taskId: number | null) => {
		if (taskId === null) {
			return;
		}
		setComputingTaskIds(computingTaskIds.filter(id => id !== taskId));
	};

	const isComputingTaskId = (taskId: number) =>
		computingTaskIds.includes(taskId);

	const value = useMemo<ComputingTasksContextProviderValue>(
		() => ({
			addComputingTaskId,
			removeComputingTaskId,
			isComputingTaskId
		}),
		[addComputingTaskId, removeComputingTaskId, isComputingTaskId]
	);

	return (
		<ComputingTasksContext.Provider value={value}>
			{children}
		</ComputingTasksContext.Provider>
	);
};

export const ComputingTasksContext =
	createContext<ComputingTasksContextProviderValue>(undefined as never);

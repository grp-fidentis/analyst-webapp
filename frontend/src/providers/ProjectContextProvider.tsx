import {
	createContext,
	Dispatch,
	FC,
	SetStateAction,
	useMemo,
	useState
} from 'react';
import { toast } from 'react-toastify';

import { Project } from '../types/ProjectTypes';
import { ProjectService } from '../services/ProjectService';

type Props = {
	children: JSX.Element;
};

type ProjectsContextValue = {
	selectedProjectId: number | undefined;
	setSelectedProjectId: Dispatch<SetStateAction<number | undefined>>;
	projects: Project[];
	setProjects: Dispatch<SetStateAction<Project[]>>;
	selectedProject: Project | undefined;
	createNewProject: (name: string) => void;
	closeProject: (id: number) => void;
	deleteProject: (id: number) => void;
	isLoadingProjects: boolean;
	getUserProjects: (isOpened: boolean) => void;
};

export const ProjectContextProvider: FC<Props> = ({ children }) => {
	const [selectedProjectId, setSelectedProjectId] = useState<
		number | undefined
	>(undefined);
	const [isLoadingProjects, setIsLoadingProjects] = useState(false);
	const [projects, setProjects] = useState<Project[]>([]);

	const selectedProject = useMemo<Project | undefined>(
		() => projects.find(project => project.id === selectedProjectId),
		[selectedProjectId, projects]
	);

	const createNewProject = async (name: string) => {
		ProjectService.createNewProject(name).then(project => {
			setProjects([...projects, project]);
			setSelectedProjectId(project.id);
			toast.success('Project created');
		});
	};

	const closeProject = async (id: number) => {
		await ProjectService.setProjectOpenState(id, false);
		filterProjectAndSelectId(id);
	};

	const deleteProject = async (id: number) => {
		await ProjectService.deleteProject(id);
		filterProjectAndSelectId(id);
		toast.success('Project deleted');
	};

	const getUserProjects = async (isOpened: boolean) => {
		setIsLoadingProjects(true);

		try {
			const projects = await ProjectService.getUserProjects(isOpened);
			setProjects(projects);
			setSelectedProjectId(projects.length ? projects[0].id : undefined);
		} finally {
			setIsLoadingProjects(false);
		}
	};

	const filterProjectAndSelectId = (id: number) => {
		const updatedProjects = projects.filter(project => project.id !== id);
		setProjects(updatedProjects);

		if (selectedProjectId === id) {
			const newSelectedProjectId =
				updatedProjects.find(project => project.id !== id)?.id ?? undefined;

			setSelectedProjectId(newSelectedProjectId);
		}
	};

	const value = useMemo<ProjectsContextValue>(
		() => ({
			selectedProjectId,
			setSelectedProjectId,
			projects,
			setProjects,
			selectedProject,
			createNewProject,
			closeProject,
			deleteProject,
			isLoadingProjects,
			getUserProjects
		}),
		[
			selectedProjectId,
			projects,
			isLoadingProjects,
			createNewProject,
			closeProject,
			deleteProject,
			getUserProjects
		]
	);

	return (
		<ProjectsContext.Provider value={value}>
			{children}
		</ProjectsContext.Provider>
	);
};

export const ProjectsContext = createContext<ProjectsContextValue>(
	undefined as never
);

import {
	createContext,
	Dispatch,
	FC,
	SetStateAction,
	useMemo,
	useState
} from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AxiosError } from 'axios';

import { TaskFile } from '../types/FileTypes';
import { ROUTES } from '../utils/RoutesUrls';
import { Task, TaskCreateDto } from '../types/TaskTypes';
import { TaskService } from '../services/TaskService';
import { ErrorResponse } from '../types/CommonTypes';

type Props = {
	children: JSX.Element;
};

type TaskContextValue = {
	file?: TaskFile;
	task?: Task;
	createTask: (task: TaskCreateDto) => void;
	projectTasks: Task[];
	setTask: Dispatch<SetStateAction<Task | undefined>>;
	getTasksOnProject: (projectId: number) => void;
	deleteTask: (taskId: number) => void;
	loading: boolean;
	updateTaskName: (name: string) => void;
	isAnalyticalTaskLoading: boolean;
	setIsAnalyticalTaskLoading: Dispatch<SetStateAction<boolean>>;
	isBatchProcessingTaskLoading: boolean;
	setIsBatchProcessingTaskLoading: Dispatch<SetStateAction<boolean>>;
	currentPanel: string;
	setCurrentPanel: Dispatch<SetStateAction<string>>;
};

export const TaskContextProvider: FC<Props> = ({ children }) => {
	const [projectTasks, setProjectTasks] = useState<Task[]>([]);
	const [loading, setLoading] = useState(false);
	const [isAnalyticalTaskLoading, setIsAnalyticalTaskLoading] = useState(false);
	const [isBatchProcessingTaskLoading, setIsBatchProcessingTaskLoading] =
		useState(false);
	const [task, setTask] = useState<Task | undefined>(undefined);
	const [currentPanel, setCurrentPanel] = useState<string>('');

	const navigate = useNavigate();

	const createTask = async (task: TaskCreateDto) => {
		const res = await toast.promise(TaskService.createTask(task), {
			pending: 'Creating new task',
			error: "Couldn't create new task"
		});
		setProjectTasks(prevState => [...prevState, res]);
		navigate(`${ROUTES.tasks}/${res.id}`);
	};

	const deleteTask = (taskId: number) => {
		TaskService.deleteTask(taskId)
			.then(() => {
				setProjectTasks(prevState =>
					prevState.filter(task => task.id !== taskId)
				);
				navigate(ROUTES.projects);
				toast.success('Task deleted');
			})
			.catch(error => {
				const data = (error as AxiosError)?.response?.data as ErrorResponse;
				toast.error(data.message);
			});
	};

	const updateTaskName = async (name: string) => {
		if (!task) return;

		await TaskService.updateTaskName(task?.id ?? -1, name);

		const updatedName: Task | undefined = { ...task, name };
		setTask(updatedName);

		projectTasks
			.filter(tasks => tasks.id === task?.id)
			.forEach(tasks => (tasks.name = name));
		setProjectTasks(projectTasks);
	};

	const getTasksOnProject = (projectId: number) => {
		setLoading(true);

		TaskService.getTasksOnProject(projectId)
			.then(tasks => {
				setProjectTasks(tasks);
			})
			.finally(() => setLoading(false));
	};

	const value = useMemo<TaskContextValue>(
		() => ({
			task,
			projectTasks,
			createTask,
			setTask,
			getTasksOnProject,
			deleteTask,
			loading,
			updateTaskName,
			isAnalyticalTaskLoading,
			setIsAnalyticalTaskLoading,
			isBatchProcessingTaskLoading,
			setIsBatchProcessingTaskLoading,
			currentPanel,
			setCurrentPanel
		}),
		[
			task,
			projectTasks,
			loading,
			createTask,
			getTasksOnProject,
			deleteTask,
			updateTaskName,
			isAnalyticalTaskLoading,
			setIsAnalyticalTaskLoading,
			isBatchProcessingTaskLoading,
			setIsBatchProcessingTaskLoading,
			currentPanel,
			setCurrentPanel
		]
	);

	return <TaskContext.Provider value={value}>{children}</TaskContext.Provider>;
};

export const TaskContext = createContext<TaskContextValue>(undefined as never);

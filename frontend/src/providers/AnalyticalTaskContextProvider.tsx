import {
	createContext,
	Dispatch,
	FC,
	SetStateAction,
	useContext,
	useEffect,
	useMemo,
	useState
} from 'react';

import { AnalyticalTaskService } from '../services/AnalyticalTaskService';
import {
	CuttingPlaneDto,
	CuttingPlaneFrontendState,
	CuttingPlanes2DProjectionDto,
	CuttingPlanesDirection,
	CuttingPlanesTaskDto,
	CuttingPlanesType,
	DistanceStrategy,
	DistanceTaskDto,
	DistanceTaskResponseDto,
	ExportType,
	FeaturePointSize,
	HeatmapRenderMode,
	LoadingOperation,
	RegistrationTaskDto,
	SamplingTaskDto,
	ShowSpheres
} from '../types/AnalyticalTaskTypes';
import {
	CurvatureTaskDto,
	FeaturePointDto,
	HumanFace
} from '../types/HumanFaceTypes';
import { FaceUtils } from '../utils/FaceUtils';
import { DownloadUtils } from '../utils/DownloadUtils';

import { TaskContext } from './TaskContextProvider';
import { SceneContext } from './SceneContextProvider';

type LoadingInfo = {
	isLoading: boolean;
	operation: LoadingOperation;
};

type Props = {
	children: JSX.Element;
};

type AnalyticalTaskContextValue = {
	executeRegistration: (dto: RegistrationTaskDto) => Promise<HumanFace | null>;
	calculateDistances: (dto: DistanceTaskDto, useAllSizes?: boolean) => void;
	calculateSamples: (dto: SamplingTaskDto) => void;
	calculateDistanceHeatmap: (
		dto: DistanceTaskDto,
		useAllSizes?: boolean,
		recomputeDistances?: boolean
	) => void;
	distances: DistanceTaskResponseDto;
	setDistances: Dispatch<SetStateAction<DistanceTaskResponseDto>>;
	distanceTaskDto: DistanceTaskDto;
	setDistanceTaskDto: Dispatch<SetStateAction<DistanceTaskDto>>;
	updateLoadingInfo: (isLoading: boolean, operation?: LoadingOperation) => void;
	isLoadingInfo: (Operation?: LoadingOperation, isEqual?: boolean) => boolean;
	exportDistances: (exportType: ExportType, useAllSizes?: boolean) => void;
	showSpheres: ShowSpheres;
	setShowSpheres: Dispatch<SetStateAction<ShowSpheres>>;
	createCuttingPlanes: (
		taskId: number,
		isSymmetryCalculated?: boolean
	) => Promise<void>;
	calculate2DProjection: () => Promise<void>;
	exportCuttingPlanesProjection: () => Promise<void>;
	cuttingPlanesTaskDto: CuttingPlanesTaskDto;
	setCuttingPlanesTaskDto: Dispatch<SetStateAction<CuttingPlanesTaskDto>>;
	calculateCurvatureHeatmap: (dto: CurvatureTaskDto) => void;
};

export const AnalyticalTaskContextProvider: FC<Props> = ({ children }) => {
	const { task, setIsAnalyticalTaskLoading } = useContext(TaskContext);
	const { updateFace, faces } = useContext(SceneContext);
	const [loadingInfo, setLoadingInfo] = useState<LoadingInfo>({
		isLoading: false,
		operation: LoadingOperation.None
	});
	const [distances, setDistances] = useState<DistanceTaskResponseDto>({
		meanDistance: 0,
		weightedMeanDistance: 0,
		featurePointWeights: []
	});
	const [distanceTaskDto, setDistanceTaskDto] = useState<DistanceTaskDto>({
		taskId: task?.id ?? -1,
		distanceStrategy: DistanceStrategy.PointToTriangle,
		crop: true,
		useRelativeDistance: false,
		heatmapRenderMode: HeatmapRenderMode.Standard
	});
	const [showSpheres, setShowSpheres] = useState<ShowSpheres>(
		ShowSpheres.HideAll
	);
	const [cuttingPlanesTaskDto, setCuttingPlanesTaskDto] =
		useState<CuttingPlanesTaskDto>({
			state: {
				taskId: task?.id ?? -1,
				normalVectorLength: 10,
				reducedVertices: 100,
				showNormals: false,
				samplingStrength: 50,
				currentDirection: CuttingPlanesDirection.DirectionVertical,
				cuttingPlanes: [],
				canvasWidth: 400,
				canvasHeight: 400
			}
		});

	useEffect(() => {
		const taskId = task?.id ?? -1;
		setDistanceTaskDto(dto => ({ ...dto, taskId }));
	}, [task]);

	const createCuttingPlanes = async (
		taskId: number,
		isSymmetryCalculated?: boolean
	): Promise<void> => {
		const cuttingPlanesInfo = await AnalyticalTaskService.createCuttingPlanes(
			taskId
		);

		if (
			isSymmetryCalculated &&
			cuttingPlanesTaskDto.state.cuttingPlanes?.length !== 0
		) {
			let cuttingPlanes: CuttingPlaneFrontendState[] =
				cuttingPlanesTaskDto.state.cuttingPlanes ?? [];
			cuttingPlanes.forEach(
				c =>
					(c.cuttingPlaneId =
						cuttingPlanesInfo.find(
							i =>
								i.order === c.order &&
								i.type === c.type &&
								i.direction === c.direction
						)?.cuttingPlaneId ?? '')
			);

			if (cuttingPlanesTaskDto.state.cuttingPlanes?.length === 9) {
				cuttingPlanes.push(
					...cuttingPlanesDtoToFrontendStatesDto(
						cuttingPlanesInfo.filter(
							c => c.type === CuttingPlanesType.SymmetryPlane
						)
					)
				);
			} else if (
				cuttingPlanesTaskDto.state.cuttingPlanes?.length === 18 &&
				cuttingPlanesInfo.length === 9
			) {
				cuttingPlanes = cuttingPlanes.filter(
					c => c.type === CuttingPlanesType.BoundingBoxPlane
				);
				cuttingPlanes.forEach(c => (c.isSelected = true));
			} else {
				cuttingPlanes
					.filter(c => c.type === CuttingPlanesType.SymmetryPlane)
					.forEach(c => {
						c.positions =
							cuttingPlanesInfo.find(i => i.cuttingPlaneId === c.cuttingPlaneId)
								?.positions ?? [];
						c.normals =
							cuttingPlanesInfo.find(i => i.cuttingPlaneId === c.cuttingPlaneId)
								?.normals ?? [];
					});
			}

			setCuttingPlanesTaskDto(prev => ({
				...prev,
				state: {
					...prev.state,
					taskId,
					cuttingPlanes: [...cuttingPlanes]
				}
			}));
			return;
		}

		const cuttingPlanes: CuttingPlaneFrontendState[] =
			cuttingPlanesDtoToFrontendStatesDto(cuttingPlanesInfo);
		const firstPlane = cuttingPlanes.find(
			c =>
				c.order === 1 &&
				c.direction === CuttingPlanesDirection.DirectionVertical &&
				c.type === CuttingPlanesType.BoundingBoxPlane
		);
		if (!firstPlane) return;
		firstPlane.isVisible = true;

		setCuttingPlanesTaskDto(prev => ({
			...prev,
			state: {
				...prev.state,
				taskId,
				cuttingPlanes,
				normalVectorLength: 10,
				reducedVertices: 100,
				showNormals: false,
				samplingStrength: 50,
				currentDirection: CuttingPlanesDirection.DirectionVertical
			}
		}));
	};

	const cuttingPlanesDtoToFrontendStatesDto = (
		cuttingPlanesInfo: CuttingPlaneDto[]
	) =>
		cuttingPlanesInfo.map(c => ({
			isVisible: false,
			isHover: false,
			isSelected: c.type === CuttingPlanesType.BoundingBoxPlane,
			sliderValue: 0,
			cuttingPlaneId: c.cuttingPlaneId,
			order: c.order,
			direction: c.direction,
			type: c.type,
			positions: c.positions,
			normals: c.normals,
			transformationDto: {
				rotation: { x: 0, y: 0, z: 0 },
				translation: { x: 0, y: 0, z: 0 }
			}
		}));

	const calculate2DProjection = async (): Promise<void> => {
		const projectionResponse =
			await AnalyticalTaskService.calculate2DProjection(getProjectionDto());

		setCuttingPlanesTaskDto(prev => ({ ...prev, projectionResponse }));
	};

	const exportCuttingPlanesProjection = async (): Promise<void> => {
		const axiosResponse = await AnalyticalTaskService.exportProjectionsAsZip(
			getProjectionDto()
		);

		if (!task) return;
		const fileName = `${
			task.name ? `task_${task.name}_` : `task_${task.id}`
		}_2D_projection.zip`;

		DownloadUtils.downloadFile(axiosResponse.data, fileName);
	};

	const getProjectionDto = () => {
		const projection: CuttingPlanes2DProjectionDto = {
			taskId: cuttingPlanesTaskDto.state.taskId,
			samplingStrength: cuttingPlanesTaskDto.state.samplingStrength,
			direction: cuttingPlanesTaskDto.state.currentDirection,
			reducedVertices: cuttingPlanesTaskDto.state.reducedVertices,
			cuttingPlaneStates:
				cuttingPlanesTaskDto.state.cuttingPlanes?.filter(
					s =>
						s.direction === cuttingPlanesTaskDto.state.currentDirection &&
						s.isSelected
				) ?? []
		};
		return projection;
	};

	const executeRegistration = async (
		dto: RegistrationTaskDto
	): Promise<HumanFace | null> => {
		updateLoadingInfo(true, LoadingOperation.Registration);
		try {
			return await AnalyticalTaskService.executeRegistration(dto);
		} finally {
			updateLoadingInfo(false);
		}
	};

	const calculateSamples = async (dto: SamplingTaskDto) => {
		updateLoadingInfo(true, LoadingOperation.Samples);
		try {
			const samples = await AnalyticalTaskService.calculateSamplePoints(dto);
			updateFace({ samples, id: samples.humanFaceId });
		} finally {
			updateLoadingInfo(false);
		}
	};

	const calculateDistances = async (
		dto: DistanceTaskDto,
		useAllSizes?: boolean
	) => {
		const responseDto = await AnalyticalTaskService.calculateDistance({
			...dto,
			featurePointSizes: getFeaturePointSizes(faces, useAllSizes ?? false)
		});
		setDistances(prev => ({
			...prev,
			meanDistance: responseDto.meanDistance,
			weightedMeanDistance: responseDto.weightedMeanDistance
		}));

		const secondaryFace = FaceUtils.getSecondary(faces);
		secondaryFace.featurePoints?.featurePointDtos.forEach(f => {
			const fpWeight = responseDto.featurePointWeights?.find(
				w => w.featurePointName === f.name
			);
			f.weight = fpWeight?.weight ?? Number.NaN;
		});
		updateFace(secondaryFace);
	};

	const calculateDistanceHeatmap = async (
		dto: DistanceTaskDto,
		useAllSizes?: boolean,
		recomputeDistances?: boolean
	) => {
		updateLoadingInfo(true, LoadingOperation.Heatmap);
		try {
			const responseDto = await AnalyticalTaskService.calculateDistanceHeatmap({
				...dto,
				recomputeDistances: recomputeDistances ?? false,
				featurePointSizes: getFeaturePointSizes(faces, useAllSizes ?? false)
			});
			setDistances({ ...responseDto });

			const secondaryFace: HumanFace = {
				...FaceUtils.getSecondary(faces),
				colorsOfHeatmap: responseDto.colorsOfHeatmap
			};

			secondaryFace.featurePoints?.featurePointDtos.forEach(f => {
				const fpWeight = responseDto.featurePointWeights?.find(
					w => w.featurePointName === f.name
				);

				f.weight = fpWeight?.weight ?? Number.NaN;
			});

			updateFace(secondaryFace);
		} finally {
			updateLoadingInfo(false);
		}
	};

	const calculateCurvatureHeatmap = async (dto: CurvatureTaskDto) => {
		updateLoadingInfo(true, LoadingOperation.Heatmap);
		try {
			const responseDto = await AnalyticalTaskService.calculateCurvatureHeatmap(
				dto
			);

			const face = {
				...faces[0],
				colorsOfHeatmap: responseDto.heatmap
			};

			updateFace(face);
		} finally {
			updateLoadingInfo(false);
		}
	};

	const exportDistances = async (
		exportType: ExportType,
		useAllSizes?: boolean
	) => {
		if (!task) return;

		const dto: DistanceTaskDto = {
			...distanceTaskDto,
			exportType,
			featurePointSizes: getFeaturePointSizes(faces, useAllSizes ?? false)
		};
		const blob = await AnalyticalTaskService.exportDistance(dto);

		let fileName = `${task.name ? `task_${task.name}_` : `task_${task.id}`}_`;
		fileName +=
			exportType === ExportType.DISTANCE ? 'distance' : 'weighted_distance';
		fileName += '.csv';

		DownloadUtils.downloadFile(blob, fileName);
	};

	const getFeaturePointSizes: (
		faces: HumanFace[],
		useAllSizes: boolean
	) => FeaturePointSize[] = (faces, useAllSizes) => {
		const sizes: FeaturePointSize[] = [];

		const secondaryFace = FaceUtils.getSecondary(faces);
		const secondaryFaceFeaturePoints: FeaturePointDto[] =
			secondaryFace?.featurePoints?.featurePointDtos ?? [];

		const notAssignedSelection = secondaryFaceFeaturePoints.every(
			s => s.selected === undefined
		);

		secondaryFaceFeaturePoints
			.filter(f => useAllSizes || notAssignedSelection || f.selected)
			.forEach(f => {
				sizes.push({ size: f.size, name: f.name });
			});

		return sizes;
	};

	const updateLoadingInfo = (
		isLoading: boolean,
		operation?: LoadingOperation
	) => {
		setIsAnalyticalTaskLoading(isLoading);

		setLoadingInfo({
			isLoading,
			operation: operation ?? LoadingOperation.None
		});
	};

	const isLoadingInfo = (Operation?: LoadingOperation, isEqual?: boolean) => {
		const isLoading = loadingInfo.isLoading;

		if (Operation) {
			if (isEqual) {
				return isLoading && loadingInfo.operation === Operation;
			}

			return isLoading && loadingInfo.operation !== Operation;
		}

		return isLoading;
	};

	const value = useMemo<AnalyticalTaskContextValue>(
		() => ({
			executeRegistration,
			calculateDistances,
			calculateDistanceHeatmap,
			distances,
			distanceTaskDto,
			setDistanceTaskDto,
			setDistances,
			updateLoadingInfo,
			isLoadingInfo,
			exportDistances,
			showSpheres,
			setShowSpheres,
			calculateSamples,
			createCuttingPlanes,
			calculate2DProjection,
			exportCuttingPlanesProjection,
			cuttingPlanesTaskDto,
			setCuttingPlanesTaskDto,
			calculateCurvatureHeatmap
		}),
		[
			executeRegistration,
			calculateDistances,
			calculateDistanceHeatmap,
			distances,
			distanceTaskDto,
			setDistanceTaskDto,
			setDistances,
			updateLoadingInfo,
			isLoadingInfo,
			exportDistances,
			showSpheres,
			setShowSpheres,
			calculateSamples,
			createCuttingPlanes,
			calculate2DProjection,
			exportCuttingPlanesProjection,
			cuttingPlanesTaskDto,
			setCuttingPlanesTaskDto,
			calculateCurvatureHeatmap
		]
	);

	return (
		<AnalyticalTaskContext.Provider value={value}>
			{children}
		</AnalyticalTaskContext.Provider>
	);
};

export const AnalyticalTaskContext = createContext<AnalyticalTaskContextValue>(
	undefined as never
);

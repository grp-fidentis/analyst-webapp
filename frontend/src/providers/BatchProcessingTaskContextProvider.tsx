import { createContext, FC, useContext, useMemo, useState } from 'react';

import { LoadingOperation } from '../types/AnalyticalTaskTypes';
import { FileUpload } from '../types/FileTypes';
import { FileUploadService } from '../services/FileUploadService';
import {
	AveragingMethod,
	BatchDistancesDto,
	BatchDistanceTaskDto,
	BatchRegistrationTaskDto,
	RegistrationMethod,
	SimilarityMethod
} from '../types/BatchProcessingTypes';
import { TaskParametersService } from '../services/TaskParametersService';
import { TaskParameters } from '../types/TaskParametersTypes';
import { LinkageStrategy } from '../types/BatchVisualizationTypes';
import { HumanFace } from '../types/HumanFaceTypes';

import { TaskContext } from './TaskContextProvider';
import { ProjectsContext } from './ProjectContextProvider';

type LoadingInfo = {
	isLoading: boolean;
	operation: LoadingOperation;
};

type Props = {
	children: JSX.Element;
};

type BatchProcessingTaskContextValue = {
	taskFiles: FileUpload[];
	taskParameters: TaskParameters | null;
	batchRegistrationTaskDto: BatchRegistrationTaskDto;
	setBatchRegistrationTaskDto: (dto: BatchRegistrationTaskDto) => void;
	batchDistanceTaskDto: BatchDistanceTaskDto;
	setBatchDistanceTaskDto: (dto: BatchDistanceTaskDto) => void;
	getBatchDistanceMapByTaskId: (taskId: number) => BatchDistancesDto | null;
	updateBatchDistancesDto: (
		taskId: number,
		dto: BatchDistancesDto | null
	) => void;
	batchDistanceForTask: BatchDistancesDto | null;
	getTaskFiles: () => void;
	getTaskParameters: () => void;
	updateLoadingInfo: (isLoading: boolean, operation?: LoadingOperation) => void;
	isLoadingBatchInfo: (
		Operation?: LoadingOperation,
		isEqual?: boolean
	) => boolean;
	selectedFileRegistration: FileUpload | null;
	setSelectedFileRegistration: (file: FileUpload | null) => void;
	selectedFileDistance: FileUpload | null;
	setSelectedFileDistance: (file: FileUpload | null) => void;
	averageFace: HumanFace | null;
	setAverageFace: (face: HumanFace | null) => void;
};

export const BatchProcessingTaskContextProvider: FC<Props> = ({ children }) => {
	const { task, setIsBatchProcessingTaskLoading } = useContext(TaskContext);
	const { selectedProjectId } = useContext(ProjectsContext);
	const [taskFiles, setTaskFiles] = useState<FileUpload[]>([]);
	const [taskParameters] = useState<TaskParameters | null>(null);

	const [selectedFileRegistration, setSelectedFileRegistration] =
		useState<FileUpload | null>(null);
	const [selectedFileDistance, setSelectedFileDistance] =
		useState<FileUpload | null>(null);
	const [averageFace, setAverageFace] = useState<HumanFace | null>(null);

	const [loadingInfo, setLoadingInfo] = useState<LoadingInfo>({
		isLoading: false,
		operation: LoadingOperation.None
	});

	const [batchRegistrationTaskDto, setBatchRegistrationTaskDto] =
		useState<BatchRegistrationTaskDto>({
			taskId: task?.id ?? -1,
			projectId: selectedProjectId ?? -1,
			selectedFile: taskFiles[0],
			fileIds: [],
			averagingTaskDto: {
				taskId: task?.id ?? -1,
				averagingMethod: AveragingMethod.NearestNeighbours,
				stabilizationThreshold: 0,
				maxRegistrationIterations: 3
			},
			registrationTaskDto: {
				taskId: task?.id ?? -1,
				registrationMethod: RegistrationMethod.MeshBasedICP,
				registrationSubsampling: 1000,
				scaleFacesDuringRegistration: false
			},
			calculationWebSocketSessionId: ''
		});

	const [batchDistanceTaskDto, setBatchDistanceTaskDto] =
		useState<BatchDistanceTaskDto>({
			taskId: task?.id ?? -1,
			selectedFile: taskFiles[0],
			fileIds: [],
			similarityMethod: SimilarityMethod.IndirectDistance,
			calculationWebSocketSessionId: ''
		});

	const [batchDistancesMap, setBatchDistancesMap] = useState<
		Map<number, BatchDistancesDto | null>
	>(new Map());

	const getBatchDistanceMapByTaskId = (
		taskId: number
	): BatchDistancesDto | null => batchDistancesMap.get(taskId) ?? null;

	const updateBatchDistancesDto = (
		taskId: number,
		dto: BatchDistancesDto | null
	) => {
		setBatchDistancesMap(prevMap => {
			const updatedMap = new Map(prevMap);
			updatedMap.set(taskId, dto);
			return updatedMap;
		});
	};

	const batchDistanceForTask = useMemo(
		() => getBatchDistanceMapByTaskId(task?.id ?? -1),
		[batchDistancesMap, task?.id]
	);

	const getTaskFiles = async () => {
		updateLoadingInfo(true, LoadingOperation.Files);
		try {
			if (!task?.id) {
				return null;
			}
			const res = await FileUploadService.getFilesOfTask(task.id);
			setBatchRegistrationTaskDto({
				...batchRegistrationTaskDto,
				taskId: task.id,
				fileIds: res.map(file => file.id)
			});
			setBatchDistanceTaskDto({
				...batchDistanceTaskDto,
				taskId: task.id,
				fileIds: res.map(file => file.id)
			});
			setTaskFiles(res);
		} finally {
			updateLoadingInfo(false);
		}
	};

	const getTaskParameters = async () => {
		updateLoadingInfo(true);
		try {
			if (!task?.id) {
				return null;
			}
			const res = await TaskParametersService.getTaskParametersByTaskId(
				task.taskParametersId
			);
			updateTaskParameters(res);
		} finally {
			updateLoadingInfo(false);
		}
	};

	const updateTaskParameters = (taskParametersDto: TaskParameters) => {
		if (!task || !selectedProjectId || !taskFiles) {
			return;
		}

		const avgFace = findAverageFace();
		let savedFileForRegistration = avgFace;
		let savedFileForDistance = avgFace;
		if (!avgFace) {
			savedFileForRegistration = findFileByName(
				taskParametersDto.templateFaceForRegistration
			);
			savedFileForDistance = findFileByName(
				taskParametersDto.templateFaceForDistance
			);
		}

		setSelectedFileRegistration(savedFileForRegistration);
		setSelectedFileDistance(savedFileForDistance);

		setBatchRegistrationTaskDto({
			...batchRegistrationTaskDto,
			taskId: task.id,
			projectId: selectedProjectId,
			selectedFile: savedFileForRegistration,
			averagingTaskDto: {
				...batchRegistrationTaskDto.averagingTaskDto,
				taskId: task.id,
				averagingMethod: taskParametersDto.averagingMethod,
				maxRegistrationIterations: taskParametersDto.maxRegistrationIterations,
				stabilizationThreshold: taskParametersDto.stabilizationThreshold
			},
			registrationTaskDto: {
				...batchRegistrationTaskDto.registrationTaskDto,
				taskId: task.id,
				registrationMethod: taskParametersDto.registrationMethod,
				registrationSubsampling: taskParametersDto.registrationSubsampling,
				scaleFacesDuringRegistration:
					taskParametersDto.scaleFacesDuringRegistration
			}
		});
		setBatchDistanceTaskDto({
			...batchDistanceTaskDto,
			taskId: task.id,
			selectedFile: savedFileForDistance,
			similarityMethod:
				taskParametersDto.similarityMethod || SimilarityMethod.IndirectDistance
		});
		if (taskParametersDto.distances && taskParametersDto.deviations) {
			const parsedDistances = JSON.parse(taskParametersDto.distances);
			const parsedDeviations = JSON.parse(taskParametersDto.deviations);

			const newBatchDistancesDto: BatchDistancesDto = {
				faceDistances: parsedDistances.map(
					(distanceArray: number[], index: number) => ({
						faceName: taskFiles[index].name,
						faceDistances: distanceArray,
						faceDeviations: parsedDeviations[index]
					})
				),
				taskId: task.id,
				linkageStrategy: LinkageStrategy.AVERAGE
			};

			updateBatchDistancesDto(task.id, newBatchDistancesDto);
		} else {
			updateBatchDistancesDto(task.id, null);
		}
	};

	const findFileByName = (name: string) =>
		taskFiles.find(file => file.name === name) ?? null;

	const findAverageFace = () =>
		taskFiles.find(file => file.isAverageFace) ?? null;

	const updateLoadingInfo = (
		isLoading: boolean,
		operation?: LoadingOperation
	) => {
		setIsBatchProcessingTaskLoading(isLoading);

		setLoadingInfo({
			isLoading,
			operation: operation ?? LoadingOperation.None
		});
	};

	const isLoadingBatchInfo = (
		Operation?: LoadingOperation,
		isEqual?: boolean
	) => {
		const isLoading = loadingInfo.isLoading;

		if (Operation) {
			if (isEqual) {
				return isLoading && loadingInfo.operation === Operation;
			}

			return isLoading && loadingInfo.operation !== Operation;
		}

		return isLoading;
	};

	const value = useMemo<BatchProcessingTaskContextValue>(
		() => ({
			taskFiles,
			averageFace,
			taskParameters,
			batchRegistrationTaskDto,
			batchDistanceTaskDto,
			setAverageFace,
			getBatchDistanceMapByTaskId,
			selectedFileRegistration,
			selectedFileDistance,
			batchDistanceForTask,
			setBatchRegistrationTaskDto,
			setBatchDistanceTaskDto,
			updateBatchDistancesDto,
			getTaskFiles,
			getTaskParameters,
			updateLoadingInfo,
			isLoadingBatchInfo,
			setSelectedFileRegistration,
			setSelectedFileDistance
		}),
		[
			taskFiles,
			averageFace,
			taskParameters,
			batchRegistrationTaskDto,
			batchDistanceTaskDto,
			batchDistanceForTask,
			setAverageFace,
			getBatchDistanceMapByTaskId,
			setBatchRegistrationTaskDto,
			setBatchDistanceTaskDto,
			updateBatchDistancesDto,
			getTaskFiles,
			getTaskParameters,
			updateLoadingInfo,
			isLoadingBatchInfo
		]
	);

	return (
		<BatchProcessingTaskContext.Provider value={value}>
			{children}
		</BatchProcessingTaskContext.Provider>
	);
};

export const BatchProcessingTaskContext =
	createContext<BatchProcessingTaskContextValue>(undefined as never);

import { createContext, FC, useMemo, useState } from 'react';
import { toast } from 'react-toastify';

import { UserService } from '../services/UserService';
import { User } from '../types/UserTypes';

type Props = {
	children: JSX.Element;
};

type UsersContextValue = {
	loading: boolean;
	getAllUsers: () => void;
	users: User[];
	deleteUser: (userId: number) => void;
	enableUser: (userId: number, enabled: boolean) => void;
};

export const UserContextProvider: FC<Props> = ({ children }) => {
	const [loading, setLoading] = useState(false);
	const [users, setUsers] = useState<User[]>([]);

	const getAllUsers = () => {
		setLoading(true);

		UserService.getAllUsers()
			.then(users => {
				setUsers(users);
			})
			.catch()
			.finally(() => {
				setLoading(false);
			});
	};

	const deleteUser = (userId: number) => {
		setLoading(true);
		UserService.deleteUser(userId)
			.then(() => {
				setUsers(prevState => prevState.filter(user => user.id !== userId));
				toast.success('User successfully deleted');
			})
			.finally(() => setLoading(false));
	};

	const enableUser = (userId: number, enabled: boolean) => {
		setLoading(true);
		UserService.enableUser(userId, enabled)
			.then(result => {
				setUsers(prevState =>
					prevState.map(user => {
						if (user.id === userId) {
							return {
								...user,
								enabled: result.enabled,
								dateEnabled: result.dateEnabled
							};
						}
						return user;
					})
				);
				toast.success(
					enabled ? 'User successfully enabled.' : 'User successfully disabled.'
				);
			})
			.catch(error => toast.error(error))
			.finally(() => setLoading(false));
	};

	const value = useMemo<UsersContextValue>(
		() => ({
			loading,
			getAllUsers,
			users,
			deleteUser,
			enableUser
		}),
		[loading, getAllUsers, users, deleteUser, enableUser]
	);

	return (
		<UsersContext.Provider value={value}>{children}</UsersContext.Provider>
	);
};

export const UsersContext = createContext<UsersContextValue>(
	undefined as never
);

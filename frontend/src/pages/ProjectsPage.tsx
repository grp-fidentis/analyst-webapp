import { Backdrop, CircularProgress, Container } from '@mui/material';
import { useContext, useEffect } from 'react';

import ProjectTabMenu from '../components/projects-view/ProjectTabMenu';
import ProjectTabPane from '../components/projects-view/ProjectTabPane';
import { ProjectsContext } from '../providers/ProjectContextProvider';
import { FilesContext } from '../providers/FileContextProvider';

const ProjectsPage = () => {
	const { getUserProjects, isLoadingProjects, selectedProjectId } =
		useContext(ProjectsContext);
	const { setFileSelection } = useContext(FilesContext);

	useEffect(() => {
		if (!selectedProjectId) {
			getUserProjects(true);
		}
		return () => {
			setFileSelection([]);
		};
	}, []);

	return (
		<Container maxWidth="xl" sx={{ mt: 5 }}>
			<Backdrop
				open={isLoadingProjects}
				sx={{ color: '#fff', zIndex: theme => theme.zIndex.drawer + 1 }}
			>
				<CircularProgress color="inherit" />
			</Backdrop>

			<ProjectTabMenu />
			<ProjectTabPane />
		</Container>
	);
};

export default ProjectsPage;

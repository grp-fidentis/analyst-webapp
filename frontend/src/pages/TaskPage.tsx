import { Backdrop, Box, CircularProgress, Grid } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import FaceBottomToolbar from '../components/task-view/visual/toolbar/FaceBottomToolbar';
import BaseTabPanel from '../components/task-view/analysis/BaseTabPanel';
import { TaskContext } from '../providers/TaskContextProvider';
import Scene from '../components/task-view/visual/scene/Scene';
import { TaskService } from '../services/TaskService';
import { HumanFaceService } from '../services/HumanFaceService';
import { SceneContext } from '../providers/SceneContextProvider';
import { ProjectsContext } from '../providers/ProjectContextProvider';
import { ProjectService } from '../services/ProjectService';
import { Task, TaskType } from '../types/TaskTypes';
import { TaskUtils } from '../utils/TaskUtils';
import { BatchProcessingTaskContext } from '../providers/BatchProcessingTaskContextProvider';

const TaskPage = () => {
	const { setTask, getTasksOnProject, projectTasks, setCurrentPanel } =
		useContext(TaskContext);
	const { setFaces, setFace } = useContext(SceneContext);
	const { setSelectedProjectId, getUserProjects, setProjects } =
		useContext(ProjectsContext);
	const { setAverageFace } = useContext(BatchProcessingTaskContext);

	const [loading, setLoading] = useState(false);
	const params = useParams();

	const setTaskAndProject = async (task: Task) => {
		await getUserProjects(true);
		setSelectedProjectId(task.projectId);
		getTasksOnProject(Number(task.projectId));

		const notOpenedProject = (await ProjectService.getUserProjects(false)).find(
			p => p.id === task.projectId
		);
		if (!notOpenedProject) return;
		await ProjectService.setProjectOpenState(notOpenedProject.id, true);
		setProjects(prev => [...prev, { ...notOpenedProject, isOpened: true }]);
	};

	useEffect(() => {
		const fetchTaskAndHumanFaces = async () => {
			setLoading(true);

			try {
				const task = await TaskService.getTaskById(Number(params.id));
				setTask(task);

				if (projectTasks.length === 0) {
					await setTaskAndProject(task);
				}
				const humanFaces = await HumanFaceService.getHumanFacesOfTask(
					Number(params.id)
				);
				const currentPanel = TaskUtils.isSingle(task)
					? 'SYMMETRY'
					: 'REGISTRATION';
				setCurrentPanel(currentPanel);
				if (task.taskType === TaskType.BatchProcessing) {
					const avgFace = humanFaces.find(face => face.isAverageFace);
					if (avgFace) {
						setAverageFace(avgFace);
						setFace(avgFace, null, task.id);
					} else {
						setFace(humanFaces[0], null, task.id);
					}
				} else {
					setFaces(humanFaces);
				}
			} finally {
				setLoading(false);
			}
		};

		fetchTaskAndHumanFaces();
	}, [params.id]);

	useEffect(
		() => () => {
			setTask(undefined);
			setFaces([]);
		},
		[]
	);

	return (
		<Box component="div" sx={{ flexGrow: 1 }}>
			<Backdrop
				open={loading}
				sx={{ color: '#fff', zIndex: theme => theme.zIndex.drawer + 1 }}
			>
				<CircularProgress color="inherit" />
			</Backdrop>

			{!loading && (
				<Grid container>
					<Grid
						id="canvas-container"
						item
						xs={6.5}
						sx={{ display: 'flex', flexDirection: 'column', flex: 1 }}
					>
						<Scene />
						<FaceBottomToolbar />
					</Grid>
					<Grid item xs={5.5}>
						<BaseTabPanel />
					</Grid>
				</Grid>
			)}
		</Box>
	);
};

export default TaskPage;

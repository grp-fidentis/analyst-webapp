import React, { FC, useContext, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { AuthContext } from '../providers/AuthContextProvider';
import { ROUTES } from '../utils/RoutesUrls';

type Props = {
	children: JSX.Element;
};

const ProtectedPage: FC<Props> = ({ children }): JSX.Element | null => {
	const navigate = useNavigate();
	const { pathname } = useLocation();
	const { getCurrentUser, currentUser } = useContext(AuthContext);

	useEffect(() => {
		if (!getCurrentUser()) {
			navigate(ROUTES.chooseLogin, { state: { returnUrl: pathname } });
		}
		if (getCurrentUser()?.enabled === false) {
			navigate(ROUTES.disabledUser);
		}
		if (getCurrentUser() && pathname === '/') {
			navigate(ROUTES.projects, { state: { returnUrl: pathname } });
		}
	}, [navigate, pathname]);

	// eslint-disable-next-line react/jsx-no-useless-fragment
	return currentUser ? <>{children}</> : null;
};

export default ProtectedPage;

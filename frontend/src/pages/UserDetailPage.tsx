import {
	ChangeEvent,
	SyntheticEvent,
	useContext,
	useEffect,
	useMemo,
	useState
} from 'react';
import {
	Box,
	Button,
	Checkbox,
	Container,
	FormControlLabel,
	Paper,
	Stack,
	TextField,
	Typography
} from '@mui/material';
import { toast } from 'react-toastify';
import { useLocation, useNavigate } from 'react-router-dom';
import { KeyboardBackspace } from '@mui/icons-material';

import MultiSelect from '../components/common/MultiSelect';
import { RoleService } from '../services/RoleService';
import { Role, User, UserDetail } from '../types/UserTypes';
import { AuthService } from '../services/AuthService';
import { UserService } from '../services/UserService';
import { DateFormatter } from '../utils/DateFormatter';
import { AuthContext } from '../providers/AuthContextProvider';
import ChangePasswordBox from '../components/auth-view/ChangePasswordBox';
import { Roles } from '../utils/Enums';
import { TokensUtils } from '../utils/TokensUtils';
import UsersProjectsBox from '../components/users-view/projects/UserProjectsBox';

const UserDetailPage = () => {
	const [roles, setRoles] = useState<Role[]>([]);
	const [open, setOpen] = useState(false);
	const { hasAdminRole, handleLogin, hasRole } = useContext(AuthContext);
	const navigate = useNavigate();
	const location = useLocation();
	const state = location.state;

	const user = useMemo<User | undefined>(() => state?.user ?? null, [state]);

	const [formData, setFormData] = useState<UserDetail>({
		id: user?.id ?? null,
		username: user?.username ?? '',
		firstName: user?.firstName ?? '',
		lastName: user?.lastName ?? '',
		email: user?.email ?? '',
		roles: [] as Role[],
		enabled: user?.enabled ?? false,
		locked: user?.locked ?? false,
		loginType: user?.loginType ?? ''
	});

	useEffect(() => {
		RoleService.getRoles().then(roles => {
			setRoles(roles);
		});
	}, []);

	const fixedOptions = useMemo(() => {
		if (!hasRole(Roles.ROLE_SUPER_ADMIN)) {
			const options = roles.find(
				role =>
					role.name === Roles.ROLE_SUPER_ADMIN &&
					user?.roles.find(role => role.name === Roles.ROLE_SUPER_ADMIN)
			);
			return options ? [options] : [];
		}
		return [] as Role[];
	}, [user, roles]);

	const handleFormDataChange = (e: ChangeEvent<HTMLInputElement>) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};

	const handleCheckboxChange = (e: ChangeEvent<HTMLInputElement>) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.checked
		});
	};

	const handleRolesSelection = (roles: Role[]) => {
		setFormData(prevState => ({ ...prevState, roles }));
	};

	const handleSubmit = async (e: SyntheticEvent) => {
		e.preventDefault();

		user
			? UserService.updateUser(formData).then(user => {
					if (state?.isProfile) {
						handleLogin(user);
						TokensUtils.setCurrentUser(user);
					}
			  })
			: await AuthService.register(formData);

		toast.success(
			state?.isProfile
				? 'Profile updated successfully'
				: user
				? 'User updated successfully'
				: 'User registered successfully'
		);
		navigate(-1);
	};

	return (
		<Container
			sx={{
				height: `calc(100vh - 200px)`,
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				zIndex: '5'
			}}
		>
			<Paper elevation={4} sx={{ position: 'relative', zIndex: 5 }}>
				<Stack direction="row" id="user-detail__stack">
					<Typography variant="h5">
						{state?.isProfile ? 'PROFILE' : user ? 'EDITING USER' : 'NEW USER'}
					</Typography>

					{state?.isProfile && user?.loginType == 'LOCAL' && (
						<Typography
							fontSize={12}
							className="cursor-pointer"
							sx={{ textDecoration: 'underline', color: '#5c5c5c' }}
							onClick={() => setOpen(!open)}
						>
							change password
						</Typography>
					)}
				</Stack>

				<Box component="form" onSubmit={handleSubmit} py={3} px={5}>
					<Stack direction="row" gap={5}>
						<TextField
							value={formData.username}
							label="Username"
							name="username"
							margin="normal"
							required
							fullWidth
							onChange={handleFormDataChange}
						/>
						<TextField
							value={formData.email}
							label="Email"
							name="email"
							margin="normal"
							type="email"
							required
							fullWidth
							onChange={handleFormDataChange}
						/>
					</Stack>

					<Stack direction="row" gap={5}>
						<TextField
							value={formData.firstName}
							label="First name"
							name="firstName"
							margin="normal"
							fullWidth
							onChange={handleFormDataChange}
						/>
						<TextField
							value={formData.lastName}
							label="Last name"
							name="lastName"
							margin="normal"
							fullWidth
							onChange={handleFormDataChange}
						/>
					</Stack>

					<Stack direction="row" gap={5} mb={1}>
						<MultiSelect
							defaultValue={user?.roles ?? []}
							options={roles}
							fixedOptions={fixedOptions}
							disabled={!hasAdminRole}
							disabledOptionIds={
								!hasRole(Roles.ROLE_SUPER_ADMIN)
									? roles
											.filter(role => role.name === Roles.ROLE_SUPER_ADMIN)
											.map(role => role.id)
									: undefined
							}
							label="Roles"
							minWidth={320}
							getOptionLabel={role => role.name.replace(/^ROLE_/, '')}
							onChange={handleRolesSelection}
						/>
						<Stack justifyContent="center" mt={1} ml={0.5}>
							<FormControlLabel
								control={
									<Checkbox
										checked={formData.locked}
										disabled={!hasAdminRole}
										name="locked"
										sx={{ p: 0.5 }}
										onChange={handleCheckboxChange}
									/>
								}
								label="Locked"
							/>
							<FormControlLabel
								control={
									<Checkbox
										checked={formData.enabled}
										disabled
										name="enabled"
										sx={{ p: 0.5 }}
										onChange={handleCheckboxChange}
									/>
								}
								label="Enabled"
							/>
						</Stack>
					</Stack>

					{user && (
						<Stack direction="row" gap={1}>
							<Stack>
								<Typography>Date created:</Typography>
								<Typography>Date enabled:</Typography>
							</Stack>
							<Stack>
								<Typography variant="body1" sx={{ color: '#949494' }}>
									{DateFormatter.format(user.dateCreated, 'medium')}
								</Typography>
								<Typography variant="body1" style={{ color: '#949494' }}>
									{DateFormatter.format(user.dateEnabled ?? '', 'medium')}
								</Typography>
							</Stack>
						</Stack>
					)}

					<Stack direction="row" justifyContent="space-between" mt={3}>
						<Button variant="contained" type="submit">
							{user ? 'save' : 'create'}
						</Button>

						<Button
							startIcon={<KeyboardBackspace />}
							onClick={() => navigate(-1)}
						>
							back
						</Button>
					</Stack>
				</Box>
				<ChangePasswordBox open={open} />
			</Paper>
			{state?.isProfile ? <UsersProjectsBox></UsersProjectsBox> : <></>}
		</Container>
	);
};

export default UserDetailPage;

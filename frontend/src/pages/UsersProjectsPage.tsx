import { Container } from '@mui/material';

import UsersProjectsBox from '../components/users-view/projects/UserProjectsBox';

const UsersPage = () => {
	return (
		<Container
			sx={{
				height: `calc(100vh - 200px)`,
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				zIndex: '5'
			}}
		>
			<UsersProjectsBox></UsersProjectsBox>
		</Container>
	);
};

export default UsersPage;

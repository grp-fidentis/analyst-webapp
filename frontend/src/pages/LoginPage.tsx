import { Box, Button, Stack, TextField, Typography } from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { SyntheticEvent, useContext, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { AxiosError } from 'axios';

import useField from '../hooks/useField';
import { ROUTES } from '../utils/RoutesUrls';
import { AuthContext } from '../providers/AuthContextProvider';
import { AuthService } from '../services/AuthService';
import { ErrorResponse } from '../types/CommonTypes';
import ForgotPasswordDialog from '../components/auth-view/ForgotPasswordDialog';

const Login = () => {
	const auth = useContext(AuthContext);
	const navigate = useNavigate();
	const location = useLocation();

	const [open, setOpen] = useState(false);
	const [username, usernameProps] = useField('username', true);
	const [password, passwordProps] = useField('password', true);

	const handleSubmit = async (e: SyntheticEvent) => {
		e.preventDefault();
		try {
			auth.handleLogin(
				await AuthService.login({ username, password }).then(res => res)
			);
			navigate(ROUTES.projects);
		} catch (error) {
			const data = (error as AxiosError)?.response?.data as ErrorResponse;
			toast.error(data.message);
		}
	};

	useEffect(() => {
		const username = location.state?.username;
		usernameProps.onChange({ target: { value: username ?? '' } } as never);
	}, []);

	return (
		<Box
			className="homepage-form__container"
			component="form"
			onSubmit={handleSubmit}
		>
			<TextField
				label="Username"
				{...usernameProps}
				type="text"
				fullWidth
				margin="dense"
			/>
			<TextField
				label="Password"
				{...passwordProps}
				type="password"
				fullWidth
				margin="dense"
			/>

			<Stack
				direction="row"
				justifyContent="space-between"
				alignItems="center"
				mt={1}
			>
				<Button variant="contained" type="submit">
					Login
				</Button>
				<Button onClick={() => navigate(ROUTES.chooseLogin)}>Back</Button>
				<Typography
					fontSize="0.75rem"
					color="gray"
					onClick={() => setOpen(true)}
					sx={{ textDecoration: 'underline', cursor: 'pointer' }}
				>
					Forgot password?
				</Typography>
			</Stack>

			{open && (
				<ForgotPasswordDialog open={open} onClose={() => setOpen(false)} />
			)}
		</Box>
	);
};

export default Login;

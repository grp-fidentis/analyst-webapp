import { useContext, useEffect } from 'react';
import { Button, Container } from '@mui/material';
import { Add } from '@mui/icons-material';
import { NavLink } from 'react-router-dom';

import { UsersContext } from '../providers/UserContextProvider';
import UsersTable from '../components/users-view/UsersTable';
import { ROUTES } from '../utils/RoutesUrls';

const UsersPage = () => {
	const { getAllUsers } = useContext(UsersContext);

	useEffect(() => {
		getAllUsers();
	}, []);

	return (
		<Container maxWidth="xl" sx={{ mt: 5 }}>
			<Button
				startIcon={<Add />}
				component={NavLink}
				to={ROUTES.newUser}
				variant="contained"
				sx={{ my: 2 }}
			>
				create user
			</Button>
			<UsersTable />
		</Container>
	);
};

export default UsersPage;

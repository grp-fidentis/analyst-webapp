import { Container } from '@mui/material';

const DisabledUserPage = () => {
	return (
		<Container
			sx={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				flexDirection: 'column',
				height: '85vh'
			}}
		>
			Your account is disabled. You can not use this app as long as the
			Administrator will enable your account.
		</Container>
	);
};

export default DisabledUserPage;

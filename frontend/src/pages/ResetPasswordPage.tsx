import { ChangeEvent, FormEvent, useMemo, useState } from 'react';
import { Box, Button } from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { AxiosError } from 'axios';
import { toast } from 'react-toastify';

import { ROUTES } from '../utils/RoutesUrls';
import { AuthService } from '../services/AuthService';
import { ErrorResponse } from '../types/CommonTypes';
import PasswordsCompare, {
	PasswordsFormData
} from '../components/auth-view/PasswordsCompare';

const ResetPasswordPage = () => {
	const navigate = useNavigate();
	const [helperTextValidation, setHelperTextValidation] = useState<
		string | undefined
	>(undefined as never);
	const [helperTextMatch, setHelperTextMatch] = useState<string | undefined>(
		undefined as never
	);

	const params = new URLSearchParams(useLocation().search);
	const token = params.get('token');

	const [formData, setFormData] = useState<PasswordsFormData>({
		password: '',
		passwordAgain: ''
	});

	const isButtonDisabled = useMemo(
		() =>
			!!helperTextValidation ||
			!!helperTextMatch ||
			Object.keys(formData).some(k => !formData[k as keyof PasswordsFormData]),
		[helperTextValidation, helperTextMatch]
	);

	const handleFormDataChange = (e: ChangeEvent<HTMLInputElement>) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};

	const handleSubmitForm = (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		AuthService.resetPassword({
			password: formData.password,
			token: token ?? ''
		})
			.then(res => {
				navigate(ROUTES.login, { state: { username: res.username } });
			})
			.catch(error => {
				const data = (error as AxiosError)?.response?.data as ErrorResponse;
				toast.error(data.message);
			});
	};

	return (
		<Box
			component="form"
			onSubmit={handleSubmitForm}
			className="homepage-form__container"
		>
			<PasswordsCompare
				formData={formData}
				setPasswordValidation={setHelperTextValidation}
				setPasswordMatch={setHelperTextMatch}
				setFormData={handleFormDataChange}
				passwordValidation={helperTextValidation}
				passwordMatch={helperTextMatch}
			/>

			<Button
				disabled={isButtonDisabled}
				variant="contained"
				type="submit"
				sx={{ mt: 1 }}
			>
				reset password
			</Button>
		</Box>
	);
};

export default ResetPasswordPage;

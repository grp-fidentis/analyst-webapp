import { useNavigate } from 'react-router-dom';
import { ROUTES } from '../utils/RoutesUrls';
import { Box, Button } from '@mui/material';
import React from 'react';

const ChooseLogin = () => {
	const navigate = useNavigate();

	return (
		<Box className="homepage-form__container" component="form">
			<Button
				variant="contained"
				type="submit"
				onClick={() => {
					navigate(ROUTES.login);
				}}
			>
				LOCAL ACCOUNTS
			</Button>
			<Button
				variant="contained"
				sx={{ mt: 1 }}
				type="submit"
				href="/oauth2/authorization/muni"
			>
				MUNI
			</Button>

			{/* Example of adding github as another login provider.
			<Button
				variant="contained"
				sx={{ mt: 1 }}
				type="submit"
				href="/oauth2/authorization/github"
			>
				GITHUB
			</Button> */}
		</Box>
	);
};

export default ChooseLogin;

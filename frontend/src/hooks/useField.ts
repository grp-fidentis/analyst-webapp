import { ChangeEvent, useCallback, useState } from 'react';

const useField = (id: string, required?: boolean) => {
	const [value, setValue] = useState('');

	return [
		value,
		{
			id,
			value,
			onChange: useCallback(
				(e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
					setValue(e.target.value),
				[]
			),
			required
		}
	] as const;
};

export default useField;

import {
	ChangeEvent,
	SyntheticEvent,
	useContext,
	useEffect,
	useState
} from 'react';
import { SelectChangeEvent } from '@mui/material';
import { Id, toast } from 'react-toastify';

import { TaskContext } from '../providers/TaskContextProvider';
import { ProjectsContext } from '../providers/ProjectContextProvider';
import { BatchProcessingTaskContext } from '../providers/BatchProcessingTaskContextProvider';
import { SceneContext } from '../providers/SceneContextProvider';
import { BatchProcessingTaskService } from '../services/BatchProcessingTaskService';
import { LoadingOperation } from '../types/AnalyticalTaskTypes';
import { DownloadUtils } from '../utils/DownloadUtils';
import {
	AddAverageFaceToProjectDto,
	BatchDistancesDto
} from '../types/BatchProcessingTypes';
import ProgressBar from '../components/common/ProgressBar';
import { ComputingTasksContext } from '../providers/ComputingTasksContextProvider';
import {
	TaskParametersDistanceUpdateDto,
	TaskParametersRegistrationUpdateDto
} from '../types/TaskParametersTypes';
import { TaskParametersService } from '../services/TaskParametersService';

let socket: WebSocket | null = null;

const useBatchTaskManagement = () => {
	const { task } = useContext(TaskContext);
	const { selectedProjectId } = useContext(ProjectsContext);
	const {
		taskFiles,
		averageFace,
		setAverageFace,
		isLoadingBatchInfo,
		getTaskFiles,
		getTaskParameters,
		updateLoadingInfo
	} = useContext(BatchProcessingTaskContext);
	const { setFace, setLocked } = useContext(SceneContext);
	const { addComputingTaskId, removeComputingTaskId } = useContext(
		ComputingTasksContext
	);

	const {
		batchDistanceForTask,
		updateBatchDistancesDto,
		batchRegistrationTaskDto,
		setBatchRegistrationTaskDto,
		batchDistanceTaskDto,
		setBatchDistanceTaskDto,
		selectedFileRegistration,
		setSelectedFileRegistration,
		selectedFileDistance,
		setSelectedFileDistance
	} = useContext(BatchProcessingTaskContext);

	const [isModalOpen, setModalOpen] = useState(false);
	const [fileName, setFileName] = useState('');
	const [isDataFetched, setIsDataFetched] = useState(false);

	useEffect(() => {
		if (!isDataFetched) {
			getTaskFiles();
			setIsDataFetched(true);
		}
	}, [isDataFetched]);

	useEffect(() => {
		if (taskFiles.length > 0) {
			getTaskParameters();
		}
	}, [taskFiles]);

	useEffect(() => {
		setBatchRegistrationTaskDto({
			...batchRegistrationTaskDto,
			selectedFile: selectedFileRegistration
		});
	}, [selectedFileRegistration]);

	useEffect(() => {
		setBatchDistanceTaskDto({
			...batchDistanceTaskDto,
			selectedFile: selectedFileDistance
		});
	}, [selectedFileDistance]);

	const exportAverageFace = async () => {
		if (!averageFace) return;
		const blob = await BatchProcessingTaskService.exportAverageFace(
			averageFace.id
		);
		const fileName = 'AverageFace.obj';

		DownloadUtils.downloadFile(blob, fileName);
	};

	const handleOpenModal = () => {
		setModalOpen(true);
	};

	const handleCloseModal = () => {
		setModalOpen(false);
		setFileName('');
	};

	const handleSaveAverageFaceToProject = () => {
		if (!fileName.trim()) {
			toast.error('File name cannot be empty');
			return;
		}

		if (!averageFace?.id || !task?.id || !selectedProjectId) return;
		const dto: AddAverageFaceToProjectDto = {
			humanFaceEntityId: averageFace.id,
			taskId: task?.id,
			projectId: selectedProjectId,
			fileName
		};
		BatchProcessingTaskService.addAverageFaceToProject(dto).then(() => {
			handleCloseModal();
			getTaskFiles();
			toast.success('Average face saved to project.');
		});
	};

	const closeWebSocket = () => {
		if (socket) {
			socket.close();
		}
	};

	const handleAveragingOptionsChange = (which: string, value: number) => {
		if (which === 'iterations') {
			setBatchRegistrationTaskDto({
				...batchRegistrationTaskDto,
				averagingTaskDto: {
					...batchRegistrationTaskDto.averagingTaskDto,
					maxRegistrationIterations: value
				}
			});
		} else {
			setBatchRegistrationTaskDto({
				...batchRegistrationTaskDto,
				averagingTaskDto: {
					...batchRegistrationTaskDto.averagingTaskDto,
					stabilizationThreshold: value
				}
			});
		}
	};

	const handleInterrupt = () => {
		if (socket) {
			socket.send(JSON.stringify({ action: 'interrupt' }));
		}
		updateLoadingInfo(false);
	};

	const handleRegistrationOptionChange = (value: number) => {
		setBatchRegistrationTaskDto({
			...batchRegistrationTaskDto,
			registrationTaskDto: {
				...batchRegistrationTaskDto.registrationTaskDto,
				registrationSubsampling: value
			}
		});
	};

	const handleCheckboxChange = (e: ChangeEvent<HTMLInputElement>) => {
		setBatchRegistrationTaskDto({
			...batchRegistrationTaskDto,
			registrationTaskDto: {
				...batchRegistrationTaskDto.registrationTaskDto,
				scaleFacesDuringRegistration: e.target.checked
			}
		});
	};

	const handleChangeTemplateForRegistration = (
		e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement> | SelectChangeEvent
	) => {
		const fileName = e.target.value;
		const newSelectedFile = taskFiles.find(
			file => `${file.name}.${file.extension}` === fileName
		);
		if (newSelectedFile) {
			setSelectedFileRegistration(newSelectedFile);
			setFace(null, newSelectedFile.id, task?.id);
		}
	};

	const handleChangeTemplateForDistance = (
		e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement> | SelectChangeEvent
	) => {
		const fileName = e.target.value;
		const newSelectedFile = taskFiles.find(
			file => `${file.name}.${file.extension}` === fileName
		);
		if (newSelectedFile) {
			setSelectedFileDistance(newSelectedFile);
			setFace(null, newSelectedFile.id, task?.id);
		}
	};

	const initializeWebSocket = (id: Id) =>
		new Promise((resolve, reject) => {
			const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
			const hostName = window.location.hostname;
			const port = '8080';

			const wsUrl = `${protocol}://${hostName}:${port}/progress`;

			socket = new WebSocket(wsUrl);

			socket.addEventListener('message', event => {
				const message = JSON.parse(event.data);
				if (message.type === 'sessionId') {
					const sessionId = message.sessionId;
					resolve(sessionId);
				} else if (message.type === 'progress') {
					toast.update(id, {
						render: <ProgressBar value={message.progress} />,
						autoClose: false,
						isLoading: true
					});
				}
			});

			socket.addEventListener('error', error => {
				reject(error);
			});
		});

	const saveParametersForRegistration = () => {
		if (!task || !selectedFileRegistration) {
			return;
		}
		const taskParametersForRegistration: TaskParametersRegistrationUpdateDto = {
			templateFaceForRegistration: selectedFileRegistration.name,
			averagingMethod:
				batchRegistrationTaskDto.averagingTaskDto.averagingMethod,
			maxRegistrationIterations:
				batchRegistrationTaskDto.averagingTaskDto.maxRegistrationIterations,
			stabilizationThreshold:
				batchRegistrationTaskDto.averagingTaskDto.stabilizationThreshold,
			registrationMethod:
				batchRegistrationTaskDto.registrationTaskDto.registrationMethod,
			registrationSubsampling:
				batchRegistrationTaskDto.registrationTaskDto.registrationSubsampling,
			scaleFacesDuringRegistration:
				batchRegistrationTaskDto.registrationTaskDto
					.scaleFacesDuringRegistration
		};

		TaskParametersService.updateTaskParametersForRegistration(
			task.id,
			taskParametersForRegistration
		);
	};

	const saveParametersForDistance = (distances: BatchDistancesDto | null) => {
		if (!task || !selectedFileDistance) {
			return;
		}

		const taskParametersForDistance: TaskParametersDistanceUpdateDto = distances
			? {
					templateFaceForDistance: selectedFileDistance.name,
					similarityMethod: batchDistanceTaskDto.similarityMethod,
					deviations: JSON.stringify(
						distances.faceDistances.map(face => face.faceDistances)
					),
					distances: JSON.stringify(
						distances.faceDistances.map(face => face.faceDeviations)
					)
			  }
			: {
					templateFaceForDistance: null,
					similarityMethod: null,
					deviations: null,
					distances: null
			  };

		TaskParametersService.updateTaskParametersForDistance(
			task.id,
			taskParametersForDistance
		);
	};

	const exportDistanceResults = async () => {
		if (!batchDistanceForTask) return;
		const blob = await BatchProcessingTaskService.exportDistanceResults(
			batchDistanceForTask
		);
		const fileName = `${task?.name}.csv`;

		DownloadUtils.downloadFile(blob, fileName);
	};

	const computeDistance = async () => {
		if (!task) return;
		addComputingTaskId(task?.id ?? null);
		updateLoadingInfo(true, LoadingOperation.Distance);
		setLocked(true);

		const toastId = toast.loading(<ProgressBar value={0} />, {
			closeButton: false,
			hideProgressBar: true,
			position: 'bottom-right',
			closeOnClick: false
		});
		try {
			const sessionId = await initializeWebSocket(toastId);

			const updatedDto = {
				...batchDistanceTaskDto,
				calculationWebSocketSessionId: sessionId
			};

			const distances = await BatchProcessingTaskService.computeDistance(
				updatedDto
			);

			if (distances !== null) {
				updateBatchDistancesDto(task.id, distances);
				toast.update(toastId, {
					render: 'Calculation completed successfully',
					type: toast.TYPE.SUCCESS,
					isLoading: false,
					autoClose: 5000
				});
				saveParametersForDistance(distances);
			} else {
				toast.update(toastId, {
					render: 'Calculation interrupted',
					type: toast.TYPE.ERROR,
					isLoading: false,
					autoClose: 5000
				});
			}
		} catch (error) {
			removeComputingTaskId(task?.id ?? null);
			toast.update(toastId, {
				render: `Calculation failed: ${error}`,
				type: toast.TYPE.ERROR,
				isLoading: false,
				autoClose: 5000
			});
		} finally {
			removeComputingTaskId(task?.id ?? null);
			closeWebSocket();
			updateLoadingInfo(false);
			setLocked(false);
			toast.dismiss(toastId);
		}
	};

	const handleSubmit = async (e: SyntheticEvent) => {
		if (!task) return;
		e.preventDefault();
		addComputingTaskId(task?.id ?? null);
		updateLoadingInfo(true, LoadingOperation.Registration);
		setLocked(true);

		const toastId = toast.loading(<ProgressBar value={0} />, {
			closeButton: false,
			hideProgressBar: true,
			position: 'bottom-right',
			closeOnClick: false
		});

		try {
			const sessionId = await initializeWebSocket(toastId);

			const updatedDto = {
				...batchRegistrationTaskDto,
				calculationWebSocketSessionId: sessionId
			};

			const results = await BatchProcessingTaskService.executeRegistration(
				updatedDto
			);

			if (results.humanFace !== null && results.completedSuccessfully) {
				updateBatchDistancesDto(task.id, null);
				setFace(results.humanFace, null, task?.id);
				setAverageFace(results.humanFace);
				getTaskFiles();
				toast.update(toastId, {
					render: 'Calculation completed successfully',
					type: toast.TYPE.SUCCESS,
					isLoading: false,
					autoClose: 5000
				});
				saveParametersForRegistration();
				saveParametersForDistance(null);
			} else if (results.humanFace === null && results.completedSuccessfully) {
				updateBatchDistancesDto(task.id, null);
				getTaskFiles();
				toast.update(toastId, {
					render: 'Calculation completed successfully',
					type: toast.TYPE.SUCCESS,
					isLoading: false,
					autoClose: 5000
				});
				saveParametersForRegistration();
				saveParametersForDistance(null);
			} else {
				toast.update(toastId, {
					render: 'Calculation interrupted',
					type: toast.TYPE.ERROR,
					isLoading: false,
					autoClose: 5000
				});
			}
		} catch (error) {
			removeComputingTaskId(task?.id ?? null);
			toast.update(toastId, {
				render: `Calculation failed: ${error}`,
				type: toast.TYPE.ERROR,
				isLoading: false,
				autoClose: 5000
			});
		} finally {
			removeComputingTaskId(task?.id ?? null);
			closeWebSocket();
			updateLoadingInfo(false);
			setLocked(false);
			toast.dismiss(toastId);
		}
	};

	return {
		taskFiles,
		selectedFileRegistration,
		selectedFileDistance,
		isModalOpen,
		fileName,
		setFileName,
		isLoadingBatchInfo,
		exportAverageFace,
		exportDistanceResults,
		computeDistance,
		handleAveragingOptionsChange,
		handleRegistrationOptionChange,
		handleCheckboxChange,
		handleChangeTemplateForRegistration,
		handleChangeTemplateForDistance,
		handleOpenModal,
		handleCloseModal,
		handleSaveAverageFaceToProject,
		handleSubmit,
		handleInterrupt
	};
};

export default useBatchTaskManagement;

import throttle from '@jcoreio/async-throttle';
import { useCallback } from 'react';

const useThrottle = <T extends (...args: any) => any>(
	func: T,
	minimumMilliseconds: number
) => useCallback(throttle(func, minimumMilliseconds), []);
export default useThrottle;

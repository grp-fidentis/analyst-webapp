import { scaleLinear } from '@visx/scale';
import React, {
	useCallback,
	useContext,
	useEffect,
	useMemo,
	useRef,
	useState
} from 'react';
import { Scale } from '@visx/visx';

import {
	binWidth,
	endColor,
	startColor
} from '../../components/visualization/visualizationConfig';
import { BatchDistancesDto } from '../../types/BatchProcessingTypes';
import { BatchProcessingTaskService } from '../../services/BatchProcessingTaskService';
import {
	ColumnState,
	ContextMenuState,
	DistanceState,
	HeatmapRowData,
	HiddenItems,
	LinkageStrategy,
	PairStep,
	RowState,
	Transform
} from '../../types/BatchVisualizationTypes';
import { BatchProcessingTaskContext } from '../../providers/BatchProcessingTaskContextProvider';
import { HumanFaceService } from '../../services/HumanFaceService';
import { SceneContext } from '../../providers/SceneContextProvider';
import { TaskContext } from '../../providers/TaskContextProvider';
import { FileUpload } from '../../types/FileTypes';

import useDendrogram from './useDendrogramManagament';

const useHeatmapManagement = () => {
	const { task } = useContext(TaskContext);
	const { batchDistanceForTask, taskFiles } = useContext(
		BatchProcessingTaskContext
	);
	const { setFaces } = useContext(SceneContext);

	const [heatmapRows, setHeatmapRows] = useState<HeatmapRowData[]>([]);
	const [filteredHeatmapRows, setFilteredHeatmapRows] = useState<
		HeatmapRowData[]
	>([]);
	const [loading, setLoading] = useState(false);
	const [selectionMode, setSelectionMode] = useState(true);
	const transform = useRef<Transform>({
		scale: 1,
		translateX: 0,
		translateY: 0
	}).current;

	const [openPairTaskConfirmation, setOpenPairTaskConfirmation] =
		useState(false);
	const [openSingleTaskConfirmation, setOpenSingleTaskConfirmation] =
		useState(false);
	const [filesPair, setFilesPair] = useState<FileUpload[]>([]);

	const [rowState, setRowState] = useState<RowState>({
		event: null,
		draggedIndex: null,
		targetIndex: null,
		clickedName: null,
		lastClickedName: null,
		lastClickedIndex: null
	});

	const [columnState, setColumnState] = useState<ColumnState>({
		event: null,
		draggedIndex: null,
		targetIndex: null,
		clickedName: null,
		lastClickedName: null,
		lastClickedIndex: null
	});

	const [hiddenItems, setHiddenItems] = useState<HiddenItems>({
		faces: [],
		selectedFaces: []
	});

	const [clusteringSteps, setClusteringSteps] = useState<PairStep[]>([]);
	const [isCtrlPressed, setIsCtrlPressed] = useState(false);
	const [localLinkageStrategy, setLocalLinkageStrategy] = useState(
		LinkageStrategy.AVERAGE
	);

	const [distances, setDistances] = useState<DistanceState | null>(null);

	const { calculateRowPositions, handleShowDendrogram } = useDendrogram(
		filteredHeatmapRows,
		setFilteredHeatmapRows,
		setClusteringSteps,
		batchDistanceForTask,
		localLinkageStrategy
	);

	const scaleAxisX = useMemo(
		() =>
			Scale.scaleLinear({
				domain: [0, heatmapRows.length],
				range: [0, heatmapRows.length * binWidth]
			}),
		[heatmapRows]
	);

	const scaleAxisY = useMemo(
		() =>
			Scale.scaleLinear({
				domain: [0, heatmapRows.length],
				range: [0, heatmapRows.length * binWidth]
			}),
		[heatmapRows]
	);

	const colorScale = useMemo(() => {
		if (heatmapRows.length === 0)
			return scaleLinear<string>({
				domain: [0, 1],
				range: [startColor, endColor]
			});

		const allDistances = heatmapRows
			.flatMap(rowData => rowData.columns.map(face => face.distance))
			.filter(distance => distance !== 0);

		const minDistance = Math.min(...allDistances);
		const maxDistance = Math.max(...allDistances);

		setDistances({
			min: minDistance,
			max: maxDistance,
			pairDistance: null
		});

		return scaleLinear<string>({
			domain: [maxDistance, minDistance],
			range: [startColor, endColor]
		});
	}, [heatmapRows, startColor, endColor]);

	const [contextMenu, setContextMenu] = useState<ContextMenuState | null>(null);

	const handleContextMenu = useCallback(
		(
			event: React.MouseEvent<HTMLCanvasElement> | null,
			type: 'row' | 'column' | 'cell',
			rowName: string | null,
			rowIndex: number | null,
			columnName: string | null,
			columnIndex: number | null,
			pairDistance: number | null
		) => {
			if (!event) return;
			event.preventDefault();

			setContextMenu({
				mouseX: event.clientX - 2,
				mouseY: event.clientY - 4,
				type,
				rowName,
				rowIndex,
				columnName,
				columnIndex,
				pairDistance
			});
		},
		[]
	);

	const handleCloseContextMenu = () => {
		setContextMenu(null);
	};

	const toggleSelectionMode = () => {
		setSelectionMode(prevMode => !prevMode);
	};

	useEffect(() => {
		if (batchDistanceForTask !== null) {
			handlePrepareHeatmapData(batchDistanceForTask);
		} else {
			setHeatmapRows([]);
		}
	}, [batchDistanceForTask]);

	useEffect(() => {
		if (heatmapRows && heatmapRows.length > 0) {
			setHiddenItems({
				...hiddenItems,
				faces: []
			});
			setFilteredHeatmapRows(heatmapRows);
			setLoading(false);
		}
	}, [heatmapRows]);

	useEffect(() => {
		setFilteredHeatmapRows(
			filteredHeatmapRows
				.filter(row => !hiddenItems.faces.includes(row.faceName))
				.map(row => ({
					...row,
					columns: row.columns.filter(
						col => !hiddenItems.faces.includes(col.faceName)
					)
				}))
		);
	}, [hiddenItems.faces]);

	const handleOpenSingleTask = useCallback(
		(faceName: string) => {
			const file = taskFiles.filter(file => file.name === faceName);
			setFilesPair(file);
			setOpenSingleTaskConfirmation(true);
		},
		[taskFiles, setFilesPair, setOpenSingleTaskConfirmation]
	);

	const handleOpenPairTask = useCallback(
		(rowFaceName: string, columnFaceName: string) => {
			const filesPair = taskFiles.filter(
				file => file.name === rowFaceName || file.name === columnFaceName
			);
			if (filesPair.length !== 2) return;
			setFilesPair(filesPair);
			setOpenPairTaskConfirmation(true);
		},
		[taskFiles, setFilesPair, setOpenPairTaskConfirmation]
	);

	const handleShowFace = useCallback(
		async (faceName: string, faceIndex: number, type: 'row' | 'column') => {
			if (!selectionMode) return;
			if (!task?.id) return [];
			if (type === 'row') {
				resetSelected();
				setRowState({
					...rowState,
					lastClickedIndex: faceIndex
				});
			} else {
				resetSelected();
				setColumnState({
					...columnState,
					lastClickedIndex: faceIndex
				});
			}
			const humanFaces = await HumanFaceService.getHumanFacesPair(
				task.id,
				faceName,
				null
			);
			setFaces(humanFaces);
		},
		[setFaces, selectionMode]
	);

	const handleShowFaces = useCallback(
		async (
			rowFaceName: string,
			rowIndex: number,
			columnFaceName: string,
			columnIndex: number,
			pairDistance: number
		) => {
			if (!selectionMode) return;
			if (!task?.id) return [];
			setRowState({
				...rowState,
				lastClickedIndex: rowIndex
			});
			setColumnState({
				...columnState,
				lastClickedIndex: columnIndex
			});
			setDistances(prev => ({
				min: prev?.min ?? 0,
				max: prev?.max ?? 0,
				pairDistance
			}));
			const humanFaces = await HumanFaceService.getHumanFacesPair(
				task.id,
				rowFaceName,
				columnFaceName
			);

			setFaces(humanFaces);
		},
		[setFaces, selectionMode]
	);

	const resetSelected = () => {
		setRowState({
			...rowState,
			lastClickedIndex: null
		});
		setColumnState({
			...columnState,
			lastClickedIndex: null
		});
		setDistances(prev => ({
			min: prev?.min ?? 0,
			max: prev?.max ?? 0,
			pairDistance: null
		}));
	};

	const hideRowAndColumn = useCallback((faceName: string) => {
		setClusteringSteps([]);
		resetSelected();
		setHiddenItems(prev => ({
			...prev,
			faces: [...prev.faces, faceName]
		}));
	}, []);

	const hideBoth = (rowFaceName: string, columnFaceName: string) => {
		hideRowAndColumn(rowFaceName);
		hideRowAndColumn(columnFaceName);
	};

	const showHidden = (selectedFaceNames: string[]) => {
		const visibleRowFaceNames = new Set(
			filteredHeatmapRows.map(row => row.faceName)
		);

		const visibleColumnFaceNames = new Set(
			filteredHeatmapRows[0].columns.map(col => col.faceName)
		);

		const newRows = heatmapRows
			.filter(
				row =>
					selectedFaceNames.includes(row.faceName) &&
					!visibleRowFaceNames.has(row.faceName)
			)
			.map(row => ({
				...row,
				columns: [
					...row.columns.filter(col =>
						selectedFaceNames.includes(col.faceName)
					),
					...row.columns.filter(col => visibleColumnFaceNames.has(col.faceName))
				]
			}));

		const updatedFilteredRows = filteredHeatmapRows.map(row => {
			const correspondingRow = heatmapRows.find(
				heatmapRow => heatmapRow.faceName === row.faceName
			);

			return {
				...row,
				columns: [
					...(correspondingRow?.columns.filter(col =>
						selectedFaceNames.includes(col.faceName)
					) ?? []),
					...row.columns.filter(col => visibleColumnFaceNames.has(col.faceName))
				]
			};
		});

		setFilteredHeatmapRows([...newRows, ...updatedFilteredRows]);

		setHiddenItems(prev => ({
			...prev,
			faces: prev.faces.filter(face => !selectedFaceNames.includes(face)),
			selectedFaces: []
		}));
	};

	const handleRowOrColumnClick = useCallback(
		(
			event: React.MouseEvent<HTMLCanvasElement>,
			type: 'row' | 'column',
			index: number,
			name: string
		) => {
			const isRow = type === 'row';
			const setState = isRow ? setRowState : setColumnState;

			setState(prevState => ({
				...prevState,
				event,
				draggedIndex: prevState.lastClickedName === name ? null : index,
				lastClickedName: prevState.draggedIndex !== index ? null : name,
				clickedName: prevState.clickedName === name ? null : name
			}));
		},
		[rowState, columnState]
	);

	const resetDraggedState = (
		type: 'row' | 'column',
		draggedIndex: number | null,
		targetIndex: number | null
	) => {
		const setState = type === 'row' ? setRowState : setColumnState;

		setClusteringSteps([]);
		setState(prevState => ({
			...prevState,
			lastClickedName: null,
			lastClickedIndex: null,
			draggedIndex: null,
			targetIndex: null
		}));

		if (
			draggedIndex !== null &&
			targetIndex !== null &&
			draggedIndex !== targetIndex
		) {
			const updatedItems = [...filteredHeatmapRows];
			if (type === 'row') {
				const [movedItem] = updatedItems.splice(draggedIndex, 1);
				updatedItems.splice(targetIndex, 0, movedItem);
			} else {
				updatedItems.forEach(row => {
					const updatedColumns = [...row.columns];
					const [movedColumn] = updatedColumns.splice(draggedIndex, 1);
					updatedColumns.splice(targetIndex, 0, movedColumn);
					row.columns = updatedColumns;
				});
			}
			setFilteredHeatmapRows(updatedItems);
		}
	};

	const finalizeDrag = useCallback(() => {
		if (rowState.draggedIndex !== null && rowState.targetIndex === null) {
			handleContextMenu(
				rowState.event,
				'row',
				rowState.clickedName,
				rowState.draggedIndex,
				null,
				null,
				null
			);

			setRowState({
				...rowState,
				event: null,
				draggedIndex: null
			});
		} else if (
			rowState.draggedIndex !== null &&
			rowState.targetIndex !== null
		) {
			if (
				rowState.lastClickedIndex !== null &&
				columnState.lastClickedIndex !== null
			) {
				resetSelected();
			}
			resetDraggedState('row', rowState.draggedIndex, rowState.targetIndex);
		}

		if (columnState.draggedIndex !== null && columnState.targetIndex === null) {
			if (
				rowState.lastClickedIndex !== null &&
				columnState.lastClickedIndex !== null
			) {
				resetSelected();
			}
			handleContextMenu(
				columnState.event,
				'column',
				null,
				null,
				columnState.clickedName,
				columnState.draggedIndex,
				null
			);

			setColumnState({
				...columnState,
				event: null,
				draggedIndex: null
			});
		} else if (
			columnState.draggedIndex !== null &&
			columnState.targetIndex !== null
		) {
			if (
				rowState.lastClickedIndex !== null &&
				columnState.lastClickedIndex !== null
			) {
				resetSelected();
			}
			resetDraggedState(
				'column',
				columnState.draggedIndex,
				columnState.targetIndex
			);
		}
	}, [
		rowState.draggedIndex,
		rowState.targetIndex,
		columnState.draggedIndex,
		columnState.targetIndex,
		filteredHeatmapRows
	]);

	const handleHeatmapDrag = useCallback(
		(event: React.MouseEvent<HTMLDivElement>) => {
			if (selectionMode) {
				const rect = event.currentTarget.getBoundingClientRect();

				if (rowState.draggedIndex !== null) {
					const yPosition =
						(event.clientY - rect.top - transform.translateY) / transform.scale;
					const newIndex = Math.floor((yPosition - 100) / binWidth);

					if (newIndex >= 0 && newIndex < heatmapRows.length) {
						if (newIndex === rowState.draggedIndex) {
							setRowState({ ...rowState, targetIndex: null });
						} else {
							setRowState({ ...rowState, targetIndex: newIndex });
						}
					}
				}
				if (columnState.draggedIndex !== null) {
					const xPosition =
						(event.clientX - rect.left - transform.translateX) /
						transform.scale;
					const newColumnIndex = Math.floor((xPosition - 100) / binWidth);
					if (
						newColumnIndex >= 0 &&
						newColumnIndex < heatmapRows[0].columns.length
					) {
						if (newColumnIndex === columnState.draggedIndex) {
							setColumnState({ ...columnState, targetIndex: null });
						} else {
							setColumnState({
								...columnState,
								targetIndex: newColumnIndex
							});
						}
					}
				}
			}
		},
		[
			selectionMode,
			rowState.draggedIndex,
			columnState.draggedIndex,
			transform.scale,
			transform.translateX,
			transform.translateY,
			heatmapRows
		]
	);

	const handleKeyDown = useCallback(
		(event: React.KeyboardEvent<HTMLDivElement>) => {
			if (event.key === 'Control' && !isCtrlPressed) {
				toggleSelectionMode();
				setIsCtrlPressed(true);
			}
		},
		[isCtrlPressed]
	);

	const handleKeyUp = useCallback(
		(event: React.KeyboardEvent<HTMLDivElement>) => {
			if (event.key === 'Control') {
				toggleSelectionMode();
				setIsCtrlPressed(false);
			}
		},
		[isCtrlPressed]
	);

	const handlePrepareHeatmapData = async (
		batchDistancesDto: BatchDistancesDto | null
	) => {
		if (!batchDistancesDto) return;
		setLoading(true);
		const heatmapRowData =
			await BatchProcessingTaskService.prepareDataForHeatmap(batchDistancesDto);
		setHeatmapRows(heatmapRowData);
		setLoading(false);
	};

	return {
		loading,
		heatmapRows: filteredHeatmapRows,
		scaleAxisX,
		scaleAxisY,
		colorScale,
		selectionMode,
		handleRowOrColumnClick,
		handleOpenPairTask,
		hideRowAndColumn,
		hiddenItems,
		rowState,
		columnState,
		transform,
		handleHeatmapDrag,
		finalizeDrag,
		openPairTaskConfirmation,
		localLinkageStrategy,
		filesPair,
		handleOpenSingleTask,
		handleKeyDown,
		openSingleTaskConfirmation,
		clusteringSteps,
		calculateRowPositions,
		contextMenu,
		handleShowFace,
		handleShowFaces,
		handleContextMenu,
		handleKeyUp,
		distances,
		resetSelected,
		handleCloseContextMenu,
		showHidden,
		hideBoth,
		setHiddenItems,
		toggleSelectionMode,
		setOpenPairTaskConfirmation,
		setLocalLinkageStrategy,
		setOpenSingleTaskConfirmation,
		handleShowDendrogram
	};
};

export default useHeatmapManagement;

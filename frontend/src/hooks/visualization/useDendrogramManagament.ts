import { useCallback } from 'react';
import { ScaleLinear } from 'd3-scale';

import { BatchDistancesDto } from '../../types/BatchProcessingTypes';
import {
	HeatmapRowData,
	LinkageStrategy,
	PairStep,
	SerializableClusterNode
} from '../../types/BatchVisualizationTypes';
import { BatchProcessingTaskService } from '../../services/BatchProcessingTaskService';

const useDendrogram = (
	filteredHeatmapRows: HeatmapRowData[],
	setFilteredHeatmapRows: (rows: HeatmapRowData[]) => void,
	setClusteringSteps: (steps: PairStep[]) => void,
	batchDistancesDto: BatchDistancesDto | null,
	localLinkageStrategy: LinkageStrategy
) => {
	const calculateRowPositions = useCallback(
		(
			heatmapRows: HeatmapRowData[],
			scaleAxisY: ScaleLinear<number, number>,
			binHeight: number
		): Record<string, number> => {
			const positions: Record<string, number> = {};
			heatmapRows.forEach((row, rowIndex) => {
				const y = scaleAxisY(rowIndex) + binHeight / 2;
				positions[row.faceName] = y;
			});
			return positions;
		},
		[filteredHeatmapRows]
	);

	const prepareDataForDendrogram = (): BatchDistancesDto | null => {
		if (!filteredHeatmapRows || !batchDistancesDto) {
			return null;
		}

		const heatmapRowData = filteredHeatmapRows;
		const validFaceNames = new Set(heatmapRowData.map(row => row.faceName));
		const validIndexes = new Set(
			heatmapRowData.flatMap(row => row.columns.map(column => column.faceIndex))
		);

		// Filter BatchDistancesDto to only include faceDistances with faceNames in HeatmapRowData
		const filteredFaceDistances = batchDistancesDto.faceDistances
			.filter(faceDistance => validFaceNames.has(faceDistance.faceName))
			.map(faceDistance => ({
				...faceDistance,
				// Filter faceDistances array to only include distances at valid indexes
				faceDistances: faceDistance.faceDistances.filter((_, index) =>
					validIndexes.has(index)
				),
				faceDeviations: faceDistance.faceDeviations.filter((_, index) =>
					validIndexes.has(index)
				)
			}));

		return {
			...batchDistancesDto,
			faceDistances: filteredFaceDistances,
			linkageStrategy: localLinkageStrategy
		};
	};

	const getClusterSteps = (node: SerializableClusterNode): PairStep[] => {
		const steps: PairStep[] = [];

		const traverse = (currentNode: SerializableClusterNode): string => {
			if (currentNode.children.length === 2) {
				const [leftChild, rightChild] = currentNode.children;

				const leftName = traverse(leftChild);
				const rightName = traverse(rightChild);

				if (currentNode.distance !== null) {
					steps.push({
						pair: [leftName, rightName],
						resultName: currentNode.name,
						distance: currentNode.distance
					});
				}

				return currentNode.name;
			}

			return currentNode.name;
		};

		traverse(node);

		steps.sort((a, b) => a.distance - b.distance);
		return steps;
	};

	const reorderRows = (clusteringSteps: PairStep[]) => {
		type RowType = (typeof filteredHeatmapRows)[0];

		const faceMap = new Map<string, RowType>();
		filteredHeatmapRows.forEach(row => {
			faceMap.set(row.faceName, row);
		});

		const visited = new Set<string>();

		const collectRows = (clusterName: string): RowType[] => {
			const step = clusteringSteps.find(
				step => step.resultName === clusterName
			);

			if (!step) {
				if (visited.has(clusterName)) return [];
				const row = faceMap.get(clusterName);
				if (row) {
					visited.add(clusterName);
					return [row];
				}
				return [];
			}

			const [left, right] = step.pair;
			return [...collectRows(left), ...collectRows(right)];
		};

		const orderedRows: RowType[] = [];
		clusteringSteps.forEach(step => {
			step.pair.forEach(item => {
				if (!visited.has(item)) {
					orderedRows.push(...collectRows(item));
				}
			});
		});

		const orderedFaceNames = orderedRows.map(row => row.faceName);
		orderedRows.forEach(row => {
			row.columns.sort(
				(a, b) =>
					orderedFaceNames.indexOf(a.faceName) -
					orderedFaceNames.indexOf(b.faceName)
			);
		});

		setFilteredHeatmapRows(orderedRows);
	};

	const handleShowDendrogram = async () => {
		const actualDistances = prepareDataForDendrogram();
		if (!actualDistances) return;
		const clusterRoot =
			await BatchProcessingTaskService.prepareDataForDendrogram(
				actualDistances
			);
		const clusteringSteps = getClusterSteps(clusterRoot);
		reorderRows(clusteringSteps);
		setClusteringSteps(clusteringSteps);
	};

	return {
		calculateRowPositions,
		handleShowDendrogram
	};
};

export default useDendrogram;

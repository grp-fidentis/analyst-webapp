export const ROUTES = {
	projects: '/projects',
	login: '/login',
	register: '/register',
	tasks: '/tasks',
	users: '/users',
	profile: '/profile',
	newUser: '/users/new',
	userDetail: '/users/detail',
	resetPassword: '/reset-password',
	oAuth2Redirect: '/oauth2/redirect',
	chooseLogin: '/choose-login',
	disabledUser: '/disabled-user',
	userProjects: '/user-projects'
};

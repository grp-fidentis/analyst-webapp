import { AxiosResponseHeaders } from 'axios';

import { User } from '../types/UserTypes';

const FIDENTIS_STORAGE_ACCESS_TOKEN_KEY = 'fidentis-currentUser-access-token';
const FIDENTIS_STORAGE_REFRESH_TOKEN_KEY = 'fidentis-currentUser-refresh-token';
const FIDENTIS_STORAGE_CURRENT_USER_KEY = 'fidentis-currentUser';

export const TokensUtils = {
	getAccessToken: () => localStorage.getItem(FIDENTIS_STORAGE_ACCESS_TOKEN_KEY),

	getRefreshToken: () =>
		localStorage.getItem(FIDENTIS_STORAGE_REFRESH_TOKEN_KEY),

	setNewTokens: (axiosHeaders: AxiosResponseHeaders) => {
		const accessToken = axiosHeaders.get('Access-Token') as string;
		const refreshToken = axiosHeaders.get('Refresh-Token') as string;

		localStorage.setItem(FIDENTIS_STORAGE_ACCESS_TOKEN_KEY, accessToken);
		localStorage.setItem(FIDENTIS_STORAGE_REFRESH_TOKEN_KEY, refreshToken);
	},

	setNewTokensOAuth2: (accessToken: string, refreshToken: string) => {
		localStorage.setItem(FIDENTIS_STORAGE_ACCESS_TOKEN_KEY, accessToken);
		localStorage.setItem(FIDENTIS_STORAGE_REFRESH_TOKEN_KEY, refreshToken);
	},

	removeTokens: () => {
		localStorage.removeItem(FIDENTIS_STORAGE_ACCESS_TOKEN_KEY);
		localStorage.removeItem(FIDENTIS_STORAGE_REFRESH_TOKEN_KEY);
	},

	getCurrentUser: () =>
		JSON.parse(
			localStorage.getItem(FIDENTIS_STORAGE_CURRENT_USER_KEY) ?? 'null'
		),

	setCurrentUser: (user: User) => {
		localStorage.setItem(
			FIDENTIS_STORAGE_CURRENT_USER_KEY,
			JSON.stringify(user)
		);
	},

	removeCurrentUser: () => {
		localStorage.removeItem(FIDENTIS_STORAGE_CURRENT_USER_KEY);
	}
};

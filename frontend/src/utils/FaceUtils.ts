import { HumanFace } from '../types/HumanFaceTypes';

export const FaceUtils = {
	getPrimary: (faces: HumanFace[]): HumanFace =>
		faces.find(f => f.isPrimaryFace) ?? {},
	getSecondary: (faces: HumanFace[]): HumanFace =>
		faces.find(f => !f.isPrimaryFace) ?? {}
};

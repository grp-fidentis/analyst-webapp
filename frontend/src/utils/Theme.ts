import { createTheme } from '@mui/material';

export const theme = createTheme({
	typography: {
		fontFamily: ['Montserrat', 'sans-serif'].join(',')
	},
	breakpoints: {
		values: {
			xs: 0,
			sm: 600,
			md: 900,
			lg: 1200,
			xl: 1824
		}
	},
	palette: {
		primary: {
			dark: '#0D4E48',
			main: '#16665F',
			light: '#238A81',
			contrastText: '#fff'
		},
		secondary: {
			dark: '#4c1215',
			main: '#66161d',
			light: '#901F29',
			contrastText: '#fff'
		}
	},
	components: {
		MuiTab: {
			styleOverrides: {
				root: {
					'fontSize': '16px',
					'&.Mui-selected': {
						color: '#0D4E48',
						fontWeight: '600'
					},
					'textTransform': 'uppercase'
				}
			}
		},
		MuiAccordionSummary: {
			styleOverrides: {
				root: {
					backgroundColor: 'rgba(38, 156, 146, 0.13)'
				},
				content: {
					justifyContent: 'center',
				}
			}
		},
		MuiTypography: {
			defaultProps: {
				variantMapping: {
					body2: 'p'
				}
			},
			styleOverrides: {
				body2: {
					fontSize: '16px',
					lineHeight: '1.5'
				}
			}
		}
	}
});

export const DownloadUtils = {
	downloadFile: (blob: Blob, fileName: string) => {
		const blobUrl = URL.createObjectURL(blob);
		const link = document.createElement('a');
		link.download = fileName;
		link.href = blobUrl;
		link.click();
		URL.revokeObjectURL(blobUrl);
	}
};

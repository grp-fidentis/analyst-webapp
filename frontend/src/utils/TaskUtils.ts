import { Task, TaskType } from '../types/TaskTypes';

export const TaskUtils = {
	isBatch: (task?: Task) => task?.taskType === TaskType.BatchProcessing,
	isPair: (task?: Task) => task?.taskType === TaskType.PairComparison,
	isSingle: (task?: Task) => task?.taskType === TaskType.SingleFaceAnalysis
};

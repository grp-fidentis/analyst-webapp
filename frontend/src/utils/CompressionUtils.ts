// @ts-ignore
import lz4 from 'lz4js';

export const textDecoder = new TextDecoder('utf-8');

export const CompressionUtils = {
	decompress: <T extends ArrayBuffer>(data: T): T | null => {
		if (data.byteLength === 0) return null;

		const decompressed = lz4.decompress(new Uint8Array(data));
		const decompressedInString: string = textDecoder.decode(decompressed);
		return JSON.parse(decompressedInString);
	}
};

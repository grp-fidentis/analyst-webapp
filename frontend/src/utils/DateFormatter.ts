type Style = 'full' | 'long' | 'medium' | 'short' | undefined;

export const DateFormatter = {
	format: (
		date?: Date | string,
		timeStyle = undefined as Style,
		dateStyle = 'medium' as Style,
		locale = 'cs'
	) => {
		const dateFormatter = new Intl.DateTimeFormat(locale, {
			dateStyle,
			timeStyle
		});

		return date ? dateFormatter.format(new Date(date)) : '-';
	}
};

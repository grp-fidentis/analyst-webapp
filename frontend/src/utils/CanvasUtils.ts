import { Task } from '../types/TaskTypes';

import { TaskUtils } from './TaskUtils';

export const CanvasUtils = {
	getColorForLine: (task: Task, isPrimaryFace: boolean, order = 1) => {
		const isPrimary = TaskUtils.isSingle(task) || isPrimaryFace;
		switch (order) {
			case 1:
				return isPrimary ? '#00FF00' : '#f08080';
			case 2:
				return isPrimary ? '#ADFF2F' : '#ff0000';
			case 3:
				return isPrimary ? '#006400' : '#8b0000';
			default:
				return 'black';
		}
	},
	getColorForNormal: (task: Task, isPrimaryFace: boolean, order = 1) => {
		const isPrimary = TaskUtils.isSingle(task) || isPrimaryFace;
		switch (order) {
			case 1:
				return isPrimary ? '#0000cd' : '#8b4513';
			case 2:
				return isPrimary ? '#1e90ff' : '#ff7518';
			case 3:
				return isPrimary ? '#00ffff' : '#ffff00';
			default:
				return 'black';
		}
	}
};

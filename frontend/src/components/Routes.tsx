import { Outlet, Route, Routes } from 'react-router-dom';

import { ROUTES } from '../utils/RoutesUrls';
import Login from '../pages/LoginPage';
import TaskPage from '../pages/TaskPage';
import UsersPage from '../pages/UsersPage';
import ProjectsPage from '../pages/ProjectsPage';
import ProtectedPage from '../pages/ProtectedPage';
import UserDetailPage from '../pages/UserDetailPage';
import ResetPasswordPage from '../pages/ResetPasswordPage';
import { SceneContextProvider } from '../providers/SceneContextProvider';
import { UserContextProvider } from '../providers/UserContextProvider';
import { AnalyticalTaskContextProvider } from '../providers/AnalyticalTaskContextProvider';
import { BatchProcessingTaskContextProvider } from '../providers/BatchProcessingTaskContextProvider';
import ChooseLogin from '../pages/ChooseLoginPage';
import DisabledUserPage from '../pages/DisabledUserPage';
import UsersProjectsPage from '../pages/UsersProjectsPage';
import { ComputingTasksContextProvider } from '../providers/ComputingTasksContextProvider';

import AuthTemplate from './auth-view/AuthTemplate';
import DefaultLayout from './layout/DefaultLayout';
import OAuth2Redirect from './oauth2/OAuth2Redirect';

const AppRoutes = () => (
	<Routes>
		<Route path={ROUTES.oAuth2Redirect} element={<OAuth2Redirect />} />
		<Route element={<AuthTemplate />}>
			<Route path={ROUTES.chooseLogin} element={<ChooseLogin />} />
			<Route path={ROUTES.login} element={<Login />} />
			<Route path={ROUTES.resetPassword} element={<ResetPasswordPage />} />
		</Route>
		<Route
			path="/"
			element={
				<ComputingTasksContextProvider>
					<ProtectedPage>
						<DefaultLayout>
							<Outlet />
						</DefaultLayout>
					</ProtectedPage>
				</ComputingTasksContextProvider>
			}
		>
			<Route path={ROUTES.projects} element={<ProjectsPage />} />
			<Route path={ROUTES.userProjects} element={<UsersProjectsPage />} />
			<Route path={ROUTES.profile} element={<UserDetailPage />} />
			<Route path={ROUTES.disabledUser} element={<DisabledUserPage />} />
			<Route
				path={ROUTES.users}
				element={
					<UserContextProvider>
						<Outlet />
					</UserContextProvider>
				}
			>
				<Route path={ROUTES.users} element={<UsersPage />} />
				<Route path={ROUTES.newUser} element={<UserDetailPage />} />
				<Route path={ROUTES.userDetail} element={<UserDetailPage />} />
			</Route>

			<Route path={ROUTES.tasks}>
				<Route
					path=":id"
					element={
						<SceneContextProvider>
							<BatchProcessingTaskContextProvider>
								<AnalyticalTaskContextProvider>
									<TaskPage />
								</AnalyticalTaskContextProvider>
							</BatchProcessingTaskContextProvider>
						</SceneContextProvider>
					}
				/>
			</Route>
		</Route>
	</Routes>
);

export default AppRoutes;

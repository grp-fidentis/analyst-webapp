import { ChangeEvent, Dispatch, FC, SetStateAction } from 'react';
import { TextField, Tooltip, Typography } from '@mui/material';
import { Info } from '@mui/icons-material';

const passwordValidationErrorText = "Password doesn't match requirements";

export type PasswordsFormData = {
	oldPassword?: string;
	password: string;
	passwordAgain: string;
};

type Props = {
	formData: PasswordsFormData;
	setPasswordValidation: Dispatch<SetStateAction<string | undefined>>;
	setPasswordMatch: Dispatch<SetStateAction<string | undefined>>;
	setFormData: (event: ChangeEvent<HTMLInputElement>) => void;
	passwordValidation: string | undefined;
	passwordMatch: string | undefined;
};

const PasswordValidationHelperText = () => (
	<Typography
		className="flex-row flex-row-center"
		component="span"
		sx={{ fontSize: '0.75rem' }}
	>
		{passwordValidationErrorText}
		<Tooltip
			title={
				<ul>
					<li>at least 8 characters</li>
					<li>at least 1 numeric character</li>
					<li>at least 1 special character [!#$%&?&quot;]</li>
				</ul>
			}
		>
			<Info sx={{ fontSize: '14px', cursor: 'pointer', marginLeft: '4px' }} />
		</Tooltip>
	</Typography>
);

const PasswordsCompare: FC<Props> = ({
	formData,
	setPasswordMatch,
	setPasswordValidation,
	setFormData,
	passwordValidation,
	passwordMatch
}) => {
	const checkPasswordMatch = () => {
		const isPasswordsSame = formData.password === formData.passwordAgain;

		if (!isPasswordsSame) {
			setPasswordMatch("Passwords doesn't match");
		} else {
			setPasswordMatch('');
		}
	};

	const checkPasswordValidation = () => {
		if (!formData.password) {
			setPasswordValidation('');
		} else {
			const regex = /^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&?"]).*$/gm;
			// eslint-disable-next-line no-extra-boolean-cast
			!!regex.exec(formData.password)
				? setPasswordValidation('')
				: setPasswordValidation(passwordValidationErrorText);

			checkPasswordMatch();
		}
	};

	return (
		<>
			<TextField
				label="Password"
				name="password"
				value={formData.password}
				onChange={setFormData}
				margin="dense"
				onBlur={checkPasswordValidation}
				error={!!passwordValidation}
				helperText={!!passwordValidation && <PasswordValidationHelperText />}
				type="password"
				fullWidth
				required
			/>
			<TextField
				label="Password again"
				name="passwordAgain"
				value={formData.passwordAgain}
				onChange={setFormData}
				margin="dense"
				type="password"
				onBlur={checkPasswordMatch}
				error={!!passwordMatch}
				helperText={passwordMatch}
				fullWidth
				required
			/>
		</>
	);
};

export default PasswordsCompare;

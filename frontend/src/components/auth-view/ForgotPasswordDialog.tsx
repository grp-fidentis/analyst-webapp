import { FC } from 'react';
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	TextField,
	Typography
} from '@mui/material';
import { toast } from 'react-toastify';
import { AxiosError } from 'axios';

import useField from '../../hooks/useField';
import { AuthService } from '../../services/AuthService';
import { ErrorResponse } from '../../types/CommonTypes';

type Props = {
	open: boolean;
	onClose: () => void;
};

const ForgotPasswordDialog: FC<Props> = ({ open, onClose }) => {
	const [email, emailProps] = useField('email', true);

	const handleOk = () => {
		AuthService.forgotPassword(email)
			.then(() => {
				toast.success('Email sent');
				onClose();
			})
			.catch(error => {
				const data = (error as AxiosError)?.response?.data as ErrorResponse;
				toast.error(data.message);
			});
	};

	return (
		<Dialog open={open} maxWidth="sm" fullWidth>
			<DialogTitle>Forgot password</DialogTitle>
			<DialogContent dividers>
				<Typography mb={2}>
					Enter your email below. In few minutes you will receive mail with link
					to page with unique token that lasts 15 minutes and form to create new
					password.
				</Typography>
				<TextField label="Email" {...emailProps} fullWidth />
			</DialogContent>
			<DialogActions sx={{ p: 3, py: 1 }}>
				<Button onClick={() => onClose()}>Cancel</Button>
				<Button variant="contained" onClick={handleOk} disabled={!email}>
					Send mail
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default ForgotPasswordDialog;

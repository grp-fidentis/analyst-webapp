import {
	ChangeEvent,
	FC,
	useContext,
	useEffect,
	useMemo,
	useState
} from 'react';
import { Button, Paper, Stack, TextField } from '@mui/material';
import { toast } from 'react-toastify';
import { AxiosError } from 'axios';

import { UserService } from '../../services/UserService';
import { AuthContext } from '../../providers/AuthContextProvider';
import { ErrorResponse } from '../../types/CommonTypes';

import PasswordsCompare, { PasswordsFormData } from './PasswordsCompare';

type Props = {
	open: boolean;
};

const ChangePasswordBox: FC<Props> = ({ open }) => {
	const { currentUser } = useContext(AuthContext);
	const [helperTextValidation, setHelperTextValidation] = useState<
		string | undefined
	>(undefined as never);
	const [helperTextMatch, setHelperTextMatch] = useState<string | undefined>(
		undefined as never
	);
	const [formData, setFormData] = useState<PasswordsFormData>({
		oldPassword: '',
		password: '',
		passwordAgain: ''
	});

	const handleFormDataChange = (e: ChangeEvent<HTMLInputElement>) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};

	useEffect(() => {
		if (!open) {
			resetForm();
		}
	}, [open]);

	const resetForm = () => {
		setFormData({ oldPassword: '', password: '', passwordAgain: '' });
		setHelperTextValidation('');
		setHelperTextMatch('');
	};

	const isButtonDisabled = useMemo(
		() =>
			!!helperTextValidation ||
			!!helperTextMatch ||
			Object.keys(formData).some(k => !formData[k as keyof PasswordsFormData]),
		[helperTextValidation, helperTextMatch, formData.oldPassword]
	);

	const handleChangePassword = () => {
		UserService.changePassword(formData)
			.then(() => {
				toast.success('Password changed successfully');
				resetForm();
			})
			.catch(error => {
				const data = (error as AxiosError)?.response?.data as ErrorResponse;
				toast.error(data.message);
			});
	};

	return (
		<Paper
			elevation={open ? 3 : 0}
			id="change-password__paper"
			sx={{
				transform: open ? 'translate(100%, -50%)' : 'translate(0, -50%)',
				display: open ? 'block' : 'none'
			}}
		>
			<Stack sx={{ minWidth: 282 }}>
				<TextField
					value={formData.oldPassword}
					name="oldPassword"
					label="Old password"
					margin="dense"
					type="password"
					required
					onChange={handleFormDataChange}
				/>
				<PasswordsCompare
					formData={formData}
					setPasswordValidation={setHelperTextValidation}
					setPasswordMatch={setHelperTextMatch}
					setFormData={handleFormDataChange}
					passwordValidation={helperTextValidation}
					passwordMatch={helperTextMatch}
				/>
				<Button
					variant="contained"
					sx={{ mt: 1 }}
					disabled={isButtonDisabled}
					onClick={handleChangePassword}
				>
					Confirm
				</Button>
			</Stack>
		</Paper>
	);
};

export default ChangePasswordBox;

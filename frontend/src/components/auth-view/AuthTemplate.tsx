import { Box, Container, Typography } from '@mui/material';
import { Outlet } from 'react-router-dom';

import { ReactComponent as FidentisLogo } from '../../assets/images/logo-fidentis.svg';

const AuthTemplate = () => (
	<Container
		sx={{
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			flexDirection: 'column',
			height: '100vh'
		}}
	>
		<Box component="div" sx={{ mt: -10 }}>
			<Box component="div" className="fidentis-logo__container">
				<FidentisLogo />
				<Typography
					variant="h4"
					color="primary.dark"
					sx={{ letterSpacing: '1px' }}
				>
					FIDENTIS ANALYST 2
				</Typography>
			</Box>

			<Outlet />
		</Box>
	</Container>
);

export default AuthTemplate;

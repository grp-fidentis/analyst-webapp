import { useNavigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import { ROUTES } from '../../utils/RoutesUrls';
import { AuthContext } from '../../providers/AuthContextProvider';
import { TokensUtils } from '../../utils/TokensUtils';

const OAuth2Redirect = () => {
	const navigate = useNavigate();
	const auth = useContext(AuthContext);

	useEffect(() => {
		const getCookie = (name: string): string | null => {
			const cookies = document.cookie.split(';');
			for (const cookie of cookies) {
				const [cookieName, cookieValue] = cookie.trim().split('=');
				if (cookieName === name) {
					return cookieValue;
				}
			}
			return null;
		};

		// obtains cookie values
		const userValue = getCookie('User');
		const accessTokenValue = getCookie('Access-Token');
		const refreshTokenValue = getCookie('Refresh-Token');

		// setting tokens
		if (accessTokenValue !== null && refreshTokenValue !== null) {
			TokensUtils.setNewTokensOAuth2(accessTokenValue, refreshTokenValue);
		}

		// setting current user
		if (userValue !== null) {
			const buffer = Uint8Array.from(atob(userValue), char =>
				char.charCodeAt(0)
			);
			const decoder = new TextDecoder('utf-8');
			const jsonString = decoder.decode(buffer);
			const userObject = JSON.parse(jsonString);
			TokensUtils.setCurrentUser(userObject);
			auth.getCurrentUser();
		}
		navigate(ROUTES.projects);
	}, []);

	return <></>;
};

export default OAuth2Redirect;

import {
	AccordionDetails,
	AccordionSummary,
	Box,
	IconButton,
	Stack,
	Typography
} from '@mui/material';
import { FC, useContext } from 'react';
import { useLocation } from 'react-router-dom';
import Accordion from '@mui/material/Accordion';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Delete } from '@mui/icons-material';
import { useConfirm } from 'material-ui-confirm';
import { AuthContext } from '../../../providers/AuthContextProvider';
import { ProjectsContext } from '../../../providers/ProjectContextProvider';
import { ProjectService } from '../../../services/ProjectService';
import { Project } from '../../../types/ProjectTypes';
import AccordionTaskItem from './AccordionTaskItem';

type Props = {
	project: Project;
	allProjects: Project[];
	setAllProjects: (allProjects: Project[]) => void;
};

const UsersProjectsAccordion: FC<Props> = ({
	project,
	allProjects,
	setAllProjects
}) => {
	const location = useLocation();
	const confirm = useConfirm();
	const { deleteProject } = useContext(ProjectsContext);
	const { currentUser } = useContext(AuthContext);
	const state = location.state;

	const filterAllProjects = (projectId: number) => {
		const updatedAllProjects = allProjects.filter(
			project => project.id != projectId
		);
		setAllProjects(updatedAllProjects);
	};

	const handleDelete = (projectId: number) => {
		const project = allProjects.find(project => project.id == projectId);
		if (project == undefined) {
			return;
		}
		confirm({
			description:
				'This action will delete project, all its tasks and other entities.',
			confirmationButtonProps: { variant: 'contained' }
		})
			.then(() => {
				if (currentUser?.id == state?.user.id) {
					deleteProject(projectId);
				} else {
					ProjectService.deleteProject(projectId);
				}
				filterAllProjects(projectId);
			})
			.catch(() => null);
	};

	return (
		<Accordion
			sx={{
				margin: 1,
				borderRadius: 2
			}}
			className="huh"
			key={project.name}
		>
			<AccordionSummary
				expandIcon={<ExpandMoreIcon />}
				sx={{
					'backgroundColor': 'rgba(255, 255, 255, 0.13)',
					'& .MuiAccordionSummary-content': {
						justifyContent: 'space-between'
					}
				}}
			>
				{project.name}

				<Box
					component="div"
					sx={{ display: 'flex', alignItems: 'center', color: 'gray' }}
				>
					<Typography>{project.tasks.length} tasks</Typography>
					<IconButton
						size="small"
						onClick={event => {
							event.stopPropagation();
							handleDelete(project.id);
						}}
					>
						<Delete />
					</IconButton>
				</Box>
			</AccordionSummary>
			<AccordionDetails>
				<Stack direction="column" gap={2}>
					{project.tasks.length == 0 ? (
						<Typography>No tasks.</Typography>
					) : (
						<>
							<Typography>Tasks:</Typography>
							{project.tasks.map(task => (
								<AccordionTaskItem
									taskName={task.name}
									taskId={task.id}
									projectId={project.id}
									allProjects={allProjects}
									setAllProjects={setAllProjects}
									key={task.id}
								></AccordionTaskItem>
							))}
						</>
					)}
				</Stack>
			</AccordionDetails>
		</Accordion>
	);
};

export default UsersProjectsAccordion;

import { Button, Container, Typography } from '@mui/material';
import { FC } from 'react';
import { useConfirm } from 'material-ui-confirm';
import { TaskService } from '../../../services/TaskService';
import { Project } from '../../../types/ProjectTypes';

type Props = {
	taskName: string;
	taskId: number;
	projectId: number;
	allProjects: Project[];
	setAllProjects: (allProjects: Project[]) => void;
};

const AccordionTaskItem: FC<Props> = ({
	taskName,
	taskId,
	projectId,
	allProjects,
	setAllProjects
}) => {
	const confirm = useConfirm();

	const handleDeleteTask = (projectId: number, taskId: number) => {
		confirm({
			description: 'This action will close the task.',
			confirmationButtonProps: { variant: 'contained' }
		})
			.then(() => {
				TaskService.deleteTask(taskId);
				const workingProject = allProjects.find(
					project => project.id == projectId
				);
				if (workingProject != undefined) {
					const projectTasks = workingProject?.tasks.filter(
						task => task.id !== taskId
					);
					workingProject.tasks = projectTasks;
					const updatedAllProjects = allProjects.map(project => {
						if (project.id == projectId) {
							return workingProject;
						}
						return project;
					});
					setAllProjects(updatedAllProjects);
				}
			})
			.catch(() => null);
	};

	return (
		<Container
			maxWidth="xl"
			sx={{
				display: 'flex',
				justifyContent: 'space-between',
				borderBottom: 1
			}}
		>
			<Typography>{taskName}</Typography>
			<Button onClick={() => handleDeleteTask(projectId, taskId)}>Close</Button>
		</Container>
	);
};

export default AccordionTaskItem;

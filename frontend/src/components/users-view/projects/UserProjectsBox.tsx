import { Box, Container, Paper, Stack, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Project } from '../../../types/ProjectTypes';
import { UserService } from '../../../services/UserService';
import UsersProjectsAccordion from './UserProjectsAccordion';

const UsersProjectsBox = () => {
	const [allProjects, setAllProjects] = useState<Project[]>([]);
	const location = useLocation();
	const state = location.state;

	useEffect(() => {
		loadAllProjects();
	}, []);

	const loadAllProjects = async () => {
		const projects = await UserService.getProjectsByUser(state?.user.id);
		setAllProjects(projects);
	};

	return (
		<Container
			sx={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center'
			}}
		>
			<Paper
				elevation={4}
				sx={{
					display: 'flex',
					flexDirection: 'column',
					overflow: 'hidden',
					height: '29.3rem',
					width: '35em'
				}}
			>
				<Stack direction="row" id="user-projects__stack">
					<Typography variant="h5">
						{state?.user.username + "'s Projects"}
					</Typography>
				</Stack>
				{allProjects.length == 0 ? (
					<Box
						component="div"
						sx={{
							display: 'flex',
							justifyContent: 'center',
							alignContent: 'center',
							marginTop: '10rem'
						}}
					>
						<Typography>No projects.</Typography>
					</Box>
				) : (
					<Box component="div" sx={{ overflowY: 'auto', maxHeight: '25rem' }}>
						{allProjects.map(project => (
							<UsersProjectsAccordion
								project={project}
								allProjects={allProjects}
								setAllProjects={setAllProjects}
								key={project.id}
							></UsersProjectsAccordion>
						))}
					</Box>
				)}
			</Paper>
		</Container>
	);
};

export default UsersProjectsBox;

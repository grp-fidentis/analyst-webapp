import { useContext } from 'react';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { Box } from '@mui/material';
import { CheckCircleOutline, HighlightOff } from '@mui/icons-material';

import { UsersContext } from '../../providers/UserContextProvider';
import { Role } from '../../types/UserTypes';
import { DateFormatter } from '../../utils/DateFormatter';

import TableActionButtons from './TableActionButtons';

const UsersTable = () => {
	const { users, loading } = useContext(UsersContext);
	const { enableUser } = useContext(UsersContext);

	const handleSubmit = async (
		userId: number,
		enabled: boolean
	): Promise<void> => {
		enableUser(userId, enabled);
	};

	const columns: GridColDef[] = [
		{
			field: 'name',
			headerName: 'Name',
			editable: false,
			width: 250,
			valueGetter: params =>
				params.row.firstName && params.row.lastName
					? `${params.row.firstName} ${params.row.lastName}`
					: '-'
		},
		{
			field: 'username',
			headerName: 'Username',
			editable: false,
			flex: 0.85
		},
		{
			field: 'loginType',
			headerName: 'Login Type',
			editable: false,
			flex: 0.75,
			valueGetter: params => (params.row.loginType ? params.row.loginType : '-')
		},
		{
			field: 'dateCreated',
			headerName: 'Date created',
			disableColumnMenu: true,
			editable: false,
			flex: 0.75,
			valueGetter: params =>
				DateFormatter.format(params.row.dateCreated, 'medium')
		},
		{
			field: 'dateEnabled',
			headerName: 'Date enabled',
			disableColumnMenu: true,
			editable: false,
			flex: 0.75,
			valueGetter: params =>
				DateFormatter.format(params.row.dateEnabled, 'medium')
		},
		{
			field: 'roles',
			headerName: 'Roles',
			editable: false,
			disableColumnMenu: true,
			sortable: false,
			flex: 0.75,
			valueGetter: params =>
				params.row.roles
					.map((role: Role) => role.name.replace(/^ROLE_/, ''))
					.join(', ')
		},
		{
			field: 'enabled',
			headerName: 'Enabled',
			editable: false,
			headerAlign: 'center',
			align: 'center',
			disableColumnMenu: true,
			width: 100,
			cellClassName: 'cursor-pointer',
			renderCell: params => (
				<div onClick={() => handleSubmit(params.row.id, !params.row.enabled)}>
					{params.row.enabled ? (
						<CheckCircleOutline color="primary" />
					) : (
						<HighlightOff color="error" />
					)}
				</div>
			)
		},
		{
			field: 'locked',
			headerName: 'Locked',
			headerAlign: 'center',
			align: 'center',
			disableColumnMenu: true,
			editable: false,
			width: 100,
			renderCell: params =>
				params.row.locked ? (
					<CheckCircleOutline color="primary" />
				) : (
					<HighlightOff color="error" />
				)
		},
		{
			field: 'actions',
			headerName: 'Actions',
			headerAlign: 'center',
			align: 'center',
			headerClassName: 'data-grid__header',
			disableColumnMenu: true,
			sortable: false,
			editable: false,
			flex: 0.55,
			renderCell: params => <TableActionButtons row={params.row} />
		}
	];

	return (
		<Box component="div" sx={{ height: 600 }}>
			<DataGrid
				columns={columns}
				rows={users}
				loading={loading}
				disableSelectionOnClick
			/>
		</Box>
	);
};

export default UsersTable;

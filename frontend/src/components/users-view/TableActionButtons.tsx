import { FC, useContext, useMemo } from 'react';
import { useConfirm } from 'material-ui-confirm';
import { IconButton, Stack } from '@mui/material';
import { Delete, Edit } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';

import { UsersContext } from '../../providers/UserContextProvider';
import { User } from '../../types/UserTypes';
import { ROUTES } from '../../utils/RoutesUrls';
import { NonNullableField } from '../../types/CommonTypes';
import { AuthContext } from '../../providers/AuthContextProvider';
import { Roles } from '../../utils/Enums';
import { toast } from 'react-toastify';
import { UserService } from '../../services/UserService';
import FolderIcon from '@mui/icons-material/Folder';

type Props = {
	row: NonNullableField<User, 'id'>;
};

const TableActionButtons: FC<Props> = ({ row }) => {
	const confirm = useConfirm();
	const navigate = useNavigate();
	const { hasRole } = useContext(AuthContext);
	const { deleteUser } = useContext(UsersContext);

	const showActionButtons = useMemo(
		() =>
			!(
				row.roles.some(role => role.name === Roles.ROLE_SUPER_ADMIN) &&
				!hasRole(Roles.ROLE_SUPER_ADMIN)
			),
		[]
	);

	const hasUserProjects = async (id: number) => {
		const projects = await UserService.getProjectsByUser(id);
		if (projects.length != 0) {
			toast.error('User can not be deleted. User still have some projects.');
			return true;
		}
		return false;
	};

	const handleEdit = () => {
		navigate(ROUTES.userDetail, { state: { user: row } });
	};

	const handleProjects = () => {
		navigate(ROUTES.userProjects, { state: { user: row } });
	};

	const handleDelete = async () => {
		if (await hasUserProjects(row.id)) {
			return;
		}
		confirm({
			description:
				'This action will delete user, all his projects, tasks and other entities',
			confirmationButtonProps: { variant: 'contained' }
		})
			.then(() => {
				deleteUser(row.id);
			})
			.catch(() => null);
	};

	return showActionButtons ? (
		<Stack direction="row">
			<IconButton onClick={handleProjects}>
				<FolderIcon />
			</IconButton>
			<IconButton onClick={handleEdit}>
				<Edit />
			</IconButton>
			<IconButton onClick={handleDelete}>
				<Delete />
			</IconButton>
		</Stack>
	) : null;
};

export default TableActionButtons;

import React from 'react';
import { Menu, MenuItem } from '@mui/material';

import { ContextMenuState } from '../../types/BatchVisualizationTypes';

type ContextMenuProps = {
	contextMenu: ContextMenuState | null;
	onClose: () => void;
	hideRowAndColumn: (name: string, isRow: boolean) => void;
	handleOpenSingleTask: (name: string) => void;
	handleOpenPairTask: (rowName: string, columnName: string) => void;
	handleShowFace: (name: string, index: number, type: 'row' | 'column') => void;
	handleShowFaces: (
		rowName: string,
		rowIndex: number,
		columnName: string,
		columnIndex: number,
		pairDistance: number
	) => void;
	hideBoth: (rowName: string, columnName: string) => void;
};

const ContextMenu: React.FC<ContextMenuProps> = ({
	contextMenu,
	onClose,
	hideRowAndColumn,
	handleOpenSingleTask,
	handleOpenPairTask,
	handleShowFace,
	handleShowFaces,
	hideBoth
}) => {
	if (!contextMenu) return null;

	let menuItems: React.ReactNode[] = [];

	if (contextMenu.type === 'row') {
		menuItems = [
			<MenuItem
				key="hide-row"
				onClick={() => {
					if (contextMenu.rowName) hideRowAndColumn(contextMenu.rowName, true);
					onClose();
				}}
			>
				Hide Face
			</MenuItem>,
			<MenuItem
				key="open-task"
				onClick={() => {
					if (contextMenu.rowName) handleOpenSingleTask(contextMenu.rowName);
					onClose();
				}}
			>
				Open Task
			</MenuItem>,
			<MenuItem
				key="show-face"
				onClick={() => {
					if (contextMenu.rowName && contextMenu.rowIndex !== null)
						handleShowFace(contextMenu.rowName, contextMenu.rowIndex, 'row');
					onClose();
				}}
			>
				Show face in the scene
			</MenuItem>
		];
	} else if (contextMenu.type === 'column') {
		menuItems = [
			<MenuItem
				key="hide-column"
				onClick={() => {
					if (contextMenu.columnName)
						hideRowAndColumn(contextMenu.columnName, false);
					onClose();
				}}
			>
				Hide Face
			</MenuItem>,
			<MenuItem
				key="open-task-column"
				onClick={() => {
					if (contextMenu.columnName)
						handleOpenSingleTask(contextMenu.columnName);
					onClose();
				}}
			>
				Open Task
			</MenuItem>,
			<MenuItem
				key="show-face-column"
				onClick={() => {
					if (contextMenu.columnName && contextMenu.columnIndex !== null)
						handleShowFace(
							contextMenu.columnName,
							contextMenu.columnIndex,
							'column'
						);
					onClose();
				}}
			>
				Show face in the scene
			</MenuItem>
		];
	} else if (contextMenu.type === 'cell') {
		menuItems = [
			<MenuItem
				key="hide-cell"
				onClick={() => {
					if (contextMenu.rowName && contextMenu.columnName) {
						hideBoth(contextMenu.rowName, contextMenu.columnName);
					}
					onClose();
				}}
			>
				{contextMenu.rowName === contextMenu.columnName
					? 'Hide Face'
					: 'Hide Both Faces'}
			</MenuItem>,
			<MenuItem
				key="open-task-cell"
				onClick={() => {
					if (contextMenu.rowName && contextMenu.columnName) {
						if (contextMenu.rowName === contextMenu.columnName) {
							handleOpenSingleTask(contextMenu.rowName);
						} else {
							handleOpenPairTask(contextMenu.rowName, contextMenu.columnName);
						}
					}
					onClose();
				}}
			>
				{contextMenu.rowName === contextMenu.columnName
					? 'Open Task'
					: 'Open Pair Task'}
			</MenuItem>
		];

		if (contextMenu.rowName !== contextMenu.columnName) {
			menuItems.push(
				<MenuItem
					key="show-faces-cell"
					onClick={() => {
						if (
							contextMenu.rowName &&
							contextMenu.columnName &&
							contextMenu.rowIndex !== null &&
							contextMenu.columnIndex !== null &&
							contextMenu.pairDistance
						) {
							handleShowFaces(
								contextMenu.rowName,
								contextMenu.rowIndex,
								contextMenu.columnName,
								contextMenu.columnIndex,
								contextMenu.pairDistance
							);
						}
						onClose();
					}}
				>
					Show faces in the scene
				</MenuItem>
			);
		}
	}

	return (
		<Menu
			open
			onClose={onClose}
			anchorReference="anchorPosition"
			anchorPosition={
				contextMenu
					? { top: contextMenu.mouseY, left: contextMenu.mouseX }
					: undefined
			}
		>
			{menuItems}
		</Menu>
	);
};

export default ContextMenu;

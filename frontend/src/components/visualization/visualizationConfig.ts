// CustomHeatmap configuration
export const binWidth = 20;

export const startColor = 'white';
export const endColor = 'blue';

export const rowHighlightColor = 'rgba(0, 255, 0, 1)';
export const columnHighlightColor = 'rgba(0, 255, 0, 1)';

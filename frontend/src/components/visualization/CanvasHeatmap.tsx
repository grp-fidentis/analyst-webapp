import React, { useCallback, useEffect, useRef } from 'react';
import { ScaleLinear } from 'd3-scale';

import {
	ColumnState,
	DistanceState,
	HeatmapRowData,
	PairStep,
	RowState,
	Transform
} from '../../types/BatchVisualizationTypes';

import {
	binWidth,
	columnHighlightColor,
	endColor,
	rowHighlightColor
} from './visualizationConfig';

type CanvasHeatmapProps = {
	heatmapRows: HeatmapRowData[];
	scaleAxisX: ScaleLinear<number, number>;
	scaleAxisY: ScaleLinear<number, number>;
	colorScale: (value: number) => string;
	handleContextMenu: (
		event: React.MouseEvent<HTMLCanvasElement>,
		type: 'row' | 'column' | 'cell',
		rowName: string | null,
		rowIndex: number | null,
		columnName: string | null,
		columnIndex: number | null,
		pairDistance: number | null
	) => void;
	handleRowOrColumnClick: (
		event: React.MouseEvent<HTMLCanvasElement>,
		type: 'row' | 'column',
		index: number,
		faceName: string
	) => void;
	rowState: RowState;
	columnState: ColumnState;
	selectionMode: boolean;
	transform: Transform;
	clusteringSteps: PairStep[] | null;
	rowPositions: Record<string, number>;
	distances: DistanceState | null;
};

const CanvasHeatmap: React.FC<CanvasHeatmapProps> = ({
	heatmapRows,
	scaleAxisX,
	scaleAxisY,
	colorScale,
	handleContextMenu,
	handleRowOrColumnClick,
	rowState,
	columnState,
	selectionMode,
	transform,
	clusteringSteps,
	rowPositions,
	distances
}) => {
	const canvasRef = useRef<HTMLCanvasElement>(null);
	const isDragging = useRef(false);
	const lastPosition = useRef({ x: 0, y: 0 });

	const isLeafNode = (name: string): boolean => !name.includes('cluster');

	const getSmallerCluster = (leftName: string, rightName: string) => {
		const leftNumber = parseInt(leftName.split('#')[1]);
		const rightNumber = parseInt(rightName.split('#')[1]);
		return leftNumber < rightNumber ? leftName : rightName;
	};

	const drawRowLabels = (context: CanvasRenderingContext2D) => {
		const labelPaddingX = 100;
		const labelPaddingY = 100;
		heatmapRows.forEach((row, rowIndex) => {
			const y = scaleAxisY(rowIndex) + labelPaddingY + binWidth / 2;
			context.textAlign = 'right';
			context.textBaseline = 'middle';
			context.fillText(row.faceName, labelPaddingX - 10, y);
		});
	};

	const drawColumnLabels = (context: CanvasRenderingContext2D) => {
		const labelPaddingX = 100;
		const labelPaddingY = 100;
		if (heatmapRows[0].columns.length > 0) {
			heatmapRows[0].columns.forEach((col, colIndex) => {
				const x = scaleAxisX(colIndex) + labelPaddingX + binWidth / 2;
				context.save();
				context.translate(x, labelPaddingY / 2);
				context.rotate(-Math.PI / 2);
				context.textAlign = 'center';
				context.textBaseline = 'middle';
				context.fillText(col.faceName, 0, 0);
				context.restore();
			});
		}
	};

	const drawHeatmapCells = (context: CanvasRenderingContext2D) => {
		const labelPaddingX = 100;
		const labelPaddingY = 100;
		heatmapRows.forEach((row, rowIndex) => {
			row.columns.forEach((col, colIndex) => {
				const x = scaleAxisX(colIndex) + labelPaddingX;
				const y = scaleAxisY(rowIndex) + labelPaddingY;
				context.fillStyle = colorScale(col.distance);
				context.fillRect(x, y, binWidth, binWidth);
				context.strokeRect(x, y, binWidth, binWidth);
			});
		});
	};

	const drawArrow = (
		context: CanvasRenderingContext2D,
		fromX: number,
		fromY: number,
		toX: number,
		toY: number
	) => {
		const headLength = 10;
		const angle = Math.atan2(toY - fromY, toX - fromX);

		context.strokeStyle = 'black';
		context.lineWidth = 2;

		context.beginPath();
		context.moveTo(fromX, fromY);
		context.lineTo(toX, toY);
		context.stroke();

		context.beginPath();
		context.moveTo(toX, toY);
		context.lineTo(
			toX - headLength * Math.cos(angle - Math.PI / 6),
			toY - headLength * Math.sin(angle - Math.PI / 6)
		);
		context.lineTo(
			toX - headLength * Math.cos(angle + Math.PI / 6),
			toY - headLength * Math.sin(angle + Math.PI / 6)
		);
		context.lineTo(toX, toY);
		context.closePath();
		context.fillStyle = 'black';
		context.fill();
	};

	const highlightTargetRowsColumns = (context: CanvasRenderingContext2D) => {
		const canvas = canvasRef.current;
		if (!canvas) return;
		const labelPaddingX = 100;
		const labelPaddingY = 100;
		if (rowState.targetIndex !== null && rowState.draggedIndex !== null) {
			isDragging.current = true;
			canvas.style.cursor = 's-resize';
			const targetY = scaleAxisY(rowState.targetIndex) + labelPaddingY;
			const dragOffset =
				rowState.draggedIndex < rowState.targetIndex ? 10 : -10;

			drawArrow(
				context,
				60,
				targetY + binWidth / 2 + dragOffset,
				labelPaddingX,
				targetY + binWidth / 2 + dragOffset
			);
		}
		if (columnState.targetIndex !== null && columnState.draggedIndex !== null) {
			isDragging.current = true;
			canvas.style.cursor = 'w-resize';
			const targetX = scaleAxisX(columnState.targetIndex) + labelPaddingX;
			const dragOffset =
				columnState.draggedIndex < columnState.targetIndex ? 10 : -10;

			drawArrow(
				context,
				targetX + binWidth / 2 + dragOffset,
				60,
				targetX + binWidth / 2 + dragOffset,
				labelPaddingY
			);
		}
		if (rowState.lastClickedIndex !== null) {
			const targetY = scaleAxisY(rowState.lastClickedIndex) + labelPaddingY;
			context.strokeStyle = rowHighlightColor;
			context.lineWidth = 4;
			context.strokeRect(
				labelPaddingX,
				targetY,
				heatmapRows[0].columns.length * binWidth,
				binWidth
			);
		}

		if (columnState.lastClickedIndex !== null) {
			const targetX = scaleAxisX(columnState.lastClickedIndex) + labelPaddingX;
			context.strokeStyle = columnHighlightColor;
			context.lineWidth = 4;
			context.strokeRect(
				targetX,
				labelPaddingY,
				binWidth,
				heatmapRows.length * binWidth
			);
		}
	};

	const drawDendrogram = useCallback(
		(context: CanvasRenderingContext2D) => {
			if (!clusteringSteps || clusteringSteps.length === 0) return;

			const clusterPositions: Record<string, { x: number; y: number }> = {};
			const labelPaddingX = 100;
			const labelPaddingY = 100;
			const width = heatmapRows[0].columns.length * binWidth;

			clusteringSteps.forEach((step, index) => {
				const {
					pair: [leftName, rightName],
					resultName
				} = step;

				// Calculate y-positions for left and right nodes
				const y1 =
					rowPositions[leftName] + labelPaddingY ||
					clusterPositions[leftName]?.y;
				const y2 =
					rowPositions[rightName] + labelPaddingY ||
					clusterPositions[rightName]?.y;
				const yMid = (y1 + y2) / 2;
				const xOffset = labelPaddingX + width + index * 8;

				// Store the position of the current cluster node
				clusterPositions[resultName] = { x: xOffset + 8, y: yMid };

				// Determine if nodes are leaves or clusters
				const isLeftLeaf = isLeafNode(leftName);
				const isRightLeaf = isLeafNode(rightName);

				// Determine the smaller cluster if both are clusters
				const smallerCluster =
					!isLeftLeaf && !isRightLeaf
						? getSmallerCluster(leftName, rightName)
						: null;

				// Calculate offset levels based on nodes
				let offsetLevel = 0;
				let biggerClusterLevel = 0;
				let smallerClusterLevel = 0;

				if (smallerCluster) {
					const leftLevel = parseInt(leftName.split('#')[1]);
					const rightLevel = parseInt(rightName.split('#')[1]);
					offsetLevel = Math.abs(leftLevel - rightLevel);

					if (smallerCluster === leftName) {
						biggerClusterLevel = rightLevel;
						smallerClusterLevel = leftLevel;
					} else {
						biggerClusterLevel = leftLevel;
						smallerClusterLevel = rightLevel;
					}
				} else if (isLeftLeaf && !isRightLeaf) {
					offsetLevel = Math.abs(index - parseInt(rightName.split('#')[1]));
				} else if (!isLeftLeaf && isRightLeaf) {
					offsetLevel = Math.abs(index - parseInt(leftName.split('#')[1]));
				}

				// Calculate x-coordinates for left and right nodes
				const leftX1 = isLeftLeaf
					? width + labelPaddingX
					: isRightLeaf && !isLeftLeaf
					? xOffset - offsetLevel * 8
					: smallerCluster === leftName
					? biggerClusterLevel < index
						? xOffset - (index - smallerClusterLevel) * 8
						: xOffset - offsetLevel * 8
					: xOffset;

				const rightX1 = isRightLeaf
					? width + labelPaddingX
					: isLeftLeaf && !isRightLeaf
					? xOffset - offsetLevel * 8
					: smallerCluster === rightName
					? xOffset - offsetLevel * 8
					: biggerClusterLevel < index
					? xOffset - (index - biggerClusterLevel) * 8
					: xOffset;

				// Drawing the dendrogram lines
				context.strokeStyle = 'black';
				context.lineWidth = 2;
				context.beginPath();

				// Draw horizontal line for the left node
				context.moveTo(leftX1, y1);
				context.lineTo(xOffset + 8, y1);

				// Draw horizontal line for the right node
				context.moveTo(rightX1, y2);
				context.lineTo(xOffset + 8, y2);

				// Draw vertical line connecting the two nodes
				context.moveTo(xOffset + 8, y1);
				context.lineTo(xOffset + 8, y2);

				context.stroke();
			});
		},
		[clusteringSteps, rowPositions]
	);

	const drawColorLegend = (
		context: CanvasRenderingContext2D,
		heatmapWidth: number,
		heatmapHeight: number
	) => {
		if (!distances) return;
		const legendWidth = heatmapWidth - 20;
		const legendHeight = 30;
		const startX = 20;
		const startY = heatmapHeight + 10;

		const gradient = context.createLinearGradient(
			startX,
			0,
			startX + legendWidth,
			0
		);
		for (let i = 0; i <= 100; i++) {
			const value = distances.min + ((distances.max - distances.min) * i) / 100;
			gradient.addColorStop(i / 100, colorScale(value));
		}
		context.fillStyle = gradient;
		context.fillRect(startX, startY, legendWidth, legendHeight);

		context.fillStyle = 'black';
		context.textAlign = 'center';
		context.font = '12px Arial';

		context.fillText(
			distances.min.toFixed(2),
			startX,
			startY + legendHeight + 15
		);
		context.fillText(
			distances.max.toFixed(2),
			startX + legendWidth,
			startY + legendHeight + 15
		);

		if (
			distances.pairDistance !== null &&
			rowState.lastClickedIndex !== null &&
			columnState.lastClickedIndex !== null
		) {
			const positionX =
				startX +
				((distances.pairDistance - distances.min) /
					(distances.max - distances.min)) *
					legendWidth;
			context.fillStyle = endColor;
			context.fillRect(positionX - 2, startY - 5, 4, legendHeight + 10);

			context.fillStyle = 'black';
			context.textAlign = 'left';
			context.fillText(
				`Selected: ${distances.pairDistance.toFixed(2)}`,
				positionX - 35,
				startY + legendHeight + 25
			);
		}
	};

	const drawHeatmap = useCallback(() => {
		const canvas = canvasRef.current;
		if (!canvas) return;

		const context = canvas.getContext('2d');
		if (!context) return;

		context.clearRect(0, 0, 1000, 1000);
		context.save();
		context.translate(transform.translateX, transform.translateY);
		context.scale(transform.scale, transform.scale);

		context.font = '12px Arial';
		context.fillStyle = '#000';
		const heatmapWidth = heatmapRows[0].columns.length * binWidth + 100;
		const heatmapHeight = heatmapRows.length * binWidth + 100;
		drawColorLegend(context, heatmapWidth, heatmapHeight);
		drawRowLabels(context);
		drawColumnLabels(context);
		drawHeatmapCells(context);
		highlightTargetRowsColumns(context);
		if (clusteringSteps) {
			drawDendrogram(context);
		}
		context.restore();
	}, [
		heatmapRows,
		scaleAxisX,
		scaleAxisY,
		colorScale,
		rowState.targetIndex,
		columnState.targetIndex,
		rowState.lastClickedIndex,
		columnState.lastClickedIndex,
		clusteringSteps,
		distances?.pairDistance
	]);

	const handleWheel = useCallback(
		(event: WheelEvent) => {
			if (selectionMode) return;
			const canvas = canvasRef.current;
			if (!canvas) return;
			event.preventDefault();

			const zoomFactor = event.deltaY < 0 ? 1.1 : 0.9;
			if (zoomFactor === 1.1) {
				canvas.style.cursor = 'zoom-in';
			} else {
				canvas.style.cursor = 'zoom-out';
			}
			const rect = canvasRef.current?.getBoundingClientRect();
			if (!rect) return;

			const mouseX =
				(event.clientX - rect.left - transform.translateX) / transform.scale;
			const mouseY =
				(event.clientY - rect.top - transform.translateY) / transform.scale;

			transform.scale *= zoomFactor;

			transform.translateX -= mouseX * (zoomFactor - 1) * transform.scale;
			transform.translateY -= mouseY * (zoomFactor - 1) * transform.scale;

			drawHeatmap();
		},
		[selectionMode, drawHeatmap]
	);

	const handleMouseDown = useCallback(
		(event: React.MouseEvent<HTMLCanvasElement>) => {
			const canvas = canvasRef.current;
			if (!canvas) return;
			if (selectionMode) return;
			isDragging.current = true;
			lastPosition.current = { x: event.clientX, y: event.clientY };
			canvas.style.cursor = 'move';
		},
		[selectionMode]
	);

	const handleMouseMove = useCallback(
		(event: React.MouseEvent<HTMLCanvasElement>) => {
			if (!selectionMode && isDragging.current) {
				const deltaX = event.clientX - lastPosition.current.x;
				const deltaY = event.clientY - lastPosition.current.y;

				transform.translateX += deltaX;
				transform.translateY += deltaY;

				lastPosition.current = { x: event.clientX, y: event.clientY };
				drawHeatmap();
			} else if (!isDragging.current) {
				const canvas = canvasRef.current;
				if (!canvas) return;

				const rect = canvas.getBoundingClientRect();
				const mouseX =
					(event.clientX - rect.left - transform.translateX) / transform.scale;
				const mouseY =
					(event.clientY - rect.top - transform.translateY) / transform.scale;

				const labelPaddingX = 100;
				const labelPaddingY = 100;
				let cursorStyle = 'default';

				let rowClicked = false;
				heatmapRows.forEach((_, rowIndex) => {
					const y = scaleAxisY(rowIndex) + labelPaddingY;
					if (mouseX < labelPaddingX && mouseY > y && mouseY < y + binWidth) {
						cursorStyle = 'pointer';
						rowClicked = true;
					}
				});

				let columnClick = false;
				if (!rowClicked && heatmapRows[0]?.columns.length > 0) {
					heatmapRows[0].columns.forEach((_, colIndex) => {
						const x = scaleAxisX(colIndex) + labelPaddingX;
						if (mouseY < labelPaddingY && mouseX > x && mouseX < x + binWidth) {
							cursorStyle = 'pointer';
							columnClick = true;
						}
					});
				}

				if (!rowClicked && !columnClick) {
					heatmapRows.forEach((row, rowIndex) => {
						row.columns.forEach((_, colIndex) => {
							const x = scaleAxisX(colIndex) + labelPaddingX;
							const y = scaleAxisY(rowIndex) + labelPaddingY;
							if (
								mouseX > x &&
								mouseX < x + binWidth &&
								mouseY > y &&
								mouseY < y + binWidth
							) {
								cursorStyle = 'pointer';
							}
						});
					});
				}

				canvas.style.cursor = cursorStyle;
			}
		},
		[drawHeatmap, selectionMode]
	);

	const handleMouseUp = useCallback(() => {
		const canvas = canvasRef.current;
		if (!canvas) return;
		isDragging.current = false;
		canvas.style.cursor = 'default';
	}, []);

	const getClickPosition = (
		event: React.MouseEvent<HTMLCanvasElement>,
		rect: DOMRect
	) => {
		const clickX =
			(event.clientX - rect.left - transform.translateX) / transform.scale;
		const clickY =
			(event.clientY - rect.top - transform.translateY) / transform.scale;
		return { clickX, clickY };
	};

	const handleRowClick = (
		event: React.MouseEvent<HTMLCanvasElement>,
		clickX: number,
		clickY: number
	) => {
		const labelPaddingX = 100;
		const labelPaddingY = 100;
		let rowClicked = false;

		heatmapRows.forEach((row, rowIndex) => {
			const y = scaleAxisY(rowIndex) + labelPaddingY;
			if (clickX < labelPaddingX && clickY > y && clickY < y + binWidth) {
				handleRowOrColumnClick(event, 'row', rowIndex, row.faceName);
				rowClicked = true;
			}
		});

		return rowClicked;
	};

	const handleColumnClick = (
		event: React.MouseEvent<HTMLCanvasElement>,
		clickX: number,
		clickY: number
	) => {
		const labelPaddingX = 100;
		const labelPaddingY = 100;

		if (heatmapRows[0].columns.length > 0) {
			heatmapRows[0].columns.forEach((col, colIndex) => {
				const x = scaleAxisX(colIndex) + labelPaddingX;
				if (clickY < labelPaddingY && clickX > x && clickX < x + binWidth) {
					handleRowOrColumnClick(event, 'column', colIndex, col.faceName);
				}
			});
		}
	};

	const handleRectangleClick = (
		event: React.MouseEvent<HTMLCanvasElement>,
		clickX: number,
		clickY: number
	) => {
		const labelPaddingX = 100;
		const labelPaddingY = 100;

		heatmapRows.forEach((row, rowIndex) => {
			row.columns.forEach((col, colIndex) => {
				const x = scaleAxisX(colIndex) + labelPaddingX;
				const y = scaleAxisY(rowIndex) + labelPaddingY;

				if (
					clickX > x &&
					clickX < x + binWidth &&
					clickY > y &&
					clickY < y + binWidth
				) {
					handleContextMenu(
						event,
						'cell',
						row.faceName,
						rowIndex,
						col.faceName,
						colIndex,
						col.distance
					);
				}
			});
		});
	};

	const handleCanvasMouseDown = useCallback(
		(event: React.MouseEvent<HTMLCanvasElement>) => {
			if (!selectionMode) return;
			const canvas = canvasRef.current;
			if (!canvas) return;

			const rect = canvas.getBoundingClientRect();
			const { clickX, clickY } = getClickPosition(event, rect);

			const rowClicked = handleRowClick(event, clickX, clickY);

			if (!rowClicked) {
				handleColumnClick(event, clickX, clickY);
			}

			handleRectangleClick(event, clickX, clickY);
		},
		[heatmapRows, scaleAxisY, scaleAxisX, selectionMode]
	);

	useEffect(() => {
		const canvas = canvasRef.current;
		if (!canvas) return;

		canvas.addEventListener('wheel', handleWheel, { passive: false });

		return () => {
			canvas.removeEventListener('wheel', handleWheel);
		};
	}, [handleWheel]);

	useEffect(() => {
		drawHeatmap();
	}, [drawHeatmap]);

	return (
		<canvas
			width={
				canvasRef.current?.parentElement
					? canvasRef.current.parentElement.offsetWidth - 2
					: 800
			}
			height={
				canvasRef.current?.parentElement
					? canvasRef.current.parentElement.offsetHeight - 2
					: 600
			}
			ref={canvasRef}
			style={{ border: '1px solid black' }}
			onMouseDown={e => {
				handleCanvasMouseDown(e);
				handleMouseDown(e);
			}}
			onMouseMove={handleMouseMove}
			onMouseUp={handleMouseUp}
		/>
	);
};

export default CanvasHeatmap;

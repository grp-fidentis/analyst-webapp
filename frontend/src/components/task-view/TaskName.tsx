import { useContext, useState } from 'react';
import { IconButton, Stack, TextField, Typography } from '@mui/material';
import { Check, Close, Edit } from '@mui/icons-material';

import { TaskContext } from '../../providers/TaskContextProvider';
import useField from '../../hooks/useField';
import TooltipIconButton from '../common/TooltipIconButton';

const TaskName = () => {
	const { task, updateTaskName } = useContext(TaskContext);
	const [name, nameProps] = useField('name', false);
	const [isEditing, setIsEditing] = useState(false);

	const handleChangeEditing = () => {
		if (!isEditing) {
			nameProps.onChange({ target: { value: task?.name ?? '' } } as never);
		}

		setIsEditing(!isEditing);
	};

	const handleSubmitNameChange = () => {
		if (task) {
			updateTaskName(name);
			setIsEditing(false);
		}
	};

	if (isEditing) {
		return (
			<Stack direction="row" alignItems="center">
				<TextField {...nameProps} fullWidth size="small" variant="standard" />
				<IconButton
					sx={{ mt: -0.5 }}
					disabled={!name}
					disableRipple
					onClick={handleSubmitNameChange}
				>
					<Check sx={{ fontSize: '18px' }} />
				</IconButton>
				<IconButton
					sx={{ mt: -0.5, pl: 0 }}
					disableRipple
					onClick={() => setIsEditing(false)}
				>
					<Close sx={{ fontSize: '18px' }} />
				</IconButton>
			</Stack>
		);
	}

	return (
		<Stack direction="row" alignItems="center" sx={{ pl: 25 }}>
			{/* eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing  -- I want empty string to be false also */}
			<Typography>{task?.name || `Task: ${task?.id}`}</Typography>
			<TooltipIconButton
				title="Edit task name"
				disableRipple
				onClick={handleChangeEditing}
			>
				<Edit sx={{ fontSize: '18px' }} />
			</TooltipIconButton>
		</Stack>
	);
};

export default TaskName;

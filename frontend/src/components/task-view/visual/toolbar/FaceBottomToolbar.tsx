import { Box, Divider, Stack } from '@mui/material';
import { useContext, useMemo } from 'react';

import { ReactComponent as LightReflectionIcon } from '../../../../assets/images/light_reflections_icon.svg';
import { ReactComponent as BackgroundIcon } from '../../../../assets/images/background.svg';
import TooltipIconButton from '../../../common/TooltipIconButton';
import { TaskContext } from '../../../../providers/TaskContextProvider';
import { TaskType } from '../../../../types/TaskTypes';
import { ReactComponent as RenderingModeIcon } from '../../../../assets/images/rendering_mode_icon.svg';
import { ReactComponent as PointsRendering } from '../../../../assets/images/points_rendering.svg';
import { ReactComponent as SmoothRendering } from '../../../../assets/images/smooth_rendering.svg';
import { ReactComponent as TextureRendering } from '../../../../assets/images/texture_rendering.svg';
import { ReactComponent as TriangleRendering } from '../../../../assets/images/triangle_rendering.svg';
import { SceneContext } from '../../../../providers/SceneContextProvider';

import ButtonWithOptions from './components/ButtonWithOptions';
import DefaultToolbar from './toolbars/DefaultToolbar';
import PairFacesToolbar from './toolbars/PairFacesToolbar';

const FaceBottomToolbar = () => {
	const { task } = useContext(TaskContext);
	const {
		renderMode,
		enableLightReflections,
		setRenderMode,
		setBackgroundColor,
		setEnableLightReflections,
		backgroundColor,
		locked
	} = useContext(SceneContext);

	const getButtonsInfo = () =>
		useMemo(
			() => [
				{
					selected: renderMode === 'TEXTURE',
					title: 'Texture rendering',
					icon: <TextureRendering width={28} height={28} />,
					onClick: () => setRenderMode('TEXTURE')
				},
				{
					selected: renderMode === 'SMOOTH',
					title: 'Smooth rendering',
					icon: <SmoothRendering width={28} height={28} />,
					onClick: () => setRenderMode('SMOOTH')
				},
				{
					selected: renderMode === 'WIREFRAME',
					title: 'Triangle rendering',
					icon: <TriangleRendering width={28} height={28} />,
					onClick: () => setRenderMode('WIREFRAME')
				},
				{
					selected: renderMode === 'POINTS',
					title: 'Points rendering',
					icon: <PointsRendering width={28} height={28} />,
					onClick: () => setRenderMode('POINTS')
				}
			],
			[renderMode]
		);

	return (
		<Stack
			direction="row"
			justifyContent="space-between"
			alignItems="center"
			sx={{
				height: '65px',
				backgroundColor: '#222',
				overflowX: 'auto',
				maxWidth: '100%'
			}}
		>
			<Box component="div" className="flex-row flex-row-center">
				<ButtonWithOptions
					mainButtonInfo={{
						title: 'Rendering mode',
						icon: <RenderingModeIcon width={30} height={30} />,
						disabled: locked
					}}
					buttonsInfo={getButtonsInfo()}
					popupSetup={{
						anchorOrigin: {
							vertical: 'top',
							horizontal: 'right'
						},
						transformOrigin: {
							vertical: 49,
							horizontal: 77
						}
					}}
				/>
				<TooltipIconButton
					disabled={locked}
					disableRipple
					props={{ sx: { px: 0.7 } }}
					title="Set dark background"
					onClick={() =>
						setBackgroundColor(c => (c === '#323232' ? '#e6e6e6' : '#323232'))
					}
				>
					<BackgroundIcon
						fill={backgroundColor === '#323232' ? '#00ada2' : '#D9D9D9'}
						width={32}
						height={32}
					/>
				</TooltipIconButton>
				<TooltipIconButton
					disabled={locked}
					disableRipple
					props={{ sx: { px: 0.7 } }}
					title="Enable light reflections"
					onClick={() => setEnableLightReflections(value => !value)}
				>
					<LightReflectionIcon
						fill={enableLightReflections ? '#00ada2' : '#D9D9D9'}
						width={28}
						height={28}
					/>
				</TooltipIconButton>
				<Divider
					sx={{ backgroundColor: '#D9D9D9' }}
					orientation="vertical"
					flexItem
				/>
			</Box>

			{task?.taskType === TaskType.PairComparison ? (
				<PairFacesToolbar />
			) : (
				<DefaultToolbar />
			)}
		</Stack>
	);
};

export default FaceBottomToolbar;

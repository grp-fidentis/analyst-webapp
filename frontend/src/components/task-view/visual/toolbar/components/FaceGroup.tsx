import { Slider, Tooltip } from '@mui/material';
import { PinDrop } from '@mui/icons-material';
import { Dispatch, FC, SetStateAction, useContext } from 'react';

import { ReactComponent as FaceSymmetryIcon } from '../../../../../assets/images/face_symmetry_icon.svg';
import { ReactComponent as HeadIcon } from '../../../../../assets/images/head.svg';
import { ReactComponent as PrimaryFaceIcon } from '../../../../../assets/images/primary_face.svg';
import { ReactComponent as SecondaryFaceIcon } from '../../../../../assets/images/secondary_face.svg';
import { ReactComponent as Rectangle } from '../../../../../assets/images/rectangle.svg';
import TooltipIconButton from '../../../../common/TooltipIconButton';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import { HumanFace } from '../../../../../types/HumanFaceTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { TaskUtils } from '../../../../../utils/TaskUtils';
import { SceneFaceVisibility } from '../../../../../types/SceneTypes';

type Props = {
	face?: HumanFace;
};

const FaceGroup: FC<Props> = ({ face }) => {
	const {
		primaryFaceVisibility,
		setPrimaryFaceVisibility,
		secondaryFaceVisibility,
		setSecondaryFaceVisibility,
		locked
	} = useContext(SceneContext);
	const { task } = useContext(TaskContext);

	const setFaceVisibility = (
		newValue: (prevValue: SceneFaceVisibility) => Partial<SceneFaceVisibility>
	) =>
		TaskUtils.isPair(task) && !face?.isPrimaryFace
			? setSecondaryFaceVisibility(v => ({ ...v, ...newValue(v) }))
			: setPrimaryFaceVisibility(v => ({ ...v, ...newValue(v) }));

	const faceVisibility: () => SceneFaceVisibility = () =>
		TaskUtils.isPair(task) && !face?.isPrimaryFace
			? secondaryFaceVisibility
			: primaryFaceVisibility;

	const handleTransparencyChange = (_: Event, newValue: number | number[]) => {
		setFaceVisibility(v => ({ opacity: newValue as number }));
	};

	return (
		<>
			{TaskUtils.isPair(task) && (
				<TooltipIconButton
					props={{
						sx: { pl: 2, pr: 1.2 }
					}}
					disabled
					disableRipple
					title={`Color of the ${
						face?.isPrimaryFace ? 'primary' : 'secondary'
					} face`}
				>
					<Rectangle
						width={12}
						fill={face?.isPrimaryFace ? '#E0AC69' : '#efc1a3'}
					/>
				</TooltipIconButton>
			)}
			<TooltipIconButton
				props={{ sx: { px: 0.5 } }}
				disableRipple
				title="Show symmetry plane"
				disabled={TaskUtils.isBatch(task) || !face?.hasSymmetryPlane}
				onClick={() =>
					setFaceVisibility(v => ({ showSymmetryPlane: !v.showSymmetryPlane }))
				}
			>
				<FaceSymmetryIcon
					width={30}
					height={30}
					fill={
						TaskUtils.isBatch(task) || !face?.hasSymmetryPlane
							? 'gray'
							: faceVisibility().showSymmetryPlane
							? '#00ada2'
							: '#D9D9D9'
					}
				/>
			</TooltipIconButton>
			<TooltipIconButton
				props={{ sx: { px: 0.5 } }}
				disableRipple
				title="Show feature points"
				disabled={TaskUtils.isBatch(task) || !face?.hasFeaturePoints}
				onClick={() =>
					setFaceVisibility(v => ({
						showFeaturesPoints: !v.showFeaturesPoints
					}))
				}
			>
				<PinDrop
					sx={{
						color:
							TaskUtils.isBatch(task) || !face?.hasFeaturePoints
								? 'gray'
								: faceVisibility().showFeaturesPoints
								? '#00ada2'
								: '#D9D9D9',
						fontSize: 32
					}}
				/>
			</TooltipIconButton>
			<TooltipIconButton
				props={{
					sx: { pr: 1, ml: 0.5, pl: 0 }
				}}
				disableRipple
				disabled={locked}
				title={`Show ${
					!TaskUtils.isPair(task)
						? ''
						: face?.isPrimaryFace
						? 'primary'
						: 'secondary'
				} face`}
				onClick={() => setFaceVisibility(v => ({ showFace: !v.showFace }))}
			>
				<>
					{!TaskUtils.isPair(task) && (
						<HeadIcon
							fill={faceVisibility().showFace ? '#00ada2' : '#D9D9D9'}
							width={31}
							height={31}
						/>
					)}
					{TaskUtils.isPair(task) && face?.isPrimaryFace && (
						<PrimaryFaceIcon
							fill={faceVisibility().showFace ? '#00ada2' : '#D9D9D9'}
							width={33}
							height={33}
						/>
					)}
					{TaskUtils.isPair(task) && !face?.isPrimaryFace && (
						<SecondaryFaceIcon
							fill={faceVisibility().showFace ? '#00ada2' : '#D9D9D9'}
							width={33}
							height={33}
						/>
					)}
				</>
			</TooltipIconButton>
			<Tooltip title="Transparency" placement="bottom" sx={{ md: 400 }}>
				<Slider
					disabled={locked}
					size="small"
					value={faceVisibility().opacity}
					onChange={handleTransparencyChange}
					aria-label="Small"
					sx={{ width: 100, color: '#D9D9D9', mr: 3 }}
				/>
			</Tooltip>
		</>
	);
};

export default FaceGroup;

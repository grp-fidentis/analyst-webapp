import { FC, MouseEvent, useState } from 'react';
import {
	Box,
	IconButton,
	Popover,
	PopoverOrigin,
	SvgIcon,
	Tooltip
} from '@mui/material';
import { ArrowDropUp } from '@mui/icons-material';

import TooltipIconButton from '../../../../common/TooltipIconButton';

type MainButtonInfo = {
	disabled?: boolean;
	title: string;
	icon: JSX.Element;
};

type ButtonInfo = {
	selected?: boolean;
	title: string;
	icon: JSX.Element;
	onClick: () => void;
};

type PopupSetup = {
	anchorOrigin: PopoverOrigin;
	transformOrigin: PopoverOrigin;
};

type Props = {
	buttonsInfo: ButtonInfo[];
	mainButtonInfo: MainButtonInfo;
	popupSetup: PopupSetup;
};

const ButtonWithOptions: FC<Props> = ({
	buttonsInfo,
	mainButtonInfo,
	popupSetup
}) => {
	const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

	const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const open = Boolean(anchorEl);
	const id = open ? 'transparency-slider' : undefined;

	return (
		<>
			<Tooltip title={mainButtonInfo.title}>
				<span>
					<IconButton
						disabled={mainButtonInfo.disabled ?? false}
						disableRipple
						aria-describedby={id}
						onClick={handleClick}
					>
						<Box component="div" className="flex-row flex-row-center">
							{mainButtonInfo.icon}
							<ArrowDropUp sx={{ color: '#D9D9D9', width: 15, height: 15 }} />
						</Box>
					</IconButton>
				</span>
			</Tooltip>

			<Popover
				id={id}
				open={open}
				anchorEl={anchorEl}
				onClose={handleClose}
				{...popupSetup}
			>
				<Box component="div" className="flex-row flex-row-center">
					{buttonsInfo.map(b => (
						<TooltipIconButton
							props={{
								sx: {
									'backgroundColor': b?.selected ? '#DDDDDD' : 'white',
									'margin': 0.4,
									'padding': 0.7,
									'&:hover': {
										backgroundColor: '#DDDDDD'
									}
								}
							}}
							key={b.title}
							placement="top"
							title={b.title}
							onClick={() => b.onClick()}
						>
							{b.icon}
						</TooltipIconButton>
					))}
				</Box>
			</Popover>
		</>
	);
};

export default ButtonWithOptions;

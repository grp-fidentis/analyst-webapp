import { Box } from '@mui/material';
import { useContext } from 'react';

import { SceneContext } from '../../../../../providers/SceneContextProvider';

import FaceGroup from '../components/FaceGroup';

const DefaultToolbar = () => {
	const { faces } = useContext(SceneContext);

	return (
		<Box component="div" className="flex-row flex-row-center">
			<FaceGroup face={faces[0]} />
		</Box>
	);
};

export default DefaultToolbar;

import { Divider, Stack } from '@mui/material';
import { useContext, useMemo } from 'react';

import { ReactComponent as IntersectionIcon } from '../../../../../assets/images/intersection.svg';
import { ReactComponent as FogToolboxIcon } from '../../../../../assets/images/fog_toolbox.svg';
import { ReactComponent as Fog1Icon } from '../../../../../assets/images/fog1.svg';
import { ReactComponent as Fog2Icon } from '../../../../../assets/images/fog2.svg';
import { ReactComponent as Fog3Icon } from '../../../../../assets/images/fog3.svg';
import { ReactComponent as Fog4Icon } from '../../../../../assets/images/fog4.svg';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import TooltipIconButton from '../../../../common/TooltipIconButton';
import FaceGroup from '../components/FaceGroup';
import ButtonWithOptions from '../components/ButtonWithOptions';
import { FaceUtils } from '../../../../../utils/FaceUtils';

const PairFacesToolbar = () => {
	const { faces } = useContext(SceneContext);

	const getButtonsInfo = () =>
		useMemo(
			() => [
				{
					title: 'No fog',
					icon: <Fog1Icon width={28} height={28} />,
					onClick: () => alert(1)
				},
				{
					title: 'Map fog to the outer face',
					icon: <Fog2Icon width={28} height={28} />,
					onClick: () => alert(1)
				},
				{
					title: 'Replace outer face with fog',
					icon: <Fog3Icon width={28} height={28} />,
					onClick: () => alert(1)
				},
				{
					title: 'Map fog to the inner face',
					icon: <Fog4Icon width={28} height={28} />,
					onClick: () => alert(1)
				}
			],
			[]
		);

	return (
		<Stack direction="row" justifyContent="space-around" alignItems="center">
			<FaceGroup face={FaceUtils.getPrimary(faces)} />
			<Divider
				sx={{ backgroundColor: '#D9D9D9' }}
				orientation="vertical"
				flexItem
			/>
			<FaceGroup face={FaceUtils.getSecondary(faces)} />
			<Divider
				sx={{ backgroundColor: '#D9D9D9' }}
				orientation="vertical"
				flexItem
			/>
			<TooltipIconButton
				disabled
				props={{ sx: { ml: 1, mt: 0.3 } }}
				disableRipple
				title="Show intersection contours"
			>
				<IntersectionIcon fill="gray" width={30} height={30} />
			</TooltipIconButton>
			<ButtonWithOptions
				mainButtonInfo={{
					disabled: true,
					title: 'Simulate fog between faces',
					icon: <FogToolboxIcon fill="gray" width={28} height={28} />
				}}
				buttonsInfo={getButtonsInfo()}
				popupSetup={{
					anchorOrigin: {
						vertical: 'top',
						horizontal: 'left'
					},
					transformOrigin: {
						vertical: 50,
						horizontal: 119
					}
				}}
			/>
		</Stack>
	);
};

export default PairFacesToolbar;

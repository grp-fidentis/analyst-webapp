import { Box, IconButton, Stack } from '@mui/material';
import CameraControls from 'camera-controls';
import { FC, useContext, useState } from 'react';
import { AiOutlineMinus, AiOutlinePlus } from 'react-icons/ai';
import { MathUtils } from 'three';

import { ReactComponent as CameraLeftIcon } from '../../../../assets/images/camera_left.svg';
import { ReactComponent as CameraRightIcon } from '../../../../assets/images/camera_right.svg';
import { ReactComponent as CameraUpIcon } from '../../../../assets/images/camera_up.svg';
import { ReactComponent as CameraDownIcon } from '../../../../assets/images/camera_down.svg';
import { ReactComponent as CameraCenterIcon } from '../../../../assets/images/camera_center.svg';
import { SceneContext } from '../../../../providers/SceneContextProvider';

type CustomIconButtonProps = {
	children: JSX.Element;
	name: string;
	onMouseDown: (e: React.MouseEvent<HTMLButtonElement>) => void;
	onMouseUp: (e: React.MouseEvent<HTMLButtonElement>) => void;
	onMouseLeave: (e: React.MouseEvent<HTMLButtonElement>) => void;
	onDoubleClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
	disabled: boolean;
};

const CustomIconButton: FC<CustomIconButtonProps> = ({
	children,
	name,
	onMouseDown,
	onMouseLeave,
	onMouseUp,
	onDoubleClick,
	disabled = false
}) => (
	<IconButton
		onDoubleClick={onDoubleClick}
		onMouseDown={onMouseDown}
		onMouseUp={onMouseUp}
		onMouseLeave={onMouseLeave}
		name={name}
		disableRipple
		disabled={disabled}
		sx={{
			'backgroundColor': 'white',
			'width': 33,
			'height': 33,
			'padding': 0.3,
			'color': 'black',
			'&:hover': {
				backgroundColor: '#B7B7B7'
			}
		}}
	>
		{children}
	</IconButton>
);

type CameraEventInfo = {
	method: (camera: CameraControls) => void;
};

const cameraEvents: Record<string, CameraEventInfo> = {
	up: {
		method: camera => camera.rotate(0, 0.2 * MathUtils.DEG2RAD, true)
	},
	left: {
		method: camera => camera.rotate(0.2 * MathUtils.DEG2RAD, 0, true)
	},
	right: {
		method: camera => camera.rotate(-0.2 * MathUtils.DEG2RAD, 0, true)
	},
	down: {
		method: camera => camera.rotate(0, -0.2 * MathUtils.DEG2RAD, true)
	},
	upDouble: {
		method: camera => {
			camera.reset(false);
			camera.setPosition(0, -300, 0);
		}
	},
	leftDouble: {
		method: camera => {
			camera.reset(false);
			camera.setPosition(300, 0, 0);
		}
	},
	rightDouble: {
		method: camera => {
			camera.reset(false);
			camera.setPosition(-300, 0, 0);
		}
	},
	downDouble: {
		method: camera => {
			camera.reset(false);
			camera.setPosition(0, 300, 0);
		}
	},

	zoomIn: { method: camera => camera.dolly(0.2, true) },
	zoomOut: { method: camera => camera.dolly(-0.2, true) },

	center: {
		method: camera => {
			camera.reset(false);
			camera.setPosition(0, 0, 300);
		}
	}
};

type Props = {
	camera: CameraControls | null;
};

const CameraControlButtons: FC<Props> = ({ camera }) => {
	const [cameraInterval, setCameraInterval] = useState<NodeJS.Timeout>();
	const { locked } = useContext(SceneContext);

	const handleMouseDown = (e: React.MouseEvent<HTMLButtonElement>) => {
		const operationName = e.currentTarget.name;

		const cameraInterval = setInterval(() => {
			if (camera) {
				cameraEvents[operationName].method(camera);
				camera.update(1);
			}
		}, 10);
		setCameraInterval(cameraInterval);
	};

	const handleMouseUpOrLeave = (e: React.MouseEvent<HTMLButtonElement>) => {
		clearInterval(cameraInterval);
	};

	const handleDoubleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
		const operationName = `${e.currentTarget.name}Double`;

		if (camera) {
			cameraEvents[operationName].method(camera);
			camera.update(1);
		}
	};

	return (
		<Box
			component="div"
			sx={{
				height: '132px',
				width: '100px',
				backgroundColor: 'rgba(38, 156, 146, 0.0)',
				zIndex: 2
			}}
			position="absolute"
			top={12}
			left={12}
		>
			<Stack direction="row" justifyContent="center">
				<CustomIconButton
					name="up"
					onMouseDown={handleMouseDown}
					onMouseUp={handleMouseUpOrLeave}
					onMouseLeave={handleMouseUpOrLeave}
					onDoubleClick={handleDoubleClick}
					disabled={locked}
				>
					<CameraUpIcon width={18} height={18} fill="#00ADA2" />
				</CustomIconButton>
			</Stack>
			<Stack direction="row" justifyContent="center">
				<CustomIconButton
					name="left"
					onMouseDown={handleMouseDown}
					onMouseUp={handleMouseUpOrLeave}
					onMouseLeave={handleMouseUpOrLeave}
					onDoubleClick={handleDoubleClick}
					disabled={locked}
				>
					<CameraLeftIcon width={18} height={18} fill="#00ADA2" />
				</CustomIconButton>
				<CustomIconButton
					name="center"
					onMouseDown={handleMouseDown}
					onMouseUp={handleMouseUpOrLeave}
					onMouseLeave={handleMouseUpOrLeave}
					disabled={locked}
				>
					<CameraCenterIcon width={20} height={20} fill="#00ADA2" />
				</CustomIconButton>
				<CustomIconButton
					name="right"
					onMouseDown={handleMouseDown}
					onMouseUp={handleMouseUpOrLeave}
					onMouseLeave={handleMouseUpOrLeave}
					onDoubleClick={handleDoubleClick}
					disabled={locked}
				>
					<CameraRightIcon width={18} height={18} fill="#00ADA2" />
				</CustomIconButton>
			</Stack>
			<Stack direction="row" justifyContent="center">
				<CustomIconButton
					name="down"
					onMouseDown={handleMouseDown}
					onMouseUp={handleMouseUpOrLeave}
					onMouseLeave={handleMouseUpOrLeave}
					onDoubleClick={handleDoubleClick}
					disabled={locked}
				>
					<CameraDownIcon width={18} height={18} fill="#00ADA2" />
				</CustomIconButton>
			</Stack>
			<Stack direction="row" justifyContent="space-between">
				<CustomIconButton
					name="zoomIn"
					onMouseDown={handleMouseDown}
					onMouseUp={handleMouseUpOrLeave}
					onMouseLeave={handleMouseUpOrLeave}
					disabled={locked}
				>
					<AiOutlinePlus fontSize="22" fill="black" />
				</CustomIconButton>
				<CustomIconButton
					name="zoomOut"
					onMouseDown={handleMouseDown}
					onMouseUp={handleMouseUpOrLeave}
					onMouseLeave={handleMouseUpOrLeave}
					disabled={locked}
				>
					<AiOutlineMinus fontSize="22" fill="black" />
				</CustomIconButton>
			</Stack>
		</Box>
	);
};

export default CameraControlButtons;

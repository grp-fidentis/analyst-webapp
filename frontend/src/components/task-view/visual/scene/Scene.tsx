/* eslint-disable react/no-unknown-property */

import { Box } from '@mui/material';
import { CameraControls } from '@react-three/drei';
import { Canvas } from '@react-three/fiber';
import { useContext, useRef, useState } from 'react';
import { PointLight } from 'three';

import { SceneContext } from '../../../../providers/SceneContextProvider';
import CameraControlButtons from '../camerabuttons/CameraControlButtons';

import HumanFaceGroupBuilder from './HumanFaceGroupBuilder';

const Scene = () => {
	const { backgroundColor, locked } = useContext(SceneContext);
	const cameraControlRef = useRef<CameraControls>(null);
	const [isMouseUsed, setIsMouseUsed] = useState(false);

	const cameraControllingProps = {
		onWheel: () => {
			setIsMouseUsed(true);
			setTimeout(() => {
				setIsMouseUsed(false);
			}, 100);
		},
		onMouseDown: () => setIsMouseUsed(true),
		onMouseUp: () => setIsMouseUsed(false),
		onMouseLeave: () => setIsMouseUsed(false)
	};

	return (
		<Box
			component="div"
			sx={{ height: `calc(100vh - 165px)` }}
			position="relative"
		>
			<Box
				component="div"
				sx={{
					zIndex: 1,
					height: '100%',
					width: '100%',
					backgroundColor
				}}
				position="absolute"
				top={0}
				left={0}
			/>

			<CameraControlButtons camera={cameraControlRef.current} />

			<Canvas
				style={{
					zIndex: 1
				}}
				{...cameraControllingProps}
				frameloop={isMouseUsed ? 'always' : 'demand'}
				camera={{ fov: 45, position: [0, 0, 300] }}
				onCreated={({ camera, scene }) => {
					camera.add(new PointLight('white', 0.33));
					scene.add(camera);
				}}
			>
				<CameraControls enabled={!locked} ref={cameraControlRef} />
				<ambientLight intensity={0.07} />
				<HumanFaceGroupBuilder />
			</Canvas>
		</Box>
	);
};

export default Scene;

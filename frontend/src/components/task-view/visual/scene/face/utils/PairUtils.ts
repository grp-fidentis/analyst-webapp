/* eslint-disable react/no-unknown-property */

import {
	BufferGeometry,
	Color,
	Event,
	Group,
	Matrix4,
	Mesh,
	MeshStandardMaterial,
	Object3D,
	Points,
	Vector3
} from 'three';
import { Dispatch, RefObject, SetStateAction } from 'react';
import { invalidate } from '@react-three/fiber';

import {
	Alignment,
	AlignmentOperations
} from '../../../../../../types/SceneTypes';
import {
	HumanFace,
	HumanFaceFeaturePointsUpdate,
	HumanFaceSymmetryPlaneUpdate,
	HumanFaceUpdate,
	TransformationDto
} from '../../../../../../types/HumanFaceTypes';
import { FaceUtils } from '../../../../../../utils/FaceUtils';

import { getGeometryAttribute } from './CommonUtils';

export const getTransformOperations: () => Map<
	AlignmentOperations | undefined,
	(mesh: Group | Mesh, value: number) => void
> = () => {
	const map = new Map<
		AlignmentOperations,
		(mesh: Group | Mesh, value: number) => void
	>();

	map.set(AlignmentOperations.TranslateXLeft, (mesh, value) =>
		mesh.translateX(-value)
	);
	map.set(AlignmentOperations.TranslateXRight, (mesh, value) =>
		mesh.translateX(value)
	);
	map.set(AlignmentOperations.TranslateYUp, (mesh, value) =>
		mesh.translateY(value)
	);
	map.set(AlignmentOperations.TranslateYDown, (mesh, value) =>
		mesh.translateY(-value)
	);
	map.set(AlignmentOperations.TranslateZBack, (mesh, value) =>
		mesh.translateZ(-value)
	);
	map.set(AlignmentOperations.TranslateZFront, (mesh, value) =>
		mesh.translateZ(value)
	);

	map.set(AlignmentOperations.RotateXUp, (mesh, value) => mesh.rotateX(-value));
	map.set(AlignmentOperations.RotateXDown, (mesh, value) =>
		mesh.rotateX(value)
	);
	map.set(AlignmentOperations.RotateYLeft, (mesh, value) =>
		mesh.rotateY(-value)
	);
	map.set(AlignmentOperations.RotateYRight, (mesh, value) =>
		mesh.rotateY(value)
	);
	map.set(AlignmentOperations.RotateZLeft, (mesh, value) =>
		mesh.rotateZ(value)
	);
	map.set(AlignmentOperations.RotateZRight, (mesh, value) =>
		mesh.rotateZ(-value)
	);

	return map;
};

export const getCurrentPositionsOfHumanFace = (
	group?: Group | null,
	refOfFaceMesh?: RefObject<Mesh<BufferGeometry>> | undefined
): number[] => {
	if (!group || !refOfFaceMesh) return [];

	const matrix = getMatrixOfPositionsChange(group);

	const positions = getGeometryAttribute(refOfFaceMesh, 'position');
	const position = new Vector3();
	const result = [];
	if (positions) {
		for (let i = 0; i < positions.count; i++) {
			position.fromBufferAttribute(positions, i); // Get the position as a Vector3
			position.applyMatrix4(matrix);
			result.push(position.x);
			result.push(position.y);
			result.push(position.z);
		}
	}

	return result ?? [];
};

const getMatrixOfPositionsChange = (group: Group) => {
	const matrix = new Matrix4();

	const position1 = group.position;
	if (position1.x || position1.y || position1.z) {
		matrix.makeTranslation(position1.x, position1.y, position1.z);
	}

	const rotation1 = group.rotation;
	if (rotation1.x || rotation1.y || rotation1.z) {
		matrix.makeRotationFromEuler(rotation1);
	}
	return matrix;
};

const flushTransformationToFeaturePointSpheres = (
	group?: Group | null,
	sphereMeshes?: Object3D<Event>[]
): void => {
	if (!group || !sphereMeshes) return;

	const matrix = getMatrixOfPositionsChange(group);

	sphereMeshes.forEach(sphereMesh => {
		const newPosition = new Vector3(
			sphereMesh.position.x,
			sphereMesh.position.y,
			sphereMesh.position.z
		);
		newPosition.applyMatrix4(matrix);
		sphereMesh.position.set(newPosition.x, newPosition.y, newPosition.z);

		const newNearestPointPosition = new Vector3(
			sphereMesh.userData.nearestPoint.x,
			sphereMesh.userData.nearestPoint.y,
			sphereMesh.userData.nearestPoint.z
		);
		newNearestPointPosition.applyMatrix4(matrix);
		sphereMesh.userData.nearestPoint.x = newNearestPointPosition.x;
		sphereMesh.userData.nearestPoint.y = newNearestPointPosition.y;
		sphereMesh.userData.nearestPoint.z = newNearestPointPosition.z;
	});
};

const flushTransformationToGeometry = (
	group: Group,
	geometry: BufferGeometry
) => {
	const position = group.position;
	if (position.x || position.y || position.z) {
		geometry.translate(position.x, position.y, position.z);
	}

	const rotation = group.rotation;
	if (rotation.x || rotation.y || rotation.z) {
		if (rotation.x) {
			geometry.rotateX(group.rotation.x);
		}
		if (rotation.y) {
			geometry.rotateY(group.rotation.y);
		}
		if (rotation.z) {
			geometry.rotateZ(group.rotation.z);
		}
	}
};

const flushTransformationToSymmetryPlane = (
	group: Group,
	geometry: BufferGeometry
) => {
	const position = group.position;
	if (position.x) {
		geometry.translate(position.x, 0, 0);
	}

	const rotation = group.rotation;
	if (rotation.y || rotation.z) {
		if (rotation.y) {
			geometry.rotateY(group.rotation.y);
		}
		if (rotation.z) {
			geometry.rotateZ(group.rotation.z);
		}
	}
};

export const highlightFpBelowThreshold = (
	featurePointsHighlightThreshold: number,
	faces: HumanFace[],
	face: HumanFace,
	featurePointsGroup: Group | undefined | null,
	sceneGroup: Group | undefined | null,
	updateFace: (face: HumanFace) => void,
	saveToFace?: boolean
) => {
	if (face.isPrimaryFace || !featurePointsGroup) {
		return;
	}

	const primaryFaceFp =
		FaceUtils.getPrimary(faces)?.featurePoints?.featurePointDtos;
	if (!primaryFaceFp) return;

	const results: number[] = [];
	for (let i = 0; i < (primaryFaceFp.length ?? 0); i++) {
		const primaryVector: Vector3 = new Vector3(
			primaryFaceFp[i].point.x,
			primaryFaceFp[i].point.y,
			primaryFaceFp[i].point.z
		);
		const secondaryFaceFpMesh = featurePointsGroup?.getObjectByName(
			`${i}`
		) as Mesh;
		if (!secondaryFaceFpMesh || !sceneGroup) return;

		const secondaryVector = new Vector3(
			secondaryFaceFpMesh.position.x,
			secondaryFaceFpMesh.position.y,
			secondaryFaceFpMesh.position.z
		);
		const matrixOfPositionsChange = getMatrixOfPositionsChange(sceneGroup);
		secondaryVector.applyMatrix4(matrixOfPositionsChange);

		primaryVector.sub(secondaryVector);

		if (primaryVector.length() <= featurePointsHighlightThreshold) {
			results.push(i);
		}
	}

	const primaryFace = FaceUtils.getPrimary(faces);
	primaryFace?.featurePoints?.featurePointDtos.forEach(
		(f, i) => (f.color = results?.includes(i) ? 'green' : 'gray')
	);
	updateFace({ ...primaryFace });

	if (saveToFace) {
		const secondaryFace = FaceUtils.getSecondary(faces);
		secondaryFace?.featurePoints?.featurePointDtos.forEach(
			(f, i) => (f.color = results?.includes(i) ? 'green' : 'gray')
		);
		updateFace({ ...secondaryFace });
	} else {
		featurePointsGroup?.children
			.map(mesh => (mesh as Mesh).material as MeshStandardMaterial)
			.forEach((material, i) =>
				material.color.set(
					results.includes(i) ? new Color('green') : new Color('gray')
				)
			);
	}
};

const stabiliseSymmetryPlane = (
	symmetryPlaneMesh: Mesh | null | undefined,
	transformMethods: Map<
		AlignmentOperations | undefined,
		(mesh: Group | Mesh, value: number) => void
	>,
	key: AlignmentOperations,
	value: number
) => {
	if (!symmetryPlaneMesh) return;

	let transformation;
	switch (key) {
		case AlignmentOperations.TranslateYDown:
			transformation = transformMethods.get(AlignmentOperations.TranslateYUp);
			break;
		case AlignmentOperations.TranslateYUp:
			transformation = transformMethods.get(AlignmentOperations.TranslateYDown);
			break;
		case AlignmentOperations.TranslateZBack:
			transformation = transformMethods.get(
				AlignmentOperations.TranslateZFront
			);
			break;
		case AlignmentOperations.TranslateZFront:
			transformation = transformMethods.get(AlignmentOperations.TranslateZBack);
			break;
		case AlignmentOperations.RotateXUp:
			transformation = transformMethods.get(AlignmentOperations.RotateXDown);
			break;
		case AlignmentOperations.RotateXDown:
			transformation = transformMethods.get(AlignmentOperations.RotateXUp);
			break;
	}

	if (!transformation) return;

	transformation(symmetryPlaneMesh, value);
};

export const getTransformationDto = (group: Group): TransformationDto => {
	const translation = {
		x: group.position.x,
		y: group.position.y,
		z: group.position.z
	};
	const rotation = {
		x: group.quaternion.x,
		y: group.quaternion.y,
		z: group.quaternion.z
	};

	return { translation, rotation };
};

export const transformSecondaryFace = (
	taskId: number,
	face: HumanFace,
	alignment: Alignment,
	transformInterval: NodeJS.Timeout | undefined,
	setTransformInterval: Dispatch<SetStateAction<NodeJS.Timeout | undefined>>,
	calculateDistanceWithDto: () => void,
	highlightFeaturePointsBelowThresholdThrottled: (saveToFace?: boolean) => void,
	updateGeometryOnBackendThrottled: (humanFaceUpdate: HumanFaceUpdate) => void,
	sceneGroup: Group | null | undefined,
	featurePointsGroup: Group | null | undefined,
	faceMesh: Mesh | null | undefined,
	samplesPoints: Points | null | undefined,
	symmetryPlaneMesh: Mesh | null | undefined
) => {
	if (face.isPrimaryFace) return;

	const group = sceneGroup;

	if (alignment.isActive && group) {
		const key = alignment?.operation ?? AlignmentOperations.RotateZLeft;
		const transformMethods = getTransformOperations();
		const currentTransformMethod = transformMethods.get(key);
		if (!currentTransformMethod) return;

		const rad = 0.0001;
		const points = 0.005;
		const value = alignment.operation?.toString().includes('Translate')
			? points
			: rad;

		const transformOnce = () => {
			currentTransformMethod(group, value);
			stabiliseSymmetryPlane(symmetryPlaneMesh, transformMethods, key, value);
			invalidate();
			calculateDistanceWithDto();
			highlightFeaturePointsBelowThresholdThrottled(false);
		};

		transformOnce();
		const interval = setInterval(() => {
			transformOnce();
		}, 5);
		setTransformInterval(interval);
	}

	if (!alignment.isActive && alignment.operation && group) {
		const faceGeometry = faceMesh?.geometry as BufferGeometry;
		const samplesGeometry = samplesPoints?.geometry as BufferGeometry;
		const symmetryPlaneGeometry = symmetryPlaneMesh?.geometry as BufferGeometry;
		const featurePointMeshes = featurePointsGroup?.children.filter(
			children => !children.name.startsWith('outer')
		);

		flushTransformationToGeometry(group, faceGeometry);
		if (samplesGeometry) {
			flushTransformationToGeometry(group, samplesGeometry);
		}
		if (face.featurePoints?.featurePointDtos) {
			highlightFeaturePointsBelowThresholdThrottled(true);
			flushTransformationToFeaturePointSpheres(group, featurePointMeshes);
		}
		if (face.symmetryPlane?.positions) {
			flushTransformationToSymmetryPlane(group, symmetryPlaneGeometry);
		}

		const transformationDto = getTransformationDto(group);
		const translationSymmetry = {
			x: group?.position.x ?? 0,
			y: 0,
			z: 0
		};
		const rotationSymmetry = {
			x: 0,
			y: group.quaternion.y,
			z: group.quaternion.z
		};
		group.position.set(0, 0, 0);
		group.rotation.set(0, 0, 0);
		symmetryPlaneMesh?.position.set(0, 0, 0);
		symmetryPlaneMesh?.rotation.set(0, 0, 0);

		const featurePointsUpdate: HumanFaceFeaturePointsUpdate = {
			positions:
				featurePointMeshes?.flatMap(sphereMesh => [
					sphereMesh.position.x,
					sphereMesh.position.y,
					sphereMesh.position.z
				]) ?? [],
			nearestPointsPositions:
				featurePointMeshes?.flatMap(sphereMesh => [
					sphereMesh.userData.nearestPoint.x,
					sphereMesh.userData.nearestPoint.y,
					sphereMesh.userData.nearestPoint.z
				]) ?? []
		};
		const symmetryPlaneUpdate: HumanFaceSymmetryPlaneUpdate = {
			symmetryTransformationDto: {
				translation: translationSymmetry,
				rotation: rotationSymmetry
			}
		};
		const humanFaceUpdate: HumanFaceUpdate = {
			taskId,
			transformationDto,
			featurePointsUpdate,
			symmetryPlaneUpdate
		};
		updateGeometryOnBackendThrottled(humanFaceUpdate);

		clearInterval(transformInterval);
		setTransformInterval(undefined);
	}
};

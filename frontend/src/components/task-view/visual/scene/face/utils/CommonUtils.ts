/* eslint-disable react/no-unknown-property */

import {
	BufferAttribute,
	BufferGeometry,
	InterleavedBufferAttribute,
	Mesh,
	SphereGeometry
} from 'three';
import { RefObject } from 'react';
import { invalidate } from '@react-three/fiber';

import { HumanFace } from '../../../../../../types/HumanFaceTypes';
import {
	CuttingPlaneDto,
	CuttingPlaneFrontendState,
	CuttingPlanesTaskDto
} from '../../../../../../types/AnalyticalTaskTypes';

export const createFaceBufferGeometry = (face: HumanFace) => {
	if (!face.geometry) {
		return undefined;
	}

	const geometry = new BufferGeometry();
	geometry.setAttribute(
		'position',
		new BufferAttribute(new Float32Array(face.geometry.positions), 3)
	);
	geometry.setAttribute(
		'normal',
		new BufferAttribute(new Float32Array(face.geometry.normals), 3)
	);
	geometry.setAttribute(
		'uv',
		new BufferAttribute(new Float32Array(face.geometry.uvs), 2)
	);

	geometry.setIndex(
		new BufferAttribute(new Uint16Array(face.geometry.indices), 1)
	);

	return geometry;
};

export const createFaceBoundingBoxGeometry = (face: HumanFace) => {
	if (!face.boundingBox) {
		return undefined;
	}
	const min = face.boundingBox?.min;
	const max = face.boundingBox?.max;

	const geometry = new BufferGeometry();
	const vertices = new Float32Array([
		// bottom rectangle:
		min.x,
		min.y,
		min.z,
		max.x,
		min.y,
		min.z,

		max.x,
		min.y,
		min.z,
		max.x,
		min.y,
		max.z,

		max.x,
		min.y,
		max.z,
		min.x,
		min.y,
		max.z,

		min.x,
		min.y,
		max.z,
		min.x,
		min.y,
		min.z,

		// top rectangle:
		min.x,
		max.y,
		min.z,
		max.x,
		max.y,
		min.z,

		max.x,
		max.y,
		min.z,
		max.x,
		max.y,
		max.z,

		max.x,
		max.y,
		max.z,
		min.x,
		max.y,
		max.z,

		min.x,
		max.y,
		max.z,
		min.x,
		max.y,
		min.z,

		// vertical edges:
		min.x,
		min.y,
		min.z,
		min.x,
		max.y,
		min.z,

		max.x,
		min.y,
		min.z,
		max.x,
		max.y,
		min.z,

		max.x,
		min.y,
		max.z,
		max.x,
		max.y,
		max.z,

		min.x,
		min.y,
		max.z,
		min.x,
		max.y,
		max.z
	]);

	geometry.setAttribute('position', new BufferAttribute(vertices, 3));

	return geometry;
};

export const setHeatmapColors = (
	face: HumanFace,
	faceMesh?: Mesh | undefined | null
) => {
	if (!face.isPrimaryFace) {
		const geometry = faceMesh?.geometry as BufferGeometry;

		if (face.colorsOfHeatmap) {
			geometry.setAttribute(
				'color',
				new BufferAttribute(new Uint8Array(face.colorsOfHeatmap ?? []), 3, true)
			);
		} else {
			geometry.setAttribute(
				'color',
				new BufferAttribute(new Uint8Array([]), 3, true)
			);
		}
		geometry.getAttribute('color').needsUpdate = true;
		invalidate();
	}
};

export const createSamplesBufferGeometry = (face: HumanFace) => {
	if (!face.samples) {
		return undefined;
	}

	const geometry = new BufferGeometry();
	geometry.setAttribute(
		'position',
		new BufferAttribute(new Float32Array(face.samples.positions), 3)
	);
	return geometry;
};

export const createFeaturePointsSphereGeometry = (face: HumanFace) => {
	if (!face.featurePoints) {
		return undefined;
	}

	return new SphereGeometry(2.5, 32, 16);
};

export const createSymmetryPlaneGeometry = (face: HumanFace) => {
	if (!face.symmetryPlane) {
		return undefined;
	}

	const geometry = new BufferGeometry();
	geometry.setAttribute(
		'position',
		new BufferAttribute(new Float32Array(face.symmetryPlane.positions), 3)
	);
	geometry.setAttribute(
		'normal',
		new BufferAttribute(new Float32Array(face.symmetryPlane.normals), 3)
	);

	return geometry;
};

export const createCuttingPlaneGeometry = (
	cuttingPlaneDto: CuttingPlaneFrontendState
) => {
	if (!cuttingPlaneDto) {
		return undefined;
	}

	const geometry = new BufferGeometry();
	geometry.setAttribute(
		'position',
		new BufferAttribute(new Float32Array(cuttingPlaneDto.positions), 3)
	);
	geometry.setAttribute(
		'normal',
		new BufferAttribute(new Float32Array(cuttingPlaneDto.normals), 3)
	);

	const trans = cuttingPlaneDto.transformationDto.translation;
	if (trans?.x !== 0 || trans?.y !== 0 || trans?.z !== 0) {
		geometry.translate(trans?.x ?? 0, trans?.y ?? 0, trans?.z ?? 0);
	}

	return geometry;
};

export const getGeometryAttribute = (
	ref: RefObject<any>,
	attribute: string
): BufferAttribute | InterleavedBufferAttribute => {
	const geometry = ref.current?.geometry as BufferGeometry;
	return geometry.getAttribute(attribute);
};
export const allowRenderUpdate = (faceMeshRef: React.RefObject<Mesh>) => {
	getGeometryAttribute(faceMeshRef, 'position').needsUpdate = true;
	getGeometryAttribute(faceMeshRef, 'normal').needsUpdate = true;
};

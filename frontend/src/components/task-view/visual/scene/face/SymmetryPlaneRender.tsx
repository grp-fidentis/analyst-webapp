/* eslint-disable react/no-unknown-property */

import React, { FC, useContext, useMemo } from 'react';
import { BufferGeometry, DoubleSide, Mesh } from 'three';

import { HumanFace } from '../../../../../types/HumanFaceTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { TaskUtils } from '../../../../../utils/TaskUtils';
import { SceneContext } from '../../../../../providers/SceneContextProvider';

import { createSymmetryPlaneGeometry } from './utils/CommonUtils';

type Props = {
	face: HumanFace;
	symmetryPlaneRef: React.RefObject<Mesh>;
};

const SymmetryPlaneRender: FC<Props> = ({ symmetryPlaneRef, face }) => {
	const { task } = useContext(TaskContext);
	const { primaryFaceVisibility, secondaryFaceVisibility } =
		useContext(SceneContext);

	const symmetryPlaneGeometry: BufferGeometry | undefined = useMemo(
		() => createSymmetryPlaneGeometry(face),
		[face.symmetryPlane]
	);

	const getFaceVisibility = () =>
		TaskUtils.isPair(task) && !face?.isPrimaryFace
			? secondaryFaceVisibility
			: primaryFaceVisibility;

	if (!face.symmetryPlane) return null;
	return (
		<mesh ref={symmetryPlaneRef} geometry={symmetryPlaneGeometry}>
			<meshBasicMaterial
				color={
					TaskUtils.isSingle(task)
						? '#666666'
						: face.isPrimaryFace
						? '#E0AC69'
						: '#EFC1A3'
				}
				side={DoubleSide}
				visible={getFaceVisibility().showSymmetryPlane}
				opacity={0.5}
				transparent
			/>
		</mesh>
	);
};

export default SymmetryPlaneRender;

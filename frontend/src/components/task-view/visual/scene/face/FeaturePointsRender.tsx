/* eslint-disable react/no-unknown-property */

import React, { FC, useContext, useMemo } from 'react';
import { BufferGeometry, Group, Points } from 'three';

import { HumanFace } from '../../../../../types/HumanFaceTypes';
import { SceneFaceVisibility } from '../../../../../types/SceneTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import { ShowSpheres } from '../../../../../types/AnalyticalTaskTypes';
import { TaskUtils } from '../../../../../utils/TaskUtils';
import { SceneContext } from '../../../../../providers/SceneContextProvider';

import { createFeaturePointsSphereGeometry } from './utils/CommonUtils';

type Props = {
	face: HumanFace;
	featurePointsGroupRef: React.RefObject<Group>;
};

const FeaturePointsRender: FC<Props> = ({ featurePointsGroupRef, face }) => {
	const { task, currentPanel } = useContext(TaskContext);
	const { primaryFaceVisibility, secondaryFaceVisibility } =
		useContext(SceneContext);
	const { showSpheres } = useContext(AnalyticalTaskContext);

	const featurePointsGeometry: BufferGeometry | undefined = useMemo(
		() => createFeaturePointsSphereGeometry(face),
		[face.featurePoints?.featurePointDtos]
	);

	const getFaceVisibility = () =>
		TaskUtils.isPair(task) && !face?.isPrimaryFace
			? secondaryFaceVisibility
			: primaryFaceVisibility;

	if (!featurePointsGeometry) return null;
	return (
		<group ref={featurePointsGroupRef}>
			{face.featurePoints?.featurePointDtos.map((f, i) => (
				<mesh
					name={`${i}`}
					key={i}
					position={[f.point.x, f.point.y, f.point.z]}
					visible={getFaceVisibility().showFeaturesPoints}
					userData={{
						nearestPoint: {
							x: f.relationToMesh?.nearestPoint.x,
							y: f.relationToMesh?.nearestPoint.y,
							z: f.relationToMesh?.nearestPoint.z
						}
					}}
					geometry={featurePointsGeometry}
				>
					<meshStandardMaterial
						color={f.hovered ? 'cyan' : f.color ?? 'gray'}
					/>
				</mesh>
			))}
			{!face.isPrimaryFace &&
				currentPanel === 'DISTANCE' &&
				showSpheres !== ShowSpheres.HideAll &&
				face.featurePoints?.featurePointDtos.map(
					(f, i) =>
						(showSpheres === ShowSpheres.ShowAll || f.selected) && (
							<mesh
								name={`outer${i}`}
								key={`outer${i}`}
								position={[f.point.x, f.point.y, f.point.z]}
							>
								<sphereGeometry args={[f.size, 16, 8]} />
								<meshStandardMaterial
									wireframe
									color={f.hovered ? 'cyan' : f.selected ? 'magenta' : 'gray'}
								/>
							</mesh>
						)
				)}
		</group>
	);
};

export default FeaturePointsRender;

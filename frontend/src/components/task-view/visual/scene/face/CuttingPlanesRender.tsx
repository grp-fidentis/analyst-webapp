/* eslint-disable react/no-unknown-property */

import React, { FC, useContext, useEffect, useMemo } from 'react';
import { BufferGeometry, DoubleSide, Group, Mesh } from 'three';

import { HumanFace } from '../../../../../types/HumanFaceTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { TaskUtils } from '../../../../../utils/TaskUtils';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import { CuttingPlaneFrontendState } from '../../../../../types/AnalyticalTaskTypes';

import {
	createCuttingPlaneGeometry,
	createSymmetryPlaneGeometry,
	getGeometryAttribute
} from './utils/CommonUtils';

type Props = {
	face: HumanFace;
	cuttingPlanesRef: React.RefObject<Group>;
};

const CuttingPlanesRender: FC<Props> = ({ cuttingPlanesRef, face }) => {
	const { task, currentPanel } = useContext(TaskContext);
	const { cuttingPlanesTaskDto } = useContext(AnalyticalTaskContext);

	const getColor = (cuttingPlane: CuttingPlaneFrontendState) => {
		let color = '#666';

		if (
			cuttingPlane.direction === cuttingPlanesTaskDto.state.currentDirection
		) {
			color = '#757159';
		}

		if (cuttingPlane.isHover && cuttingPlane.isSelected) {
			color = '#BBA520';
		}

		return color;
	};

	if (currentPanel !== 'CUTTING PLANES') return null;
	return (
		<group ref={cuttingPlanesRef}>
			{cuttingPlanesTaskDto.state.cuttingPlanes
				?.filter(c => c.isSelected || c.isHover)
				.map((c, i) => (
					<mesh key={i} geometry={createCuttingPlaneGeometry(c)}>
						<meshBasicMaterial
							color={getColor(c)}
							side={DoubleSide}
							visible={c.isVisible || (c.isHover && c.isSelected)}
							opacity={0.5}
							transparent
						/>
					</mesh>
				))}
		</group>
	);
};

export default CuttingPlanesRender;

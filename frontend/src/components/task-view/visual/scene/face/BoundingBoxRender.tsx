/* eslint-disable react/no-unknown-property */

import React, { FC, useContext, useMemo } from 'react';
import { BufferGeometry } from 'three';

import { HumanFace } from '../../../../../types/HumanFaceTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { TaskType } from '../../../../../types/TaskTypes';
import { TaskUtils } from '../../../../../utils/TaskUtils';

import { createFaceBoundingBoxGeometry } from './utils/CommonUtils';

type Props = {
	face: HumanFace;
};

const BoundingBoxRender: FC<Props> = ({ face }) => {
	const { task, currentPanel } = useContext(TaskContext);

	const boxGeometry: BufferGeometry | undefined = useMemo(
		() => createFaceBoundingBoxGeometry(face),
		[face.boundingBox]
	);

	if (
		currentPanel !== 'CUTTING PLANES' ||
		(TaskUtils.isPair(task) && !face.isPrimaryFace)
	)
		return null;
	return (
		<lineSegments geometry={boxGeometry}>
			<lineBasicMaterial color="gray" />
		</lineSegments>
	);
};

export default BoundingBoxRender;

/* eslint-disable react/no-unknown-property */

import { FC, useContext, useEffect, useRef, useState } from 'react';
import { Group, Mesh, Points } from 'three';

import {
	HumanFace,
	HumanFaceUpdate
} from '../../../../../types/HumanFaceTypes';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import { HumanFaceService } from '../../../../../services/HumanFaceService';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import {
	DistanceTaskDto,
	LoadingOperation
} from '../../../../../types/AnalyticalTaskTypes';
import useThrottle from '../../../../../hooks/useThrottle';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { TaskType } from '../../../../../types/TaskTypes';

import {
	getTransformationDto,
	highlightFpBelowThreshold,
	transformSecondaryFace
} from './utils/PairUtils';
import { setHeatmapColors, allowRenderUpdate } from './utils/CommonUtils';
import SamplesRender from './SamplesRender';
import FeaturePointsRender from './FeaturePointsRender';
import SymmetryPlaneRender from './SymmetryPlaneRender';
import HumanFaceRender from './HumanFaceRender';
import CuttingPlanesRender from './CuttingPlanesRender';
import BoundingBoxRender from './BoundingBoxRender';

type Props = {
	face: HumanFace;
};

const HumanFaceGroupRender: FC<Props> = ({ face }) => {
	const humanFaceGroupRef = useRef<Group>(null);
	const faceMeshRef = useRef<Mesh>(null);
	const facePointsRef = useRef<Points>(null);
	const featurePointsGroupRef = useRef<Group>(null);
	const symmetryPlaneRef = useRef<Mesh>(null);
	const cuttingPlanesRef = useRef<Group>(null);
	const samplesRef = useRef<Points>(null);
	const { task } = useContext(TaskContext);
	const { calculateDistances, distanceTaskDto, updateLoadingInfo } = useContext(
		AnalyticalTaskContext
	);
	const {
		alignment,
		featurePointsHighlightThreshold,
		faces,
		updateFace,
		setSceneRendered
	} = useContext(SceneContext);
	const [transformInterval, setTransformInterval] = useState<NodeJS.Timeout>();

	const calculateDistancesThrottled = useThrottle(
		async (
			calculateDistancesFunc,
			dto: DistanceTaskDto,
			useAllSizes: boolean
		) => {
			await calculateDistancesFunc(dto, useAllSizes);
		},
		1000
	);
	const updateGeometryOnBackendThrottled = useThrottle(
		async (humanFaceUpdate: HumanFaceUpdate) => {
			updateLoadingInfo(true, LoadingOperation.Alignment);
			try {
				await HumanFaceService.saveGeometry(face.id ?? -1, humanFaceUpdate);
			} finally {
				updateLoadingInfo(false);
			}
		},
		500
	);
	const highlightFeaturePointsBelowThresholdThrottled = useThrottle(
		highlightFeaturePointsBelowThresholdFunc =>
			highlightFeaturePointsBelowThresholdFunc(),
		300
	);

	const highlightFeaturePointsBelowThreshold = (saveToFace?: boolean) =>
		highlightFpBelowThreshold(
			featurePointsHighlightThreshold,
			faces,
			face,
			featurePointsGroupRef.current,
			humanFaceGroupRef.current,
			updateFace,
			saveToFace ?? false
		);

	const calculateDistanceWithDto = async () => {
		const group = humanFaceGroupRef.current;
		if (!group) return;

		const distanceDto: DistanceTaskDto = {
			...distanceTaskDto,
			transformationDto: getTransformationDto(group)
		};
		await calculateDistancesThrottled(calculateDistances, distanceDto, false);
	};

	useEffect(() => {
		const calculateDistanceWithDtoFunc = async () => {
			if (task?.taskType === TaskType.PairComparison && !face.isPrimaryFace) {
				await calculateDistanceWithDto();
			}
			setSceneRendered(true);
		};

		calculateDistanceWithDtoFunc();

		return () => {
			calculateDistancesThrottled.cancel();
			highlightFeaturePointsBelowThresholdThrottled.cancel();
			updateGeometryOnBackendThrottled.cancel();
		};
	}, []);

	useEffect(() => {
		setHeatmapColors(face, faceMeshRef.current);
	}, [face.colorsOfHeatmap]);

	useEffect(
		() => highlightFeaturePointsBelowThreshold(true),
		[featurePointsHighlightThreshold, face.geometry?.positions]
	);

	useEffect(() => allowRenderUpdate(faceMeshRef), [face]);

	useEffect(() => {
		transformSecondaryFace(
			task?.id ?? -1,
			face,
			alignment,
			transformInterval,
			setTransformInterval,
			calculateDistanceWithDto,
			(saveToFace?: boolean) =>
				highlightFeaturePointsBelowThresholdThrottled(() =>
					highlightFeaturePointsBelowThreshold(saveToFace)
				),
			updateGeometryOnBackendThrottled,
			humanFaceGroupRef.current,
			featurePointsGroupRef.current,
			faceMeshRef.current,
			samplesRef.current,
			symmetryPlaneRef.current
		);
	}, [alignment.isActive]);

	return (
		<group ref={humanFaceGroupRef}>
			<SamplesRender
				key={`samples${face.id}`}
				face={face}
				samplesRef={samplesRef}
			/>

			<FeaturePointsRender
				key={`featurePoints${face.id}`}
				face={face}
				featurePointsGroupRef={featurePointsGroupRef}
			/>

			<SymmetryPlaneRender
				key={`symmetryPlane${face.id}`}
				face={face}
				symmetryPlaneRef={symmetryPlaneRef}
			/>

			<CuttingPlanesRender
				key={`cuttingPlanes${face.id}`}
				face={face}
				cuttingPlanesRef={cuttingPlanesRef}
			/>

			<BoundingBoxRender key={`boundingBox${face.id}`} face={face} />

			<HumanFaceRender
				face={face}
				faceMeshRef={faceMeshRef}
				facePointsRef={facePointsRef}
			/>
		</group>
	);
};

export default HumanFaceGroupRender;

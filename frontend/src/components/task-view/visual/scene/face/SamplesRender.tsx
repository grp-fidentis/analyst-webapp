/* eslint-disable react/no-unknown-property */

import { FC, useEffect, useMemo, useState } from 'react';
import { BufferGeometry, Points } from 'three';
import { useTexture } from '@react-three/drei';

import {
	HumanFace,
	HumanFaceUpdate
} from '../../../../../types/HumanFaceTypes';
import ball from '../../../../../assets/images/ball.png';

import {
	createSamplesBufferGeometry,
	getGeometryAttribute
} from './utils/CommonUtils';

type Props = {
	face: HumanFace;
	samplesRef: React.RefObject<Points>;
};

const HumanFaceRender: FC<Props> = ({ samplesRef, face }) => {
	const ballTexture = useTexture(ball);

	const samplesGeometry: BufferGeometry | undefined = useMemo(
		() => createSamplesBufferGeometry(face),
		[face.samples]
	);

	const allowRenderUpdate = () => {
		if (samplesGeometry) {
			getGeometryAttribute(samplesRef, 'position').needsUpdate = true;
		}
	};

	useEffect(allowRenderUpdate, [face]);

	if (!samplesGeometry) return null;
	return (
		<points ref={samplesRef} geometry={samplesGeometry}>
			<pointsMaterial
				size={1.5}
				color="black"
				transparent
				map={ballTexture}
				alphaTest={0.5}
			/>
		</points>
	);
};

export default HumanFaceRender;

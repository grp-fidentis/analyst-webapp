/* eslint-disable react/no-unknown-property */

import { FC, useContext, useMemo, useState } from 'react';
import {
	BufferGeometry,
	DoubleSide,
	GreaterEqualDepth,
	Mesh,
	Points
} from 'three';
import { useTexture } from '@react-three/drei';

import { HumanFace } from '../../../../../types/HumanFaceTypes';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import { HeatmapRenderMode } from '../../../../../types/AnalyticalTaskTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { TaskUtils } from '../../../../../utils/TaskUtils';

import { createFaceBufferGeometry } from './utils/CommonUtils';

type Props = {
	face: HumanFace;
	facePointsRef: React.RefObject<Points>;
	faceMeshRef: React.RefObject<Mesh>;
};

const HumanFaceRender: FC<Props> = ({ face, facePointsRef, faceMeshRef }) => {
	const { task, currentPanel } = useContext(TaskContext);
	const { distanceTaskDto } = useContext(AnalyticalTaskContext);
	const {
		renderMode,
		primaryFaceVisibility,
		secondaryFaceVisibility,
		enableLightReflections
	} = useContext(SceneContext);

	const texture = face.texture
		? useTexture(`data:image/jpeg;base64,${face.texture}`)
		: null;

	const faceGeometry: BufferGeometry | undefined = useMemo(
		() => createFaceBufferGeometry(face),
		[face.geometry?.positions]
	);

	const getFaceColorProps = (isPointMesh: boolean) => {
		const primary = '#E0AC69';
		const secondary = '#EFC1A3';
		let color = face.isPrimaryFace ? primary : secondary;

		const isDistanceHeatmapShown =
			distanceTaskDto.heatmapRenderMode !== HeatmapRenderMode.None &&
			currentPanel === 'DISTANCE';

		const isCurvatureHeatmapShown = currentPanel === 'CURVATURE';

		if (!isPointMesh) {
			const faceColor = TaskUtils.isSingle(task)
				? primary
				: face.isPrimaryFace
				? primary
				: secondary;

			color =
				renderMode === 'TEXTURE' && !face.colorsOfHeatmap && face.texture
					? 'white'
					: faceColor;

			if (
				!face.isPrimaryFace &&
				(isDistanceHeatmapShown || isCurvatureHeatmapShown)
			) {
				color = 'white';
			}
		}

		return {
			vertexColors:
				!face.isPrimaryFace &&
				face?.colorsOfHeatmap !== undefined &&
				(isDistanceHeatmapShown || isCurvatureHeatmapShown),
			color
		};
	};

	const getFaceVisibility = () =>
		TaskUtils.isPair(task) && !face?.isPrimaryFace
			? secondaryFaceVisibility
			: primaryFaceVisibility;

	return (
		<>
			<points ref={facePointsRef} geometry={faceGeometry}>
				<pointsMaterial
					{...getFaceColorProps(true)}
					size={0.1}
					transparent // value not needed because it is boolean flag
					opacity={getFaceVisibility().opacity / 100}
					visible={renderMode === 'POINTS' && getFaceVisibility().showFace}
				/>
			</points>
			<mesh renderOrder={-1} ref={faceMeshRef} geometry={faceGeometry}>
				<meshPhongMaterial
					{...getFaceColorProps(false)}
					side={DoubleSide}
					wireframe={renderMode === 'WIREFRAME'}
					map={
						renderMode === 'TEXTURE' && !face.colorsOfHeatmap && face.texture
							? texture
							: undefined
					}
					visible={renderMode !== 'POINTS' && getFaceVisibility().showFace}
					opacity={getFaceVisibility().opacity / 100}
					specular={enableLightReflections ? '#404040' : 'black'}
					shininess={50}
					transparent
				/>
			</mesh>
		</>
	);
};

export default HumanFaceRender;

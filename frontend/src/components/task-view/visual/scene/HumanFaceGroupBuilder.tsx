import { useContext } from 'react';

import { SceneContext } from '../../../../providers/SceneContextProvider';

import HumanFaceGroupRender from './face/HumanFaceGroupRender';

const HumanFaceGroupBuilder = () => {
	const { faces } = useContext(SceneContext);

	return (
		// eslint-disable-next-line react/jsx-no-useless-fragment
		<>
			{faces?.map(face => (
				<HumanFaceGroupRender key={`face-render-${face.id}`} face={face} />
			))}
		</>
	);
};

export default HumanFaceGroupBuilder;

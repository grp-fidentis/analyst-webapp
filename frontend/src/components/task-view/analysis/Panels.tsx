import { TabPanel } from '@mui/lab';
import { Box } from '@mui/material';
import { FC } from 'react';

type Props = {
	panelData: JSX.Element[];
};

const Panels: FC<Props> = ({ panelData }) => (
	<>
		{panelData.map((panel, index) => (
			<TabPanel
				key={index}
				sx={{ padding: 0}}
				value={`${index + 1}`}
			>
				<Box
					sx={{
						overflowY: 'auto',
						maxHeight: 'calc(100vh - 150px)',
						padding: '20px'
					}}
					component="div"
				>
					{panel}
				</Box>
			</TabPanel>
		))}
	</>
);

export default Panels;

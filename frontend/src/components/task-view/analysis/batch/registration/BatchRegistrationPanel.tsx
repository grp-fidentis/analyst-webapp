import React, { useContext } from 'react';
import {
	Box,
	Button,
	Checkbox,
	Divider,
	FormControlLabel,
	Grid,
	IconButton,
	Input,
	MenuItem,
	Modal,
	Select,
	Slider,
	Stack,
	SxProps,
	TextField,
	Theme,
	Typography
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Help } from '@mui/icons-material';

import {
	AveragingMethod,
	RegistrationMethod
} from '../../../../../types/BatchProcessingTypes';
import LabeledTextField from '../../../../common/LabeledTextField';
import { LoadingOperation } from '../../../../../types/AnalyticalTaskTypes';
import useBatchTaskManagement from '../../../../../hooks/useBatchTaskManagement';
import { ComputingTasksContext } from '../../../../../providers/ComputingTasksContextProvider';
import { BatchProcessingTaskContext } from '../../../../../providers/BatchProcessingTaskContextProvider';

const BatchRegistrationPanel = () => {
	const { isComputingTaskId } = useContext(ComputingTasksContext);
	const { batchRegistrationTaskDto, setBatchRegistrationTaskDto, averageFace } =
		useContext(BatchProcessingTaskContext);

	const {
		taskFiles,
		selectedFileRegistration,
		isModalOpen,
		fileName,
		setFileName,
		isLoadingBatchInfo,
		exportAverageFace,
		handleAveragingOptionsChange,
		handleRegistrationOptionChange,
		handleCheckboxChange,
		handleChangeTemplateForRegistration,
		handleOpenModal,
		handleCloseModal,
		handleSaveAverageFaceToProject,
		handleSubmit,
		handleInterrupt
	} = useBatchTaskManagement();

	const boxSx: SxProps<Theme> = {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 400,
		bgcolor: 'background.paper',
		border: '2px solid #000',
		boxShadow: 24,
		p: 4
	};

	return (
		<>
			<Box component="form" onSubmit={handleSubmit}>
				<Stack direction="column" gap={2}>
					<Grid item xs={12} sx={{ textAlign: 'center' }}>
						<b>Dataset</b>
					</Grid>
					<LabeledTextField text="Template face:" labelMinWidth={270}>
						<Select
							disabled={
								isLoadingBatchInfo(LoadingOperation.Files, true) ||
								taskFiles.length === 0 ||
								isLoadingBatchInfo(LoadingOperation.Registration, true)
							}
							sx={{ minWidth: 230, ml: 2 }}
							name="templateFace"
							size="small"
							value={
								selectedFileRegistration !== null
									? `${selectedFileRegistration.name}.${selectedFileRegistration.extension}`
									: ''
							}
							onChange={handleChangeTemplateForRegistration}
							MenuProps={{
								PaperProps: {
									style: {
										maxHeight: 300,
										overflow: 'auto'
									}
								}
							}}
						>
							{taskFiles.map(file => (
								<MenuItem
									key={file.id}
									value={`${file.name}.${file.extension}`}
								>
									{`${file.name}.${file.extension}`}
								</MenuItem>
							))}
						</Select>
					</LabeledTextField>

					<Divider orientation="horizontal" color="#EEEEEE" />

					<Grid item xs={12} sx={{ textAlign: 'center' }}>
						<b>Averaging options</b>
					</Grid>
					<Stack direction="column" gap={2}>
						<LabeledTextField text="Averaging method:" labelMinWidth={270}>
							<Select
								disabled={isLoadingBatchInfo(
									LoadingOperation.Registration,
									true
								)}
								sx={{ minWidth: 230, ml: 2 }}
								name="averagingMethod"
								size="small"
								value={
									batchRegistrationTaskDto.averagingTaskDto.averagingMethod ||
									AveragingMethod.NearestNeighbours
								}
								onChange={e => {
									setBatchRegistrationTaskDto({
										...batchRegistrationTaskDto,
										averagingTaskDto: {
											...batchRegistrationTaskDto.averagingTaskDto,
											averagingMethod: e.target.value as AveragingMethod
										}
									});
								}}
							>
								<MenuItem value={AveragingMethod.NearestNeighbours}>
									Nearest neighbours
								</MenuItem>
								<MenuItem value={AveragingMethod.RayCasting}>
									Ray casting [experimental]
								</MenuItem>
								<MenuItem value={AveragingMethod.Skip}>
									Skip (use selected face for registration)
								</MenuItem>
							</Select>
						</LabeledTextField>

						<LabeledTextField
							text="Max. registration iterations:"
							labelMinWidth={270}
						>
							<>
								<Slider
									name="maxIterations"
									aria-labelledby="max-iterations-slider"
									disabled={isLoadingBatchInfo(
										LoadingOperation.Registration,
										true
									)}
									value={
										batchRegistrationTaskDto.averagingTaskDto
											.maxRegistrationIterations
									}
									onChange={(_, value) =>
										handleAveragingOptionsChange('iterations', value as number)
									}
									min={1}
									max={5}
									sx={{ maxWidth: 250 }}
								/>
								<span>
									<Input
										name="maxIterations"
										size="small"
										sx={{ width: '50px' }}
										disabled={isLoadingBatchInfo(
											LoadingOperation.Registration,
											true
										)}
										value={
											batchRegistrationTaskDto.averagingTaskDto
												.maxRegistrationIterations
										}
										onChange={e =>
											handleAveragingOptionsChange(
												'iterations',
												Number(e.target.value as unknown as number)
											)
										}
										inputProps={{
											step: 1,
											min: 1,
											max: 5,
											type: 'number'
										}}
									/>
								</span>
							</>
						</LabeledTextField>

						<LabeledTextField
							text="Stabilization threshold"
							labelMinWidth={270}
						>
							<>
								<Slider
									name="stabilizationTreshold"
									aria-labelledby="stabilization-treshold-slider"
									disabled={isLoadingBatchInfo(
										LoadingOperation.Registration,
										true
									)}
									value={
										batchRegistrationTaskDto.averagingTaskDto
											.stabilizationThreshold
									}
									onChange={(_, value) =>
										handleAveragingOptionsChange('threshold', value as number)
									}
									min={0}
									max={5}
									sx={{ maxWidth: 250 }}
								/>
								<span>
									<Input
										name="stabilizationTreshold"
										size="small"
										sx={{ width: '50px' }}
										disabled={isLoadingBatchInfo(
											LoadingOperation.Registration,
											true
										)}
										value={
											batchRegistrationTaskDto.averagingTaskDto
												.stabilizationThreshold
										}
										onChange={e =>
											handleAveragingOptionsChange(
												'threshold',
												Number(e.target.value as unknown as number)
											)
										}
										inputProps={{
											step: 0.1,
											min: 0,
											max: 5,
											type: 'number'
										}}
									/>
								</span>
							</>
						</LabeledTextField>
					</Stack>

					<Divider orientation="horizontal" color="#EEEEEE" />

					<Grid item xs={12} sx={{ textAlign: 'center' }}>
						<b>Registration options</b>
					</Grid>

					<Stack direction="column" gap={2}>
						<LabeledTextField text="Registration method:" labelMinWidth={270}>
							<Select
								disabled={isLoadingBatchInfo(
									LoadingOperation.Registration,
									true
								)}
								sx={{ minWidth: 230, ml: 2 }}
								name="averagingMethod"
								size="small"
								value={
									batchRegistrationTaskDto.registrationTaskDto
										.registrationMethod
								}
								onChange={e => {
									setBatchRegistrationTaskDto({
										...batchRegistrationTaskDto,
										registrationTaskDto: {
											...batchRegistrationTaskDto.registrationTaskDto,
											registrationMethod: e.target.value as RegistrationMethod
										}
									});
								}}
							>
								<MenuItem value={RegistrationMethod.MeshBasedICP}>
									mesh-based (ICP)
								</MenuItem>
								<MenuItem value={RegistrationMethod.FeaturePoints}>
									feature points (Procrustes)
								</MenuItem>
								<MenuItem value={RegistrationMethod.SkipRegistration}>
									Skip registration
								</MenuItem>
							</Select>
						</LabeledTextField>

						<LabeledTextField
							text="Registration subsampling (0 = off):"
							labelMinWidth={270}
						>
							<>
								<Slider
									name="registrationSubsampling"
									aria-labelledby="registration-subsampling-slider"
									disabled={isLoadingBatchInfo(
										LoadingOperation.Registration,
										true
									)}
									value={
										batchRegistrationTaskDto.registrationTaskDto
											.registrationSubsampling
									}
									onChange={(_, value) =>
										handleRegistrationOptionChange(value as number)
									}
									min={0}
									max={5000}
									sx={{ maxWidth: 250 }}
								/>
								<span>
									<Input
										name="registrationSubsampling"
										size="small"
										sx={{ width: '60px' }}
										disabled={isLoadingBatchInfo(
											LoadingOperation.Registration,
											true
										)}
										value={
											batchRegistrationTaskDto.registrationTaskDto
												.registrationSubsampling
										}
										onChange={e =>
											handleRegistrationOptionChange(
												Number(e.target.value as unknown as number)
											)
										}
										inputProps={{
											step: 1,
											min: 0,
											max: 5000,
											type: 'number'
										}}
									/>
								</span>
							</>
						</LabeledTextField>

						<FormControlLabel
							control={
								<Checkbox
									checked={
										batchRegistrationTaskDto.registrationTaskDto
											.scaleFacesDuringRegistration
									}
									name="scale"
									onChange={handleCheckboxChange}
									disabled={isLoadingBatchInfo(
										LoadingOperation.Registration,
										true
									)}
								/>
							}
							label="Scale faces during registration"
						/>
					</Stack>

					<Divider orientation="horizontal" color="#EEEEEE" />

					<Stack direction="row">
						<LoadingButton
							sx={{ ml: 3 }}
							variant="contained"
							disabled={
								isLoadingBatchInfo(LoadingOperation.Registration) ||
								isComputingTaskId(batchRegistrationTaskDto.taskId)
							}
							loading={isLoadingBatchInfo(LoadingOperation.Registration, true)}
							type="submit"
						>
							Compute
						</LoadingButton>
						<LoadingButton
							sx={{ ml: 3 }}
							variant="contained"
							disabled={
								isLoadingBatchInfo(LoadingOperation.Registration, true) ||
								!averageFace ||
								isComputingTaskId(batchRegistrationTaskDto.taskId)
							}
							onClick={handleOpenModal}
						>
							Add average face to project
						</LoadingButton>
						<LoadingButton
							sx={{ ml: 3 }}
							variant="contained"
							disabled={
								isLoadingBatchInfo(LoadingOperation.Registration, true) ||
								!averageFace ||
								isComputingTaskId(batchRegistrationTaskDto.taskId)
							}
							onClick={exportAverageFace}
						>
							Export AVG face
						</LoadingButton>
						<LoadingButton
							sx={{ ml: 3 }}
							variant="contained"
							disabled={
								!isLoadingBatchInfo(LoadingOperation.Registration, true) &&
								!isComputingTaskId(batchRegistrationTaskDto.taskId)
							}
							onClick={handleInterrupt}
						>
							Interrupt
						</LoadingButton>
						<IconButton
							sx={{ ml: 2 }}
							component="a"
							href="https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/batch-registration"
							target="_blank"
							rel="noopener noreferrer"
						>
							<Help fontSize="medium" color="primary" />
						</IconButton>
					</Stack>
				</Stack>
			</Box>
			<Modal
				open={isModalOpen}
				onClose={handleCloseModal}
				aria-labelledby="modal-title"
				aria-describedby="modal-description"
			>
				<Box sx={boxSx} component="div">
					<Typography id="modal-title" variant="h6" component="h2">
						Save Average Face
					</Typography>
					<TextField
						id="file-name"
						label="New file name"
						fullWidth
						value={fileName}
						onChange={e => setFileName(e.target.value)}
						sx={{ mt: 2 }}
					/>
					<Stack direction="row" spacing={2} sx={{ mt: 2 }}>
						<Button
							variant="contained"
							color="primary"
							onClick={() => handleSaveAverageFaceToProject()}
						>
							Save
						</Button>
						<Button variant="outlined" onClick={handleCloseModal}>
							Cancel
						</Button>
					</Stack>
				</Box>
			</Modal>
		</>
	);
};

export default BatchRegistrationPanel;

import React, { useMemo } from 'react';
import {
	Button,
	FormControlLabel,
	Grid,
	IconButton,
	InputLabel,
	MenuItem,
	Select,
	Switch,
	Typography
} from '@mui/material';
import { Help } from '@mui/icons-material';

import CanvasHeatmap from '../../../../visualization/CanvasHeatmap';
import useHeatmapManagement from '../../../../../hooks/visualization/useHeatmapManagement';
import { LinkageStrategy } from '../../../../../types/BatchVisualizationTypes';
import OpenSingleTaskConfirmationDialog from '../../../../projects-view/dialogs/OpenSingleTaskConfirmationDialog';
import ContextMenu from '../../../../visualization/ContextMenu';
import OpenPairTaskConfirmationDialog from '../../../../projects-view/dialogs/OpenPairTaskConfirmationDialog';
import { binWidth } from '../../../../visualization/visualizationConfig';

const BatchVisualizationPanel = () => {
	const {
		loading,
		heatmapRows,
		scaleAxisX,
		scaleAxisY,
		colorScale,
		selectionMode,
		handleRowOrColumnClick,
		handleOpenPairTask,
		hideRowAndColumn,
		hiddenItems,
		rowState,
		columnState,
		transform,
		handleHeatmapDrag,
		finalizeDrag,
		openPairTaskConfirmation,
		localLinkageStrategy,
		filesPair,
		handleOpenSingleTask,
		handleKeyDown,
		openSingleTaskConfirmation,
		clusteringSteps,
		calculateRowPositions,
		contextMenu,
		handleShowFace,
		handleShowFaces,
		handleContextMenu,
		handleKeyUp,
		distances,
		resetSelected,
		handleCloseContextMenu,
		showHidden,
		hideBoth,
		setHiddenItems,
		toggleSelectionMode,
		setOpenPairTaskConfirmation,
		setLocalLinkageStrategy,
		setOpenSingleTaskConfirmation,
		handleShowDendrogram
	} = useHeatmapManagement();

	const rowPositions = useMemo(
		() => calculateRowPositions(heatmapRows, scaleAxisY, binWidth),
		[heatmapRows]
	);

	const memoizedClusteringSteps = useMemo(
		() => clusteringSteps,
		[clusteringSteps]
	);

	if (!heatmapRows.length) {
		return (
			<Grid container>
				<Grid item xs={12} sx={{ textAlign: 'center' }}>
					<p>No data, compute distances first...</p>
				</Grid>
			</Grid>
		);
	}

	return (
		<Grid container>
			<Grid container direction="column" spacing={1} gap={1}>
				<Grid
					container
					justifyContent="flex-start"
					alignItems="center"
					spacing={2}
				>
					<Grid item>
						<InputLabel id="row-select-label">Hidden Faces</InputLabel>
						<Select
							multiple
							value={hiddenItems.selectedFaces}
							onChange={e =>
								setHiddenItems({
									...hiddenItems,
									selectedFaces: e.target.value as string[]
								})
							}
							sx={{ width: '150px', height: '30px' }}
							displayEmpty
							renderValue={selected => (selected.length > 0 ? selected[0] : '')}
							MenuProps={{
								PaperProps: {
									style: {
										maxHeight: 300,
										overflow: 'auto'
									}
								}
							}}
						>
							{hiddenItems.faces.map(faceName => (
								<MenuItem key={faceName} value={faceName}>
									{faceName}
								</MenuItem>
							))}
						</Select>
					</Grid>

					<Grid item>
						<Button
							variant="contained"
							onClick={() => showHidden(hiddenItems.selectedFaces)}
							sx={{ height: '30px', marginTop: '22px' }}
						>
							Show Hidden
						</Button>
					</Grid>
					{(rowState.lastClickedIndex !== null ||
						columnState.lastClickedIndex !== null) && (
						<Grid item>
							<Button
								variant="contained"
								onClick={() => resetSelected()}
								sx={{ height: '30px', marginTop: '22px' }}
							>
								Clear selected
							</Button>
						</Grid>
					)}
				</Grid>

				<Grid
					container
					justifyContent="flex-start"
					alignItems="center"
					spacing={2}
				>
					<Grid item>
						<Typography sx={{ fontWeight: 'bold' }}>Linkage method:</Typography>
					</Grid>

					<Grid item>
						<Select
							sx={{ width: '130px', height: '30px' }}
							value={localLinkageStrategy}
							onChange={e =>
								setLocalLinkageStrategy(e.target.value as LinkageStrategy)
							}
						>
							<MenuItem value={LinkageStrategy.SINGLE}>Single</MenuItem>
							<MenuItem value={LinkageStrategy.COMPLETE}>Complete</MenuItem>
							<MenuItem value={LinkageStrategy.AVERAGE}>Average</MenuItem>
						</Select>
					</Grid>

					<Grid item>
						<Button
							variant="contained"
							sx={{ height: '30px' }}
							onClick={handleShowDendrogram}
						>
							Make clustering
						</Button>
					</Grid>
				</Grid>

				<Grid
					container
					justifyContent="flex-start"
					alignItems="center"
					spacing={2}
				>
					<Grid item>
						<span style={{ fontWeight: 'bold' }}>Layout mode:</span>
					</Grid>
					<Grid item>
						<FormControlLabel
							control={
								<Switch
									checked={selectionMode}
									onChange={() => toggleSelectionMode()}
									color="primary"
								/>
							}
							label={`${selectionMode ? 'on' : 'off'}`}
							sx={{
								height: '30px',
								width: '130px',
								alignItems: 'center',
								justifyContent: 'center'
							}}
						/>
					</Grid>
					<Grid item>
						<IconButton
							sx={{ ml: 2, marginRight: '91px' }}
							component="a"
							href="https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/Batch-visialization"
							target="_blank"
							rel="noopener noreferrer"
						>
							<Help fontSize="medium" color="primary" />
						</IconButton>
					</Grid>
				</Grid>
			</Grid>
			<Grid item xs={12} sx={{ textAlign: 'center' }}>
				{loading ? (
					<p>Loading heatmap...</p>
				) : (
					<div
						style={{
							width: '100%',
							height: 'calc(100vh - 325px)',
							display: 'flex',
							outline: 'none'
						}}
						onMouseMove={handleHeatmapDrag}
						onMouseUp={finalizeDrag}
						onKeyDown={handleKeyDown}
						onKeyUp={handleKeyUp}
						role="button"
						tabIndex={0}
					>
						<CanvasHeatmap
							heatmapRows={heatmapRows}
							scaleAxisX={scaleAxisX}
							scaleAxisY={scaleAxisY}
							colorScale={colorScale}
							handleContextMenu={handleContextMenu}
							handleRowOrColumnClick={handleRowOrColumnClick}
							rowState={rowState}
							columnState={columnState}
							selectionMode={selectionMode}
							transform={transform}
							clusteringSteps={memoizedClusteringSteps}
							rowPositions={rowPositions}
							distances={distances}
						/>
					</div>
				)}
			</Grid>
			<ContextMenu
				contextMenu={contextMenu}
				onClose={handleCloseContextMenu}
				hideRowAndColumn={hideRowAndColumn}
				handleOpenSingleTask={handleOpenSingleTask}
				handleOpenPairTask={handleOpenPairTask}
				handleShowFace={handleShowFace}
				handleShowFaces={handleShowFaces}
				hideBoth={hideBoth}
			/>
			{openPairTaskConfirmation && (
				<OpenPairTaskConfirmationDialog
					open={openPairTaskConfirmation}
					onClose={() => setOpenPairTaskConfirmation(false)}
					selectedFiles={filesPair}
				/>
			)}
			{openSingleTaskConfirmation && (
				<OpenSingleTaskConfirmationDialog
					open={openSingleTaskConfirmation}
					onClose={() => setOpenSingleTaskConfirmation(false)}
					fileId={filesPair[0].id}
					faceName={filesPair[0].name}
				/>
			)}
		</Grid>
	);
};

export default BatchVisualizationPanel;

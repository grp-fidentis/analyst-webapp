import React, { useContext } from 'react';
import { Box, Divider, Grid, MenuItem, Select, Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab';

import LabeledTextField from '../../../../common/LabeledTextField';
import useBatchTaskManagement from '../../../../../hooks/useBatchTaskManagement';
import { LoadingOperation } from '../../../../../types/AnalyticalTaskTypes';
import { SimilarityMethod } from '../../../../../types/BatchProcessingTypes';
import { ComputingTasksContext } from '../../../../../providers/ComputingTasksContextProvider';
import { BatchProcessingTaskContext } from '../../../../../providers/BatchProcessingTaskContextProvider';

const BatchDistancePanel = () => {
	const { isComputingTaskId } = useContext(ComputingTasksContext);
	const {
		batchDistanceTaskDto,
		setBatchDistanceTaskDto,
		batchDistanceForTask
	} = useContext(BatchProcessingTaskContext);

	const {
		taskFiles,
		selectedFileDistance,
		computeDistance,
		isLoadingBatchInfo,
		exportDistanceResults,
		handleChangeTemplateForDistance,
		handleSubmit,
		handleInterrupt
	} = useBatchTaskManagement();

	return (
		<Box component="form" onSubmit={handleSubmit}>
			<Stack direction="column" gap={2}>
				<Grid item xs={12} sx={{ textAlign: 'center' }}>
					<b>Dataset</b>
				</Grid>
				<LabeledTextField text="Template face:" labelMinWidth={270}>
					<Select
						disabled={
							isLoadingBatchInfo(LoadingOperation.Files, true) ||
							taskFiles.length === 0 ||
							isLoadingBatchInfo(LoadingOperation.Distance, true)
						}
						sx={{ minWidth: 230, ml: 2 }}
						name="templateFace"
						size="small"
						value={
							selectedFileDistance !== null
								? `${selectedFileDistance.name}.${selectedFileDistance.extension}`
								: ''
						}
						onChange={handleChangeTemplateForDistance}
						MenuProps={{
							PaperProps: {
								style: {
									maxHeight: 300,
									overflow: 'auto'
								}
							}
						}}
					>
						{taskFiles.map(file => (
							<MenuItem key={file.id} value={`${file.name}.${file.extension}`}>
								{`${file.name}.${file.extension}`}
							</MenuItem>
						))}
					</Select>
				</LabeledTextField>
				<Divider />
				<Grid item xs={12} sx={{ textAlign: 'center' }}>
					<b>Similarity options</b>
				</Grid>
				<LabeledTextField text="Similarity method:" labelMinWidth={270}>
					<Select
						disabled={isLoadingBatchInfo(LoadingOperation.Distance, true)}
						sx={{ minWidth: 230, ml: 2 }}
						name="similarityMethod"
						size="small"
						value={
							batchDistanceTaskDto.similarityMethod ||
							SimilarityMethod.IndirectDistance
						}
						onChange={e => {
							setBatchDistanceTaskDto({
								...batchDistanceTaskDto,
								similarityMethod: e.target.value as SimilarityMethod
							});
						}}
					>
						<MenuItem value={SimilarityMethod.IndirectDistance}>
							Indirect - distance
						</MenuItem>
						<MenuItem value={SimilarityMethod.IndirectVectors}>
							Indirect - vectors
						</MenuItem>
						<MenuItem value={SimilarityMethod.IndirectCombined}>
							Indirect - combined
						</MenuItem>
						<MenuItem value={SimilarityMethod.IndirectRayCasting}>
							Indirect - ray casting
						</MenuItem>
						<MenuItem value={SimilarityMethod.PairwiseTwoWay}>
							Pairwise two-way (very slow)
						</MenuItem>
						<MenuItem value={SimilarityMethod.PairwiseTwoWayCropped}>
							Pairwise two-way - cropped (very slow)
						</MenuItem>
					</Select>
				</LabeledTextField>

				<Divider orientation="horizontal" color="#EEEEEE" />

				<Stack direction="row">
					<LoadingButton
						sx={{ ml: 3 }}
						variant="contained"
						disabled={
							isLoadingBatchInfo(LoadingOperation.Distance) ||
							isComputingTaskId(batchDistanceTaskDto.taskId)
						}
						loading={isLoadingBatchInfo(LoadingOperation.Distance, true)}
						onClick={computeDistance}
					>
						Compute
					</LoadingButton>
					<LoadingButton
						sx={{ ml: 3 }}
						variant="contained"
						disabled={
							isLoadingBatchInfo(LoadingOperation.Distance, true) ||
							!batchDistanceForTask ||
							isComputingTaskId(batchDistanceTaskDto.taskId)
						}
						onClick={exportDistanceResults}
					>
						Export results
					</LoadingButton>
					<LoadingButton
						sx={{ ml: 3 }}
						variant="contained"
						disabled={
							!isLoadingBatchInfo(LoadingOperation.Distance, true) &&
							!isComputingTaskId(batchDistanceTaskDto.taskId)
						}
						onClick={handleInterrupt}
					>
						Interrupt
					</LoadingButton>
				</Stack>
			</Stack>
		</Box>
	);
};

export default BatchDistancePanel;

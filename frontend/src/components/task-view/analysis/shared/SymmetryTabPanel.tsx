import {
	ChangeEvent,
	FC,
	SyntheticEvent,
	useContext,
	useEffect,
	useRef,
	useState
} from 'react';
import {
	Box,
	Divider,
	Grid,
	IconButton,
	Input,
	MenuItem,
	Select,
	SelectChangeEvent,
	Slider,
	Stack,
	Tooltip,
	Typography
} from '@mui/material';
import { toast } from 'react-toastify';
import { LoadingButton } from '@mui/lab';
import { Help, HelpOutline } from '@mui/icons-material';

import LabeledTextField from '../../../common/LabeledTextField';
import { TaskType } from '../../../../types/TaskTypes';
import { AnalyticalTaskService } from '../../../../services/AnalyticalTaskService';
import {
	LoadingOperation,
	PointSamplingStrategy,
	SamplingTaskDto,
	SymmetryMethod,
	SymmetryTaskDto
} from '../../../../types/AnalyticalTaskTypes';
import { TaskContext } from '../../../../providers/TaskContextProvider';
import useThrottle from '../../../../hooks/useThrottle';
import { FaceUtils } from '../../../../utils/FaceUtils';
import { SceneContext } from '../../../../providers/SceneContextProvider';
import { AnalyticalTaskContext } from '../../../../providers/AnalyticalTaskContextProvider';
import { HumanFace } from '../../../../types/HumanFaceTypes';
import { TaskUtils } from '../../../../utils/TaskUtils';

type Props = {
	taskType: TaskType;
};

const SymmetryTabPanel: FC<Props> = ({ taskType }) => {
	const { task } = useContext(TaskContext);
	const {
		faces,
		updateFace,
		primaryFaceVisibility,
		secondaryFaceVisibility,
		setPrimaryFaceVisibility,
		setSecondaryFaceVisibility
	} = useContext(SceneContext);
	const {
		isLoadingInfo,
		updateLoadingInfo,
		createCuttingPlanes,
		calculateSamples
	} = useContext(AnalyticalTaskContext);
	const [symmetryDto, setSymmetryDto] = useState<SymmetryTaskDto>({
		taskId: task?.id ?? -1,
		symmetryMethod: SymmetryMethod.FastMeshSurface,
		candidateSearchSampling: {
			samplingStrategy: PointSamplingStrategy.UniformSpaceSampling,
			samplingStrength: 200
		},
		candidatePruningSampling: {
			samplingStrategy: PointSamplingStrategy.UniformSpaceSampling,
			samplingStrength: 200
		}
	});
	const facesRef = useRef<HumanFace[]>();
	facesRef.current = faces;

	const calculateSamplePointsThrottled = useThrottle(
		async (dto, calculateSamples) => {
			await calculateSamples(dto);
		},
		300
	);

	useEffect(() => {
		if (TaskUtils.isSingle(task)) {
			const samplingTaskDto: SamplingTaskDto = {
				...symmetryDto.candidateSearchSampling,
				taskId: task?.id ?? -1
			};
			calculateSamplePointsThrottled(samplingTaskDto, calculateSamples);
		}
	}, [
		symmetryDto.candidateSearchSampling.samplingStrength,
		symmetryDto.candidateSearchSampling.samplingStrategy
	]);

	useEffect(() => {
		const primarySymmetryPlane = primaryFaceVisibility.showSymmetryPlane;
		const secondarySymmetryPlane = secondaryFaceVisibility.showSymmetryPlane;
		setPrimaryFaceVisibility(v => ({ ...v, showSymmetryPlane: true }));
		setSecondaryFaceVisibility(v => ({ ...v, showSymmetryPlane: true }));

		return () => {
			setPrimaryFaceVisibility(v => ({
				...v,
				showSymmetryPlane: primarySymmetryPlane
			}));
			setSecondaryFaceVisibility(v => ({
				...v,
				showSymmetryPlane: secondarySymmetryPlane
			}));

			if (TaskUtils.isSingle(task)) {
				updateFace(
					{
						samples: undefined,
						id: faces[0].id
					},
					facesRef.current
				);
			}
		};
	}, []);

	const handleSymmetryMethodChange = (
		e:
			| ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
			| SelectChangeEvent<string>
	) => {
		const symmetryMethod = e.target.value as SymmetryMethod;
		const newSearchStrength: number =
			symmetryMethod === SymmetryMethod.RobustMeshVertices ? 100 : 200;
		const newPruningStrength: number =
			symmetryMethod === SymmetryMethod.RobustMeshVertices ? 1000 : 200;

		setSymmetryDto({
			...symmetryDto,
			symmetryMethod,
			candidateSearchSampling: {
				...symmetryDto.candidateSearchSampling,
				samplingStrength: newSearchStrength
			},
			candidatePruningSampling: {
				...symmetryDto.candidatePruningSampling,
				samplingStrength: newPruningStrength
			}
		});
	};

	const handleSamplingStrengthChange = (which: string, value: number) => {
		if (which === 'search') {
			setSymmetryDto({
				...symmetryDto,
				candidateSearchSampling: {
					...symmetryDto.candidateSearchSampling,
					samplingStrength: value
				}
			});
		} else {
			setSymmetryDto({
				...symmetryDto,
				candidatePruningSampling: {
					...symmetryDto.candidatePruningSampling,
					samplingStrength: value
				}
			});
		}
	};

	const handleSubmit = async (e: SyntheticEvent) => {
		e.preventDefault();

		updateLoadingInfo(true, LoadingOperation.Symmetry);
		try {
			const symmetryResponseTaskDtos =
				await AnalyticalTaskService.calculateSymmetryPlane(symmetryDto);

			symmetryResponseTaskDtos.forEach(s => {
				const face: HumanFace = {
					id: s.humanFaceId,
					symmetryPlane: s.symmetryPlane,
					samples: faces.find(f => f.id === s.humanFaceId)?.samples
				};
				updateFace(face);
			});

			createCuttingPlanes(task?.id ?? -1, true);
			symmetryResponseTaskDtos.every(s => s.symmetryPlane.positions.length > 0)
				? toast.success('Symmetry plane computed')
				: toast.warn(
						'Symmetry plane could not be computed with current parameters'
				  );
		} finally {
			updateLoadingInfo(false);
		}
	};

	return (
		<Stack direction="column" gap={2}>
			<Box component="form" onSubmit={handleSubmit}>
				<Stack direction="column" gap={2}>
					<Grid item xs={12} sx={{ textAlign: 'center' }}>
						<b>Symmetry method</b>
					</Grid>
					<Stack direction="row" justifyContent="center" alignItems="center">
						Compute from
						<Select
							disabled={isLoadingInfo(LoadingOperation.Symmetry, true)}
							sx={{ minWidth: 230, ml: 2 }}
							name="symmetryMethod"
							size="small"
							value={symmetryDto.symmetryMethod}
							onChange={handleSymmetryMethodChange}
						>
							<MenuItem value={SymmetryMethod.FastMeshSurface}>
								Mesh surface (fast)
							</MenuItem>
							<MenuItem value={SymmetryMethod.RobustMeshSurface}>
								Mesh surface (robust)
							</MenuItem>
							<MenuItem value={SymmetryMethod.RobustMeshVertices}>
								Mesh vertices (robust)
							</MenuItem>
							<MenuItem
								disabled={faces.some(face => !face.hasFeaturePoints)}
								value={SymmetryMethod.FeaturePoints}
							>
								feature points
							</MenuItem>
						</Select>
						<LoadingButton
							sx={{ ml: 3 }}
							variant="contained"
							disabled={isLoadingInfo(LoadingOperation.Symmetry)}
							loading={isLoadingInfo(LoadingOperation.Symmetry, true)}
							type="submit"
						>
							Compute
						</LoadingButton>
						<IconButton
							sx={{ ml: 2 }}
							component="a"
							href="https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/symmetry"
							target="_blank"
							rel="noopener noreferrer"
						>
							<Help fontSize="medium" color="primary" />
						</IconButton>
					</Stack>

					{symmetryDto.symmetryMethod !== SymmetryMethod.FeaturePoints && (
						<Box component="div">
							<Divider orientation="horizontal" color="#EEEEEE" />

							<Grid item xs={12} sx={{ pt: 2, pb: 2, textAlign: 'center' }}>
								<b>Parameters</b>
							</Grid>
							<Stack direction="column" gap={2}>
								<LabeledTextField
									text="Sub-sampling strategy:"
									labelMinWidth={270}
								>
									<Select
										size="small"
										name="samplingStrategy"
										sx={{ minWidth: 270 }}
										value={symmetryDto.candidateSearchSampling.samplingStrategy}
										disabled={isLoadingInfo(LoadingOperation.Samples)}
										onChange={e => {
											setSymmetryDto({
												...symmetryDto,
												candidateSearchSampling: {
													...symmetryDto.candidateSearchSampling,
													samplingStrategy: e.target
														.value as PointSamplingStrategy
												},
												candidatePruningSampling: {
													...symmetryDto.candidatePruningSampling,
													samplingStrategy: e.target
														.value as PointSamplingStrategy
												}
											});
										}}
									>
										<MenuItem
											value={PointSamplingStrategy.UniformSpaceSampling}
										>
											Uniform Space Sampling
										</MenuItem>
										<MenuItem value={PointSamplingStrategy.RandomSampling}>
											Random Sampling
										</MenuItem>
										<MenuItem value={PointSamplingStrategy.MeanCurvature}>
											Mean curvature
										</MenuItem>
										<MenuItem value={PointSamplingStrategy.GaussianCurvature}>
											Gaussian Curvature
										</MenuItem>
										<MenuItem value={PointSamplingStrategy.MaxCurvature}>
											Max Curvature
										</MenuItem>
										<MenuItem value={PointSamplingStrategy.MinCurvature}>
											Min Curvature
										</MenuItem>
										<MenuItem
											value={PointSamplingStrategy.NormalPoissonDiskSubSampling}
										>
											Poisson disk sub sampling
										</MenuItem>
									</Select>
								</LabeledTextField>

								<LabeledTextField
									text="Strength for candidate search:"
									labelMinWidth={270}
								>
									<>
										<Slider
											name="samplingStrength"
											aria-labelledby="sampling-strength-1-slider"
											disabled={isLoadingInfo(LoadingOperation.Samples)}
											value={
												symmetryDto.candidateSearchSampling.samplingStrength
											}
											onChange={(_, value) =>
												handleSamplingStrengthChange('search', value as number)
											}
											min={10}
											max={
												symmetryDto.symmetryMethod ===
												SymmetryMethod.RobustMeshVertices
													? 1000
													: 500
											}
											sx={{ maxWidth: 250 }}
										/>
										<span>
											<Input
												name="samplingStrength"
												size="small"
												sx={{ width: '58px' }}
												disabled={isLoadingInfo(LoadingOperation.Samples)}
												value={
													symmetryDto.candidateSearchSampling.samplingStrength
												}
												onChange={e =>
													handleSamplingStrengthChange(
														'search',
														Number(e.target.value as unknown as number)
													)
												}
												inputProps={{
													step: 1,
													min: 10,
													max:
														symmetryDto.symmetryMethod ===
														SymmetryMethod.RobustMeshVertices
															? 1000
															: 500,
													type: 'number'
												}}
											/>
										</span>
									</>
								</LabeledTextField>

								{symmetryDto.symmetryMethod !==
									SymmetryMethod.FastMeshSurface && (
									<LabeledTextField
										text="Strength for pruning candidates:"
										labelMinWidth={270}
									>
										<>
											<Slider
												name="samplingStrength"
												aria-labelledby="sampling-strength-2-slider"
												disabled={isLoadingInfo(LoadingOperation.Samples)}
												value={
													symmetryDto.candidatePruningSampling.samplingStrength
												}
												onChange={(_, value) =>
													handleSamplingStrengthChange(
														'pruning',
														value as number
													)
												}
												min={10}
												max={
													symmetryDto.symmetryMethod ===
													SymmetryMethod.RobustMeshSurface
														? 500
														: 5000
												}
												sx={{ maxWidth: 250 }}
											/>
											<span>
												<Input
													name="samplingStrength"
													size="small"
													sx={{ width: '58px' }}
													disabled={isLoadingInfo(LoadingOperation.Samples)}
													value={
														symmetryDto.candidatePruningSampling
															.samplingStrength
													}
													onChange={e =>
														handleSamplingStrengthChange(
															'pruning',
															Number(e.target.value as unknown as number)
														)
													}
													inputProps={{
														step: 1,
														min: 10,
														max:
															symmetryDto.symmetryMethod ===
															SymmetryMethod.RobustMeshVertices
																? 5000
																: 500,
														type: 'number'
													}}
												/>
											</span>
										</>
									</LabeledTextField>
								)}
							</Stack>
						</Box>
					)}
				</Stack>
			</Box>

			<Divider orientation="horizontal" color="#EEEEEE" />

			<Grid
				direction="row"
				container
				gap={2}
				sx={{ justifyContent: 'space-around' }}
			>
				<Grid item xs={12} sx={{ pb: 1, textAlign: 'center' }}>
					<b>Measurements</b>
				</Grid>
				<>
					{faces.map((face, i) => (
						<Grid item xs={12} key={i}>
							<LabeledTextField
								labelMinWidth={210}
								text={`${
									faces.length === 1
										? 'Face'
										: face.isPrimaryFace
										? 'Primary face'
										: 'Secondary face'
								} precision: `}
							>
								<>
									<Typography
										sx={{
											textAlign: 'center',
											width: '65px',
											backgroundColor: '#f1f1f1',
											paddingX: '7px',
											fontFamily: 'monospace'
										}}
									>
										{face.symmetryPlane?.precision
											? Number(face.symmetryPlane?.precision).toFixed(3)
											: '-'}
									</Typography>
									<Tooltip title="The smaller the value, the more accurate the result = zero value is best fit">
										<HelpOutline color="action" fontSize="small" />
									</Tooltip>
								</>
							</LabeledTextField>
						</Grid>
					))}
				</>
			</Grid>
		</Stack>
	);
};

export default SymmetryTabPanel;

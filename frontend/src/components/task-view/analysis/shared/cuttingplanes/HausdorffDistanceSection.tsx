import { Help } from '@mui/icons-material';
import {
	Box,
	Input,
	Slider,
	Stack,
	styled,
	Tooltip,
	tooltipClasses,
	TooltipProps,
	Typography
} from '@mui/material';
import React, { FC, useContext } from 'react';

import { CuttingPlanesTaskDto } from '../../../../../types/AnalyticalTaskTypes';
import LabeledTextField from '../../../../common/LabeledTextField';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import { TaskUtils } from '../../../../../utils/TaskUtils';
import { TaskContext } from '../../../../../providers/TaskContextProvider';

type Props = {
	dto: CuttingPlanesTaskDto;
};

const HausdorffDistanceSection: FC<Props> = ({ dto }) => {
	const { setCuttingPlanesTaskDto } = useContext(AnalyticalTaskContext);
	const { task } = useContext(TaskContext);

	const HtmlTooltip = styled(({ className, ...props }: TooltipProps) => (
		<Tooltip {...props} classes={{ popper: className }} />
	))(() => ({
		[`& .${tooltipClasses.tooltip}`]: {}
	}));

	const getHausdorffDistance = (order: number) => {
		const hausdorffDistance = dto.projectionResponse?.hausdorffDistances?.find(
			h => h?.order === order
		);
		if (hausdorffDistance && Number.isFinite(hausdorffDistance.distance)) {
			return hausdorffDistance?.distance.toFixed(3);
		} else if (
			hausdorffDistance &&
			!Number.isFinite(hausdorffDistance.distance)
		) {
			return 'infinite';
		} else {
			return 'hidden';
		}
	};

	return (
		<Box component="div" sx={{ width: 200 }}>
			<Stack direction="column" gap={2}>
				{TaskUtils.isPair(task) && (
					<>
						<b> Cross-section similarity</b>
						<Stack direction="row" alignItems="center">
							<Typography sx={{ pr: 1.5 }}>Sampling:</Typography>
							<HtmlTooltip
								title={
									<div>
										Sampling determines the percentage of curve points that are
										used for Hausdorff distance computation.
										<br />
										<br />
										<strong>Lower sampling value</strong> means faster <br />
										computation and possibly less accurate result.
										<br />
										<br />
										<strong>Higher sampling value</strong> means slower
										<br />
										computation and possibly more accurate results.
									</div>
								}
							>
								<Help fontSize="small" color="primary" />
							</HtmlTooltip>
						</Stack>
						<Stack direction="row" alignItems="center">
							<Slider
								name="vectorSize"
								value={dto.state?.samplingStrength}
								onChange={(_, value) => {
									setCuttingPlanesTaskDto(prev => ({
										...prev,
										state: {
											...prev.state,
											samplingStrength: value as number
										}
									}));
								}}
								min={0}
								max={100}
								sx={{ width: 120 }}
							/>
							<Input
								name="samplingStrength"
								size="small"
								sx={{ width: '42px', ml: 3 }}
								value={dto.state?.samplingStrength}
								onChange={e =>
									setCuttingPlanesTaskDto(prev => ({
										...prev,
										state: {
											...prev.state,
											samplingStrength: Number(e.target.value)
										}
									}))
								}
								inputProps={{
									step: 1,
									min: 0,
									max: 100,
									type: 'number'
								}}
							/>
							<Typography>%</Typography>
						</Stack>

						<Box component="div">
							<Stack direction="column" gap={3} />
							<b>Hausdorff Distances:</b>

							<LabeledTextField text="Cutting Plane 1:" labelMinWidth={140}>
								<Typography>{getHausdorffDistance(1)}</Typography>
							</LabeledTextField>

							<LabeledTextField text="Cutting Plane 2:" labelMinWidth={140}>
								<Typography>{getHausdorffDistance(2)}</Typography>
							</LabeledTextField>

							<LabeledTextField text="Cutting Plane 3:" labelMinWidth={140}>
								<Typography>{getHausdorffDistance(3)}</Typography>
							</LabeledTextField>
						</Box>
					</>
				)}

				<Stack direction="row" alignItems="center">
					<Typography sx={{ pr: 1.5 }}>Reduce vertices:</Typography>
				</Stack>
				<Stack direction="row" alignItems="center">
					<Slider
						name="reducedVertices"
						value={dto.state?.reducedVertices}
						onChange={(_, value) => {
							setCuttingPlanesTaskDto(prev => ({
								...prev,
								state: {
									...prev.state,
									reducedVertices: value as number
								}
							}));
						}}
						min={0}
						max={100}
						sx={{ width: 120 }}
					/>
					<Input
						name="reducedVertices"
						size="small"
						sx={{ width: '42px', ml: 3 }}
						value={dto.state?.reducedVertices}
						onChange={e =>
							setCuttingPlanesTaskDto(prev => ({
								...prev,
								state: {
									...prev.state,
									reducedVertices: Number(e.target.value)
								}
							}))
						}
						inputProps={{
							step: 1,
							min: 0,
							max: 100,
							type: 'number'
						}}
					/>
					<Typography>%</Typography>
				</Stack>
			</Stack>
		</Box>
	);
};

export default HausdorffDistanceSection;

import React, { ChangeEvent, FC, useContext, useRef } from 'react';
import {
	Box,
	Checkbox,
	FormControlLabel,
	Grid,
	Input,
	MenuItem,
	Radio,
	Select,
	SelectChangeEvent,
	Slider,
	Stack,
	Typography
} from '@mui/material';

import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import {
	CuttingPlaneFrontendState,
	CuttingPlanesDirection,
	CuttingPlanesTaskDto,
	CuttingPlanesType
} from '../../../../../types/AnalyticalTaskTypes';
import TooltipIconButton from '../../../../common/TooltipIconButton';
import { ReactComponent as ShowIcon } from '../../../../../assets/images/show.svg';
import { ReactComponent as HorizontalIcon } from '../../../../../assets/images/horiz.svg';
import { ReactComponent as VerticalIcon } from '../../../../../assets/images/vert.svg';
import { ReactComponent as FrontIcon } from '../../../../../assets/images/front.svg';
import Scene from '../../../visual/scene/Scene';
import { TaskUtils } from '../../../../../utils/TaskUtils';
import { CanvasUtils } from '../../../../../utils/CanvasUtils';

type CuttingPlaneInputGroupProps = {
	dto: CuttingPlanesTaskDto;
	plane: CuttingPlaneFrontendState;
};

const CuttingPlaneInputGroup: FC<CuttingPlaneInputGroupProps> = ({
	dto,
	plane
}) => {
	const { task } = useContext(TaskContext);
	const { faces } = useContext(SceneContext);
	const { setSecondaryFaceVisibility } = useContext(SceneContext);
	const { calculate2DProjection, setCuttingPlanesTaskDto } = useContext(
		AnalyticalTaskContext
	);
	const boxRef = useRef<HTMLDivElement>(null);

	const changeVisibility = () =>
		updateCuttingPlanes(() => {
			plane.isVisible = !plane.isVisible;
		});

	const changeHover = (isHovered: boolean) =>
		updateCuttingPlanes(() => {
			plane.isHover = isHovered;
		});

	const changeSliderValue = (value: number) =>
		updateCuttingPlanes(() => {
			plane.sliderValue = value;

			const face = TaskUtils.isPair(task)
				? faces.find(f => f.isPrimaryFace)
				: faces[0];

			if (!face?.boundingBox) return;
			const mid = face.boundingBox?.mid;
			const max = face.boundingBox?.max;
			let xValue = 0;
			let yValue = 0;
			let zValue = 0;

			if (plane.direction === CuttingPlanesDirection.DirectionVertical) {
				xValue = max.x - mid.x;
			} else if (
				plane.direction === CuttingPlanesDirection.DirectionHorizontal
			) {
				yValue = max.y - mid.y;
			} else {
				zValue = max.z - mid.z;
			}

			xValue *= value;
			yValue *= value;
			zValue *= value;

			plane.transformationDto = {
				rotation: { x: 0, y: 0, z: 0 },
				translation: { x: xValue, y: yValue, z: zValue }
			};
		});

	const changeType = (e: SelectChangeEvent<string>) =>
		updateCuttingPlanes(() => {
			plane.isSelected = false;

			const type =
				e.target.value === 'bbox'
					? CuttingPlanesType.BoundingBoxPlane
					: CuttingPlanesType.SymmetryPlane;
			const alternativePlane = dto.state.cuttingPlanes?.find(
				c =>
					c.order === plane.order &&
					c.direction === plane.direction &&
					c.type === type
			);
			if (!alternativePlane) return;
			alternativePlane.isSelected = true;
			alternativePlane.isVisible = plane.isVisible;
		});

	const updateCuttingPlanes = (change: () => void) =>
		setCuttingPlanesTaskDto(prev => {
			change();
			return {
				...prev,
				state: {
					...prev.state,
					cuttingPlanes: [...(prev.state.cuttingPlanes ?? [])]
				}
			};
		});

	return (
		<Grid item xs={3.5}>
			<Box
				component="div"
				ref={boxRef}
				sx={{
					backgroundColor: plane.isHover ? 'rgba(204,204,0, 0.6)' : 'white'
				}}
				onMouseEnter={() => changeHover(true)}
				onMouseLeave={() => changeHover(false)}
			>
				<Stack direction="row" alignItems="center">
					<TooltipIconButton
						props={{ sx: { padding: 0, margin: 0, height: 0 } }}
						title="Show/hide cutting plane"
						onClick={() => changeVisibility()}
					>
						<ShowIcon
							style={{ margin: 0, padding: 0 }}
							width={25}
							fill={plane.isVisible ? '#16665F' : '#BBB'}
						/>
					</TooltipIconButton>

					<Slider
						name="cuttingPlane"
						value={plane.sliderValue}
						onChange={(_, value) => changeSliderValue(value as number)}
						min={-1}
						step={0.01}
						max={1}
						sx={{
							'.MuiSlider-thumb': {
								'height': 15,
								'width': 15,
								'&:after': {
									width: 15,
									height: 15
								},
								'&:focus, &:hover': {
									boxShadow: '0px 0px 0px 3px rgba(22, 102, 95, 0.16)'
								},
								'&.Mui-active': {
									boxShadow: '0px 0px 0px 4px rgba(22, 102, 95, 0.16)'
								}
							},

							'maxWidth': 95,
							'ml': 1.3,
							'mr': 1.3,
							'padding': 0
						}}
					/>
					<Input
						name="cuttingPlane"
						size="small"
						sx={{ width: '46px', fontSize: 12 }}
						value={plane.sliderValue}
						onChange={e => changeSliderValue(Number(e.target.value))}
						inputProps={{
							step: 0.01,
							min: -1,
							max: 1,
							type: 'number'
						}}
					/>
				</Stack>
				<Stack direction="row" alignItems="center">
					<Select
						disabled={dto.state.cuttingPlanes?.length !== 18}
						sx={{
							width: '100%',
							fontSize: 12,
							height: 24
						}}
						onClick={e => {
							const current = boxRef.current;
							if (!current) return;
							const eleBounds = current.getBoundingClientRect();
							const isInXBoundary =
								e.clientX >= eleBounds.left && e.clientX <= eleBounds.right;
							const isInYBoundary =
								e.clientY >= eleBounds.top && e.clientY <= eleBounds.bottom;
							changeHover(isInXBoundary && isInYBoundary);
						}}
						name="heatmapRenderMode"
						size="small"
						onChange={e => changeType(e)}
						value={
							plane.type === CuttingPlanesType.BoundingBoxPlane
								? 'bbox'
								: 'symmetry'
						}
					>
						<MenuItem sx={{ width: '100%', fontSize: 12 }} value="bbox">
							Plane from bounding box
						</MenuItem>
						<MenuItem sx={{ width: '100%', fontSize: 12 }} value="symmetry">
							Symmetry plane (primary face)
						</MenuItem>
					</Select>
				</Stack>
			</Box>
		</Grid>
	);
};

type CuttingPlanesInputSectionProps = {
	dto: CuttingPlanesTaskDto;
};

const CuttingPlanesInputSection: FC<CuttingPlanesInputSectionProps> = ({
	dto
}) => {
	const { task } = useContext(TaskContext);
	const { setSecondaryFaceVisibility } = useContext(SceneContext);
	const { calculate2DProjection, setCuttingPlanesTaskDto } = useContext(
		AnalyticalTaskContext
	);

	const getInputRowData = () => [
		{
			icon: (
				<VerticalIcon
					name="DIRECTION_VERTICAL"
					style={{ margin: 0, padding: 0 }}
					fill={
						dto.state.currentDirection ===
						CuttingPlanesDirection.DirectionVertical
							? '#16665f'
							: '#BBB'
					}
					width={25}
					height={25}
				/>
			),
			iconTitle: 'Activate vertical cutting planes',
			direction: CuttingPlanesDirection.DirectionVertical
		},
		{
			icon: (
				<HorizontalIcon
					name="DIRECTION_HORIZONTAL"
					style={{ margin: 0, padding: 0 }}
					fill={
						dto.state.currentDirection ===
						CuttingPlanesDirection.DirectionHorizontal
							? '#16665f'
							: '#BBB'
					}
					width={25}
					height={25}
				/>
			),
			iconTitle: 'Activate horizontal cutting planes',
			direction: CuttingPlanesDirection.DirectionHorizontal
		},
		{
			icon: (
				<FrontIcon
					name="DIRECTION_FRONT_BACK"
					style={{ margin: 0, padding: 0 }}
					fill={
						dto.state.currentDirection ===
						CuttingPlanesDirection.DirectionFrontBack
							? '#16665f'
							: '#BBB'
					}
					width={25}
					height={25}
				/>
			),
			iconTitle: 'Activate front-back cutting planes',
			direction: CuttingPlanesDirection.DirectionFrontBack
		}
	];

	const changeDirection = (direction: CuttingPlanesDirection) => {
		setCuttingPlanesTaskDto(prev => ({
			...prev,
			state: {
				...prev.state,
				currentDirection: direction
			}
		}));
	};

	if (!task) return null;
	const getSquareColor = (isPrimaryFace: boolean, order: number) =>
		TaskUtils.isSingle(task) && !isPrimaryFace ? null : (
			<Typography
				display="inline"
				sx={{
					fontSize: 22,
					pl: 0.5,
					color: CanvasUtils.getColorForLine(task, isPrimaryFace, order)
				}}
			>
				■
			</Typography>
		);

	return (
		<Grid container justifyContent="space-around" spacing={1}>
			<Grid justifyContent="space-between" gap={1} container item xs={12}>
				<Grid item sx={{ textAlign: 'center' }} xs={1} />
				<Grid item sx={{ textAlign: 'center' }} xs={3.5}>
					Cutting plane 1 {getSquareColor(true, 1)}
					{getSquareColor(false, 1)}
				</Grid>
				<Grid item sx={{ textAlign: 'center' }} xs={3.5}>
					Cutting plane 2 {getSquareColor(true, 2)}
					{getSquareColor(false, 2)}
				</Grid>
				<Grid item sx={{ textAlign: 'center' }} xs={3.5}>
					Cutting plane 3 {getSquareColor(true, 3)}
					{getSquareColor(false, 3)}
				</Grid>
			</Grid>
			<Grid
				container
				alignItems="center"
				justifyContent="space-between"
				gap={1}
				item
				xs={12}
			>
				{getInputRowData().map((d, i) => (
					<React.Fragment key={i}>
						<Grid item xs={0.8}>
							<TooltipIconButton
								placement="left"
								onClick={() => changeDirection(d.direction)}
								props={{ sx: { padding: 0, margin: 0, height: 0 } }}
								title={d.iconTitle}
							>
								{d.icon}
							</TooltipIconButton>
						</Grid>
						{dto.state.cuttingPlanes
							?.filter(c => c.isSelected && c.direction === d.direction)
							.sort((c1, c2) => (c1.order > c2.order ? 1 : -1))
							.map(c => (
								<CuttingPlaneInputGroup
									key={c.cuttingPlaneId}
									plane={c}
									dto={dto}
								/>
							))}
					</React.Fragment>
				))}
			</Grid>
		</Grid>
	);
};

export default CuttingPlanesInputSection;

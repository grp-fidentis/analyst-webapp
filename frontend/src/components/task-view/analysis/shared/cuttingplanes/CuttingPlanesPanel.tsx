import React, { FC, useContext, useEffect } from 'react';
import { Stack } from '@mui/material';

import { TaskType } from '../../../../../types/TaskTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import useThrottle from '../../../../../hooks/useThrottle';

import Canvas from './Canvas';
import HausdorffDistanceSection from './HausdorffDistanceSection';
import CuttingPlanesFooter from './CuttingPlanesFooter';
import CuttingPlanesInputSection from './CuttingPlanesInputSection';

type Props = {
	taskType: TaskType;
};

const CuttingPlanesPanel: FC<Props> = ({ taskType }) => {
	const { task } = useContext(TaskContext);
	const { cuttingPlanesTaskDto, createCuttingPlanes, calculate2DProjection } =
		useContext(AnalyticalTaskContext);
	const calculate2DProjectionThrottled = useThrottle(
		async calculate2DProjectionThrottledFunc =>
			await calculate2DProjectionThrottledFunc(),
		100
	);

	useEffect(() => {
		const taskId = task?.id ?? -1;
		if (
			taskId !== -1 &&
			(cuttingPlanesTaskDto.state.taskId !== taskId ||
				!cuttingPlanesTaskDto?.state.cuttingPlanes ||
				cuttingPlanesTaskDto?.state.cuttingPlanes.length === 0)
		) {
			createCuttingPlanes(task?.id ?? -1);
		}
	}, []);

	useEffect(() => {
		const taskId = task?.id ?? -1;
		if (
			cuttingPlanesTaskDto.state.cuttingPlanes?.length !== 0 &&
			taskId === cuttingPlanesTaskDto.state.taskId
		) {
			calculate2DProjectionThrottled(calculate2DProjection);
		}
	}, [cuttingPlanesTaskDto.state]);

	return (
		<Stack direction="column" gap={2}>
			<Stack direction="row" justifyContent="space-around">
				<Canvas
					width={cuttingPlanesTaskDto.state.canvasWidth}
					height={cuttingPlanesTaskDto.state.canvasHeight}
					dto={cuttingPlanesTaskDto}
				/>
				<HausdorffDistanceSection dto={cuttingPlanesTaskDto} />
			</Stack>

			<CuttingPlanesInputSection dto={cuttingPlanesTaskDto} />

			<CuttingPlanesFooter dto={cuttingPlanesTaskDto} />
		</Stack>
	);
};

export default CuttingPlanesPanel;

import { Help } from '@mui/icons-material';
import {
	Box,
	FormControlLabel,
	Checkbox,
	Slider,
	Button,
	Stack
} from '@mui/material';
import React, { FC, useContext } from 'react';

import { CuttingPlanesTaskDto } from '../../../../../types/AnalyticalTaskTypes';
import LabeledTextField from '../../../../common/LabeledTextField';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';

type Props = {
	dto: CuttingPlanesTaskDto;
};

const CuttingPlanesFooter: FC<Props> = ({ dto }) => {
	const { setCuttingPlanesTaskDto, exportCuttingPlanesProjection } = useContext(
		AnalyticalTaskContext
	);

	return (
		<Stack direction="row" gap={3} alignItems="center">
			<Button
				sx={{ mr: 2 }}
				variant="contained"
				onClick={exportCuttingPlanesProjection}
			>
				Export
			</Button>

			<FormControlLabel
				sx={{ mr: 0 }}
				control={
					<Checkbox
						sx={{ paddingRight: 0.25 }}
						checked={dto.state.showNormals}
						name="showNormalVectors"
						onChange={e => {
							setCuttingPlanesTaskDto(prev => ({
								...prev,
								state: {
									...prev.state,
									showNormals: e.target.checked
								}
							}));
						}}
					/>
				}
				label="Show normal vectors"
			/>

			<LabeledTextField text="Vector size:" labelMinWidth={10}>
				<Slider
					name="vectorSize"
					value={dto.state.normalVectorLength}
					onChange={(_, value) =>
						setCuttingPlanesTaskDto(prev => ({
							...prev,
							state: {
								...prev.state,
								normalVectorLength: value as number
							}
						}))
					}
					min={-100}
					max={100}
					sx={{ width: 150 }}
				/>
			</LabeledTextField>
		</Stack>
	);
};

export default CuttingPlanesFooter;

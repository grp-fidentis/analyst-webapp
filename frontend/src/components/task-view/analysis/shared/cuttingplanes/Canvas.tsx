import { Box, IconButton, Stack, Typography } from '@mui/material';
import React, { useState, useEffect, useRef, useContext } from 'react';
import { Help } from '@mui/icons-material';

import { ReactComponent as CameraCenterIcon } from '../../../../../assets/images/camera_center.svg';
import {
	Bounding2DBox,
	CuttingPlanesTaskDto,
	Line2D,
	Point2D
} from '../../../../../types/AnalyticalTaskTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { TaskUtils } from '../../../../../utils/TaskUtils';
import { CanvasUtils } from '../../../../../utils/CanvasUtils';
import useThrottle from '../../../../../hooks/useThrottle';

type CanvasProps = {
	width: number;
	height: number;
	dto: CuttingPlanesTaskDto;
};

const Canvas: React.FC<CanvasProps> = ({ width, height, dto }) => {
	const canvasRef = useRef<HTMLCanvasElement | null>(null);
	const { task } = useContext(TaskContext);
	const [lastPosition, setLastPosition] = useState<Point2D>({ x: 0, y: 0 });
	const lastPositionRef = useRef<Point2D>();
	lastPositionRef.current = lastPosition;
	const [dragStart, setDragStart] = useState<Point2D | null>(null);
	const dragStartRef = useRef<Point2D | null>();
	dragStartRef.current = dragStart;

	if (!task) return null;

	useEffect(() => {
		const canvas = canvasRef.current;
		const ctx = canvas?.getContext('2d');
		if (!ctx || !canvas) return;

		canvas.addEventListener('wheel', handleScroll, false);

		redraw();
	}, [
		dto.projectionResponse,
		dto.state.normalVectorLength,
		dto.state.showNormals
	]);

	const handleMouseDown = (evt: MouseEvent) => {
		const { canvas } = getCanvasProps();
		if (!canvas) return;

		document.body.style.userSelect = 'none';

		const lastPosition = {
			x: evt.offsetX || evt.pageX - canvas?.offsetLeft,
			y: evt.offsetY || evt.pageY - canvas?.offsetTop
		};

		setLastPosition(lastPosition);
		setDragStart(transformedPoint(lastPosition));
	};

	const handleMouseMove = (evt: MouseEvent) => {
		const { ctx, canvas } = getCanvasProps();
		if (!ctx || !canvas) return;

		const lastPosition = {
			x: evt.offsetX || evt.pageX - canvas.offsetLeft,
			y: evt.offsetY || evt.pageY - canvas.offsetTop
		};

		setLastPosition(lastPosition);
		const dragStartCurrent = dragStartRef.current;
		if (dragStartCurrent) {
			const pt = transformedPoint(lastPosition);
			ctx.translate(pt.x - dragStartCurrent.x, pt.y - dragStartCurrent.y);
			redraw();
		}
	};

	const handleMouseUp = () => {
		setDragStart(null);
	};

	const getCanvasProps = (): {
		canvas?: HTMLCanvasElement;
		ctx?: CanvasRenderingContext2D;
	} => {
		const canvas = canvasRef.current;
		const ctx = canvas?.getContext('2d');
		if (!ctx || !canvas) return {};

		return { canvas, ctx };
	};

	const throttleZooming = useThrottle(
		(zoomFunc: (clicks: number, zoomIn: boolean) => void, zoomIn: boolean) =>
			zoomFunc(60, zoomIn),
		1
	);

	const handleScroll = (evt: WheelEvent) => {
		throttleZooming(zoom, evt.deltaY < 0);
		evt.preventDefault();
	};

	const zoom = (clicks: number, zoomIn: boolean) => {
		const { ctx } = getCanvasProps();
		if (!ctx) return;

		const scaleFactor = 1 + 0.002 * (zoomIn ? 1 : -1);

		const lastPositionCurrent = lastPositionRef.current;
		if (!lastPositionCurrent) return;
		const pt = transformedPoint(lastPositionCurrent);
		ctx.translate(pt.x, pt.y);
		const factor = Math.pow(scaleFactor, clicks);
		ctx.scale(factor, factor);
		ctx.translate(-pt.x, -pt.y);
		redraw();
	};

	const transformedPoint = (point: Point2D): DOMPoint => {
		const { ctx } = getCanvasProps();
		if (!ctx) return new DOMPoint(0, 0);

		return ctx
			.getTransform()
			.inverse()
			.transformPoint(new DOMPoint(point.x, point.y));
	};

	const redraw = () => {
		const { canvas, ctx } = getCanvasProps();
		if (!ctx || !canvas) return;

		ctx.clearRect(0, 0, canvas.width, canvas.height);
		drawProjection(ctx);
	};

	const drawProjection = (ctx: CanvasRenderingContext2D) => {
		const projectionResponse = dto?.projectionResponse;
		if (!projectionResponse) return;

		const boundingBoxDto = projectionResponse.boundingBoxDto;
		const intersectionPointsDtos = projectionResponse.intersectionPointsDtos;

		intersectionPointsDtos.map(intersections => {
			intersections.cuttingPlanesIntersections.forEach(intersection => {
				drawFaceLine(
					ctx,
					intersection.isPrimaryFace,
					intersection.curveSegments,
					boundingBoxDto,
					intersection.order
				);

				drawFeaturePoints(
					ctx,
					intersection.isPrimaryFace,
					intersection.featurePoints,
					boundingBoxDto,
					intersection.order
				);

				if (dto.state.showNormals) {
					drawNormals(
						ctx,
						intersection.isPrimaryFace,
						intersection.normals,
						boundingBoxDto,
						intersection.order
					);
				}
			});
		});
	};

	const drawFaceLine = (
		ctx: CanvasRenderingContext2D,
		isPrimaryFace: boolean,
		curveSegments: Point2D[][] | undefined,
		boundingBox: Bounding2DBox,
		order: number
	): void => {
		if (!curveSegments) return;
		curveSegments.forEach(segment => {
			for (let i = 0; i < (segment?.length ?? 0) - 1; i++) {
				ctx.strokeStyle = CanvasUtils.getColorForLine(
					task,
					isPrimaryFace,
					order
				);
				ctx.fillStyle = CanvasUtils.getColorForLine(task, isPrimaryFace, order);
				ctx.lineWidth = 1.5;

				const currentPoint = segment[i];
				const nextPoint = segment[i + 1];

				ctx.beginPath();
				ctx.moveTo(
					currentPoint.x * boundingBox.scale + boundingBox.offsetX,
					currentPoint.y * boundingBox.scale + boundingBox.offsetY
				);
				ctx.lineTo(
					nextPoint.x * boundingBox.scale + boundingBox.offsetX,
					nextPoint.y * boundingBox.scale + boundingBox.offsetY
				);
				ctx.stroke();
			}
		});
	};

	const drawNormals = (
		ctx: CanvasRenderingContext2D,
		isPrimaryFace: boolean,
		normals: Line2D[],
		boundingBox: Bounding2DBox,
		order: number
	): void => {
		ctx.strokeStyle = CanvasUtils.getColorForNormal(task, isPrimaryFace, order);
		ctx.fillStyle = CanvasUtils.getColorForNormal(task, isPrimaryFace, order);
		ctx.lineWidth = 0.5;

		normals.forEach(line => {
			const x1 = line.start.x * boundingBox.scale + boundingBox.offsetX;
			const y1 = line.start.y * boundingBox.scale + boundingBox.offsetY;
			// This calculation is required to keep the normal vector sizes
			const x2 =
				(line.start.x + line.end.x * dto.state.normalVectorLength) *
					boundingBox.scale +
				boundingBox.offsetX;
			const y2 =
				(line.start.y + line.end.y * dto.state.normalVectorLength) *
					boundingBox.scale +
				boundingBox.offsetY;

			ctx.beginPath();
			ctx.moveTo(x1, y1);
			ctx.lineTo(x2, y2);
			ctx.stroke();
		});
	};

	const drawFeaturePoints = (
		ctx: CanvasRenderingContext2D,
		isPrimaryFace: boolean,
		featurePoints: Point2D[],
		boundingBox: Bounding2DBox,
		order: number
	) => {
		ctx.strokeStyle = CanvasUtils.getColorForLine(task, isPrimaryFace, order);
		ctx.fillStyle = CanvasUtils.getColorForLine(task, isPrimaryFace, order);

		featurePoints.forEach(fp => {
			ctx.beginPath();
			ctx.arc(
				fp.x * boundingBox.scale + boundingBox.offsetX,
				fp.y * boundingBox.scale + boundingBox.offsetY,
				2.5,
				0,
				2 * Math.PI
			);
			ctx.fill();
			ctx.closePath();
		});
	};

	const resetCanvas = () => {
		setDragStart(null);
		setLastPosition({ x: 0, y: 0 });
		getCanvasProps().ctx?.resetTransform();
		redraw();
	};

	return (
		<Box component="div" width={width} height={height + 18}>
			<Box component="div" sx={{ position: 'relative' }}>
				<IconButton
					onClick={() => resetCanvas()}
					name="center"
					disableRipple
					sx={{
						'position': 'absolute',
						'ml': '2px',
						'mt': '2px',
						'zIndex': 1,
						'backgroundColor': 'white',
						'width': 33,
						'height': 33,
						'padding': 0.0,
						'color': 'black',
						'&:hover': {
							backgroundColor: '#EEE'
						}
					}}
				>
					<CameraCenterIcon width={20} height={20} fill="#16665F" />
				</IconButton>
			</Box>
			<canvas
				onMouseDown={e => handleMouseDown(e as unknown as MouseEvent)}
				onMouseMove={e => handleMouseMove(e as unknown as MouseEvent)}
				onMouseUp={() => handleMouseUp()}
				onMouseLeave={() => handleMouseUp()}
				style={{
					border: '2px solid black',
					zIndex: 1
				}}
				ref={canvasRef}
				width={width}
				height={height}
			/>
			<Stack direction="row" justifyContent="space-between">
				<Box component="div" sx={{ width: '80%' }}>
					<Typography
						display="inline"
						sx={{ fontSize: '11px', color: 'black' }}
					>
						Legend:
					</Typography>
					{TaskUtils.isPair(task) && (
						<>
							<Typography
								display="inline"
								sx={{
									pl: 0.5,
									fontSize: '11px',
									color: 'green'
								}}
							>
								Primary face
							</Typography>
							<Typography
								display="inline"
								sx={{
									pl: 0.5,
									fontSize: '11px',
									color: 'blue'
								}}
							>
								(normal)
							</Typography>
							<Typography
								display="inline"
								sx={{ pl: 0.5, fontSize: '11px', color: 'black' }}
							>
								|
							</Typography>
							<Typography
								display="inline"
								sx={{
									pl: 0.5,
									fontSize: '11px',
									color: 'red'
								}}
							>
								Secondary face
							</Typography>
							<Typography
								display="inline"
								sx={{
									pl: 0.5,
									fontSize: '11px',
									color: 'orange'
								}}
							>
								(normal)
							</Typography>
						</>
					)}
					{TaskUtils.isSingle(task) && (
						<>
							<Typography
								display="inline"
								sx={{
									pl: 0.5,
									fontSize: '11px',
									color: 'green'
								}}
							>
								Face
							</Typography>
							<Typography
								display="inline"
								sx={{
									pl: 0.5,
									fontSize: '11px',
									color: 'blue'
								}}
							>
								(normal)
							</Typography>
						</>
					)}
				</Box>

				<Box component="div">
					<IconButton
						component="a"
						href="https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/cutting-planes"
						target="_blank"
						rel="noopener noreferrer"
					>
						<Help sx={{ fontSize: '20px', mt: -0.5 }} color="primary" />
					</IconButton>
				</Box>
			</Stack>
		</Box>
	);
};

export default Canvas;

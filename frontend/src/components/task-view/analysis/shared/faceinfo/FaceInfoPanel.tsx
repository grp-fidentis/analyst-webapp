import React, { FC, useContext } from 'react';

import { TaskType } from '../../../../../types/TaskTypes';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import FaceInfoTabPanel from '../../../../projects-view/FaceInfoTabPanel';

type Props = {
	taskType: TaskType;
	isPrimary: boolean;
};

const FaceInfoPanel: FC<Props> = ({ taskType, isPrimary }) => {
	const { updateFace, faces } = useContext(SceneContext);

	const onFeaturePointsHover = (isMouseEnter: boolean, name: string) => {
		const face =
			taskType === TaskType.SingleFaceAnalysis
				? faces[0]
				: faces.find(f => isPrimary === f.isPrimaryFace);
		if (!face) return;
		const featurePoint = face?.featurePoints?.featurePointDtos.find(
			f => f.name === name
		);
		if (!featurePoint) return;
		featurePoint.hovered = isMouseEnter;
		updateFace(face);
	};

	return (
		<FaceInfoTabPanel
			onFeaturePointsHover={onFeaturePointsHover}
			isTaskFaceInfo
			taskFaceInfo={
				taskType === TaskType.SingleFaceAnalysis
					? faces[0].info
					: faces.find(f => isPrimary === f.isPrimaryFace)?.info
			}
		/>
	);
};

export default FaceInfoPanel;

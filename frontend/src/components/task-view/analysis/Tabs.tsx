import { TabList } from '@mui/lab';
import { Tab } from '@mui/material';
import { Dispatch, FC, SetStateAction, useContext } from 'react';

import { TabInfo } from '../../../types/CommonTypes';
import { TaskContext } from '../../../providers/TaskContextProvider';

type Props = {
	setSelectedTab: Dispatch<SetStateAction<string>>;
	tabList: TabInfo[];
	disabled: boolean;
};

const Tabs: FC<Props> = ({ disabled, setSelectedTab, tabList }) => {
	const { setCurrentPanel } = useContext(TaskContext);

	return (
		<TabList
			className="face-tabs"
			variant="scrollable"
			onChange={(_, value) => {
				setSelectedTab(value);
				setCurrentPanel(tabList[value - 1].label);
			}}
			aria-label="face task tabs"
		>
			{tabList?.map((tab, i) => (
				<Tab
					sx={{ pl: 0.6, pr: 1, fontSize: 15 }}
					disabled={disabled}
					key={i}
					label={tab.label}
					value={`${i + 1}`}
					icon={tab.icon}
					iconPosition="start"
				/>
			))}
		</TabList>
	);
};

export default Tabs;

import React from 'react';
import { Grid } from '@mui/material';

const FeaturePointsPanel = () => (
	<Grid container rowGap={4}>
		<Grid item xs={12}>
			single face feature points
		</Grid>
	</Grid>
);

export default FeaturePointsPanel;

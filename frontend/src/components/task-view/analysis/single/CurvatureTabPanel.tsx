import { useState, useContext, useEffect } from 'react';
import {
	Box,
	Checkbox,
	IconButton,
	MenuItem,
	Select,
	SelectChangeEvent,
	Stack,
	Typography
} from '@mui/material';
import { Help } from '@mui/icons-material';

import SectionHeaderLabel from '../../../common/SectionHeaderLabel';
import {
	CurvatureMethod,
	CurvatureTaskDto
} from '../../../../types/HumanFaceTypes';
import { TaskContext } from '../../../../providers/TaskContextProvider';
import { AnalyticalTaskContext } from '../../../../providers/AnalyticalTaskContextProvider';
import { FaceUtils } from '../../../../utils/FaceUtils';
import { SceneContext } from '../../../../providers/SceneContextProvider';

const CurvatureTabPanel = () => {
	const { task } = useContext(TaskContext);
	const { updateFace, faces } = useContext(SceneContext);
	const { calculateCurvatureHeatmap } = useContext(AnalyticalTaskContext);
	const [curvatureDto, setCurvatureDto] = useState<CurvatureTaskDto>({
		taskId: task?.id ?? -1,
		curvatureMethod: CurvatureMethod.Gaussian
	});

	useEffect(() => {
		calculateCurvatureHeatmap(curvatureDto);

		return () => {
			updateFace({
				id: faces[0].id ?? -1,
				colorsOfHeatmap: undefined
			});
		};
	}, []);

	useEffect(() => {
		calculateCurvatureHeatmap(curvatureDto);
	}, [curvatureDto]);

	const onChange = (e: SelectChangeEvent<string>) => {
		setCurvatureDto(prev => ({
			...prev,
			curvatureMethod: e.target.value as CurvatureMethod
		}));
	};

	return (
		<Box component="div">
			<Box component="div" sx={{ pb: 2, textAlign: 'center' }}>
				<b>Visualization options</b>
			</Box>
			<Stack direction="row" justifyContent="center">
				<Select
					size="small"
					sx={{ ml: 2, minWidth: 120 }}
					value={curvatureDto.curvatureMethod}
					onChange={onChange}
				>
					<MenuItem value={CurvatureMethod.Gaussian}>Gaussian</MenuItem>
					<MenuItem value={CurvatureMethod.Mean}>Mean</MenuItem>
					<MenuItem value={CurvatureMethod.Min}>Min</MenuItem>
					<MenuItem value={CurvatureMethod.Max}>Max</MenuItem>
				</Select>

				<IconButton
					sx={{ ml: 2 }}
					component="a"
					href="https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/curvature"
					target="_blank"
					rel="noopener noreferrer"
				>
					<Help fontSize="medium" color="primary" />
				</IconButton>
			</Stack>
		</Box>
	);
};

export default CurvatureTabPanel;

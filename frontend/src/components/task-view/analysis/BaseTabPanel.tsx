import React, { useContext, useState } from 'react';
import { TabContext } from '@mui/lab';
import { Divider, Stack } from '@mui/material';
import { Delete } from '@mui/icons-material';
import { useConfirm } from 'material-ui-confirm';

import TooltipIconButton from '../../common/TooltipIconButton';
import { TaskContext } from '../../../providers/TaskContextProvider';
import { TaskType } from '../../../types/TaskTypes';
import { TabInfo } from '../../../types/CommonTypes';
import { ReactComponent as Curvature } from '../../../assets/images/curvature.svg';
import { ReactComponent as Symmetry } from '../../../assets/images/symmetry_icon.svg';
import { ReactComponent as Profiles } from '../../../assets/images/profiles_icon.svg';
import { ReactComponent as FeaturePoints } from '../../../assets/images/feature_points_icon.svg';
import {
	ReactComponent as FaceInfo,
	ReactComponent as PrimaryFaceInfo
} from '../../../assets/images/primary_face.svg';
import { ReactComponent as Registration } from '../../../assets/images/registration.svg';
import { ReactComponent as Distance } from '../../../assets/images/distance_icon.svg';
import { ReactComponent as Visualization } from '../../../assets/images/data_visualization_icon.svg';
import { ReactComponent as SecondaryFaceInfo } from '../../../assets/images/secondary_face.svg';
import { AnalyticalTaskContext } from '../../../providers/AnalyticalTaskContextProvider';
import { BatchProcessingTaskContext } from '../../../providers/BatchProcessingTaskContextProvider';

import Tabs from './Tabs';
import CurvatureTabPanel from './single/CurvatureTabPanel';
import SymmetryTabPanel from './shared/SymmetryTabPanel';
import Panels from './Panels';
import BatchRegistrationPanel from './batch/registration/BatchRegistrationPanel';
import BatchDistancePanel from './batch/distance/BatchDistancePanel';
import CuttingPlanesPanel from './shared/cuttingplanes/CuttingPlanesPanel';
import FeaturePointsPanel from './single/FeaturePointsPanel';
import FaceInfoPanel from './shared/faceinfo/FaceInfoPanel';
import InteractiveMaskPanel from './shared/InteractiveMaskPanel';
import PairRegistrationPanel from './pair/registration/PairRegistrationPanel';
import PairDistancePanel from './pair/distance/PairDistancePanel';
import BatchVisualizationPanel from './batch/visualization/BatchVisualizationPanel';

const singleFacePanels = [
	<SymmetryTabPanel key={1} taskType={TaskType.SingleFaceAnalysis} />,
	<CuttingPlanesPanel key={2} taskType={TaskType.SingleFaceAnalysis} />,
	<FeaturePointsPanel key={3} />,
	<CurvatureTabPanel key={4} />,
	<FaceInfoPanel key={5} taskType={TaskType.SingleFaceAnalysis} isPrimary />,
	<InteractiveMaskPanel key={6} />
];

const singleFaceTabs: TabInfo[] = [
	{ label: 'SYMMETRY', icon: <Symmetry width={20} /> },
	{ label: 'CUTTING PLANES', icon: <Profiles width={20} /> },
	{ label: 'FEATURE POINTS', icon: <FeaturePoints width={20} /> },
	{ label: 'CURVATURE', icon: <Curvature width={20} /> },
	{ label: 'FACE', icon: <FaceInfo width={20} /> },
	{ label: 'INTERACTIVE MASK', icon: <FeaturePoints width={20} /> }
];

const pairPanels = [
	<PairRegistrationPanel key={1} />,
	<PairDistancePanel key={2} />,
	<SymmetryTabPanel key={3} taskType={TaskType.PairComparison} />,
	<CuttingPlanesPanel key={4} taskType={TaskType.PairComparison} />,
	<FaceInfoPanel key={5} taskType={TaskType.PairComparison} isPrimary />,
	<FaceInfoPanel
		key={6}
		taskType={TaskType.PairComparison}
		isPrimary={false}
	/>,
	<InteractiveMaskPanel key={7} />
];

const pairTabs: TabInfo[] = [
	{ label: 'REGISTRATION', icon: <Registration width={20} /> },
	{ label: 'DISTANCE', icon: <Distance width={20} /> },
	{ label: 'SYMMETRY', icon: <Symmetry width={20} /> },
	{ label: 'CUTTING PLANES', icon: <Profiles width={20} /> },
	{ label: 'PRIMARY FACE', icon: <PrimaryFaceInfo width={20} /> },
	{ label: 'SECONDARY FACE', icon: <SecondaryFaceInfo width={20} /> },
	{ label: 'INTERACTIVE MASK', icon: <FeaturePoints width={20} /> }
];

const batchPanels = [
	<BatchRegistrationPanel key={1} />,
	<BatchDistancePanel key={2} />,
	<BatchVisualizationPanel key={3} />
];

const batchTabs: TabInfo[] = [
	{ label: 'REGISTRATION', icon: <Registration width={20} /> },
	{ label: 'DISTANCE', icon: <Distance width={20} /> },
	{ label: 'VISUALIZATION', icon: <Visualization width={20} /> }
];

const BaseTabPanel = () => {
	const [selectedTab, setSelectedTab] = useState('1');
	const { task, deleteTask } = useContext(TaskContext);
	const { isLoadingInfo } = useContext(AnalyticalTaskContext);
	const { isLoadingBatchInfo } = useContext(BatchProcessingTaskContext);
	const confirm = useConfirm();

	const getPanels = (type?: TaskType) => {
		switch (type) {
			case TaskType.SingleFaceAnalysis:
				return singleFacePanels;
			case TaskType.PairComparison:
				return pairPanels;
			case TaskType.BatchProcessing:
				return batchPanels;
			case undefined:
				return [];
		}
	};

	const getTabs = (type?: TaskType) => {
		switch (type) {
			case TaskType.SingleFaceAnalysis:
				return singleFaceTabs;
			case TaskType.PairComparison:
				return pairTabs;
			case TaskType.BatchProcessing:
				return batchTabs;
			case undefined:
				return [];
		}
	};

	const handleDeleteTask = () => {
		confirm({
			description:
				"This action will delete task and you won't be able to return to the task",
			confirmationButtonProps: { variant: 'contained' }
		})
			.then(() => {
				if (task) {
					deleteTask(task.id);
				}
			})
			.catch(() => null);
	};

	return (
		<TabContext value={selectedTab}>
			<Stack
				direction="row"
				sx={{ borderBottom: 1, borderColor: '#595959' }}
				justifyContent="space-between"
			>
				<Tabs
					disabled={isLoadingInfo() || isLoadingBatchInfo()}
					setSelectedTab={setSelectedTab}
					tabList={getTabs(task?.taskType)}
				/>

				<Stack alignItems="center" direction="row">
					<Divider orientation="vertical" flexItem sx={{ my: 1 }} />

					<TooltipIconButton
						title="Delete task"
						ariaControls="delete-task"
						onClick={handleDeleteTask}
					>
						<Delete color="error" />
					</TooltipIconButton>
				</Stack>
			</Stack>

			<Panels panelData={getPanels(task?.taskType)} />
		</TabContext>
	);
};

export default BaseTabPanel;

import React, { ChangeEvent, FC, useContext } from 'react';
import {
	Box,
	Grid,
	Divider,
	SvgIcon,
	CircularProgress,
	Typography
} from '@mui/material';

import { SceneContext } from '../../../../../providers/SceneContextProvider';
import { ReactComponent as ArrowUp } from '../../../../../assets/images/arrows/arrow-up.svg';
import { ReactComponent as ArrowDown } from '../../../../../assets/images/arrows/arrow-down.svg';
import { ReactComponent as ArrowLeft } from '../../../../../assets/images/arrows/arrow-left.svg';
import { ReactComponent as ArrowRight } from '../../../../../assets/images/arrows/arrow-right.svg';
import { ReactComponent as ArrowPointingIn } from '../../../../../assets/images/arrows/arrow-pointing-in.svg';
import { ReactComponent as ArrowPointingOut } from '../../../../../assets/images/arrows/arrow-pointing-out.svg';
import { ReactComponent as RotationXUp } from '../../../../../assets/images/arrows/rotation-x-up.svg';
import { ReactComponent as RotationXDown } from '../../../../../assets/images/arrows/rotation-x-down.svg';
import { ReactComponent as RotationYLeft } from '../../../../../assets/images/arrows/rotation-y-left.svg';
import { ReactComponent as RotationYRight } from '../../../../../assets/images/arrows/rotation-y-right.svg';
import { ReactComponent as RotationZLeft } from '../../../../../assets/images/arrows/rotation-z-left.svg';
import { ReactComponent as RotationZRight } from '../../../../../assets/images/arrows/rotation-z-right.svg';
import { AlignmentOperations } from '../../../../../types/SceneTypes';
import TooltipIconButton from '../../../../common/TooltipIconButton';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import sync from '../../../../../assets/images/sync.gif';
import { LoadingOperation } from '../../../../../types/AnalyticalTaskTypes';

type DirectionProps = {
	icon: React.ElementType;
	name: string;
	tooltipTitle: string;
};

type ArrowLayoutProps = {
	left: DirectionProps;
	right: DirectionProps;
	up: DirectionProps;
	down: DirectionProps;
	startAlignmentHandler: (e: React.MouseEvent<HTMLButtonElement>) => void;
	stopAlignmentHandler: (e: React.MouseEvent<HTMLButtonElement>) => void;
	disabled: boolean;
};

const ArrowLayout: FC<ArrowLayoutProps> = ({
	left,
	right,
	up,
	down,
	startAlignmentHandler,
	stopAlignmentHandler,
	disabled
}) => (
	<Box component="div" sx={{ width: 120, height: 120 }}>
		<Box
			// @ts-ignore
			sx={{
				display: 'flex',
				justifyContent: 'center'
			}}
		>
			<TooltipIconButton
				title={up.tooltipTitle}
				placement="top"
				disableRipple
				disabled={disabled}
				props={{
					name: up.name,
					onMouseUp: stopAlignmentHandler,
					onMouseLeave: stopAlignmentHandler,
					onMouseDown: startAlignmentHandler,
					sx: {
						'color': 'black',
						'backgroundColor': '#E9E9E9',
						'&:hover': {
							backgroundColor: '#C9C9C9'
						},
						'&:active:hover': {
							backgroundColor: '#A9A9A9'
						}
					}
				}}
			>
				<SvgIcon component={up.icon} />
			</TooltipIconButton>
		</Box>
		<Box component="div">
			<Box
				component="div"
				gap={5}
				sx={{ display: 'flex', justifyContent: 'space-around' }}
			>
				<TooltipIconButton
					title={left.tooltipTitle}
					placement="bottom-end"
					disableRipple
					disabled={disabled}
					props={{
						name: left.name,
						onMouseUp: stopAlignmentHandler,
						onMouseLeave: stopAlignmentHandler,
						onMouseDown: startAlignmentHandler,
						sx: {
							'color': 'black',
							'backgroundColor': '#E9E9E9',
							'&:hover': {
								backgroundColor: '#C9C9C9'
							},
							'&:active:hover': {
								backgroundColor: '#A9A9A9'
							}
						}
					}}
				>
					<SvgIcon component={left.icon} />
				</TooltipIconButton>
				<TooltipIconButton
					title={right.tooltipTitle}
					disableRipple
					disabled={disabled}
					placement="bottom-start"
					props={{
						name: right.name,
						onMouseUp: stopAlignmentHandler,
						onMouseLeave: stopAlignmentHandler,
						onMouseDown: startAlignmentHandler,
						sx: {
							'color': 'black',
							'backgroundColor': '#E9E9E9',
							'&:hover': {
								backgroundColor: '#C9C9C9'
							},
							'&:active:hover': {
								backgroundColor: '#A9A9A9'
							}
						}
					}}
				>
					<SvgIcon component={right.icon} />
				</TooltipIconButton>
			</Box>
		</Box>
		<Box sx={{ display: 'flex', justifyContent: 'center' }}>
			<TooltipIconButton
				disableRipple
				disabled={disabled}
				title={down.tooltipTitle}
				props={{
					name: down.name,
					onMouseUp: stopAlignmentHandler,
					onMouseLeave: stopAlignmentHandler,
					onMouseDown: startAlignmentHandler,
					sx: {
						'color': 'black',
						'backgroundColor': '#E9E9E9',
						'&:hover': {
							backgroundColor: '#C9C9C9'
						},
						'&:active:hover': {
							backgroundColor: '#A9A9A9'
						}
					}
				}}
			>
				<SvgIcon component={down.icon} />
			</TooltipIconButton>
		</Box>
	</Box>
);

const ManualAlignment = () => {
	const { setAlignment, alignment } = useContext(SceneContext);
	const { isLoadingInfo } = useContext(AnalyticalTaskContext);

	const startAlignmentHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
		const operationName = e.currentTarget.name;
		const operation =
			AlignmentOperations[operationName as keyof typeof AlignmentOperations];
		setAlignment({ isActive: true, operation });
	};

	const stopAlignmentHandler = () => {
		setAlignment(alignment => ({
			operation: alignment.operation,
			isActive: false
		}));
	};

	return (
		<Grid direction="row" container sx={{ justifyContent: 'space-evenly' }}>
			<Grid
				direction="row"
				item
				xs={5}
				container
				gap={2}
				sx={{ justifyContent: 'center' }}
			>
				<Grid item xs={10} sx={{ textAlign: 'center', pb: 1 }}>
					<b>Translation</b>
				</Grid>
				<Grid item xs={1} sx={{ textAlign: 'right' }}>
					{isLoadingInfo(LoadingOperation.Alignment, true) &&
						alignment.operation?.includes('Translate') && (
							<img src={sync} alt="sync" width={25} />
						)}
				</Grid>
				<Grid item xs={4} sx={{ display: 'flex', justifyContent: 'center' }}>
					<ArrowLayout
						up={{
							name: AlignmentOperations.TranslateYUp,
							icon: ArrowUp,
							tooltipTitle: 'Y-axis up'
						}}
						left={{
							name: AlignmentOperations.TranslateXLeft,
							icon: ArrowLeft,
							tooltipTitle: 'X-axis left'
						}}
						right={{
							name: AlignmentOperations.TranslateXRight,
							icon: ArrowRight,
							tooltipTitle: 'X-axis right'
						}}
						down={{
							name: AlignmentOperations.TranslateYDown,
							icon: ArrowDown,
							tooltipTitle: 'Y-axis down'
						}}
						stopAlignmentHandler={stopAlignmentHandler}
						startAlignmentHandler={startAlignmentHandler}
						disabled={isLoadingInfo(LoadingOperation.Alignment)}
					/>
				</Grid>
				<Grid
					item
					xs={4}
					gap={1}
					sx={{
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						flexDirection: 'column'
					}}
				>
					<TooltipIconButton
						title="Z-axis back"
						placement="top"
						disableRipple
						disabled={isLoadingInfo(LoadingOperation.Alignment)}
						props={{
							name: AlignmentOperations.TranslateZBack,
							onMouseUp: stopAlignmentHandler,
							onMouseLeave: stopAlignmentHandler,
							onMouseDown: startAlignmentHandler,
							sx: {
								'backgroundColor': '#E9E9E9',
								'&:hover': {
									backgroundColor: '#C9C9C9'
								},
								'color': 'black',
								'&:active:hover': {
									backgroundColor: '#A9A9A9'
								}
							}
						}}
					>
						<SvgIcon component={ArrowPointingIn} />
					</TooltipIconButton>
					<TooltipIconButton
						title="Z-axis front"
						placement="bottom"
						disableRipple
						disabled={isLoadingInfo(LoadingOperation.Alignment)}
						props={{
							name: AlignmentOperations.TranslateZFront,
							onMouseUp: stopAlignmentHandler,
							onMouseLeave: stopAlignmentHandler,
							onMouseDown: startAlignmentHandler,
							sx: {
								'color': 'black',
								'backgroundColor': '#E9E9E9',
								'&:hover': {
									backgroundColor: '#C9C9C9'
								},
								'&:active:hover': {
									backgroundColor: '#A9A9A9'
								}
							}
						}}
					>
						<SvgIcon component={ArrowPointingOut} />
					</TooltipIconButton>
				</Grid>
			</Grid>

			<Divider orientation="vertical" flexItem sx={{ mx: 2 }} />

			<Grid
				direction="row"
				item
				xs={5}
				container
				gap={2}
				sx={{ justifyContent: 'center' }}
			>
				<Grid item xs={10} sx={{ textAlign: 'center', pb: 1 }}>
					<b>Rotation</b>
				</Grid>
				<Grid item xs={1} sx={{ textAlign: 'right' }}>
					{isLoadingInfo(LoadingOperation.Alignment, true) &&
						!alignment.operation?.includes('Translate') && (
							<img src={sync} alt="sync" width={25} />
						)}
				</Grid>
				<Grid item xs={4} sx={{ display: 'flex', justifyContent: 'center' }}>
					<ArrowLayout
						up={{
							name: AlignmentOperations.RotateXUp,
							icon: RotationXUp,
							tooltipTitle: 'X-axis up'
						}}
						left={{
							name: AlignmentOperations.RotateYLeft,
							icon: RotationYLeft,
							tooltipTitle: 'Y-axis left'
						}}
						right={{
							name: AlignmentOperations.RotateYRight,
							icon: RotationYRight,
							tooltipTitle: 'Y-axis right'
						}}
						down={{
							name: AlignmentOperations.RotateXDown,
							icon: RotationXDown,
							tooltipTitle: 'X-axis down'
						}}
						stopAlignmentHandler={stopAlignmentHandler}
						startAlignmentHandler={startAlignmentHandler}
						disabled={isLoadingInfo(LoadingOperation.Alignment)}
					/>
				</Grid>
				<Grid
					item
					xs={6}
					gap={1}
					sx={{
						pl: 4,
						display: 'flex',
						alignItems: 'center'
					}}
				>
					<TooltipIconButton
						title="Z-axis left"
						disableRipple
						disabled={isLoadingInfo(LoadingOperation.Alignment)}
						props={{
							name: AlignmentOperations.RotateZLeft,
							onMouseUp: stopAlignmentHandler,
							onMouseLeave: stopAlignmentHandler,
							onMouseDown: startAlignmentHandler,
							sx: {
								'color': 'black',
								'height': '40px',
								'width': '40px',
								'backgroundColor': '#E9E9E9',
								'&:hover': {
									backgroundColor: '#C9C9C9'
								},
								'&:active:hover': {
									backgroundColor: '#A9A9A9'
								}
							}
						}}
					>
						<SvgIcon sx={{ fontSize: 21 }} component={RotationZLeft} />
					</TooltipIconButton>
					<TooltipIconButton
						title="Z-axis right"
						disableRipple
						disabled={isLoadingInfo(LoadingOperation.Alignment)}
						props={{
							name: AlignmentOperations.RotateZRight,
							onMouseUp: stopAlignmentHandler,
							onMouseLeave: stopAlignmentHandler,
							onMouseDown: startAlignmentHandler,
							sx: {
								'color': 'black',
								'backgroundColor': '#E9E9E9',
								'&:hover': {
									backgroundColor: '#C9C9C9'
								},
								'&:active:hover': {
									backgroundColor: '#A9A9A9'
								},
								'height': '40px',
								'width': '40px'
							}
						}}
					>
						<SvgIcon sx={{ fontSize: 21 }} component={RotationZRight} />
					</TooltipIconButton>
				</Grid>
			</Grid>
		</Grid>
	);
};

export default ManualAlignment;

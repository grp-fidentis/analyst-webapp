import React, {
	ChangeEvent,
	SyntheticEvent,
	useContext,
	useEffect,
	useState
} from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
	Accordion,
	AccordionDetails,
	AccordionSummary,
	Box,
	Checkbox,
	FormControlLabel,
	Grid,
	IconButton,
	Input,
	MenuItem,
	Select,
	Slider,
	Stack
} from '@mui/material';
import { toast } from 'react-toastify';
import { LoadingButton } from '@mui/lab';
import { Help } from '@mui/icons-material';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useConfirm } from 'material-ui-confirm';

import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import {
	LoadingOperation,
	PointSamplingStrategy,
	RegistrationMethod,
	RegistrationTaskDto,
	SamplingTaskDto
} from '../../../../../types/AnalyticalTaskTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import LabeledTextField from '../../../../common/LabeledTextField';
import useThrottle from '../../../../../hooks/useThrottle';
import { FaceUtils } from '../../../../../utils/FaceUtils';

const ParametersForm = () => {
	const { task } = useContext(TaskContext);
	const { faces, updateFace } = useContext(SceneContext);
	const {
		isLoadingInfo,
		executeRegistration,
		distanceTaskDto,
		setDistances,
		calculateSamples
	} = useContext(AnalyticalTaskContext);
	const [prevSamplingStrategy, setPrevSamplingStrategy] =
		useState<PointSamplingStrategy>(PointSamplingStrategy.None);
	const [formData, setFormData] = useState<RegistrationTaskDto>({
		taskId: task?.id ?? -1,
		registrationMethod: RegistrationMethod.Icp,
		scale: false,
		icpMaxIter: 100,
		icpMinError: 0.05,
		icpAutoCrop: true,
		samplingTaskDto: {
			samplingStrategy: PointSamplingStrategy.None,
			samplingStrength: 500
		},
		distanceTaskDto
	});

	const calculateSamplePointsThrottled = useThrottle(
		async (dto, calculateSamples) => {
			await calculateSamples(dto);
		},
		300
	);

	useEffect(
		() => () => {
			updateFace({
				samples: undefined,
				id: FaceUtils.getSecondary(faces)?.id ?? -1
			});
		},
		[]
	);

	useEffect(() => {
		setFormData({
			...formData,
			distanceTaskDto,
			taskId: task?.id ?? -1
		});
	}, [distanceTaskDto, task]);

	useEffect(() => {
		if (
			formData.samplingTaskDto.samplingStrategy !==
				PointSamplingStrategy.None ||
			(prevSamplingStrategy !== PointSamplingStrategy.None &&
				formData.samplingTaskDto.samplingStrategy ===
					PointSamplingStrategy.None)
		) {
			const samplingTaskDto: SamplingTaskDto = {
				...formData.samplingTaskDto,
				taskId: task?.id ?? -1
			};
			calculateSamplePointsThrottled(samplingTaskDto, calculateSamples);
		}
	}, [
		formData.samplingTaskDto.samplingStrength,
		formData.samplingTaskDto.samplingStrategy
	]);

	const handleValueChange = (
		e:
			| ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
			| SelectChangeEvent<string>
	) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};

	const handleSamplingValueChange = (
		e:
			| ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
			| SelectChangeEvent<string>
	) => {
		setFormData({
			...formData,
			samplingTaskDto: {
				...formData.samplingTaskDto,
				[e.target.name]: e.target.value
			}
		});
	};

	const handleCheckboxChange = (e: ChangeEvent<HTMLInputElement>) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.checked
		});
	};

	const handleSubmit = async (e: SyntheticEvent) => {
		e.preventDefault();

		const secondaryFace = await executeRegistration(formData);

		if (!secondaryFace) {
			toast.error(
				'Procrustes algorithm has failed: not enough feature points of the same type among both faces'
			);
			return;
		}

		updateFace(secondaryFace);
		if (secondaryFace.distances) {
			setDistances(secondaryFace.distances);
		}
		toast.success('Registration done');
	};

	return (
		<Box component="form" onSubmit={handleSubmit}>
			<Grid item xs={12} sx={{ pb: 2, textAlign: 'center' }}>
				<b>Registration method</b>
			</Grid>
			<Stack direction="row" justifyContent="center">
				<Select
					sx={{ minWidth: 270 }}
					name="registrationMethod"
					size="small"
					value={formData.registrationMethod}
					onChange={handleValueChange}
				>
					<MenuItem value={RegistrationMethod.Icp}>mesh-based (ICP)</MenuItem>
					<MenuItem
						disabled={faces.some(face => !face.hasFeaturePoints)}
						value={RegistrationMethod.FeaturePoints}
					>
						feature points (Procrustes)
					</MenuItem>
					<MenuItem
						disabled={faces.some(face => !face.hasSymmetryPlane)}
						value={RegistrationMethod.Symmetry}
					>
						symmetry-based
					</MenuItem>
				</Select>
				<LoadingButton
					sx={{ ml: 3 }}
					variant="contained"
					disabled={isLoadingInfo(LoadingOperation.Registration)}
					loading={isLoadingInfo(LoadingOperation.Registration, true)}
					type="submit"
				>
					Compute
				</LoadingButton>
				<IconButton
					sx={{ ml: 2 }}
					component="a"
					href="https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/registration"
					target="_blank"
					rel="noopener noreferrer"
				>
					<Help fontSize="medium" color="primary" />
				</IconButton>
			</Stack>

			{formData.registrationMethod !== RegistrationMethod.Symmetry && (
				<Accordion sx={{ mt: 2 }} disableGutters defaultExpanded>
					<AccordionSummary
						aria-controls="panel1a-content"
						id="panel1a-header"
						expandIcon={<ExpandMoreIcon />}
					>
						Parameters
					</AccordionSummary>
					<AccordionDetails>
						<Stack sx={{ paddingBottom: '3px' }}>
							<LabeledTextField text="Scale:">
								<Checkbox
									name="scale"
									checked={formData.scale}
									onChange={handleCheckboxChange}
									sx={{ p: 0.5 }}
								/>
							</LabeledTextField>
						</Stack>

						{formData.registrationMethod === RegistrationMethod.Icp && (
							<Stack direction="column" gap={1}>
								<LabeledTextField text="Auto-crop:">
									<FormControlLabel
										control={
											<Checkbox
												checked={formData.icpAutoCrop}
												name="icpAutoCrop"
												sx={{ ml: '6px' }}
												onChange={handleCheckboxChange}
											/>
										}
										label="(only for sufficiently overlapping faces)"
									/>
								</LabeledTextField>
								<LabeledTextField text="Min error:">
									<Input
										name="icpMinError"
										size="small"
										value={formData.icpMinError}
										onChange={handleValueChange}
										sx={{ width: 70 }}
										inputProps={{
											'step': 0.01,
											'min': 0,
											'type': 'number',
											'aria-labelledby': 'sampling-strength-2-input'
										}}
									/>
								</LabeledTextField>
								<LabeledTextField text="Max iterations:">
									<Input
										onChange={handleValueChange}
										value={formData.icpMaxIter}
										name="icpMaxIter"
										sx={{ width: 70 }}
										size="small"
										inputProps={{
											'min': 1,
											'step': 1,
											'type': 'number',
											'aria-labelledby': 'sampling-strength-2-input'
										}}
									/>
								</LabeledTextField>
								<LabeledTextField text="Sub-sampling strength:">
									<>
										<Slider
											name="samplingStrength"
											aria-labelledby="sampling-strength-1-slider"
											disabled={isLoadingInfo(LoadingOperation.Samples)}
											value={formData.samplingTaskDto.samplingStrength}
											onChange={(_, value) =>
												setFormData({
													...formData,
													samplingTaskDto: {
														...formData.samplingTaskDto,
														samplingStrength: Array.isArray(value)
															? value[0]
															: value
													}
												})
											}
											min={50}
											max={5000}
											sx={{ maxWidth: 250 }}
										/>
										<span>
											<Input
												name="samplingStrength"
												size="small"
												sx={{ width: '58px' }}
												disabled={isLoadingInfo(LoadingOperation.Samples)}
												value={formData.samplingTaskDto.samplingStrength}
												onChange={handleSamplingValueChange}
												inputProps={{
													'step': 1,
													'min': 50,
													'max': 5000,
													'type': 'number',
													'aria-labelledby': 'sampling-strength-1-input'
												}}
											/>
										</span>
									</>
								</LabeledTextField>
								<LabeledTextField text="Sub-sampling strategy:">
									<Select
										size="small"
										name="samplingStrategy"
										sx={{ minWidth: 270 }}
										value={formData.samplingTaskDto.samplingStrategy}
										disabled={isLoadingInfo(LoadingOperation.Samples)}
										onChange={e => {
											setPrevSamplingStrategy(
												formData.samplingTaskDto.samplingStrategy
											);
											handleSamplingValueChange(e);
										}}
									>
										<MenuItem value={PointSamplingStrategy.None}>None</MenuItem>
										<MenuItem value={PointSamplingStrategy.RandomSampling}>
											Random Sampling
										</MenuItem>
										<MenuItem
											value={PointSamplingStrategy.ShapePoissonDiskSubSampling}
										>
											Poisson Disk Sub Sampling
										</MenuItem>
										<MenuItem value={PointSamplingStrategy.GaussianCurvature}>
											Gaussian Curvature
										</MenuItem>
										<MenuItem value={PointSamplingStrategy.MeanCurvature}>
											Mean Curvature
										</MenuItem>
										<MenuItem
											value={PointSamplingStrategy.UniformSpaceSampling}
										>
											Uniform Space Sampling
										</MenuItem>
										<MenuItem value={PointSamplingStrategy.UniformMeshSampling}>
											Uniform Mesh Sampling
										</MenuItem>
									</Select>
								</LabeledTextField>
							</Stack>
						)}
					</AccordionDetails>
				</Accordion>
			)}
		</Box>
	);
};

export default ParametersForm;

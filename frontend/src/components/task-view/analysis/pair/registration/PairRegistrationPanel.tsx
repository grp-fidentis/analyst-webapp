import React from 'react';
import { Divider, Stack } from '@mui/material';

import Measurements from '../shared/Measurements';

import ParametersForm from './ParametersForm';
import ManualAlignment from './ManualAlignment';

const PairRegistrationPanel = () => (
	<Stack direction="column" gap={2} divider={<Divider color="#EEEEEE" />}>
		<ParametersForm />
		<ManualAlignment />
		<Measurements />
	</Stack>
);

export default PairRegistrationPanel;

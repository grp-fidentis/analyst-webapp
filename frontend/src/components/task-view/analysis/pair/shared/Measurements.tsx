import React, { useContext } from 'react';
import { Grid, Input, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';

import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import LabeledTextField from '../../../../common/LabeledTextField';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import { AnalyticalTaskService } from '../../../../../services/AnalyticalTaskService';
import {
	ExportType,
	LoadingOperation
} from '../../../../../types/AnalyticalTaskTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { DownloadUtils } from '../../../../../utils/DownloadUtils';

const Measurements = () => {
	const { task, currentPanel } = useContext(TaskContext);
	const { distances, exportDistances, isLoadingInfo, updateLoadingInfo } =
		useContext(AnalyticalTaskContext);
	const {
		featurePointsHighlightThreshold,
		setFeaturePointsHighlightThreshold
	} = useContext(SceneContext);

	const exportDistance = async (exportType: ExportType) => {
		if (!task) return;

		updateLoadingInfo(
			true,
			exportType === ExportType.DISTANCE
				? LoadingOperation.ExportDistance
				: LoadingOperation.ExportWeightedDistance
		);
		try {
			await exportDistances(exportType);
		} finally {
			updateLoadingInfo(false);
		}
	};

	return (
		<Grid
			direction="row"
			container
			gap={2}
			sx={{ justifyContent: 'space-around' }}
		>
			<Grid item xs={12} sx={{ pb: 1, textAlign: 'center' }}>
				<b>Measurements</b>
			</Grid>
			<Grid item xs={12}>
				<LabeledTextField
					labelMinWidth={currentPanel === 'DISTANCE' ? 225 : 330}
					text="Mean distance:"
				>
					<>
						<Typography
							sx={{
								textAlign: 'center',
								width: '65px',
								backgroundColor: '#f1f1f1',
								paddingX: '7px',
								fontFamily: 'monospace'
							}}
						>
							{Number(distances.meanDistance).toFixed(3)}
						</Typography>
						{currentPanel === 'DISTANCE' && (
							<LoadingButton
								sx={{ ml: 2 }}
								size="small"
								variant="contained"
								onClick={() => exportDistance(ExportType.DISTANCE)}
								loading={isLoadingInfo(LoadingOperation.ExportDistance, true)}
								disabled={isLoadingInfo(LoadingOperation.ExportDistance)}
							>
								Export
							</LoadingButton>
						)}
					</>
				</LabeledTextField>
			</Grid>
			<Grid item xs={12}>
				<LabeledTextField
					labelMinWidth={currentPanel === 'DISTANCE' ? 225 : 330}
					text="Weighted mean distance:"
				>
					<>
						<Typography
							sx={{
								textAlign: 'center',
								width: '65px',
								backgroundColor: '#f1f1f1',
								paddingX: '7px',
								fontFamily: 'monospace'
							}}
						>
							{Number(distances.weightedMeanDistance).toFixed(3)}
						</Typography>
						{currentPanel === 'DISTANCE' && (
							<LoadingButton
								sx={{ ml: 2 }}
								size="small"
								variant="contained"
								onClick={() => exportDistance(ExportType.WEIGHTED_DISTANCE)}
								loading={isLoadingInfo(
									LoadingOperation.ExportWeightedDistance,
									true
								)}
								disabled={isLoadingInfo(
									LoadingOperation.ExportWeightedDistance
								)}
							>
								Export
							</LoadingButton>
						)}
					</>
				</LabeledTextField>
			</Grid>
			<Grid item xs={12}>
				{currentPanel !== 'DISTANCE' && (
					<LabeledTextField
						labelMinWidth={330}
						text="Highlight feature point pairs closer than:"
					>
						<Input
							size="small"
							sx={{ width: '60px', ml: '0px', pl: '7px' }}
							value={featurePointsHighlightThreshold}
							onChange={e =>
								setFeaturePointsHighlightThreshold(Number(e.target.value))
							}
							inputProps={{
								step: 0.1,
								type: 'number'
							}}
						/>
					</LabeledTextField>
				)}
			</Grid>
		</Grid>
	);
};

export default Measurements;

import React, { ChangeEvent, useContext } from 'react';
import {
	Box,
	Checkbox,
	FormControlLabel,
	Grid,
	IconButton,
	MenuItem,
	Select,
	SelectChangeEvent,
	Stack
} from '@mui/material';
import { Help } from '@mui/icons-material';

import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import { DistanceStrategy } from '../../../../../types/AnalyticalTaskTypes';

const DistanceSettings = () => {
	const {
		calculateDistanceHeatmap,
		distanceTaskDto,
		setDistanceTaskDto,
		isLoadingInfo
	} = useContext(AnalyticalTaskContext);

	const handleChange = (name: string, value: string | boolean) => {
		const dto = {
			...distanceTaskDto,
			[name]: value
		};

		setDistanceTaskDto(dto);
		calculateDistanceHeatmap(dto, false, true);
	};

	const handleValueChange = (
		e:
			| ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
			| SelectChangeEvent<string>
	) => {
		const name = e.target.name;
		const value = e.target.value;
		if (name === 'useRelativeDistance') {
			handleChange(name, value === 'true');
		} else {
			handleChange(name, value);
		}
	};

	const handleCheckboxChange = (e: ChangeEvent<HTMLInputElement>) =>
		handleChange(e.target.name, e.target.checked);

	return (
		<Box component="div">
			<Grid item xs={12} sx={{ pb: 2, textAlign: 'center' }}>
				<b>Distance settings</b>
			</Grid>
			<Stack direction="row" justifyContent="center" gap={3}>
				<Select
					sx={{ minWidth: 200 }}
					name="useRelativeDistance"
					size="small"
					value={distanceTaskDto.useRelativeDistance ? 'true' : 'false'}
					onChange={handleValueChange}
					disabled={isLoadingInfo()}
				>
					<MenuItem value="false">Absolute distance</MenuItem>
					<MenuItem value="true">Relative distance</MenuItem>
				</Select>

				<Select
					sx={{ minWidth: 180 }}
					name="distanceStrategy"
					size="small"
					value={distanceTaskDto.distanceStrategy}
					onChange={handleValueChange}
					disabled={isLoadingInfo()}
				>
					<MenuItem value={DistanceStrategy.PointToTriangle}>
						Point to triangle
					</MenuItem>
					<MenuItem value={DistanceStrategy.PointToPoint}>
						Point to point
					</MenuItem>
					<MenuItem value={DistanceStrategy.RayCasting}>Ray-casting</MenuItem>
				</Select>

				<FormControlLabel
					control={
						<Checkbox
							checked={distanceTaskDto.crop}
							name="crop"
							onChange={handleCheckboxChange}
							disabled={isLoadingInfo()}
						/>
					}
					label="crop"
				/>

				<IconButton
					sx={{ ml: -3 }}
					component="a"
					href="https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/distance"
					target="_blank"
					rel="noopener noreferrer"
				>
					<Help fontSize="medium" color="primary" />
				</IconButton>
			</Stack>
		</Box>
	);
};
export default DistanceSettings;

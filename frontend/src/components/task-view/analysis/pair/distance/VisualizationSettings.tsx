import React, {
	ChangeEvent,
	useContext,
	useState,
	useMemo,
	useEffect
} from 'react';
import {
	Box,
	Button,
	Grid,
	Input,
	MenuItem,
	Select,
	SelectChangeEvent,
	Slider,
	Stack,
	Tooltip
} from '@mui/material';
import { DataGrid, GridColDef, GridSelectionModel } from '@mui/x-data-grid';

import {
	DistanceTaskDto,
	HeatmapRenderMode,
	LoadingOperation,
	ShowSpheres
} from '../../../../../types/AnalyticalTaskTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import LabeledTextField from '../../../../common/LabeledTextField';
import { FaceUtils } from '../../../../../utils/FaceUtils';

const VisualizationSettings = () => {
	const {
		calculateDistanceHeatmap,
		distanceTaskDto,
		setDistanceTaskDto,
		isLoadingInfo,
		setShowSpheres,
		showSpheres
	} = useContext(AnalyticalTaskContext);
	const { updateFace, faces } = useContext(SceneContext);

	const handleValueChange = (
		e:
			| ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
			| SelectChangeEvent<string>
	) => {
		const name = e.target.name;
		const value = e.target.value;

		const dto: DistanceTaskDto = {
			...distanceTaskDto,
			[name]: value
		};

		setDistanceTaskDto(dto);
		if (dto.heatmapRenderMode === HeatmapRenderMode.None) {
			const face = {
				id: FaceUtils.getSecondary(faces).id ?? -1,
				colorsOfHeatmap: undefined
			};
			updateFace(face);
		} else {
			calculateDistanceHeatmap(dto);
		}
	};

	const hasSecondaryFaceFeaturePoints = () =>
		FaceUtils.getSecondary(faces).featurePoints?.featurePointDtos !== undefined;

	return (
		<Box component="div">
			<Grid item xs={12} sx={{ pb: 2, textAlign: 'center' }}>
				<b>Visualization settings</b>
			</Grid>
			<Stack direction="row" justifyContent="space-around" gap={0}>
				<LabeledTextField text="CustomHeatmap:" labelMinWidth={40}>
					<Select
						sx={{ minWidth: 143 }}
						name="heatmapRenderMode"
						size="small"
						value={distanceTaskDto.heatmapRenderMode}
						onChange={handleValueChange}
						disabled={isLoadingInfo()}
					>
						<MenuItem value={HeatmapRenderMode.Standard}>Standard</MenuItem>
						{hasSecondaryFaceFeaturePoints() && (
							<MenuItem value={HeatmapRenderMode.Weighted}>Weighted</MenuItem>
						)}
						<MenuItem value={HeatmapRenderMode.None}>None (hide)</MenuItem>
					</Select>
				</LabeledTextField>

				{hasSecondaryFaceFeaturePoints() && (
					<LabeledTextField text="Spheres:" labelMinWidth={40}>
						<Select
							sx={{ minWidth: 165 }}
							size="small"
							onChange={e => setShowSpheres(e.target.value as ShowSpheres)}
							value={showSpheres}
							disabled={isLoadingInfo()}
						>
							<MenuItem value={ShowSpheres.HideAll}>Hide all</MenuItem>
							<MenuItem value={ShowSpheres.ShowSelected}>
								Show selected
							</MenuItem>
							<MenuItem value={ShowSpheres.ShowAll}>Show all</MenuItem>
						</Select>
					</LabeledTextField>
				)}
			</Stack>
		</Box>
	);
};

export default VisualizationSettings;

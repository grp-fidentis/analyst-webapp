import React, { useContext, useState, useMemo, useEffect } from 'react';
import {
	Box,
	Button,
	CircularProgress,
	Divider,
	Grid,
	Input,
	Slider,
	Stack,
	Tooltip,
	Typography
} from '@mui/material';
import { DataGrid, GridColDef, GridSelectionModel } from '@mui/x-data-grid';
import { Help } from '@mui/icons-material';

import { DistanceTaskDto } from '../../../../../types/AnalyticalTaskTypes';
import { TaskContext } from '../../../../../providers/TaskContextProvider';
import { AnalyticalTaskContext } from '../../../../../providers/AnalyticalTaskContextProvider';
import { SceneContext } from '../../../../../providers/SceneContextProvider';
import { FaceUtils } from '../../../../../utils/FaceUtils';
import useThrottle from '../../../../../hooks/useThrottle';
import useFirstRender from '../../../../../hooks/useFirstRender';

const WeightsOfFeaturePoints = () => {
	const { task } = useContext(TaskContext);
	const { calculateDistanceHeatmap, distanceTaskDto, isLoadingInfo } =
		useContext(AnalyticalTaskContext);
	const { updateFace, faces } = useContext(SceneContext);
	const [selection, setSelection] = useState<GridSelectionModel>([]);
	const [isSignificantSet, setIsSignificantSet] = useState<boolean>(false);
	const isFirstRender = useFirstRender();

	const calculateHeatmapThrottled = useThrottle(
		async calculateHeatmapThrottledFunc =>
			await calculateHeatmapThrottledFunc(),
		500
	);

	const columns: GridColDef[] = useMemo(
		() => [
			{
				field: 'name',
				headerName: 'Name',
				headerAlign: 'center',
				sortable: true,
				align: 'left',
				valueGetter: params => params.row.name,
				flex: 1,
				renderCell: params => (
					<>
						<Tooltip title={params.row.description}>
							<Help sx={{ fontSize: 18 }} color="primary" />
						</Tooltip>
						<Typography sx={{ ml: 1 }}>{params.row.name}</Typography>
					</>
				)
			},
			{
				field: 'size',
				headerName: 'Size',
				headerAlign: 'center',
				sortable: true,
				align: 'right',
				width: 250,
				renderCell: params => (
					<>
						<Slider
							disabled={false}
							value={params.row.size}
							onChange={(_, value) =>
								handleSizeChange(params.row.name, value as number)
							}
							min={0}
							max={100}
							step={0.1}
							sx={{ maxWidth: 160, ml: 0 }}
						/>
						<Input
							size="small"
							sx={{ width: '50px', ml: 2 }}
							disabled={false}
							type="number"
							value={params.row.size}
							onChange={e =>
								handleSizeChange(params.row.name, Number(e.target.value))
							}
							inputProps={{
								step: 0.1,
								min: 0,
								max: 100
							}}
						/>
					</>
				)
			},
			{
				field: 'weight',
				headerName: 'Weight',
				headerAlign: 'center',
				sortable: true,
				align: 'center',
				width: 73,
				renderCell: params => (
					<b>
						{selection.includes(params.row.name)
							? Number(params.row.weight).toFixed(3) ?? -1
							: null}
					</b>
				)
			}
		],
		[selection, distanceTaskDto]
	);

	useEffect(() => {
		selectSignificant();

		return () => {
			updateFace({
				id: FaceUtils.getSecondary(faces).id ?? -1,
				colorsOfHeatmap: undefined
			});
		};
	}, []);

	const handleSizeChange = (name: string, size: number) => {
		const secondary = FaceUtils.getSecondary(faces);
		const fp = secondary.featurePoints?.featurePointDtos.find(
			f => f.name === name
		);
		if (!fp) return;

		fp.size = size;
		updateFace(secondary);

		calculateHeatmapThrottled(() => calculateDistanceHeatmap(distanceTaskDto));
	};

	useEffect(() => {
		if (isFirstRender) return;

		const secondary = FaceUtils.getSecondary(faces);
		secondary.featurePoints?.featurePointDtos.forEach(
			f => (f.selected = selection.includes(f.name))
		);
		updateFace(secondary);

		if (isSignificantSet) {
			setIsSignificantSet(false);
			return;
		}

		calculateHeatmapThrottled(() => calculateDistanceHeatmap(distanceTaskDto));
	}, [selection]);

	const selectSignificant = async () => {
		const dto: DistanceTaskDto = {
			...distanceTaskDto,
			taskId: task?.id ?? -1
		};
		await calculateHeatmapThrottled(
			async () => await calculateDistanceHeatmap(dto, true)
		);

		const names: GridSelectionModel =
			FaceUtils.getSecondary(faces)
				.featurePoints?.featurePointDtos.map(f =>
					!isNaN(f.weight ?? Number.NaN) ? f.name : ''
				)
				.filter(f => f !== '') ?? [];

		setIsSignificantSet(true);
		setSelection(names);
	};

	const onHover = (type: string, name: string) => {
		const isEnter = type === 'mouseenter';

		const secondary = FaceUtils.getSecondary(faces);
		const fp = secondary.featurePoints?.featurePointDtos.find(
			f => f.name === name
		);
		if (!fp) return;

		fp.hovered = isEnter;
		updateFace(secondary);
	};

	return FaceUtils.getSecondary(faces).featurePoints?.featurePointDtos !==
		undefined ? (
		<>
			<Box component="div">
				<Grid item xs={12} sx={{ textAlign: 'center' }}>
					<b>Weights of feature points</b>
				</Grid>
				<Stack direction="row" justifyContent="center">
					<Box
						component="div"
						sx={{
							'mt': '15px',
							'width': '95%',
							'height': '400px',
							'fontSize': '15px',
							'& .MuiDataGrid-footerContainer': {
								minHeight: 30
							}
						}}
					>
						<DataGrid
							sx={{ height: '90%', borderRadius: '0' }}
							rows={
								FaceUtils.getSecondary(faces).featurePoints?.featurePointDtos ??
								[]
							}
							componentsProps={{
								row: {
									onMouseEnter: (e: {
										type: string;
										currentTarget: { dataset: { id: string } };
									}) =>
										onHover(
											e.type as string,
											e.currentTarget.dataset.id as string
										),
									onMouseLeave: (e: {
										type: string;
										currentTarget: { dataset: { id: string } };
									}) =>
										onHover(
											e.type as string,
											e.currentTarget.dataset.id as string
										)
								}
							}}
							rowHeight={35}
							getRowId={row => row.name}
							columns={columns}
							selectionModel={selection}
							onSelectionModelChange={setSelection}
							checkboxSelection
							disableSelectionOnClick
							hideFooterPagination
							hideFooter
						/>
						<Stack
							sx={{
								backgroundColor: 'white',
								border: '0.5px solid #DDD',
								borderTop: '0px solid #DDD',
								px: 2,
								height: '10%'
							}}
							direction="row"
							justifyContent="space-between"
							alignItems="center"
						>
							{selection.length} row{selection.length !== 1 && 's'} selected
							<Box component="div">
								<Button
									size="small"
									variant="contained"
									sx={{ fontSize: 11, ml: -22 }}
									onClick={() => selectSignificant()}
								>
									Select significant
								</Button>
								{isLoadingInfo() && (
									<CircularProgress
										sx={{ ml: 2, mb: -0.7 }}
										thickness={5}
										size={20}
										color="primary"
									/>
								)}
							</Box>
						</Stack>
					</Box>
				</Stack>
			</Box>
			<Divider orientation="horizontal" color="#EEEEEE" />
		</>
	) : null;
};

export default WeightsOfFeaturePoints;

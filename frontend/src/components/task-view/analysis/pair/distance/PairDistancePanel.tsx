import React from 'react';
import { Divider, Stack } from '@mui/material';

import Measurements from '../shared/Measurements';

import DistanceSettings from './DistanceSettings';
import VisualizationSettings from './VisualizationSettings';
import WeightsOfFeaturePoints from './WeightsOfFeaturePoints';

const PairDistancePanel = () => (
	<Stack direction="column" gap={2}>
		<DistanceSettings />
		<Divider orientation="horizontal" color="#EEEEEE" />
		<VisualizationSettings />
		<Divider orientation="horizontal" color="#EEEEEE" />
		<WeightsOfFeaturePoints />
		<Measurements />
	</Stack>
);

export default PairDistancePanel;

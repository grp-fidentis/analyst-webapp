import { FC } from 'react';
import { LinearProgress, Typography } from '@mui/material';

type Props = {
	value: number;
};

const ProgressBar: FC<Props> = ({ value }) => (
	<div style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
		<div style={{ display: 'flex', alignItems: 'center', width: '100%' }}>
			<div style={{ width: '100%', marginRight: '8px' }}>
				<LinearProgress variant="determinate" value={value} />
			</div>
			<div style={{ minWidth: '35px' }}>
				<Typography variant="body2" style={{ fontWeight: 700 }}>{`${Math.round(
					value
				)}%`}</Typography>
			</div>
		</div>
	</div>
);

export default ProgressBar;

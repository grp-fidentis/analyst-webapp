import React, { FC } from 'react';
import { Box, IconButton, Typography } from '@mui/material';
import { Help } from '@mui/icons-material';

type LabeledTextFieldProps = {
	text: string;
	showHelp?: boolean;
	children: JSX.Element;
	labelMinWidth?: number;
};

const LabeledTextField: FC<LabeledTextFieldProps> = ({
	children,
	text,
	showHelp = false,
	labelMinWidth = 190
}) => (
	<Box component="div" className="flex-row flex-row-center" sx={{ gap: 2 }}>
		<Typography sx={{ minWidth: labelMinWidth }}>{text}</Typography>
		{children}
		{showHelp && (
			<IconButton>
				<Help fontSize="medium" color="primary" />
			</IconButton>
		)}
	</Box>
);

export default LabeledTextField;

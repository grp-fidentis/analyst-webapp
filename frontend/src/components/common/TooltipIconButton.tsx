import { FC } from 'react';
import { IconButton, Tooltip } from '@mui/material';
import { IconButtonProps } from '@mui/material/IconButton/IconButton';

type Props = {
	props?: IconButtonProps;
	children: JSX.Element;
	title: string;
	disableRipple?: boolean;
	onClick?: () => void;
	disabled?: boolean;
	isLabel?: boolean;
	className?: string;
	ariaControls?: string;
	placement?:
		| 'bottom-end'
		| 'bottom-start'
		| 'bottom'
		| 'left-end'
		| 'left-start'
		| 'left'
		| 'right-end'
		| 'right-start'
		| 'right'
		| 'top-end'
		| 'top-start'
		| 'top';
};

const TooltipIconButton: FC<Props> = ({
	props,
	children,
	title,
	isLabel = false,
	disableRipple = false,
	disabled = false,
	placement = 'bottom',
	ariaControls,
	className = '',
	onClick
}) => {
	const getComponentType = () => {
		if (disabled) return 'div';
		if (isLabel) return 'label';
		return undefined;
	};

	const adjustedButtonProps = {
		disabled,
		component: getComponentType(),
		onClick: disabled ? undefined : onClick
	};

	return (
		<Tooltip title={title} placement={placement}>
			<span>
				<IconButton
					{...props}
					className={className}
					aria-controls={ariaControls}
					disableRipple={disableRipple}
					{...adjustedButtonProps}
				>
					{children}
				</IconButton>
			</span>
		</Tooltip>
	);
};

export default TooltipIconButton;

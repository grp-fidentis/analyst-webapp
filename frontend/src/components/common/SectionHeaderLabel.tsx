import { FC } from 'react';
import { Typography } from '@mui/material';

type Props = {
	text: string;
};

const SectionHeaderLabel: FC<Props> = ({ text }) => (
	<Typography sx={{ fontWeight: 700, pb: 1, fontSize: 20 }}>{text}</Typography>
);

export default SectionHeaderLabel;

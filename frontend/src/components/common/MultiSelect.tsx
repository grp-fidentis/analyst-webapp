import { SyntheticEvent, useState, useEffect } from 'react';
import { Autocomplete, Chip, TextField } from '@mui/material';

type Props<T> = {
	readonly options: T[];
	defaultValue?: T[];
	label: string;
	required?: boolean;
	getOptionLabel: (option: T) => string;
	onChange: (value: T[]) => void;
	minWidth?: number;
	disabled?: boolean;
	fixedOptions?: T[];
	disabledOptionIds?: number[];
};

const ChipLabel = ({ name }: { name: string }) => <span>{name}</span>;

const MultiSelect = <T extends Record<string, unknown>>({
	defaultValue,
	label,
	required,
	options,
	getOptionLabel,
	onChange,
	fixedOptions = [],
	disabledOptionIds = [],
	disabled = false,
	minWidth = 226
}: Props<T>) => {
	const [value, setValue] = useState<T[]>(defaultValue ?? []);
	const [inputValue, setInputValue] = useState('');

	const handleInputChange = (_: SyntheticEvent, newInputValue: string) => {
		setInputValue(newInputValue);
	};

	useEffect(() => {
		onChange(value);
	}, [value]);

	const handleValueChange = (_: SyntheticEvent, newValue: T[]) => {
		setValue(newValue);
	};

	return (
		<Autocomplete
			value={value}
			onChange={handleValueChange}
			inputValue={inputValue}
			onInputChange={handleInputChange}
			getOptionDisabled={(option: T) =>
				disabledOptionIds?.includes(option.id as number)
			}
			renderTags={(tagValue, getTagProps) =>
				tagValue.map((option, index) => (
					// eslint-disable-next-line react/jsx-key  -- it has specified key by `...getTagProps`
					<Chip
						label={<ChipLabel name={option.name as string} />}
						{...getTagProps({ index })}
						disabled={
							disabled ||
							fixedOptions?.map(option => option.id)?.indexOf(option.id) !== -1
						}
					/>
				))
			}
			disabled={disabled}
			multiple
			id="tags-standard"
			options={options}
			getOptionLabel={getOptionLabel}
			isOptionEqualToValue={(option, value) => option.id === value.id}
			sx={{ minWidth }}
			renderInput={params => (
				<TextField
					{...params}
					variant="outlined"
					label={label}
					margin="normal"
					required={required}
				/>
			)}
		/>
	);
};

export default MultiSelect;

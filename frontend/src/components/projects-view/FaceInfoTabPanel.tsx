import { ChangeEvent, FC, useContext, useEffect, useState } from 'react';
import { Box, LinearProgress, Stack, Typography } from '@mui/material';

import { ReactComponent as Silhouette } from '../../assets/images/silhouette.svg';
import SectionHeaderLabel from '../common/SectionHeaderLabel';
import { FilesContext } from '../../providers/FileContextProvider';
import { ProjectsContext } from '../../providers/ProjectContextProvider';
import { FaceInfoService } from '../../services/FaceInfoService';
import { FaceInfo } from '../../types/FileTypes';

import FaceInfoGeometry from './FaceInfoGeometry';
import FaceInfoFileNames from './FaceInfoFileNames';

const FileInfoLabels = [
	'Geometry file',
	'Geometry file size',
	'Feature points file',
	'Preview photo file',
	'Texture file'
];

type Props = {
	isTaskFaceInfo?: boolean;
	taskFaceInfo?: FaceInfo;
	onFeaturePointsHover?: (isMouseEnter: boolean, name: string) => void;
};

const FaceInfoTabPanel: FC<Props> = ({
	isTaskFaceInfo,
	taskFaceInfo,
	onFeaturePointsHover
}) => {
	const {
		uploadAdditionalFile,
		fileSelection,
		setFaceInfo,
		faceInfo,
		getFaceInfoIdByUploadId
	} = useContext(FilesContext);
	const { selectedProjectId } = useContext(ProjectsContext);
	const [currentFaceInfo, setCurrentFaceInfo] = useState(
		isTaskFaceInfo ? taskFaceInfo : faceInfo
	);

	useEffect(() => {
		if (fileSelection.length === 1) {
			FaceInfoService.getFaceInfoById(
				getFaceInfoIdByUploadId(fileSelection[0] as number)
			).then(res => {
				setFaceInfo(res);
				setCurrentFaceInfo(res);
			});
		} else {
			setFaceInfo(undefined);
		}
	}, [fileSelection, selectedProjectId]);

	const handleAddFace = (
		event: ChangeEvent<HTMLInputElement>,
		type: string
	) => {
		if (!selectedProjectId) {
			return null;
		}

		const formData = new FormData();
		const files = event.target.files;

		Array.from(files ?? []).forEach(file => formData.append('file', file));
		formData.append('fileType', type);

		if (faceInfo) {
			uploadAdditionalFile(formData, selectedProjectId, faceInfo.id);
		}
	};

	return (
		<>
			<Box
				component="div"
				className="flex-row"
				sx={{ flexWrap: 'wrap', mb: 4 }}
			>
				<Box component="div" sx={{ flexGrow: 1 }}>
					<SectionHeaderLabel text="Photo" />

					{currentFaceInfo?.previewPhotoData ? (
						<img
							src={`data:image/jpeg;base64,${currentFaceInfo.previewPhotoData}`}
							alt="preview"
							width={307}
							height={205}
						/>
					) : (
						<Silhouette style={{ width: '168px', height: '168px' }} />
					)}
				</Box>

				<FaceInfoGeometry
					faceInfo={currentFaceInfo}
					onFeaturePointsHover={onFeaturePointsHover}
				/>
			</Box>

			<Box component="div" sx={{ flexGrow: 1 }}>
				<SectionHeaderLabel text="File info" />
				<Stack direction="row" gap={1}>
					<Stack>
						{FileInfoLabels.map((value, index) => (
							<Typography key={`${value}-${index}`}>{value}:</Typography>
						))}
					</Stack>

					<Stack flex={1}>
						<Typography>
							{currentFaceInfo?.geometryFileName ?? 'N/A'}
						</Typography>
						<Typography>
							{currentFaceInfo?.geometryFileSize ?? 'N/A'} kB
						</Typography>
						<FaceInfoFileNames
							name={currentFaceInfo?.featurePointsFileName}
							showUpload={fileSelection.length === 1}
							acceptedExtensions=".csv"
							fileType="points"
							tooltipText="File has to match name with `.obj` file and needs suffix: '_landmarks'"
							onChange={e => handleAddFace(e, 'points')}
						/>
						<FaceInfoFileNames
							name={currentFaceInfo?.previewPhotoFileName}
							showUpload={fileSelection.length === 1}
							acceptedExtensions="image/*"
							fileType="preview"
							onChange={e => handleAddFace(e, 'preview')}
						/>
						<FaceInfoFileNames
							name={currentFaceInfo?.textureFileName}
							showUpload={fileSelection.length === 1}
							acceptedExtensions="image/*"
							fileType="texture"
							tooltipText="Texture file has to match name with `.obj` file"
							onChange={e => handleAddFace(e, 'texture')}
						/>
					</Stack>
				</Stack>
			</Box>
		</>
	);
};

export default FaceInfoTabPanel;

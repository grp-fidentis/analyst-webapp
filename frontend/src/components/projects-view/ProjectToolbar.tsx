import { ChangeEvent, FC, useContext, useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import { useConfirm } from 'material-ui-confirm';
import { toast } from 'react-toastify';

import { ReactComponent as AddFaceIcon } from '../../assets/images/face_add_icon.svg';
import { ReactComponent as RemoveFaceIcon } from '../../assets/images/face_minus_icon.svg';
import { ReactComponent as AnalyzeIcon } from '../../assets/images/analyze_icon.svg';
import TooltipIconButton from '../common/TooltipIconButton';
import { TaskContext } from '../../providers/TaskContextProvider';
import { FilesContext } from '../../providers/FileContextProvider';
import { ProjectsContext } from '../../providers/ProjectContextProvider';
import { FileUploadService } from '../../services/FileUploadService';

import SelectPrimaryFaceDialog from './dialogs/SelectPrimaryFaceDialog';

type AnalyzeButtonProps = {
	text: string;
	onClick?: () => void;
	disabled: boolean;
};

const AnalyzeButton: FC<AnalyzeButtonProps> = ({ text, onClick, disabled }) => (
	<Button
		startIcon={<AnalyzeIcon />}
		sx={{ color: 'black' }}
		onClick={onClick}
		className="analyze__button"
		disabled={disabled}
	>
		<Typography sx={{ textTransform: 'lowercase', fontSize: '16px' }}>
			{text}
		</Typography>
	</Button>
);

const ProjectToolbar = () => {
	const confirm = useConfirm();
	const [openSelectPrimaryFace, setOpenSelectPrimaryFace] = useState(false);
	const { createTask } = useContext(TaskContext);
	const { selectedProjectId } = useContext(ProjectsContext);
	const { saveFiles, deleteFilesFromProject, fileSelection, files, setFiles } =
		useContext(FilesContext);

	const handleClickRemoveFace = async () => {
		await confirm({
			description:
				'This will delete ONLY selected files which are NOT in some tasks. If you want to delete file make sure it is not in any task',
			confirmationButtonProps: { variant: 'contained' }
		});

		await deleteFilesFromProject(fileSelection as number[]);
		if (!selectedProjectId) {
			return;
		}
		const projectFiles = await FileUploadService.getUserFilesOnProject(
			selectedProjectId
		);
		setFiles(projectFiles);
	};

	const handleAddFace = (event: ChangeEvent<HTMLInputElement>) => {
		const formData = new FormData();
		const files = event.target.files;

		Array.from(files ?? []).forEach(file => formData.append('files', file));
		if (files != undefined && files?.length > 20) {
			toast.error('You can upload a maximum of 20 files at the once.');
			return;
		}
		saveFiles(formData);
	};

	const handleClickAnalyzeButton = async () => {
		if (fileSelection.length === 2) {
			setOpenSelectPrimaryFace(true);
		} else {
			createTask({
				projectId: Number(selectedProjectId),
				fileUploadIds: fileSelection as number[]
			});
		}
	};

	return (
		<Box component="div" className="flex-row" sx={{ my: 2 }}>
			<Box component="div" className="flex-row">
				<TooltipIconButton
					title="Add face"
					ariaControls="add-face-button"
					isLabel
				>
					<>
						<input
							hidden
							accept=".obj"
							multiple
							type="file"
							onChange={handleAddFace}
						/>
						<AddFaceIcon />
					</>
				</TooltipIconButton>
				<TooltipIconButton
					title="Remove face"
					ariaControls="remove-face-button"
					disabled={!fileSelection.length}
					className="remove-face__icon"
					onClick={handleClickRemoveFace}
				>
					<RemoveFaceIcon />
				</TooltipIconButton>

				<Divider orientation="vertical" flexItem sx={{ mx: 2 }} />

				<Box
					component="div"
					className="flex-row flex-row-center"
					sx={{ gap: 1 }}
				>
					<AnalyzeButton
						text="single face analysis"
						disabled={fileSelection.length !== 1}
						onClick={handleClickAnalyzeButton}
					/>
					<AnalyzeButton
						text="pair comparison"
						disabled={fileSelection.length !== 2}
						onClick={handleClickAnalyzeButton}
					/>
					<AnalyzeButton
						text="batch processing"
						disabled={fileSelection.length < 3}
						onClick={handleClickAnalyzeButton}
					/>
				</Box>
			</Box>

			{openSelectPrimaryFace && (
				<SelectPrimaryFaceDialog
					open={openSelectPrimaryFace}
					selectedFiles={files.filter(f => fileSelection.includes(f.id))}
					onClose={() => setOpenSelectPrimaryFace(false)}
				/>
			)}
		</Box>
	);
};

export default ProjectToolbar;

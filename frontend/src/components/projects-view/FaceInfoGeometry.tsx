import { FC, useContext, useState } from 'react';
import {
	Box,
	List,
	ListItem,
	ListItemText,
	Stack,
	Typography
} from '@mui/material';
import { LoadingButton } from '@mui/lab';

import SectionHeaderLabel from '../common/SectionHeaderLabel';
import { FaceInfo } from '../../types/FileTypes';
import { FilesContext } from '../../providers/FileContextProvider';
import { FaceInfoService } from '../../services/FaceInfoService';

type Props = {
	faceInfo?: FaceInfo;
	onFeaturePointsHover?: (isMouseEnter: boolean, name: string) => void;
};

const FaceInfoGeometry: FC<Props> = ({ faceInfo, onFeaturePointsHover }) => {
	const [loading, setLoading] = useState(false);
	const { fileSelection, setFaceInfo } = useContext(FilesContext);

	const showComputeButton =
		!faceInfo?.numberOfVertices && fileSelection.length !== 0 && faceInfo;

	const computeGeometry = async () => {
		if (faceInfo && fileSelection.length === 1) {
			setLoading(true);
			FaceInfoService.computeFaceInfoGeometry(
				faceInfo?.id,
				fileSelection[0] as number
			)
				.then(res => setFaceInfo(res))
				.finally(() => setLoading(false));
		}
	};

	return (
		<Box component="div" sx={{ flexGrow: 4 }}>
			<SectionHeaderLabel text="Geometry Info" />

			<Stack direction="row" gap={1}>
				<Stack>
					<Typography>Number of vertices:</Typography>
					<Typography>Number of facets:</Typography>
					{faceInfo?.featurePointsNames?.length && (
						<Typography>Feature points:</Typography>
					)}
				</Stack>
				<Stack>
					<Typography>{faceInfo?.numberOfVertices ?? 'N/A'}</Typography>
					<Typography>{faceInfo?.numberOfFacets ?? 'N/A'}</Typography>

					{faceInfo?.featurePointsNames?.length && (
						<Box
							component="div"
							sx={{
								width: '100%',
								height: '100%',
								maxHeight: 66,
								overflow: 'auto',
								border: '1px solid black',
								borderRadius: 0.7
							}}
						>
							<List
								style={{
									padding: 0,
									margin: 0
								}}
							>
								{faceInfo?.featurePointsNames
									.sort((a, b) => (a > b ? 1 : -1))
									.map((f, i) => (
										<ListItem
											style={{
												padding: 5,
												cursor: 'default',
												height: 22,
												backgroundColor: i % 2 === 0 ? 'white' : '#269c9221'
											}}
											key={i}
											onMouseEnter={_ =>
												onFeaturePointsHover
													? onFeaturePointsHover(true, f)
													: null
											}
											onMouseLeave={_ =>
												onFeaturePointsHover
													? onFeaturePointsHover(false, f)
													: null
											}
										>
											<ListItemText primary={f} />
										</ListItem>
									))}
							</List>
						</Box>
					)}
				</Stack>
			</Stack>

			{showComputeButton && (
				<LoadingButton
					variant="contained"
					sx={{ mt: 2 }}
					onClick={computeGeometry}
					loading={loading}
				>
					Compute
				</LoadingButton>
			)}
		</Box>
	);
};

export default FaceInfoGeometry;

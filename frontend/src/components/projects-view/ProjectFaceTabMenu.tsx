import { useState, SyntheticEvent } from 'react';
import { Box, Stack, Tab } from '@mui/material';
import { TabContext, TabList, TabPanel } from '@mui/lab';

import FaceInfoTabPanel from './FaceInfoTabPanel';
import FaceFilterTabPanel from './FaceFilterTabPane';

const ProjectFaceTabMenu = () => {
	const [value, setValue] = useState('face-info');

	const handleChange = (_: SyntheticEvent, newValue: string) => {
		setValue(newValue);
	};

	return (
		<Stack sx={{ width: '100%', typography: 'body1' }}>
			<TabContext value={value}>
				<Box component="div" sx={{ borderBottom: 1, borderColor: '#595959' }}>
					<TabList
						className="face-tabs"
						onChange={handleChange}
						aria-label="lab API tabs example"
					>
						<Tab label="FACE INFO" value="face-info" />
						<Tab label="FILTER" value="face-filter" />
					</TabList>
				</Box>
				<TabPanel value="face-info" sx={{ flex: 1, pr: 0 }}>
					<FaceInfoTabPanel />
				</TabPanel>
				<TabPanel value="face-filter">
					<FaceFilterTabPanel />
				</TabPanel>
			</TabContext>
		</Stack>
	);
};

export default ProjectFaceTabMenu;

import { ChangeEvent, useState } from 'react';
import {
	Box,
	Checkbox,
	FormControlLabel,
	FormGroup,
	TextField
} from '@mui/material';

import SectionHeaderLabel from '../common/SectionHeaderLabel';

const FilterByName = () => {
	const [state, setState] = useState({
		gilad: true,
		jason: false,
		antoine: false
	});

	const handleChangeCheckbox = (event: ChangeEvent<HTMLInputElement>) => {
		setState({
			...state,
			[event.target.name]: event.target.checked
		});
	};

	return (
		<Box component="div">
			<SectionHeaderLabel text="Assign responsibility" />
			<FormGroup sx={{ gap: 1 }}>
				<Box component="div" className="flex-row">
					<FormControlLabel
						control={
							<Checkbox
								checked={state.gilad}
								onChange={handleChangeCheckbox}
								name="gilad"
							/>
						}
						label="has text"
					/>
					<TextField
						variant="outlined"
						size="small"
						fullWidth
						sx={{ flex: 1 }}
					/>
				</Box>
				<Box component="div" className="flex-row">
					<FormControlLabel
						control={
							<Checkbox
								checked={state.jason}
								onChange={handleChangeCheckbox}
								name="jason"
							/>
						}
						label="has not text"
					/>
					<TextField variant="outlined" size="small" sx={{ flex: 1 }} />
				</Box>
			</FormGroup>
		</Box>
	);
};

export default FilterByName;

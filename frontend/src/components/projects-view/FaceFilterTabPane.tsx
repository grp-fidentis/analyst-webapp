import { Box, Button } from '@mui/material';

import FilterByFeaturePoints from './FilterByFeaturePoints';
import FilterByName from './FilterByName';
import FilterFeaturePoints from './FilterFeaturePoints';

const FaceFilterTabPane = () => (
	<Box component="div">
		<Box component="div" sx={{ display: 'flex', gap: 2, mb: 3 }}>
			<Button variant="outlined">Apply filter</Button>
			<Button variant="outlined">Clear filters</Button>
			<Button variant="outlined">Step back</Button>
		</Box>

		<Box component="div" className="flex-row" sx={{ gap: 2, mb: 2 }}>
			<FilterByFeaturePoints />
			<FilterByName />
		</Box>

		<FilterFeaturePoints />
	</Box>
);

export default FaceFilterTabPane;

import { DataGrid, GridColDef } from '@mui/x-data-grid';
import {
	Box,
	FormControl,
	IconButton,
	MenuItem,
	Select,
	SelectChangeEvent
} from '@mui/material';
import { ChangeEvent, useContext, useEffect, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';

import { ReactComponent as Silhouette } from '../../assets/images/silhouette.svg';
import { ProjectsContext } from '../../providers/ProjectContextProvider';
import { FilesContext } from '../../providers/FileContextProvider';
import { ROUTES } from '../../utils/RoutesUrls';
import { Task } from '../../types/TaskTypes';

const BasicSelect = ({ tasks }: { tasks: Array<Task> }) => {
	const navigate = useNavigate();

	const handleChange = (event: SelectChangeEvent) => {
		navigate(`${ROUTES.tasks}/${event.target.value}`);
	};

	return (
		<Box component="div" sx={{ minWidth: 160 }}>
			<FormControl sx={{ pl: 0 }} fullWidth>
				<Select
					labelId="demo-simple-select-label"
					id="demo-simple-select"
					value=""
					label="Age"
					onChange={handleChange}
				>
					{tasks?.map(task => (
						<MenuItem
							key={task.id}
							value={task.id}
							disableRipple
							disableTouchRipple
						>
							{/* eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing  -- I want empty string to be false also */}
							{task.name ? task.name : `Task ${task.id}`}
						</MenuItem>
					))}
				</Select>
			</FormControl>
		</Box>
	);
};

const PreviewPhoto = ({ fileData, id }: { fileData: string; id: number }) => {
	const { uploadSmallPreviewPhoto, fileSelection } = useContext(FilesContext);

	const canUploadPreviewPhoto = useMemo(
		() => fileSelection.length === 1 && fileSelection[0] === id,
		[fileSelection]
	);

	const handleFileUpload = (event: ChangeEvent<HTMLInputElement>) => {
		const formData = new FormData();
		const files = event.target.files;

		Array.from(files ?? []).forEach(file => formData.append('file', file));

		uploadSmallPreviewPhoto(formData, id);
	};

	if (fileData) {
		return (
			<img
				src={`data:image/jpeg;base64,${fileData}`}
				alt="Face preview"
				height={50}
			/>
		);
	}

	return canUploadPreviewPhoto ? (
		<IconButton component="label" sx={{ p: 0 }}>
			<input hidden accept="image/*" type="file" onChange={handleFileUpload} />
			<Silhouette />
		</IconButton>
	) : (
		<Silhouette />
	);
};

const columns: GridColDef[] = [
	{
		field: 'name',
		headerName: 'Face Name',
		editable: false,
		flex: 1
	},
	{
		field: 'tasks',
		headerName: 'Open in task',
		sortable: false,
		disableColumnMenu: true,
		cellClassName: 'select-cell',
		width: 160,
		editable: false,
		renderCell: params => <BasicSelect tasks={params.row.tasks} />
	},
	{
		field: 'photo',
		headerName: 'Photo',
		headerAlign: 'center',
		align: 'center',
		sortable: false,
		disableColumnMenu: true,
		width: 150,
		editable: false,
		renderCell: params => (
			<PreviewPhoto
				fileData={params.row.smallPreviewPhotoData}
				id={params.row.id}
			/>
		)
	}
];

const ProjectFilesTable = () => {
	const { selectedProjectId } = useContext(ProjectsContext);
	const { files, loading, getProjectFiles, fileSelection, setFileSelection } =
		useContext(FilesContext);

	useEffect(() => {
		getProjectFiles();
	}, [selectedProjectId]);

	return (
		<Box
			component="div"
			sx={{
				'height': '600px',
				'& .MuiDataGrid-cell.select-cell': {
					p: 0
				}
			}}
		>
			<DataGrid
				rows={files ?? []}
				columns={columns}
				checkboxSelection
				selectionModel={fileSelection}
				onSelectionModelChange={setFileSelection}
				disableSelectionOnClick
				loading={loading}
			/>
		</Box>
	);
};
export default ProjectFilesTable;

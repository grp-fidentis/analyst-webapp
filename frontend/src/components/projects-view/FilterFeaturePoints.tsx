import { Box, Checkbox, FormControlLabel } from '@mui/material';

import SectionHeaderLabel from '../common/SectionHeaderLabel';

const FeaturePoints = [
	'Exocanthion R',
	'Endocanthion R',
	'Glabella',
	'Subnasale',
	'Pronasale',
	'Cheilion R',
	'Zygion II R'
];

const FilterFeaturePoints = () => (
	<Box component="div">
		<SectionHeaderLabel text="Feature points" />

		<Box
			component="div"
			sx={{ display: 'flex', flexWrap: 'wrap', columnGap: 1 }}
		>
			{FeaturePoints.map(point => (
				<FormControlLabel
					key={`key-${point}`}
					control={<Checkbox name="gilad" />}
					label={point}
					sx={{ width: '33%', mr: 0 }}
				/>
			))}
		</Box>
	</Box>
);

export default FilterFeaturePoints;

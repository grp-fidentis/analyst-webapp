import { SyntheticEvent, useContext, useState } from 'react';
import { Divider, IconButton, Stack, Tab } from '@mui/material';
import { TabContext, TabList } from '@mui/lab';
import { Close } from '@mui/icons-material';

import { ReactComponent as OpenProject } from '../../assets/images/open_project.svg';
import { ReactComponent as CreateNewProject } from '../../assets/images/create_new_project.svg';
import { ReactComponent as DeleteProject } from '../../assets/images/delete_project.svg';
import { ProjectsContext } from '../../providers/ProjectContextProvider';
import TooltipIconButton from '../common/TooltipIconButton';
import { FilesContext } from '../../providers/FileContextProvider';

import NewProjectDialog from './dialogs/NewProjectDialog';
import OpenProjectDialog from './dialogs/OpenProjectDialog';
import DeleteProjectsDialog from './dialogs/DeleteProjectsDialog';

const ProjectTabMenu = () => {
	const [open, setOpen] = useState(false);
	const [openNew, setOpenNew] = useState(false);
	const [openDelete, setOpenDelete] = useState(false);
	const { setFileSelection } = useContext(FilesContext);
	const { selectedProjectId, setSelectedProjectId, projects, closeProject } =
		useContext(ProjectsContext);

	const handleChange = (_: SyntheticEvent, projectId: string) => {
		setFileSelection([]);
		setSelectedProjectId(Number(projectId));
	};

	const handleCloseProject = (e: SyntheticEvent, id: number) => {
		e.stopPropagation();
		setFileSelection([]);
		closeProject(Number(id));
	};

	return (
		<Stack direction="row" sx={{ borderBottom: 1, borderColor: '#000' }}>
			<TabContext value={selectedProjectId?.toString() ?? ''}>
				<TabList
					onChange={handleChange}
					aria-label="scrollable force tabs"
					variant="scrollable"
					className="project-tabs"
					scrollButtons
					allowScrollButtonsMobile
					sx={{ flex: '1' }}
				>
					{projects.map(project => (
						<Tab
							key={project.id}
							label={
								<span>
									{project.name}
									<IconButton
										component="span"
										size="small"
										className="close-button"
										onClick={e => handleCloseProject(e, project.id)}
									>
										<Close />
									</IconButton>
								</span>
							}
							value={project.id.toString() ?? ''}
						/>
					))}
				</TabList>
			</TabContext>

			<Divider orientation="vertical" flexItem sx={{ mr: 1 }} />

			<Stack direction="row">
				<TooltipIconButton
					title="Create new project"
					onClick={() => setOpenNew(true)}
				>
					<CreateNewProject style={{ marginTop: '3px' }} />
				</TooltipIconButton>

				<TooltipIconButton
					title="Delete project"
					onClick={() => setOpenDelete(true)}
				>
					<DeleteProject style={{ marginTop: '3px' }} />
				</TooltipIconButton>

				<TooltipIconButton title="Open project" onClick={() => setOpen(true)}>
					<OpenProject style={{ marginTop: '3px' }} />
				</TooltipIconButton>
			</Stack>

			{open && <OpenProjectDialog open={open} onClose={() => setOpen(false)} />}
			{openNew && (
				<NewProjectDialog open={openNew} onClose={() => setOpenNew(false)} />
			)}
			{openDelete && (
				<DeleteProjectsDialog
					open={openDelete}
					onClose={() => setOpenDelete(false)}
				/>
			)}
		</Stack>
	);
};

export default ProjectTabMenu;

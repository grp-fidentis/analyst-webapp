import { ChangeEvent, FC, useContext } from 'react';
import { IconButton, Stack, Tooltip, Typography } from '@mui/material';
import { Delete, HelpOutline, Upload } from '@mui/icons-material';
import { toast } from 'react-toastify';
import { useConfirm } from 'material-ui-confirm';

import { FilesContext } from '../../providers/FileContextProvider';
import { FileUploadService } from '../../services/FileUploadService';
import { FaceInfoService } from '../../services/FaceInfoService';

type FileNamesProps = {
	name?: string | null;
	onChange: (event: ChangeEvent<HTMLInputElement>) => void;
	acceptedExtensions: string;
	showUpload: boolean;
	fileType: string;
	tooltipText?: string;
};

const parseNameAndExtension = (name: string) => name.split(/\.(?=[^.]+$)/);

const FaceInfoFileNames: FC<FileNamesProps> = ({
	name,
	onChange,
	acceptedExtensions,
	showUpload,
	fileType,
	tooltipText
}) => {
	const { faceInfo, setFaceInfo } = useContext(FilesContext);
	const confirm = useConfirm();

	const handleDelete = async () => {
		if (faceInfo?.id) {
			const [fileName, extension] = parseNameAndExtension(name ?? '');

			confirm({
				description:
					'This action will delete linked file for selected `.obj` file',
				confirmationButtonProps: { variant: 'contained' }
			})
				.then(async () => {
					await toast.promise(
						FileUploadService.deleteFileByFaceInfoId(faceInfo.id, {
							name: fileName,
							extension,
							fileInfoType: fileType
						}),
						{
							pending: 'Deleting file',
							error: "Couldn't delete file",
							success: 'File deleted successfully'
						}
					);
					FaceInfoService.getFaceInfoById(faceInfo.id).then(res => {
						setFaceInfo(res);
					});
				})
				.catch(() => null);
		}
	};

	return (
		<Stack direction="row">
			<Stack direction="row">
				<Typography variant="body2" pr={1}>
					{name ?? 'N/A'}
				</Typography>
				{name && showUpload && (
					<IconButton sx={{ p: 0 }} onClick={handleDelete}>
						<Delete />
					</IconButton>
				)}
			</Stack>
			{!name && showUpload && (
				<Stack direction="row" alignItems="center">
					<IconButton component="label" sx={{ p: 0 }}>
						<input
							hidden
							accept={acceptedExtensions}
							type="file"
							onChange={onChange}
						/>
						<Upload />
					</IconButton>

					{tooltipText && (
						<Tooltip title={tooltipText}>
							<HelpOutline color="action" fontSize="small" />
						</Tooltip>
					)}
				</Stack>
			)}
		</Stack>
	);
};

export default FaceInfoFileNames;

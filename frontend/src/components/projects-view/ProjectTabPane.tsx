import { useContext } from 'react';
import { TabContext, TabPanel } from '@mui/lab';
import { Box, Divider, Stack, Typography } from '@mui/material';

import { ProjectsContext } from '../../providers/ProjectContextProvider';

import ProjectFaceTabMenu from './ProjectFaceTabMenu';
import ProjectToolbar from './ProjectToolbar';
import ProjectFilesTable from './ProjectFilesTable';

const ProjectTabPane = () => {
	const { selectedProjectId, projects, isLoadingProjects } =
		useContext(ProjectsContext);

	if (!projects.length && !isLoadingProjects) {
		return (
			<TabContext value={selectedProjectId?.toString() ?? ''}>
				<Stack justifyContent="center" alignItems="center" sx={{ height: 250 }}>
					<Typography variant="h5">
						No project is selected. Open existing project or create new one.
					</Typography>
				</Stack>
			</TabContext>
		);
	}

	return (
		<TabContext value={selectedProjectId?.toString() ?? ''}>
			{projects.map(project => (
				<TabPanel
					key={project.id}
					value={project.id.toString() ?? ''}
					sx={{ p: 0 }}
				>
					<Stack
						direction="row"
						divider={
							<Divider
								sx={{ backgroundColor: '#000' }}
								orientation="vertical"
								flexItem
							/>
						}
					>
						<Stack px={2} sx={{ flex: '1.5' }}>
							<ProjectToolbar />
							<ProjectFilesTable />
						</Stack>

						<Box component="div" sx={{ display: 'flex', flex: '1' }}>
							<ProjectFaceTabMenu />
						</Box>
					</Stack>
				</TabPanel>
			))}
		</TabContext>
	);
};

export default ProjectTabPane;

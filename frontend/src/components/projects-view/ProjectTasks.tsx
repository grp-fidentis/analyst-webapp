import { MouseEvent, useContext, useEffect, useState } from 'react';
import { Button, Menu, MenuItem } from '@mui/material';
import { KeyboardArrowDownRounded } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';

import { TaskContext } from '../../providers/TaskContextProvider';
import { ProjectsContext } from '../../providers/ProjectContextProvider';
import { ROUTES } from '../../utils/RoutesUrls';

const ITEM_HEIGHT = 48;

const ProjectTasks = () => {
	const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
	const { getTasksOnProject, projectTasks, isAnalyticalTaskLoading } = useContext(TaskContext);
	const { selectedProjectId } = useContext(ProjectsContext);
	const navigate = useNavigate();
	const open = Boolean(anchorEl);

	useEffect(() => {
		if (selectedProjectId) {
			getTasksOnProject(Number(selectedProjectId));
		}
	}, [selectedProjectId]);

	const handleOpenMenu = (event: MouseEvent<HTMLButtonElement>) => {
		document.body.style.overflow = 'hidden';
		setAnchorEl(event.currentTarget);
	};

	const handleClose = (taskId?: number) => {
		document.body.style.overflowY = 'auto';
		document.body.style.overflowX = 'hidden';
		if (taskId) {
			navigate(`${ROUTES.tasks}/${taskId}`);
		}
		setAnchorEl(null);
	};

	return projectTasks?.length ? (
		<>
			<Button
				disabled={isAnalyticalTaskLoading}
				id="project-tasks__button"
				onClick={handleOpenMenu}
				disableRipple
				endIcon={<KeyboardArrowDownRounded />}
				aria-controls={open ? 'project-task__menu' : undefined}
				aria-haspopup="true"
				aria-expanded={open ? 'true' : undefined}
			>
				Tasks
			</Button>

			<Menu
				id="project-task__menu"
				anchorEl={anchorEl}
				open={open}
				onClose={() => handleClose()}
				PaperProps={{
					style: {
						maxHeight: ITEM_HEIGHT * 6
					}
				}}
				disableScrollLock
				MenuListProps={{
					'aria-labelledby': 'project-tasks__button'
				}}
			>
				{projectTasks
					?.sort((a, b) => 0 - (a.id > b.id ? 1 : -1))
					.map(task => (
						<MenuItem key={task.id} onClick={() => handleClose(task.id)}>
							{/* eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing  -- I want empty string to be false also */}
							{task.name || `Task ${task.id}`}
						</MenuItem>
					))}
			</Menu>
		</>
	) : null;
};

export default ProjectTasks;

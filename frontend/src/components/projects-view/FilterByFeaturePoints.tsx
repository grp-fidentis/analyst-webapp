import { ChangeEvent, useState } from 'react';
import { Box, FormControlLabel, Radio, RadioGroup } from '@mui/material';

import SectionHeaderLabel from '../common/SectionHeaderLabel';

export enum ByFeatureValues {
	'SOME' = 'SOME',
	'ALL' = 'ALL',
	'NONE' = 'NONE'
}

const FilterByFeaturePoints = () => {
	const [value, setValue] = useState<keyof ByFeatureValues | null>(null);

	const handleChangeRadio = (event: ChangeEvent<HTMLInputElement>) => {
		setValue(event.target.value as keyof ByFeatureValues);
	};

	return (
		<Box component="div">
			<SectionHeaderLabel text="By feature points" />
			<RadioGroup
				aria-labelledby="by-feature-points-group"
				name="controlled-radio-buttons-group"
				value={value}
				onChange={handleChangeRadio}
			>
				<FormControlLabel
					value={ByFeatureValues.SOME}
					control={<Radio />}
					label="has some feature point"
				/>
				<FormControlLabel
					value={ByFeatureValues.ALL}
					control={<Radio />}
					label="has selected feature points"
				/>
				<FormControlLabel
					value={ByFeatureValues.NONE}
					control={<Radio />}
					label="has not feature points"
				/>
			</RadioGroup>
		</Box>
	);
};

export default FilterByFeaturePoints;

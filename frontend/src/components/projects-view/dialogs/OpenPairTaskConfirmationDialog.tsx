import React, { FC, useContext } from 'react';
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogTitle,
	Grid,
	IconButton
} from '@mui/material';
import { GridCloseIcon } from '@mui/x-data-grid';
import { useNavigate } from 'react-router-dom';

import { TaskContext } from '../../../providers/TaskContextProvider';
import { TaskCreateDto } from '../../../types/TaskTypes';
import { FileUpload } from '../../../types/FileTypes';
import { ProjectsContext } from '../../../providers/ProjectContextProvider';
import { TaskService } from '../../../services/TaskService';
import { ROUTES } from '../../../utils/RoutesUrls';

type OpenPairTaskConfirmationDialogProps = {
	open: boolean;
	selectedFiles: FileUpload[];
	onClose: (value?: boolean) => void;
};

const OpenPairTaskConfirmationDialog: FC<
	OpenPairTaskConfirmationDialogProps
> = ({ open, selectedFiles, onClose }) => {
	const { selectedProjectId } = useContext(ProjectsContext);
	const { createTask } = useContext(TaskContext);
	const navigate = useNavigate();

	const handleFaceSelected = async (
		primaryFaceIndex: number,
		secondaryFaceIndex: number
	) => {
		const existingTask = await TaskService.getTaskByName(
			`${selectedFiles[primaryFaceIndex].name}:${selectedFiles[secondaryFaceIndex].name}`
		);
		console.log(existingTask);
		if (existingTask) {
			navigate(`${ROUTES.tasks}/${existingTask.id}`);
		} else {
			onClose();
			const createTaskDto: TaskCreateDto = {
				projectId: Number(selectedProjectId),
				fileUploadIds: [
					selectedFiles[primaryFaceIndex].id,
					selectedFiles[secondaryFaceIndex].id
				]
			};

			createTask(createTaskDto);
		}
	};

	return (
		<Dialog open={open} maxWidth="md">
			<DialogTitle>
				<Grid container>
					<Grid item>Select primary face</Grid>
					<Grid ml="auto" item>
						<IconButton
							size="medium"
							aria-label="close"
							onClick={() => onClose()}
						>
							<GridCloseIcon sx={{ fontSize: '22px' }} />
						</IconButton>
					</Grid>
				</Grid>
			</DialogTitle>
			<DialogActions sx={{ py: 2, px: 7 }}>
				<Box component="div" className="flex-row" sx={{ gap: 3 }}>
					<Button variant="contained" onClick={() => handleFaceSelected(0, 1)}>
						{selectedFiles[0].name}
					</Button>
					<Button variant="contained" onClick={() => handleFaceSelected(1, 0)}>
						{selectedFiles[1].name}
					</Button>
				</Box>
			</DialogActions>
		</Dialog>
	);
};

export default OpenPairTaskConfirmationDialog;

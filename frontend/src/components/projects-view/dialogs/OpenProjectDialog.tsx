import { FC, useContext, useEffect, useState } from 'react';
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle
} from '@mui/material';
import { DataGrid, GridColDef, GridSelectionModel } from '@mui/x-data-grid';

import { Project } from '../../../types/ProjectTypes';
import { ProjectService } from '../../../services/ProjectService';
import { ProjectsContext } from '../../../providers/ProjectContextProvider';
import { FilesContext } from '../../../providers/FileContextProvider';

type OpenProjectDialogProps = {
	open: boolean;
	onClose: (value?: string) => void;
};

const columns: GridColDef[] = [
	{
		field: 'name',
		headerName: 'Project name',
		editable: false,
		flex: 1
	}
];

const OpenProjectDialog: FC<OpenProjectDialogProps> = ({ open, onClose }) => {
	const [notOpenedProjects, setNotOpenedProjects] = useState<Project[]>([]);
	const [loading, setLoading] = useState(false);
	const { setFileSelection } = useContext(FilesContext);
	const [selectionModel, setSelectionModel] = useState<GridSelectionModel>([]);
	const { setProjects, projects, setSelectedProjectId } =
		useContext(ProjectsContext);

	useEffect(() => {
		setLoading(true);

		ProjectService.getUserProjects(false)
			.then(projects => setNotOpenedProjects(projects))
			.finally(() => setLoading(false));
	}, []);

	const handleOk = () => {
		if (selectionModel.length) {
			const id = Number(selectionModel[0]);

			ProjectService.setProjectOpenState(id, true).then(_ => {
				const selectedProject = notOpenedProjects.find(
					project => project.id === id
				) as Project;

				setProjects([...projects, selectedProject]);
				setSelectedProjectId(id);
				setSelectionModel([]);
				setFileSelection([]);
				onClose();
			});
		}
	};

	return (
		<Dialog open={open} maxWidth="md" fullWidth>
			<DialogTitle>Open project</DialogTitle>
			<DialogContent dividers>
				<Box component="div" sx={{ height: 600 }}>
					<DataGrid
						columns={columns}
						rows={notOpenedProjects}
						selectionModel={selectionModel}
						onSelectionModelChange={setSelectionModel}
						loading={loading}
					/>
				</Box>
			</DialogContent>
			<DialogActions sx={{ p: 3, py: 1 }}>
				<Button onClick={() => onClose()}>Cancel</Button>
				<Button
					variant="contained"
					onClick={handleOk}
					disabled={!selectionModel.length}
				>
					Open
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default OpenProjectDialog;

import { FC, useContext } from 'react';
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle
} from '@mui/material';
import { useNavigate } from 'react-router-dom';

import { TaskContext } from '../../../providers/TaskContextProvider';
import { ProjectsContext } from '../../../providers/ProjectContextProvider';
import { TaskService } from '../../../services/TaskService';
import { ROUTES } from '../../../utils/RoutesUrls';

type OpenSingleTaskConfirmationDialogProps = {
	open: boolean;
	onClose: (value?: boolean) => void;
	fileId: number;
	faceName: string;
};

const OpenSingleTaskConfirmationDialog: FC<
	OpenSingleTaskConfirmationDialogProps
> = ({ open, onClose, fileId, faceName }) => {
	const { createTask } = useContext(TaskContext);
	const { selectedProjectId } = useContext(ProjectsContext);
	const navigate = useNavigate();

	const handleOk = async () => {
		const existingTask = await TaskService.getTaskByName(faceName);

		if (existingTask) {
			navigate(`${ROUTES.tasks}/${existingTask.id}`);
		} else {
			onClose();
			createTask({
				projectId: Number(selectedProjectId),
				fileUploadIds: [fileId]
			});
		}
	};

	return (
		<Dialog open={open} maxWidth="sm" fullWidth>
			<DialogTitle>Open single task</DialogTitle>
			<DialogContent dividers>
				<Box component="div">
					<p>Are you sure you want to open task for face {faceName}?</p>
				</Box>
			</DialogContent>
			<DialogActions sx={{ p: 3, py: 1 }}>
				<Button onClick={() => onClose(false)}>Cancel</Button>
				<Button variant="contained" onClick={handleOk}>
					Open
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default OpenSingleTaskConfirmationDialog;

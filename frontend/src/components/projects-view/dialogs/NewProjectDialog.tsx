import { FC, useContext } from 'react';
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	TextField
} from '@mui/material';

import useField from '../../../hooks/useField';
import { ProjectsContext } from '../../../providers/ProjectContextProvider';
import { FilesContext } from '../../../providers/FileContextProvider';

type NewProjectDialogProps = {
	open: boolean;
	onClose: (value?: string) => void;
};

const NewProjectDialog: FC<NewProjectDialogProps> = ({ open, onClose }) => {
	const [projectName, projectNameProps] = useField('projectName', true);
	const { createNewProject } = useContext(ProjectsContext);
	const { setFileSelection } = useContext(FilesContext);

	const handleCancel = () => {
		onClose();
		projectNameProps.onChange({ target: { value: '' } } as never); // reset project name
	};

	const handleOk = () => {
		createNewProject(projectName);
		setFileSelection([]);
		onClose(projectName);
		projectNameProps.onChange({ target: { value: '' } } as never); // reset project name
	};

	return (
		<Dialog open={open} maxWidth="sm" fullWidth>
			<DialogTitle>Create project</DialogTitle>
			<DialogContent dividers>
				<Box component="div">
					<TextField
						label="Project name"
						type="text"
						{...projectNameProps}
						fullWidth
					/>
				</Box>
			</DialogContent>
			<DialogActions sx={{ p: 3, py: 1 }}>
				<Button onClick={handleCancel}>Cancel</Button>
				<Button variant="contained" onClick={handleOk} disabled={!projectName}>
					Add
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default NewProjectDialog;

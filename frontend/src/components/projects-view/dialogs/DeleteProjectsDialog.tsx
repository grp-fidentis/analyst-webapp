import { FC, useContext, useEffect, useState } from 'react';
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle
} from '@mui/material';
import { DataGrid, GridColDef, GridSelectionModel } from '@mui/x-data-grid';
import { useConfirm } from 'material-ui-confirm';

import { Project } from '../../../types/ProjectTypes';
import { ProjectService } from '../../../services/ProjectService';
import { ProjectsContext } from '../../../providers/ProjectContextProvider';

type DeleteProjectsDialogProps = {
	open: boolean;
	onClose: (value?: string) => void;
};

const columns: GridColDef[] = [
	{
		field: 'name',
		headerName: 'Project name',
		editable: false,
		flex: 1
	}
];

const DeleteProjectsDialog: FC<DeleteProjectsDialogProps> = ({
	open,
	onClose
}) => {
	const [allProjects, setAllProjects] = useState<Project[]>([]);
	const [loading, setLoading] = useState(false);
	const [selectionModel, setSelectionModel] = useState<GridSelectionModel>([]);
	const { deleteProject } = useContext(ProjectsContext);
	const confirm = useConfirm();

	useEffect(() => {
		setLoading(true);

		ProjectService.getUserProjects()
			.then(projects => setAllProjects(projects))
			.finally(() => setLoading(false));
	}, []);

	const handleSelectChange = (selectedIds: GridSelectionModel) => {
		setSelectionModel(selectedIds);
	};

	const handleOk = () => {
		confirm({
			description: 'This action is permanent!',
			confirmationButtonProps: { variant: 'contained' }
		})
			.then(() => {
				deleteProject(Number(selectionModel[0]));
				onClose();
			})
			.catch(() => null);
	};

	return (
		<Dialog open={open} maxWidth="md" fullWidth>
			<DialogTitle>Delete project</DialogTitle>
			<DialogContent dividers>
				<Box component="div" sx={{ height: 600 }}>
					<DataGrid
						columns={columns}
						rows={allProjects}
						selectionModel={selectionModel}
						onSelectionModelChange={handleSelectChange}
						loading={loading}
					/>
				</Box>
			</DialogContent>
			<DialogActions sx={{ p: 3, py: 1 }}>
				<Button onClick={() => onClose()}>Cancel</Button>
				<Button
					variant="contained"
					onClick={handleOk}
					disabled={!selectionModel.length}
				>
					Delete
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default DeleteProjectsDialog;

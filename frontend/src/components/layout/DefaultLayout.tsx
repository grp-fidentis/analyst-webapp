import { FC } from 'react';
import { Box } from '@mui/material';

import { ProjectContextProvider } from '../../providers/ProjectContextProvider';
import { TaskContextProvider } from '../../providers/TaskContextProvider';
import { FileContextProvider } from '../../providers/FileContextProvider';

import Header from './Header';

type Props = {
	children: JSX.Element;
};

const DefaultLayout: FC<Props> = ({ children }): JSX.Element => (
	<Box component="div" sx={{ display: 'flex', flexDirection: 'column' }}>
		<ProjectContextProvider>
			<FileContextProvider>
				<TaskContextProvider>
					<>
						<Header />
						{children}
					</>
				</TaskContextProvider>
			</FileContextProvider>
		</ProjectContextProvider>
	</Box>
);

export default DefaultLayout;

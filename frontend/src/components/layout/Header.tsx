import { useContext, useMemo } from 'react';
import { Button, Stack, Typography } from '@mui/material';
import { Logout } from '@mui/icons-material';
import { NavLink, useLocation, useNavigate } from 'react-router-dom';

import { ReactComponent as FidentisLogo } from '../../assets/images/logo-fidentis.svg';
import { AuthContext } from '../../providers/AuthContextProvider';
import { ROUTES } from '../../utils/RoutesUrls';
import { TaskContext } from '../../providers/TaskContextProvider';
import { ProjectsContext } from '../../providers/ProjectContextProvider';
import ProjectTasks from '../projects-view/ProjectTasks';
import TooltipIconButton from '../common/TooltipIconButton';
import TaskName from '../task-view/TaskName';

const Header = () => {
	const { task } = useContext(TaskContext);
	const { projects } = useContext(ProjectsContext);
	const { currentUser, logout, hasAdminRole, isUserEnabled } =
		useContext(AuthContext);
	const navigate = useNavigate();
	const location = useLocation();

	const showTasksMenu = useMemo(
		() =>
			projects.length > 0 &&
			(location.pathname.includes('projects') ||
				location.pathname.includes('tasks')) &&
			!location.pathname.includes('user-projects'),
		[projects, location]
	);

	const handleLogout = () => {
		logout();
		navigate(ROUTES.chooseLogin);
	};

	const rerouteToUserDetail = () => {
		if (isUserEnabled) {
			navigate(ROUTES.profile, {
				state: { user: currentUser, isProfile: true }
			});
		}
	};

	return (
		<Stack
			direction="row"
			justifyContent="space-between"
			alignItems="center"
			px={7}
			boxShadow={4}
			sx={{
				height: '100px'
			}}
		>
			<NavLink to={ROUTES.projects} style={{ textDecoration: 'none' }}>
				<Stack direction="row" alignItems="center">
					<FidentisLogo
						style={{
							width: '60px',
							height: 'max-content',
							marginRight: '16px'
						}}
					/>
					<Typography variant="h4" color="primary.dark">
						FIDENTIS ANALYST 2
					</Typography>
				</Stack>
			</NavLink>

			<Stack alignItems="center" direction="row" gap={2}>
				{task && <TaskName />}

				{showTasksMenu ? <ProjectTasks /> : null}

				{isUserEnabled && (
					<Button component={NavLink} to={ROUTES.projects} disableRipple>
						Projects
					</Button>
				)}

				{hasAdminRole && (
					<Button component={NavLink} to={ROUTES.users} disableRipple>
						Users
					</Button>
				)}

				<Typography component="span">
					<Button onClick={rerouteToUserDetail}>{currentUser?.username}</Button>
					<TooltipIconButton
						title="Logout"
						onClick={handleLogout}
						disableRipple
					>
						<Logout />
					</TooltipIconButton>
				</Typography>
			</Stack>
		</Stack>
	);
};

export default Header;

package cz.fidentis.web.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fidentis.web.user.User;
import cz.fidentis.web.base.BaseEntity;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.task.Task;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 30.01.2023
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "project")
public class Project extends BaseEntity {

    public Project(String name, User user) {
        this.name = name;
        this.isOpened = true;
        this.fileUploads = new ArrayList<>();
        this.user = user;
    }

    private String name;

    private Boolean isOpened;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<FileUpload> fileUploads = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Task> tasks = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "app_user_id", referencedColumnName = "id")
    private User user;

    public void addTask(Task task) {
        tasks.add(task);
        task.setProject(this);
    }
}

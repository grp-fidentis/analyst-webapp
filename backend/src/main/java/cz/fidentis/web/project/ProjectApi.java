package cz.fidentis.web.project;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 30.01.2023
 */
@RestController
@Tag(name = "Project API", description = "API for managing and retrieving projects")
@RequestMapping("/api/projects")
@RequiredArgsConstructor
@Validated
public class ProjectApi {

    private final ProjectService projectsService;

    @GetMapping
    @Operation(summary = "Gets current user's projects")
    public ResponseEntity<List<ProjectSimpleDto>> getUserProjects(@RequestParam(required = false) Boolean isOpen) {
        return ResponseEntity.ok(projectsService.getProjects(isOpen));
    }

    @PostMapping
    @Operation(summary = "Creates project")
    public ResponseEntity<Project> createProject(@RequestParam @NotBlank String name) {
        return ResponseEntity.ok(projectsService.createProject(name));
    }

    @PutMapping("/{id}/set-open-status")
    @Operation(summary = "Marks selected project by id as open/closed")
    @PreAuthorize("isOwner(#id, 'PROJECT')")
    public ResponseEntity<Void> setOpenToProjects(@PathVariable Long id, @RequestParam @NotNull Boolean open) {
        projectsService.setStatusToProject(id, open);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Deletes project by id")
    @PreAuthorize("isOwner(#id, 'PROJECT') or hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Void> deleteProject(@PathVariable Long id) {
        projectsService.deleteProject(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}

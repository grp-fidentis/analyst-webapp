package cz.fidentis.web.project;

/**
 * @author Petr Hendrych
 * @since 06.03.2023
 */
public record ProjectSimpleDto(Long id, String name, Boolean isOpened) {

}

package cz.fidentis.web.project;

import cz.fidentis.web.user.User;
import cz.fidentis.web.user.UserService;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Petr Hendrych
 * @since 03.02.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepo projectsRepo;
    private final UserService userService;

    @Override
    public Project getProjectById(Long id) {
        return projectsRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Project with id: %s does not exist".formatted(id)));
    }

    @Override
    public List<ProjectSimpleDto> getProjects(Boolean isOpen) {
        User user = userService.getCurrentAuthenticatedUser();

        return isOpen == null
                ? projectsRepo.findAllByUserId(user.getId())
                : projectsRepo.findAllByUserIdAndIsOpened(user.getId(), isOpen);
    }

    @Override
    public Project createProject(String name) {
        User user = userService.getCurrentAuthenticatedUser();
        log.info("Creating new project: {} for user: {}", name, user.getUsername());

        Project project = new Project(name, user);
        return projectsRepo.save(project);
    }

    @Override
    public void setStatusToProject(Long id, Boolean status) {
        Project project = projectsRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Project with id: %s does not exist".formatted(id)));

        project.setIsOpened(status);

        projectsRepo.save(project);
    }

    @Override
    public void deleteProject(Long id) {
        Project project = projectsRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Project with id: %s does not exist".formatted(id)));

        log.info("Deleting project with id: {}", id);
        projectsRepo.delete(project);
    }
}

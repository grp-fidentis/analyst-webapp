package cz.fidentis.web.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Petr Hendrych
 * @since 03.02.2023
 */
@Repository
public interface ProjectRepo extends JpaRepository<Project, Long> {

    /**
     * Method to fetch all projects by user (simplified to get only certain columns)
     *
     * @param userId ID of user
     * @return List of simplified {@code Project} entity that contains only: id, name, isOpened flag
     */
    List<ProjectSimpleDto> findAllByUserId(Long userId);

    /**
     * Method to fetch projects by user (not simplified)
     * @param userId ID of user
     * @return List of full {@code Project} entities
     */
    List<Project> findByUserId(Long userId);

    /**
     * Method to fetch project by user. It also filters projects by {@code isOpened} flag that determines
     * if user want all projects or closed or opened
     *
     * @param userId ID of user
     * @param isOpen Boolean flag if we want all projects or only that are "opened"
     * @return List of simplified {@code Project} entity that contains only: id, name, isOpened flag
     */
    List<ProjectSimpleDto> findAllByUserIdAndIsOpened(Long userId, Boolean isOpen);
}

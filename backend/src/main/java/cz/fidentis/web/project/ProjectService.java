package cz.fidentis.web.project;

import java.util.List;

/**
 * @author Petr Hendrych
 * @since 03.02.2023
 */
public interface ProjectService {

    /**
     * Service to get all projects for currently logged user. User is determined from Authorization token
     * value in header of the request
     *
     * @param isOpen Flag if we are getting all project, opened or closed projects
     * @return List of filtered user projects mapped into simple Dto
     */
    List<ProjectSimpleDto> getProjects(Boolean isOpen);

    /**
     * Service to create new project. User is linked via request header
     *
     * @param name String value for name of project
     * @return Newly created project with its id
     */
    Project createProject(String name);

    /**
     * Service to change status if project is opened or no. Working only on created projects
     *
     * @param id ID of project
     * @param status True or false value depending if closing or opening project
     */
    void setStatusToProject(Long id, Boolean status);

    /**
     * Service to delete project. Deleting project will delete also all linked tasks.
     *
     * @param id ID of project
     */
    void deleteProject(Long id);

    /**
     * Service to get project by its ID
     *
     * @param id ID of project
     * @return Project entity
     */
    Project getProjectById(Long id);
}

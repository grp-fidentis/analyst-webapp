package cz.fidentis.web.project;

import cz.fidentis.web.task.dto.TaskSimpleDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Adam Majzlik
 * @since 26.03.2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProjectDto {

    private Long id;

    private String name;

    private Boolean isOpened;

    private List<TaskSimpleDto> tasks;

}

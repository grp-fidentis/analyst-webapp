package cz.fidentis.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * @author Petr Hendrych
 * @since 20.11.2022
 */
@SpringBootApplication(scanBasePackages = "cz.fidentis")
@ConfigurationPropertiesScan
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}

package cz.fidentis.web.role;

/**
 * @author Adam Majzlik
 */
public enum Roles {

    ROLE_USER,
    ROLE_ADMIN,
    ROLE_SUPER_ADMIN;

}

package cz.fidentis.web.role;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Petr Hendrych
 * @since 28.11.2022
 */
public interface RoleRepo extends JpaRepository<Role, Long> {

    /**
     * Method do fetch role by name from database
     *
     * @param name Searched string value
     * @return If role is found in db then inside optional is found role
     * else optional contain null value
     */
    Optional<Role> findByName(String name);
}

package cz.fidentis.web.role;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import jakarta.validation.Valid;
import java.net.URI;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 29.11.2022
 */
@RestController
@Tag(name = "Role API", description = "API for managing and retrieving user roles")
@RequestMapping("/api/roles")
@RequiredArgsConstructor
public class RoleApi {

    private final RoleService roleService;

    @GetMapping
    @Operation(summary = "Gets roles")
    public ResponseEntity<List<Role>> getRoles() {
        return ResponseEntity.ok(roleService.getRoles());
    }

    @PostMapping
    @Operation(summary = "Creates role")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Role> createRole(@Valid @RequestBody Role role) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/roles/new").toUriString());
        return ResponseEntity.created(uri).body(roleService.saveRole(role));
    }
}

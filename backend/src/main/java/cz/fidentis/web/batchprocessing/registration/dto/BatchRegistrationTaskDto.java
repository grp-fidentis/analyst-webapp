package cz.fidentis.web.batchprocessing.registration.dto;

import cz.fidentis.web.fileupload.dto.FileUploadDto;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Patrik Tomov
 * @since  06.03.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BatchRegistrationTaskDto {

    @NotNull
    private Long taskId;

    @NotNull
    private Long projectId;

    @NotNull
    @Valid
    private AveragingOptionsTaskDto averagingTaskDto;

    @NotNull
    @Valid
    private RegistrationOptionsTaskDto registrationTaskDto;

    @NotNull
    private FileUploadDto selectedFile;

    @NotNull
    private List<Long> fileIds;

    @NotNull
    private String calculationWebSocketSessionId;

    private double calculationProgress;
}

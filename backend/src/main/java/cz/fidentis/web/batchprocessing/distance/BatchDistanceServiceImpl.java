package cz.fidentis.web.batchprocessing.distance;

import cz.fidentis.analyst.engines.face.batch.distance.BatchFaceDistanceServices;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceResponseTaskDto;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceTaskDto;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistancesDto;
import cz.fidentis.web.batchprocessing.distance.dto.FaceDistancesDto;
import cz.fidentis.web.batchprocessing.distance.service.BatchIndirectService;
import cz.fidentis.web.batchprocessing.distance.service.BatchPairwiseService;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.FileUploadRepo;
import cz.fidentis.web.fileupload.FileUploadService;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Patrik Tomov
 * @since  09.09.2024
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class BatchDistanceServiceImpl implements BatchDistanceService {

    private final FileUploadService fileUploadService;
    private final BatchIndirectService batchIndirectService;
    private final BatchPairwiseService batchPairwiseService;
    private final FileUploadRepo fileUploadRepo;

    @Override
    public BatchDistancesDto calculateDistance(BatchDistanceTaskDto dto) {
        BatchDistanceResponseTaskDto responseDto = switch (dto.getSimilarityMethod()) {
            case INDIRECT_DISTANCE ->
                    batchIndirectService.calculateDistance(dto, BatchFaceDistanceServices.DistanceStrategy.NEAREST_NEIGHBORS_RELATIVE_DISTANCE);
            case INDIRECT_VECTORS ->
                    batchIndirectService.calculateDistance(dto, BatchFaceDistanceServices.DistanceStrategy.NEAREST_NEIGHBORS_DIRECT_DISTANCE);
            case INDIRECT_COMBINED ->
                    batchIndirectService.calculateDistance(dto, BatchFaceDistanceServices.DistanceStrategy.NEAREST_NEIGHBORS_COMBINED_DISTANCE);
            case INDIRECT_RAY_CASTING -> batchIndirectService.calculateDistance(dto, BatchFaceDistanceServices.DistanceStrategy.PROJECTION);
            case PAIRWISE_TWO_WAY -> batchPairwiseService.calculateDistance(dto, false);
            case PAIRWISE_TWO_WAY_CROPPED -> batchPairwiseService.calculateDistance(dto, true);
        };

        if (responseDto == null) {
            return null;
        }
        BatchDistancesDto batchDistancesDto = transformResponseDto(dto.getFileIds(), responseDto);
        batchDistancesDto.setTaskId(dto.getTaskId());
        return batchDistancesDto;
    }

    private BatchDistancesDto transformResponseDto(List<Long> fileIds, BatchDistanceResponseTaskDto responseDto) {
        BatchDistancesDto batchDistancesDto = new BatchDistancesDto();

        List<FaceDistancesDto> faceDistancesList = new ArrayList<>();

        for (int i = 0; i < fileIds.size(); i++) {
            Long fileId = fileIds.get(i);
            FileUpload fileUpload = fileUploadRepo.findById(fileId).orElseThrow(() -> new RuntimeException("File not found"));


            FaceDistancesDto faceDistancesDto = new FaceDistancesDto();
            faceDistancesDto.setFaceName(fileUpload.getName());
            faceDistancesDto.setFaceDistances(responseDto.getDistances()[i]);
            faceDistancesDto.setFaceDeviations(responseDto.getDeviations()[i]);

            faceDistancesList.add(faceDistancesDto);
        }

        batchDistancesDto.setFaceDistances(faceDistancesList);

        return batchDistancesDto;
    }

    @Override
    public byte[] exportDistanceResults(BatchDistancesDto dto) {
        if (dto.getFaceDistances() == null || dto.getFaceDistances().isEmpty()) {
            return new byte[0];
        }

        List<FileUploadDetail> files = fileUploadService.getFilesOfTask(dto.getTaskId());

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try (OutputStreamWriter writer = new OutputStreamWriter(byteArrayOutputStream, StandardCharsets.UTF_8)) {

            writer.write("PRI FACE;SEC FACE;AVG dist PRI-SEC;Std. deviation PRI-SEC;AVG dist SEC-PRI;Std. deviation SEC-PRI\n");

            int size = dto.getFaceDistances().size();
            double[][] distances = new double[size][];
            double[][] deviations = new double[size][];

            for (int i = 0; i < size; i++) {
                FaceDistancesDto faceDistancesDto = dto.getFaceDistances().get(i);
                distances[i] = faceDistancesDto.getFaceDistances();
                deviations[i] = faceDistancesDto.getFaceDeviations();
            }

            for (int i = 0; i < distances.length; i++) {
                for (int j = i + 1; j < distances.length; j++) {
                    String priFaceName = files.get(i).getName();
                    String secFaceName = files.get(j).getName();

                    writer.write(priFaceName + ";");
                    writer.write(secFaceName + ";");
                    writer.write(String.format("%.8f", distances[i][j]) + ";");
                    writer.write(String.format("%.8f", deviations[i][j]) + ";");
                    writer.write(String.format("%.8f", distances[j][i]) + ";");
                    writer.write(String.format("%.8f", deviations[j][i]) + ";");
                    writer.write("\n");
                }
            }

            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException("Failed to create or read the file", e);
        }

        return byteArrayOutputStream.toByteArray();
    }
}

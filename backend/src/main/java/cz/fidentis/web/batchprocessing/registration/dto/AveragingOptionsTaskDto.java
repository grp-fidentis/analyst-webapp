package cz.fidentis.web.batchprocessing.registration.dto;

import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices;
import cz.fidentis.web.batchprocessing.registration.enums.BatchAveragingMethod;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  06.03.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AveragingOptionsTaskDto {

    @NotNull
    private BatchAveragingMethod averagingMethod;

    @NotNull
    @Min(1)
    @Max(5)
    private Integer maxRegistrationIterations;

    @NotNull
    @Min(0)
    @Max(5)
    private Integer stabilizationThreshold;

    public BatchFaceRegistrationServices.AverageFaceStrategy getAverageFaceStrategy() {
        return switch (averagingMethod) {
            case NEAREST_NEIGHBOURS -> BatchFaceRegistrationServices.AverageFaceStrategy.NEAREST_NEIGHBOURS;
            case RAY_CASTING -> BatchFaceRegistrationServices.AverageFaceStrategy.PROJECTION_CPU;
            case SKIP -> BatchFaceRegistrationServices.AverageFaceStrategy.NONE;
        };
    }
}

package cz.fidentis.web.batchprocessing.registration;

import cz.fidentis.web.batchprocessing.registration.dto.AddAverageFaceToProjectDto;
import cz.fidentis.web.batchprocessing.registration.dto.BatchRegistrationTaskDto;
import cz.fidentis.web.batchprocessing.registration.dto.RegistrationResponseDto;

/**
 * @author Patrik Tomov
 * @since  06.03.2024
 */
public interface BatchRegistrationService {

    /**
     * Method that executes registration for multiple faces of task
     *
     * @param dto with registration parameters
     * @return {@code RegistrationResponseDto} object with registration results
     */
    RegistrationResponseDto execute(BatchRegistrationTaskDto dto);

    /**
     * Export average face as OBJ file
     *
     * @param humanFaceEntityId id of human face entity which is connected to average face
     * @return OBJ file of average face
     */
    byte[] exportAverageFace(Long humanFaceEntityId);

    /**
     * Add average face to project
     *
     * @param dto with average face id, new file name, task id and project id
     */
    void addAverageFaceToProject(AddAverageFaceToProjectDto dto);
}

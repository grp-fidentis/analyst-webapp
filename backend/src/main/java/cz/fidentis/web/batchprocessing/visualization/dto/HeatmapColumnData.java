package cz.fidentis.web.batchprocessing.visualization.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  14.09.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HeatmapColumnData {
    private String faceName;
    private int faceIndex;
    private double distance;
}

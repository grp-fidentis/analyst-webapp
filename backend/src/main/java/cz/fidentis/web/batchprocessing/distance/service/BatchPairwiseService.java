package cz.fidentis.web.batchprocessing.distance.service;

import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceResponseTaskDto;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceTaskDto;

/**
 * @author Patrik Tomov
 * @since  19.09.2024
 */
public interface BatchPairwiseService {

    /**
     * Calculate distance between faces.
     * @param dto task dto
     * @param crop crop
     * @return response dto with distances and deviations
     */
    BatchDistanceResponseTaskDto calculateDistance(BatchDistanceTaskDto dto, boolean crop);
}

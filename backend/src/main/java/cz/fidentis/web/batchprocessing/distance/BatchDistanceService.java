package cz.fidentis.web.batchprocessing.distance;

import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceTaskDto;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistancesDto;

/**
 * @author Patrik Tomov
 * @since  09.09.2024
 */
public interface BatchDistanceService {

    /**
     * Method that calculates distance between multiple faces
     *
     * @param dto with distance calculation parameters
     * @return dto with distance results
     */
    BatchDistancesDto calculateDistance(BatchDistanceTaskDto dto);

    /**
     * Method that exports distance as CSV file
     *
     * @param dto with distance calculation parameters
     * @return CSV file with distance results
     */
    byte[] exportDistanceResults(BatchDistancesDto dto);
}

package cz.fidentis.web.batchprocessing.distance.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  27.10.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FaceDistancesDto {
    private String faceName;
    private double[] faceDistances;
    private double[] faceDeviations;
}

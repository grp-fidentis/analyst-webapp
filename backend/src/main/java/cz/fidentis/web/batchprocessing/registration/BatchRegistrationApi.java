package cz.fidentis.web.batchprocessing.registration;

import cz.fidentis.web.batchprocessing.registration.dto.AddAverageFaceToProjectDto;
import cz.fidentis.web.batchprocessing.registration.dto.BatchRegistrationTaskDto;
import cz.fidentis.web.batchprocessing.registration.dto.RegistrationResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author Patrik Tomov
 * @since  06.03.2024
 */
@RestController
@Tag(name = "Batch processing - Registration API", description = "API for executing registration")
@RequestMapping("/api/batch-processing/registration")
@RequiredArgsConstructor
@Slf4j
public class BatchRegistrationApi {

    private final BatchRegistrationService batchRegistrationService;

    @PostMapping("/execute")
    @Operation(summary = "Executes registration of mupltiple faces")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<RegistrationResponseDto> execute(@Valid @RequestBody BatchRegistrationTaskDto dto) {
        return ResponseEntity.ok(batchRegistrationService.execute(dto));
    }

    @PostMapping(value = "/export-average-face/{humanFaceEntityId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Operation(summary = "Export average face as obj file")
    public ResponseEntity<byte[]> exportAverageFace(@PathVariable Long humanFaceEntityId) {
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + "Average Face.obj")
                .body(batchRegistrationService.exportAverageFace(humanFaceEntityId));
    }

    @PostMapping("/add-average-face-to-project")
    @Operation(summary = "Add average face to project")
    public ResponseEntity<Void> addAverageFaceToProject(@Valid @RequestBody AddAverageFaceToProjectDto dto) {
        batchRegistrationService.addAverageFaceToProject(dto);
        return ResponseEntity.noContent().build();
    }
}

package cz.fidentis.web.batchprocessing.distance.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  09.09.2024
 */
@Getter
@Setter
@NoArgsConstructor
public class BatchDistanceResponseTaskDto {

    @NotNull
    private double[][] distances;
    @NotNull
    private double[][] deviations;

    public BatchDistanceResponseTaskDto(double[][] distances, double[][] deviations) {
        this.distances = distances;
        this.deviations = deviations;
    }
}

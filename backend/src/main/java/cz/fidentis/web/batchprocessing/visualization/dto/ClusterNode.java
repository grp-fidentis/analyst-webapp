package cz.fidentis.web.batchprocessing.visualization.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Patrik Tomov
 * @since  14.10.2024
 */
@Getter
@Setter
public class ClusterNode {

    private String name;
    private ClusterNode parent;
    private List<ClusterNode> children;
    private List<String> leafNames;
    private double distance;

    public ClusterNode(String name) {
        this.name = name;
        this.leafNames = new ArrayList<>();
        this.children = new ArrayList<>();
    }

    public void addLeafName(String leafName) {
        this.leafNames.add(leafName);
    }

    public void appendLeafNames(List<String> names) {
        this.leafNames.addAll(names);
    }

    public void addChild(ClusterNode child) {
        this.children.add(child);
    }
}

package cz.fidentis.web.batchprocessing.visualization;

import cz.fidentis.web.batchprocessing.distance.dto.BatchDistancesDto;
import cz.fidentis.web.batchprocessing.visualization.dto.HeatmapRowData;
import cz.fidentis.web.batchprocessing.visualization.dto.SerializableClusterNode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Patrik Tomov
 * @since  14.09.2024
 */
@RestController
@Tag(name = "Batch processing - Visualization API", description = "API for preparation data for visualization")
@RequestMapping("/api/batch-processing/visualization")
@RequiredArgsConstructor
@Slf4j
public class BatchVisualizationApi {

    private final BatchVisualizationService batchVisualizationService;

    @PostMapping("/prepare-heatmap")
    @Operation(summary = "Prepare data for heatmap visualization")
    public ResponseEntity<List<HeatmapRowData>> prepareDataForHeatmapVisualization(@Valid @RequestBody BatchDistancesDto dto) {
        return ResponseEntity.ok(batchVisualizationService.prepareDataForHeatMapVisualization(dto));
    }

    @PostMapping("/prepare-dendrogram")
    @Operation(summary = "Prepare data for dendrogram visualization")
    public ResponseEntity<SerializableClusterNode> prepareDataForDendrogramVisualization(@Valid @RequestBody BatchDistancesDto dto) {
        return ResponseEntity.ok(batchVisualizationService.prepareDataForDendrogramVisualization(dto));
    }
}

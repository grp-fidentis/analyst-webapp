package cz.fidentis.web.batchprocessing.registration.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  04.09.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddAverageFaceToProjectDto {

    @NotNull
    private Long humanFaceEntityId;

    @NotNull
    private Long taskId;

    @NotNull
    private Long projectId;

    @NotNull
    private String fileName;
}

package cz.fidentis.web.batchprocessing.distance.service;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.engines.face.batch.distance.BatchFaceDistanceServices;
import cz.fidentis.analyst.engines.face.batch.distance.BatchIndirectDistance;
import cz.fidentis.web.async.WebSocketProgressHandler;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceResponseTaskDto;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceTaskDto;
import cz.fidentis.web.batchprocessing.service.BatchBaseService;
import cz.fidentis.web.fileupload.FileUploadRepo;
import cz.fidentis.web.fileupload.FileUploadService;
import cz.fidentis.web.humanface.service.HumanFaceService;
import cz.fidentis.web.project.ProjectService;
import cz.fidentis.web.task.TaskRepo;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

/**
 * @author Patrik Tomov
 * @since  19.09.2024
 */
@Service
@Slf4j
public class BatchIndirectServiceImpl extends BatchBaseService implements BatchIndirectService {

    @PersistenceContext
    private EntityManager em;

    public BatchIndirectServiceImpl(HumanFaceService humanFaceService, FileUploadService fileUploadService, FileUploadRepo fileUploadRepo, TaskRepo taskRepo, ProjectService projectService, WebSocketProgressHandler webSocketProgressHandler) {
        super(humanFaceService, fileUploadService, fileUploadRepo, taskRepo, projectService, webSocketProgressHandler);
    }

    @Override
    @Transactional
    public BatchDistanceResponseTaskDto calculateDistance(BatchDistanceTaskDto dto, BatchFaceDistanceServices.DistanceStrategy distanceStrategy) {
        try {
            return calculateDistanceAsync(dto, distanceStrategy).get();
        } catch (Exception e) {
            log.error("Error while calculating distance", e);
            return null;
        }
    }

    @Async
    protected CompletableFuture<BatchDistanceResponseTaskDto> calculateDistanceAsync(BatchDistanceTaskDto dto, BatchFaceDistanceServices.DistanceStrategy distanceStrategy) throws IOException {
        int numFaces = dto.getFileIds().size();
        HumanFace selectedFace = getSelectedFace(dto.getSelectedFile().getId(), dto.getTaskId());

        BatchIndirectDistance batchDistance = BatchFaceDistanceServices.initIndirectMeasurement(distanceStrategy, selectedFace, numFaces, null);

        for (int i = 0; i < numFaces; i++) {
            if (webSocketProgressHandler.isInterrupted(dto.getCalculationWebSocketSessionId())) {
                webSocketProgressHandler.clearInterrupt(dto.getCalculationWebSocketSessionId());
                return CompletableFuture.completedFuture(null);
            }

            HumanFace face = getHumanFaceAtIndexFromArray(i, dto.getFileIds(), dto.getTaskId());
            em.clear();
            batchDistance.addToMeasurement(face);

            updateProgress(dto.getCalculationWebSocketSessionId(), i , numFaces);
        }
        batchDistance.measure();

        return CompletableFuture.completedFuture(new BatchDistanceResponseTaskDto(batchDistance.getDistSimilarities(), batchDistance.getDistDeviations()));
    }
}

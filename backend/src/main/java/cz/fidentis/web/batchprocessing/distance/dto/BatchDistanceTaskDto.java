package cz.fidentis.web.batchprocessing.distance.dto;

import cz.fidentis.web.batchprocessing.distance.enums.SimilarityMethod;
import cz.fidentis.web.fileupload.dto.FileUploadDto;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Patrik Tomov
 * @since  09.09.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BatchDistanceTaskDto {

    @NotNull
    private Long taskId;

    @NotNull
    private SimilarityMethod similarityMethod;

    @NotNull
    private FileUploadDto selectedFile;

    @NotNull
    private List<Long> fileIds;

    @NotNull
    private String calculationWebSocketSessionId;
}

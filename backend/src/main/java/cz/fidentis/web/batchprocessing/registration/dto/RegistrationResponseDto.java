package cz.fidentis.web.batchprocessing.registration.dto;

import cz.fidentis.web.humanface.dto.HumanFaceDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  09.12.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationResponseDto {
    private HumanFaceDto humanFace;
    private boolean completedSuccessfully;
}

package cz.fidentis.web.batchprocessing.visualization.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Patrik Tomov
 * @since  14.09.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HeatmapRowData {
    private String faceName;
    private int faceIndex;
    private List<HeatmapColumnData> columns;
}

package cz.fidentis.web.batchprocessing.registration.enums;

import lombok.Getter;

/**
 * @author Patrik Tomov
 * @since  06.03.2024
 */
@Getter
public enum BatchRegistrationMethod {
    MESH_BASED_ICP,
    FEATURE_POINTS,
    SKIP_REGISTRATION
}

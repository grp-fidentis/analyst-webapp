package cz.fidentis.web.batchprocessing.distance.enums;

import lombok.Getter;

/**
 * @author Patrik Tomov
 * @since  09.09.2024
 */
@Getter
public enum SimilarityMethod {
    INDIRECT_DISTANCE,
    INDIRECT_VECTORS,
    INDIRECT_COMBINED,
    INDIRECT_RAY_CASTING,
    PAIRWISE_TWO_WAY,
    PAIRWISE_TWO_WAY_CROPPED
}

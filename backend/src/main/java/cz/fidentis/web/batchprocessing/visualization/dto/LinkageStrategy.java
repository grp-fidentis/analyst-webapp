package cz.fidentis.web.batchprocessing.visualization.dto;

import lombok.Getter;

/**
 * @author Patrik Tomov
 * @since  01.11.2024
 */
@Getter
public enum LinkageStrategy {
    SINGLE,
    COMPLETE,
    AVERAGE
}

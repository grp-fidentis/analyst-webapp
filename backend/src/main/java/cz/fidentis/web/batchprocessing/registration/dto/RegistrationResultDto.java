package cz.fidentis.web.batchprocessing.registration.dto;

import cz.fidentis.analyst.data.face.HumanFace;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  09.12.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationResultDto {
    private HumanFace humanFace;
    private boolean completedSuccessfully;
}

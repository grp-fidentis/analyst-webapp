package cz.fidentis.web.batchprocessing.distance.dto;

import cz.fidentis.web.batchprocessing.visualization.dto.LinkageStrategy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Patrik Tomov
 * @since  27.10.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BatchDistancesDto {
    private List<FaceDistancesDto> faceDistances;
    private Long taskId;
    private LinkageStrategy linkageStrategy;
}

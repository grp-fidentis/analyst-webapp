package cz.fidentis.web.batchprocessing.visualization;

import cz.fidentis.analyst.engines.face.batch.clustering.ClusteringService;
import cz.fidentis.analyst.engines.face.batch.clustering.ClusteringServices;
import cz.fidentis.analyst.engines.face.batch.clustering.LinkageStrategy;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;
import cz.fidentis.analyst.engines.face.batch.clustering.impl.AverageLinkageStrategy;
import cz.fidentis.analyst.engines.face.batch.clustering.impl.CompleteLinkageStrategy;
import cz.fidentis.analyst.engines.face.batch.clustering.impl.SingleLinkageStrategy;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistancesDto;
import cz.fidentis.web.batchprocessing.distance.dto.FaceDistancesDto;
import cz.fidentis.web.batchprocessing.visualization.dto.HeatmapColumnData;
import cz.fidentis.web.batchprocessing.visualization.dto.HeatmapRowData;
import cz.fidentis.web.batchprocessing.visualization.dto.SerializableClusterNode;
import cz.fidentis.web.fileupload.FileUploadService;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Patrik Tomov
 * @since  14.09.2024
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class BatchVisualizationServiceImpl implements BatchVisualizationService {

    private final FileUploadService fileUploadService;

    @Override
    public List<HeatmapRowData> prepareDataForHeatMapVisualization(BatchDistancesDto dto) {
        List<FileUploadDetail> files = fileUploadService.getFilesOfTask(dto.getTaskId());

        int actualFileSizeInMatrix = files.size();

        if (files.size() != dto.getFaceDistances().size()) {
            // average face was computed after distance matrix was computed
            actualFileSizeInMatrix = actualFileSizeInMatrix - 1;
        }

        return IntStream.range(0, actualFileSizeInMatrix)
                .mapToObj(i -> {
                    FaceDistancesDto faceDistancesDto = dto.getFaceDistances().get(i);

                    List<HeatmapColumnData> binEntries = IntStream.range(0, faceDistancesDto.getFaceDistances().length)
                            .mapToObj(j -> new HeatmapColumnData(
                                    files.get(j).getName(),
                                    j,
                                    faceDistancesDto.getFaceDistances()[j]))
                            .collect(Collectors.toList());

                    return new HeatmapRowData(files.get(i).getName(), i, binEntries);
                })
                .collect(Collectors.toList());
    }

    @Override
    public SerializableClusterNode prepareDataForDendrogramVisualization(BatchDistancesDto dto) {

        double[][] distancesArray = convertTo2DArray(dto);
        List<String> faceNames = extractFaceNames(dto);

        LinkageStrategy linkageStrategy = switch (dto.getLinkageStrategy()) {
            case SINGLE -> new SingleLinkageStrategy();
            case COMPLETE -> new CompleteLinkageStrategy();
            case AVERAGE -> new AverageLinkageStrategy();
        };

        ClusteringService clusteringService = ClusteringServices.initClusteringService(distancesArray, faceNames, linkageStrategy);
        ClusterNode clusterRoot = clusteringService.performAgglomerativeClustering();

        return new SerializableClusterNode(clusterRoot);
    }


    private double[][] convertTo2DArray(BatchDistancesDto batchDistancesDto) {
        List<FaceDistancesDto> faceDistancesList = batchDistancesDto.getFaceDistances();

        int numRows = faceDistancesList.size();
        int numCols = faceDistancesList.getFirst().getFaceDistances().length;

        double[][] distancesArray = new double[numRows][numCols];

        for (int i = 0; i < numRows; i++) {
            FaceDistancesDto faceDistancesDto = faceDistancesList.get(i);
            distancesArray[i] = faceDistancesDto.getFaceDistances();
        }

        return distancesArray;
    }

    public List<String> extractFaceNames(BatchDistancesDto batchDistancesDto) {
        return batchDistancesDto.getFaceDistances().stream()
                .map(FaceDistancesDto::getFaceName)
                .collect(Collectors.toList());
    }
}

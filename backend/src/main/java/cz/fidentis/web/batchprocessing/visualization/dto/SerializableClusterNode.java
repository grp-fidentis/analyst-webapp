package cz.fidentis.web.batchprocessing.visualization.dto;

import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patrik Tomov
 * @since  14.09.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SerializableClusterNode {
    private String name;
    private List<SerializableClusterNode> children;
    private List<String> leafNames;
    private Double distance;

    public SerializableClusterNode(ClusterNode node) {
        this.name = node.getName();
        this.leafNames = node.getLeafNames();
        this.distance = node.getDistance() != null ? node.getDistance().getDistance() : null;
        this.children = node.getChildren().stream()
                .map(SerializableClusterNode::new)
                .collect(Collectors.toList());
    }
}

package cz.fidentis.web.batchprocessing.service;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.web.async.WebSocketProgressHandler;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceTaskDto;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.FileUploadRepo;
import cz.fidentis.web.fileupload.FileUploadService;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.service.HumanFaceService;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.project.ProjectService;
import cz.fidentis.web.task.Task;
import cz.fidentis.web.task.TaskRepo;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;


/**
 * @author Patrik Tomov
 * @since  19.09.2024
 */
@RequiredArgsConstructor
@Slf4j
public abstract class BatchBaseService {

    protected final HumanFaceService humanFaceService;
    protected final FileUploadService fileUploadService;
    protected final FileUploadRepo fileUploadRepo;
    protected final TaskRepo taskRepo;
    private final ProjectService projectService;
    protected final WebSocketProgressHandler webSocketProgressHandler;

    protected HumanFace getSelectedFace(Long fileId, Long taskId) {
        return humanFaceService.getHumanFaceEntityByFileUploadIdAndTaskId(fileId, taskId, true)
                .getHumanFace();
    }

    protected HumanFace getAverageFaceIfExists(BatchDistanceTaskDto dto) {
        return fileUploadRepo.findAverageFaceFileUploadByTaskId(dto.getTaskId())
                .map(FileUpload::getId)
                .map(fileUploadId -> {
                    try {
                        return humanFaceService.getHumanFaceEntityByFileUploadIdAndTaskId(fileUploadId, dto.getTaskId(), true)
                                .getHumanFace();
                    } catch (EntityNotFoundException e) {
                        return null;
                    }
                })
                .orElse(null);
    }

    protected HumanFace getHumanFaceAtIndexFromArray(int faceIndex, List<Long> filesIds, Long taskId) {
        Long fileId = filesIds.get(faceIndex);
        try {
            HumanFaceEntity humanFaceEntity = humanFaceService.getHumanFaceEntityByFileUploadIdAndTaskId(fileId, taskId, true);
            return humanFaceEntity.getHumanFace();
        } catch (EntityNotFoundException e) {
            return createAndFetchHumanFace(fileId, taskId);
        }
    }

    protected HumanFace createAndFetchHumanFace(Long fileId, Long taskId) {
        FileUpload fileUpload = fileUploadRepo.getReferenceById(fileId);
        Task task = taskRepo.getReferenceById(taskId);
        HumanFace newHumanFaceEntity = humanFaceService.createAndReturnHumanFace(fileUpload, task);
        log.info("Human face not found, creating new one");
        return newHumanFaceEntity;
    }

    protected void updateProgress(String sessionId, int current, int total) throws IOException {
        int progressValue = (int) Math.round(100.0 * current / total);
        webSocketProgressHandler.updateProgress(sessionId, progressValue);
    }

    protected Project getProjectById(Long projectId) {
        return projectService.getProjectById(projectId);
    }
}

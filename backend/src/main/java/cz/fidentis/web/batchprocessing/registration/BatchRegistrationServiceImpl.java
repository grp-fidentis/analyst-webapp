package cz.fidentis.web.batchprocessing.registration;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceFactory;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.measurement.DistanceRecord;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistration;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationConfig;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices;
import cz.fidentis.web.async.WebSocketProgressHandler;
import cz.fidentis.web.batchprocessing.registration.dto.AddAverageFaceToProjectDto;
import cz.fidentis.web.batchprocessing.registration.dto.BatchRegistrationTaskDto;
import cz.fidentis.web.batchprocessing.registration.dto.RegistrationResponseDto;
import cz.fidentis.web.batchprocessing.registration.dto.RegistrationResultDto;
import cz.fidentis.web.batchprocessing.service.BatchBaseService;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.FileUploadRepo;
import cz.fidentis.web.fileupload.FileUploadService;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.humanface.service.HumanFaceService;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.project.ProjectService;
import cz.fidentis.web.task.TaskRepo;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

/**
 * @author Patrik Tomov
 * @since  06.03.2024
 */
@Service
@Slf4j
public class BatchRegistrationServiceImpl extends BatchBaseService implements BatchRegistrationService {

    @PersistenceContext
    private EntityManager em;

    private static final int ICP_ITERATIONS = 100;
    private static final double ICP_ERROR = 0.05;
    private static final int ICP_AUTO_CROP_SINCE = 1;

    public BatchRegistrationServiceImpl(HumanFaceService humanFaceService, FileUploadService fileUploadService, FileUploadRepo fileUploadRepo, TaskRepo taskRepo, ProjectService projectService, WebSocketProgressHandler webSocketProgressHandler) {
        super(humanFaceService, fileUploadService, fileUploadRepo, taskRepo, projectService, webSocketProgressHandler);
    }

    @Override
    @Transactional
    public RegistrationResponseDto execute(BatchRegistrationTaskDto dto) {
        HumanFace selectedFace = getSelectedFace(dto.getSelectedFile().getId(), dto.getTaskId());
        HumanFaceDto newAvgFace = null;
        RegistrationResultDto resultDto = executeRegistration(dto, selectedFace);
        if (resultDto == null) {
            return new RegistrationResponseDto(null, false);
        }

        if (resultDto.getHumanFace() != null) {
            newAvgFace = createOrUpdateAverageFaceFile(dto, resultDto.getHumanFace());
        }
        return new RegistrationResponseDto(newAvgFace, resultDto.isCompletedSuccessfully());
    }

    @Override
    public byte[] exportAverageFace(Long humanFaceEntityId) {
        HumanFaceEntity humanFaceEntity = humanFaceService.getHumanFaceEntityById(humanFaceEntityId);

        HumanFace avgFace = SerializationUtils.deserialize(humanFaceEntity.getHumanFaceDump());
        try {
            File file = createTempFileForAverageFace();
            MeshIO.exportMeshModel(avgFace.getMeshModel(), file);
            return Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            throw new RuntimeException("Failed to create or read the file", e);
        }
    }

    @Override
    @Transactional
    public void addAverageFaceToProject(AddAverageFaceToProjectDto dto) {
        FileUpload fileUpload = fileUploadService.getAverageFaceFileOfTask(dto.getTaskId());
        fileUpload.getFaceInfo().setGeometryFileName(dto.getFileName());
        fileUpload.setName(dto.getFileName());
        Project project = getProjectById(dto.getProjectId());
        fileUpload.setProject(project);
    }

    private RegistrationResultDto executeRegistration(BatchRegistrationTaskDto dto, HumanFace selectedFace) {
        HumanFace newAvgFace;
        RegistrationResultDto resultDto;
        dto.setCalculationProgress(0);
        double prevPSM = 0;
        int iter = 0;
        while (true) {
            iter++;
            resultDto = averageAndRegisterIteration(dto);
            newAvgFace = resultDto.getHumanFace();
            if (newAvgFace == null) {
                return null;
            }
            if (iter >= dto.getAveragingTaskDto().getMaxRegistrationIterations()) {
                break;
            }

            double newPSM = procrustesSurfaceMetric(selectedFace, newAvgFace);
            double psmDiff = Math.abs(prevPSM - newPSM);

            Logger.print("Iter. " + iter + " (PSM, |diff|): " + newPSM + ", " + psmDiff);

            if (psmDiff < dto.getAveragingTaskDto().getStabilizationThreshold()) {
                break;
            }

            selectedFace = newAvgFace;
            prevPSM = newPSM;
        }
        return resultDto;
    }

    private HumanFaceDto createOrUpdateAverageFaceFile(BatchRegistrationTaskDto dto, HumanFace newAvgFace) {
        FileUpload averageFaceFile = fileUploadService.getAverageFaceFileOfTask(dto.getTaskId());
        FileUpload fileUpload = fileUploadService.createOrUpdateAverageFaceFile(dto.getProjectId(), dto.getTaskId(), averageFaceFile, newAvgFace);
        return humanFaceService.getHumanFaceDtoByFileUploadIdAndTaskId(fileUpload.getId(), dto.getTaskId());
    }

    private static double procrustesSurfaceMetric(HumanFace f1, HumanFace f2) {
        double distanceF2toF1 = computeDistance(f1, f2);
        double distanceF1toF2 = computeDistance(f2, f1);

        return Math.sqrt(distanceF2toF1 + distanceF1toF2);
    }

    private static double computeDistance(HumanFace f1, HumanFace f2) {
        FaceStateServices.updateKdTree(f1, FaceStateServices.Mode.COMPUTE_IF_ABSENT);

        MeshDistances distances = MeshDistanceServices.measure(
                f2.getMeshModel(),
                new MeshDistanceConfig(
                        MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS,
                        f1.getKdTree(),
                        false,
                        true
                )
        );

        List<Double> distancesF2toF1 = distances.stream()
                .mapToDouble(DistanceRecord::getDistance)
                .filter(Double::isFinite)
                .boxed()
                .toList();

        double distance = distancesF2toF1.stream().mapToDouble(d -> d*d).sum();
        distance *= 1.0 / (2.0 * distancesF2toF1.size());
        return distance;
    }

    private RegistrationResultDto averageAndRegisterIteration(BatchRegistrationTaskDto dto) {
        try {
            return doInBackground(dto).join();
        } catch (CompletionException | IOException ex) {
            Logger.print(ex.getCause().toString());
            return new RegistrationResultDto(null, false);
        }
    }

    @Async
    protected CompletableFuture<RegistrationResultDto> doInBackground(BatchRegistrationTaskDto dto) throws IOException {
        HumanFace avgFace;

        BatchFaceRegistrationConfig config = new BatchFaceRegistrationConfig(
                dto.getRegistrationTaskDto().getRegistrationStrategy(),
                dto.getAveragingTaskDto().getAverageFaceStrategy(),
                dto.getRegistrationTaskDto().isScaleFacesDuringRegistration(),
                dto.getRegistrationTaskDto().getRegistrationSubsampling(),
                ICP_ERROR,
                ICP_ITERATIONS,
                ICP_AUTO_CROP_SINCE,
               null);


        HumanFace initFace = getSelectedFace(dto.getSelectedFile().getId(), dto.getTaskId());
        BatchFaceRegistration batchFaceRegistration = BatchFaceRegistrationServices.initRegistration(initFace, config);

        int numFaces = dto.getFileIds().size();
        double progressStep = 100.0 / numFaces / dto.getAveragingTaskDto().getMaxRegistrationIterations();
        BigDecimal bd = new BigDecimal(progressStep).setScale(2, RoundingMode.HALF_UP);
        progressStep = bd.doubleValue();
        for (int i = 0; i < numFaces; i++) {
            if (webSocketProgressHandler.isInterrupted(dto.getCalculationWebSocketSessionId())) {
                webSocketProgressHandler.clearInterrupt(dto.getCalculationWebSocketSessionId());
                return CompletableFuture.completedFuture(new RegistrationResultDto(null, false));
            }
            HumanFace superimposedFace = getHumanFaceAtIndexFromArray(i, dto.getFileIds(), dto.getTaskId());
            HumanFaceEntity faceEntity = humanFaceService.getHumanFaceEntityByFileUploadIdAndTaskId(dto.getFileIds().get(i), dto.getTaskId(), false);

            batchFaceRegistration.register(superimposedFace);

            faceEntity.setHumanFaceDump(SerializationUtils.serialize(superimposedFace));

            em.flush();
            em.clear();

            dto.setCalculationProgress(dto.getCalculationProgress() + progressStep);
            webSocketProgressHandler.updateProgress(dto.getCalculationWebSocketSessionId(), dto.getCalculationProgress());
        }

        if (config.avgFaceStrategy() != BatchFaceRegistrationServices.AverageFaceStrategy.NONE) {
            avgFace = createAvgFace(batchFaceRegistration.getAverageMesh());
            return CompletableFuture.completedFuture(new RegistrationResultDto(avgFace, true));
        }
        return CompletableFuture.completedFuture(new RegistrationResultDto(null, true));
    }

    private File createTempFileForAverageFace() throws IOException {
        File file = File.createTempFile("AverageFace", ".obj");
        file.deleteOnExit();
        return file;
    }

    protected HumanFace createAvgFace(MeshModel avgMesh) throws IOException {
        File tempFile = File.createTempFile(this.getClass().getSimpleName(), ".obj");
        tempFile.deleteOnExit();

        HumanFace newAvgFace = HumanFaceFactory.create(avgMesh, tempFile.getCanonicalPath());
        try {
            MeshIO.exportMeshModel(newAvgFace.getMeshModel(), tempFile);
        } catch (IOException ex) {
            Logger.print(ex.toString());
        }

        return newAvgFace;
    }
}

package cz.fidentis.web.batchprocessing.distance.service;

import cz.fidentis.analyst.engines.face.batch.distance.BatchFaceDistanceServices;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceResponseTaskDto;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceTaskDto;

/**
 * @author Patrik Tomov
 * @since  19.09.2024
 */
public interface BatchIndirectService {

    /**
     * Calculate distance between faces.
     * @param dto task dto
     * @param distanceStrategy distance strategy
     * @return response dto with distances and deviations
     */
    BatchDistanceResponseTaskDto calculateDistance(BatchDistanceTaskDto dto, BatchFaceDistanceServices.DistanceStrategy distanceStrategy);
}

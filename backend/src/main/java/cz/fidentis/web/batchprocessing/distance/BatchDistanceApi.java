package cz.fidentis.web.batchprocessing.distance;

import cz.fidentis.web.batchprocessing.distance.dto.BatchDistanceTaskDto;
import cz.fidentis.web.batchprocessing.distance.dto.BatchDistancesDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Patrik Tomov
 * @since  09.09.2024
 */
@RestController
@Tag(name = "Batch processing - Distance API", description = "API for calculating distance")
@RequestMapping("/api/batch-processing/distance")
@RequiredArgsConstructor
@Slf4j
public class BatchDistanceApi {

    private final BatchDistanceService batchDistanceService;

    @PostMapping("/calculate")
    @Operation(summary = "Calculates distances of multiple faces")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<BatchDistancesDto> calculateDistance(@Valid @RequestBody BatchDistanceTaskDto dto) {
        return ResponseEntity.ok(batchDistanceService.calculateDistance(dto));
    }

    @PostMapping(value ="/export", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Operation(summary = "Export distances of multiple faces")
    public ResponseEntity<byte[]> exportDistanceResults(@Valid @RequestBody BatchDistancesDto dto) {
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + "distanceResults.csv")
                .body(batchDistanceService.exportDistanceResults(dto));
    }
}

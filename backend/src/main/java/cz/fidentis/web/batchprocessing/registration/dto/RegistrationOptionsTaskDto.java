package cz.fidentis.web.batchprocessing.registration.dto;

import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices;
import cz.fidentis.web.batchprocessing.registration.enums.BatchRegistrationMethod;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  06.03.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationOptionsTaskDto {

    @NotNull
    private BatchRegistrationMethod registrationMethod;

    @NotNull
    @Min(0)
    @Max(5000)
    private Integer registrationSubsampling;

    @NotNull
    private boolean scaleFacesDuringRegistration;


    public BatchFaceRegistrationServices.RegistrationStrategy getRegistrationStrategy() {
       return switch (registrationMethod) {
           case MESH_BASED_ICP -> BatchFaceRegistrationServices.RegistrationStrategy.ICP;
           case FEATURE_POINTS -> BatchFaceRegistrationServices.RegistrationStrategy.GPA;
           case SKIP_REGISTRATION -> BatchFaceRegistrationServices.RegistrationStrategy.NONE;
       };
    }
}

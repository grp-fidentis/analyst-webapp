package cz.fidentis.web.batchprocessing.visualization;

import cz.fidentis.web.batchprocessing.distance.dto.BatchDistancesDto;
import cz.fidentis.web.batchprocessing.visualization.dto.HeatmapRowData;
import cz.fidentis.web.batchprocessing.visualization.dto.SerializableClusterNode;

import java.util.List;

/**
 * @author Patrik Tomov
 * @since  14.09.2024
 */
public interface BatchVisualizationService {

    /**
     * Prepares data for heatmap visualization.
     *
     * @param dto DTO with data for visualization
     * @return List of HeatmapRowData which represents data for visualization
     */
    List<HeatmapRowData> prepareDataForHeatMapVisualization(BatchDistancesDto dto);

    /**
     * Prepares data for dendrogram visualization.
     *
     * @param dto DTO with data for visualization
     * @return root node of dendrogram
     */
    SerializableClusterNode prepareDataForDendrogramVisualization(BatchDistancesDto dto);
}

package cz.fidentis.web.fileupload.faceinfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Petr Hendrych
 * @since 27.03.2023
 */
@Repository
public interface FaceInfoRepo extends JpaRepository<FaceInfo, Long> {
}

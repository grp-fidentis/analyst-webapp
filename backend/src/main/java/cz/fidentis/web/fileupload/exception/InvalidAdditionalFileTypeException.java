package cz.fidentis.web.fileupload.exception;

/**
 * @author Petr Hendrych
 * @since 12.05.2023
 */
public class InvalidAdditionalFileTypeException extends RuntimeException {

    public InvalidAdditionalFileTypeException(String message) {
        super(message);
    }
}

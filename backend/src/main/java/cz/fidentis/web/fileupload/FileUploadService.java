package cz.fidentis.web.fileupload;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 30.01.2023
 */
@Service
public interface FileUploadService {
    /**
     * Service to upload new file and save to database
     *
     * @param file File to upload (UI filter only obj files)
     * @throws IOException Throws exception when upload fails
     */
    void saveFile(MultipartFile file, Long projectId) throws IOException;

    /**
     * Service to delete multiple {@code FileUpload} entities at once
     *
     * @param ids List of ids that user want to delete
     */
    void deleteFiles(List<Long> ids);

    /**
     * Service to get user uploaded {@code .obj} files on project. This returns {@code FileUpload} entity
     * without its byte array {@code fileData} to make requests fast. In database all projects are picked
     * and then filtered in service to get only .obj files for table view.
     *
     * @param projectId ID of project
     * @return List of simplified {@code FileUpload} entity
     */
    List<FileUploadDetail> getFilesOnProject(Long projectId);

    /**
     *
     * Service that handles uploading additional files for main `.obj` file. These are: texture file,
     * preview photo file and file with feature points.
     *
     * @param file Any file with mimeType image/*
     * @param projectId ID of project
     * @param faceInfoId ID of {@code FaceInfo} entity
     * @param fileType String value that determines which file is uploaded (texture, preview, feature points)
     * @throws IOException throw exception when handling file data fails
     */
    FaceInfo uploadAdditionalFile(MultipartFile file, Long projectId, Long faceInfoId, String fileType) throws IOException;

    /**
     * Service for uploading small preview photo in {@code FileUpload} used in table in FE
     *
     * @param fileUploadId ID of {@code FileUpload}
     * @param file         Preview photo
     * @return simplified {@code FileUpload} entity with new uploaded preview photo
     * @throws IOException throws exception when working with files fails
     */
    FileUpload uploadSmallPreviewPhoto(Long fileUploadId, MultipartFile file) throws IOException;

    /**
     * Service to delete additional file like: preview image, uploaded csv with feature points or texture
     * file
     *
     * @param name string representation of file to delete
     * @param extension extension of file to delete
     * @param fileInfoType determines if it is additional file: feature points, preview, texture
     * @param faceInfoId ID of {@code FaceInfo}
     */
    void deleteAdditionalFile(String name, String extension, String fileInfoType, Long faceInfoId);

    /**
     * Retrieves a list of file upload details associated with a specific task.
     *
     * @param taskId The ID of the task for which files are being retrieved.
     * @return A list of {@link FileUploadDetail} representing the files associated with the task.
     */
    List<FileUploadDetail> getFilesOfTask(Long taskId);

    /**
     * Creates or update an "Average Face" file for a given project and task, based on a human face entity.
     *
     * @param projectId The ID of the project for which the average face file is being created.
     * @param taskId The ID of the task associated with the average face file creation.
     * @param fileUpload The file upload entity representing the average face file or null if does not exist
     * @param newAvgFace New calculated average face
     * @return A {@link FileUpload} representing average face file.
     */
    FileUpload createOrUpdateAverageFaceFile(Long projectId, Long taskId, FileUpload fileUpload, HumanFace newAvgFace);

    /**
     * Retrieves the "Average Face" file associated with a specific task.
     *
     * @param taskId The ID of the task for which the average face file is being retrieved.
     * @return A {@link FileUpload} representing the average face file associated with the task.
     */
    FileUpload getAverageFaceFileOfTask(Long taskId);
}

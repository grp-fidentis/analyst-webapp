package cz.fidentis.web.fileupload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fidentis.web.base.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

/**
 * @author Petr Hendrych
 * @since 30.03.2023
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "file_data")
public class FileData extends BaseEntity {

    @Lob
    private byte[] data;

    @JsonIgnore
    @OneToOne(mappedBy = "fileData")
    private FileUpload fileUpload;

    public FileData(byte[] data) {
        this.data = data;
    }
}
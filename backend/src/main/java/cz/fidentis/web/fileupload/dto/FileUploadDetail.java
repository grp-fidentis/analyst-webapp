package cz.fidentis.web.fileupload.dto;


import org.springframework.beans.factory.annotation.Value;

import java.util.Collection;

/**
 * @author Ondřej Bazala
 * @since 03.09.2023
 */
public interface FileUploadDetail {

    Long getId();
    String getName();
    String getExtension();
    Boolean getIsAverageFace();
    byte[] getSmallPreviewPhotoData();
    @Value("#{target.faceInfo != null ? target.faceInfo.id : null}")
    Long getFaceInfoId();
    Collection<TaskDetail> getTasks();

    /**
     * @author Ondřej Bazala
     * @since 03.09.2023
     */
    interface TaskDetail {
        Long getId();
        String getName();
    }
}

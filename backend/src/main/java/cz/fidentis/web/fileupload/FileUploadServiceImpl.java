package cz.fidentis.web.fileupload;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.fileupload.exception.InvalidAdditionalFileTypeException;
import cz.fidentis.web.fileupload.exception.UserHasNoPermissionException;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.fileupload.faceinfo.FaceInfoRepo;
import cz.fidentis.web.humanface.HelperHumanFace;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.TaskFaceInfo;
import cz.fidentis.web.humanface.dto.HumanFaceEntityWithFaceInfo;
import cz.fidentis.web.humanface.service.HumanFaceService;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.project.ProjectService;
import cz.fidentis.web.task.Task;
import cz.fidentis.web.task.TaskService;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;

import static cz.fidentis.web.fileupload.FileExtensions.OBJ_EXTENSION;

/**
 * @author Petr Hendrych
 * @since 30.01.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class FileUploadServiceImpl implements FileUploadService {

    public static final String AVERAGE_FACE_NAME = "Average Face";

    private final FileUploadRepo fileUploadRepo;
    private final FaceInfoRepo faceInfoRepo;
    private final ProjectService projectService;
    private final TaskService taskService;
    private final UserService userService;
    private final HumanFaceService humanFaceService;

    @Override
    public void saveFile(MultipartFile file, Long projectId) throws IOException {
        User user = userService.getCurrentAuthenticatedUser();
        Project project = projectService.getProjectById(projectId);
        String fileName = FilenameUtils.removeExtension(file.getOriginalFilename());
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        FaceInfo faceInfo = getFaceInfo(file.getSize(), fileName, extension);

        FileUpload fileUpload = new FileUpload(fileName, extension, user, project, faceInfo, new FileData(file.getBytes()));

        fileUploadRepo.save(fileUpload);
    }

    @Override
    public void deleteFiles(List<Long> ids) {
        User user = userService.getCurrentAuthenticatedUser();
        List<FileUpload> files = fileUploadRepo.findAllById(ids);

        if (!files.stream().allMatch(fileUpload -> fileUpload.getUser().equals(user))) {
            throw new UserHasNoPermissionException("You don't have permissions to update this file!");
        }

        for (FileUpload file : files) {
            if (file.getTasks().isEmpty()) {
                fileUploadRepo.deleteAllByFaceInfoId(file.getFaceInfo().getId());
                faceInfoRepo.deleteById(file.getFaceInfo().getId());
            }
        }
    }

    @Override
    public List<FileUploadDetail> getFilesOnProject(Long projectId) {
        User user = userService.getCurrentAuthenticatedUser();

        return fileUploadRepo.findFileUploadByUserIdAndProjectIdAndExtensionOrderById(user.getId(), projectId, OBJ_EXTENSION);
    }

    @Override
    public FaceInfo uploadAdditionalFile(MultipartFile file, Long projectId, Long faceInfoId, String fileType) throws IOException {
        User user = userService.getCurrentAuthenticatedUser();
        Project project = projectService.getProjectById(projectId);
        FaceInfo faceInfo = faceInfoRepo.findById(faceInfoId)
                .orElseThrow(() -> new EntityNotFoundException("Face info with id: %s does not exist".formatted(faceInfoId)));

        switch (fileType) {
            case "points" -> faceInfo.setFeaturePointsFileName(file.getOriginalFilename());
            case "preview" -> {
                faceInfo.setPreviewPhotoFileName(file.getOriginalFilename());
                faceInfo.setPreviewPhotoData(file.getBytes());
            }
            case "texture" -> faceInfo.setTextureFileName(file.getOriginalFilename());
            default -> throw new InvalidAdditionalFileTypeException("File type for: %s is not supported".formatted(fileType));
        }

        String fileName = FilenameUtils.removeExtension(file.getOriginalFilename());
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());

        FileUpload newFile = new FileUpload(fileName, extension, user, project, faceInfo, new FileData(file.getBytes()));
        fileUploadRepo.save(newFile);

        return faceInfo;
    }

    @Override
    public FileUpload uploadSmallPreviewPhoto(Long fileUploadId, MultipartFile file) throws IOException {
        FileUpload fileUpload = fileUploadRepo.findById(fileUploadId)
                .orElseThrow(() -> new EntityNotFoundException("File upload with id: %s does not exist".formatted(fileUploadId)));

        fileUpload.setSmallPreviewPhotoData(file.getBytes());
        fileUploadRepo.save(fileUpload);

        return fileUploadRepo.findFileUploadById(fileUploadId)
                .orElseThrow(() -> new EntityNotFoundException("File upload with id: %s does not exists".formatted(fileUploadId)));
    }

    @Override
    public void deleteAdditionalFile(String name, String extension, String fileInfoType, Long faceInfoId) {
        FileUpload upload = fileUploadRepo.findFileUploadByNameAndExtensionAndFaceInfoId(name, extension, faceInfoId)
                .orElseThrow(() -> new EntityNotFoundException("File upload with name: %s, extension: %s and face info id: %s does not exist".formatted(name, extension, faceInfoId)));

        switch (fileInfoType) {
            case "points" -> upload.getFaceInfo().setFeaturePointsFileName(null);
            case "preview" -> {
                upload.getFaceInfo().setPreviewPhotoFileName(null);
                upload.getFaceInfo().setPreviewPhotoData(null);
            }
            case "texture" -> upload.getFaceInfo().setTextureFileName(null);
            default -> throw new IllegalArgumentException("fileInfoType");
        }

        fileUploadRepo.delete(upload);
    }

    @Override
    public FileUpload createOrUpdateAverageFaceFile(Long projectId, Long taskId, FileUpload averageFaceFile, HumanFace newAvgFace) {
        if (averageFaceFile != null) {
            return updateExistingAverageFaceFile(averageFaceFile, newAvgFace, taskId);
        } else {
            return createNewAverageFaceFile(taskId, newAvgFace);
        }
    }

    private FileUpload updateExistingAverageFaceFile(FileUpload averageFaceFile, HumanFace newAvgFace, Long taskId) {
        HumanFaceEntity avgFaceEntity = humanFaceService.getHumanFaceEntityByFileUploadIdAndTaskId(averageFaceFile.getId(), taskId, false);
        avgFaceEntity.setHumanFaceDump(SerializationUtils.serialize(newAvgFace));
        return averageFaceFile;
    }

    private FileUpload createNewAverageFaceFile(Long taskId, HumanFace newAvgFace) {
        User user = userService.getCurrentAuthenticatedUser();
        Task task = taskService.getTaskById(taskId);

        try {
            File file = File.createTempFile(AVERAGE_FACE_NAME, ".obj");
            file.deleteOnExit();
            HumanFaceEntityWithFaceInfo humanFaceEntityWithFaceInfo = prepareAndSaveHumanFaceEntity(file, task, newAvgFace);
            HumanFaceEntity avgFaceEntity = humanFaceEntityWithFaceInfo.getHumanFaceEntity();
            avgFaceEntity.setIsAverageFace(true);
            FaceInfo averageFaceFaceInfo = humanFaceEntityWithFaceInfo.getFaceInfo();

            MeshIO.exportMeshModel(avgFaceEntity.getHumanFace().getMeshModel(), file);

            byte[] fileContent = Files.readAllBytes(file.toPath());
            averageFaceFaceInfo.setGeometryFileSize((long) (fileContent.length / 1024));

            FileUpload fileUpload = new FileUpload(AVERAGE_FACE_NAME, OBJ_EXTENSION, user, null, averageFaceFaceInfo, new FileData(fileContent));
            fileUpload.setIsAverageFace(true);
            fileUpload.addTask(task);
            fileUpload.addHumanFaceEntity(avgFaceEntity);

            return fileUploadRepo.save(fileUpload);
        } catch (IOException e) {
            throw new RuntimeException("Failed to create or read the file", e);
        }
    }

    private HumanFaceEntityWithFaceInfo prepareAndSaveHumanFaceEntity(File averageFaceFile, Task task, HumanFace newAvgFace) {
        FaceInfo faceInfo = getFaceInfo(averageFaceFile.length(), AVERAGE_FACE_NAME, OBJ_EXTENSION);
        HelperHumanFace helperHumanFace = new HelperHumanFace(new TaskFaceInfo(faceInfo, newAvgFace));
        HumanFaceEntity avgFaceEntity = new HumanFaceEntity(task, newAvgFace, helperHumanFace);
        humanFaceService.save(task.getId(), avgFaceEntity);

        return new HumanFaceEntityWithFaceInfo(avgFaceEntity, faceInfo);
    }

    private FaceInfo getFaceInfo(long size, String name, String extension) {
        if (Objects.equals(extension, OBJ_EXTENSION)) {
            FaceInfo faceInfo = new FaceInfo();

            faceInfo.setGeometryFileSize(size / 1024);
            faceInfo.setGeometryFileName(name.equals(AVERAGE_FACE_NAME) ? name : name + "." + extension);

            return faceInfoRepo.save(faceInfo);
        }
        return faceInfoRepo.save(new FaceInfo());
    }

    @Override
    public List<FileUploadDetail> getFilesOfTask(Long taskId) {
        return fileUploadRepo.findByTasksIdOrderByName(taskId);
    }

    @Override
    public FileUpload getAverageFaceFileOfTask(Long taskId) {
        return fileUploadRepo.findAverageFaceFileUploadByTaskId(taskId).orElse(null);
    }
}

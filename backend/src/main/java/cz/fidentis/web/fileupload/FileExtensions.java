package cz.fidentis.web.fileupload;

/**
 * @author Petr Hendrych
 * @since 07.03.2023
 */
public final class FileExtensions {

    private FileExtensions() {}

    public static final String OBJ_EXTENSION = "obj";
    public static final String TEXTURE_EXTENSION = "jpg";
    public static final String CSV_EXTENSION = "csv";
}

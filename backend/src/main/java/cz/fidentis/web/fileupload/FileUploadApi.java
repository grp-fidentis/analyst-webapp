package cz.fidentis.web.fileupload;

import cz.fidentis.web.fileupload.dto.DeleteAdditionalFileDto;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import io.swagger.v3.oas.annotations.tags.Tag;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 31.01.2023
 */
@RestController
@Tag(name = "File Upload API", description = "API for managing and retrieving user's files")
@RequestMapping("/api/file-uploads")
@RequiredArgsConstructor
@Validated
public class FileUploadApi {

    private final FileUploadService fileUploadService;

    @GetMapping(value = "/projects/{projectId}", produces = "application/json")
    @Operation(summary = "Gets files of project")
    @PreAuthorize("isOwner(#projectId, 'PROJECT')")
    public ResponseEntity<List<FileUploadDetail>> getFilesOfProject(@PathVariable Long projectId) {
        return ResponseEntity.ok(fileUploadService.getFilesOnProject(projectId));
    }

    @GetMapping(value = "/tasks/{taskId}", produces = "application/json")
    @Operation(summary = "Gets files of task")
    @PreAuthorize("isOwner(#taskId, 'TASK')")
    public ResponseEntity<List<FileUploadDetail>> getFilesOfTask(@PathVariable Long taskId) {
        return ResponseEntity.ok(fileUploadService.getFilesOfTask(taskId));
    }

    @PostMapping(value = "/{projectId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Uploads geometry files of faces to project")
    @PreAuthorize("isOwner(#projectId, 'PROJECT')")
    public ResponseEntity<Void> saveFiles(@Valid @RequestParam MultipartFile[] files, @PathVariable Long projectId) throws IOException {
        for (MultipartFile file : files) {
            fileUploadService.saveFile(file, projectId);
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/{projectId}/face-info/{faceInfoId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Uploads additional file (preview photo, texture or feature points)")
    @PreAuthorize("isOwner(#projectId, 'PROJECT') and isOwner(#faceInfoId, 'FACE_INFO')")
    public ResponseEntity<FaceInfo> saveAdditionalFile(@Valid @RequestParam MultipartFile file,
                                                       @RequestParam @NotBlank String fileType,
                                                       @PathVariable Long faceInfoId,
                                                       @PathVariable Long projectId) throws IOException {
        return ResponseEntity.ok(fileUploadService.uploadAdditionalFile(file, projectId, faceInfoId, fileType));
    }

    @PostMapping(value = "/{id}/preview-photo", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Uploads small preview photo")
    @PreAuthorize("isOwner(#id, 'FILE_UPLOAD')")
    public ResponseEntity<FileUpload> uploadSmallPreviewPhoto(@PathVariable Long id, @Valid @RequestParam MultipartFile file) throws IOException {
        return ResponseEntity.ok(fileUploadService.uploadSmallPreviewPhoto(id, file));
    }

    @DeleteMapping
    @Operation(summary = "Deletes geometry files selected by ID and all their linked additional files")
    @PreAuthorize("isOwner(#ids, 'FILE_UPLOAD')")
    public ResponseEntity<Object> deleteFiles(@RequestBody @NotEmpty List<@NotNull Long> ids) {
        fileUploadService.deleteFiles(ids);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/face-info/{faceInfoId}")
    @Operation(summary = "Deletes additional file (preview photo, texture or feature points")
    @PreAuthorize("isOwner(#faceInfoId, 'FACE_INFO')")
    public ResponseEntity<Void> deleteAdditionalFile(@PathVariable Long faceInfoId, @Valid @RequestBody DeleteAdditionalFileDto dto) {
        fileUploadService.deleteAdditionalFile(dto.name(), dto.extension(), dto.fileInfoType(), faceInfoId);
        return ResponseEntity.noContent().build();
    }
}

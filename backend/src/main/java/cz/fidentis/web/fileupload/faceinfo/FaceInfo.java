package cz.fidentis.web.fileupload.faceinfo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fidentis.web.base.BaseEntity;
import cz.fidentis.web.fileupload.FileUpload;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 27.03.2023
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "face_info")
public class FaceInfo extends BaseEntity {

    private String geometryFileName;

    private Long geometryFileSize;

    private String featurePointsFileName;

    private String previewPhotoFileName;

    private String textureFileName;

    @Lob
    private byte[] previewPhotoData;

    private Long numberOfVertices;

    private Integer numberOfFacets;

    @JsonIgnore
    @OneToMany(mappedBy = "faceInfo", fetch = FetchType.LAZY)
    private List<FileUpload> fileUploads;
}

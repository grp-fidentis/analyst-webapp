package cz.fidentis.web.fileupload.dto;

import jakarta.validation.constraints.NotBlank;

/**
 * @author Petr Hendrych
 * @param name
 * @param extension
 * @param fileInfoType
 */
public record DeleteAdditionalFileDto(
        @NotBlank String name,
        @NotBlank String extension,
        @NotBlank String fileInfoType) {
}

package cz.fidentis.web.fileupload;

import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Petr Hendrych
 * @since 30.01.2023
 */
@Repository
public interface FileUploadRepo extends JpaRepository<FileUpload, Long> {
    
    /**
     * Method to fetch file uploads by user on certain project and specific extension
     *
     * @param user_id ID of {@code User}
     * @param project_id ID of {@code Project}
     * @param extension String value of desired file extension
     *
     * @return List of simplified {@code FileUpload} entity that contains only: id, name, extension and file size ordered by name
     */
    @EntityGraph(attributePaths = "tasks")
    List<FileUploadDetail> findFileUploadByUserIdAndProjectIdAndExtensionOrderById(Long user_id, Long project_id, String extension);

    /**
     * Method to fetch simplified {@code FileUpload} by id
     *
     * @param id ID of {@code FileUpload}
     * @return simplified {@code FileUpload} without main file data
     */
    Optional<FileUpload> findFileUploadById(Long id);

    /**
     * Method to fetch {@code FileUpload} for certain {@code FaceInfo} entity determines by its
     * name and extension
     *
     * @param name string representation of file name
     * @param extension extension of file
     * @param faceInfo_id ID of {@code FaceInfo}
     * @return {@code FileUpload} entity
     */
    Optional<FileUpload> findFileUploadByNameAndExtensionAndFaceInfoId(String name, String extension, Long faceInfo_id);

    /**
     * Method to delete all related files of selected `.obj` files (all linked files are sharing same {@code FaceInfo} id,
     * so we can safely delete all files with this {@code FaceInfo} id
     *
     * @param faceInfo_id ID of {@code FaceInfo} entity
     */
    void deleteAllByFaceInfoId(Long faceInfo_id);

    /**
     * Method to fetch file uploads associated with a specific task ID.
     *
     * @param taskId ID of the {@code Task}
     * @return List of {@code FileUploadDetail} projection for all file uploads associated with the task ordered by name
     */
    @EntityGraph(attributePaths = "tasks")
    @Query("SELECT f FROM FileUpload f JOIN f.tasks t WHERE t.id = :taskId")
    List<FileUploadDetail> findByTasksIdOrderByName(@Param("taskId") Long taskId);

    /**
     * Fetches the "Average Face" file upload associated with a specific task.
     *
     * @param taskId ID of the {@code Task}
     * @return {@code FileUpload} entity
     */
    @EntityGraph(attributePaths = "tasks")
    @Query("SELECT f FROM FileUpload f JOIN f.tasks t WHERE t.id = :taskId AND f.isAverageFace = true")
    Optional<FileUpload> findAverageFaceFileUploadByTaskId(@Param("taskId") Long taskId);
}

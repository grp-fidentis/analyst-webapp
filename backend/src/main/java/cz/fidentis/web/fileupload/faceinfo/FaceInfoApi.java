package cz.fidentis.web.fileupload.faceinfo;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author Petr Hendrych
 * @since 27.03.2023
 */
@RestController
@Tag(name = "Face Info API", description = "API for managing and retrieving face info")
@RequestMapping("/api/face-info")
@RequiredArgsConstructor
public class FaceInfoApi {

    private final FaceInfoService faceInfoService;

    @GetMapping("/{id}")
    @Operation(summary = "Gets faceInfo by id")
    @PreAuthorize("isOwner(#id, 'FACE_INFO')")
    public ResponseEntity<FaceInfo> getFaceInfoByFileUploadId(@PathVariable Long id) {
        return ResponseEntity.ok(faceInfoService.getFaceInfoById(id));
    }

    @PostMapping("/{id}/compute-geometry-info/{uploadId}")
    @Operation(summary = "Computes faceInfo geometry")
    @PreAuthorize("isOwner(#id, 'FACE_INFO') and isOwner(#uploadId, 'FILE_UPLOAD')")
    public ResponseEntity<FaceInfo> computeFaceInfoGeometry(@PathVariable Long id, @PathVariable Long uploadId) throws IOException {
        return ResponseEntity.ok(faceInfoService.computeFaceInfoGeometry(id, uploadId));
    }
}

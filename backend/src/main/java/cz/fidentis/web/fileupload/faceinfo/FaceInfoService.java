package cz.fidentis.web.fileupload.faceinfo;

import java.io.IOException;

/**
 * @author Petr Hendrych
 * @since 27.03.2023
 */
public interface FaceInfoService {

    /**
     * Service to get {@code FaceInfo} entity by id
     *
     * @param id ID of {@code FaceInfo}
     * @return {@code FaceInfo} entity with all data
     */
    FaceInfo getFaceInfoById(Long id);

    /**
     * Service to compute number of vertices and facets before creating new task
     *
     * @param id ID of {@code FaceInfo}
     * @param uploadId ID of {@code FileUpload}
     * @return updated {@code FaceInfo} with computed vertices and facets
     * @throws IOException throws when parsing file fails
     */
    FaceInfo computeFaceInfoGeometry(Long id, Long uploadId) throws IOException;
}

package cz.fidentis.web.fileupload.exception;

/**
 * @author Petr Hendrych
 * @since 19.02.2023
 */
public class UserHasNoPermissionException extends RuntimeException {

    public UserHasNoPermissionException(String message) {
        super(message);
    }
}

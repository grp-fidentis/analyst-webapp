package cz.fidentis.web.fileupload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.user.User;
import cz.fidentis.web.base.BaseEntity;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.task.Task;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Petr Hendrych
 * @since 30.01.2023
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "file_upload")
public class FileUpload extends BaseEntity {

    public FileUpload(String name, String extension, User user, Project project, FaceInfo faceInfo, FileData fileData) {
        this.name = name;
        this.extension = extension;
        this.user = user;
        this.project = project;
        this.faceInfo = faceInfo;
        this.fileData = fileData;
    }

    private String name;

    private String extension;

    private Boolean isAverageFace = false;

    @Lob
    private byte[] smallPreviewPhotoData;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_user_id", referencedColumnName = "id")
    private User user;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    private Project project;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "face_info_id", referencedColumnName = "id")
    private FaceInfo faceInfo;

    @ManyToMany(mappedBy = "fileUploads", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Task> tasks = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "file_data_id", referencedColumnName = "id")
    @JsonIgnore
    private FileData fileData;

    @ManyToMany(mappedBy = "fileUploads", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<HumanFaceEntity> humanFaceEntities = new ArrayList<>();

    public void addTask(Task task) {
        this.tasks.add(task);
        task.getFileUploads().add(this);
    }

    public void addHumanFaceEntity(HumanFaceEntity humanFaceEntity) {
        if (this.humanFaceEntities == null) {
            this.humanFaceEntities = new ArrayList<>();
        }
        this.humanFaceEntities.add(humanFaceEntity);
        humanFaceEntity.getFileUploads().add(this);
    }
}

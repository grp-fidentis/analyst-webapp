package cz.fidentis.web.fileupload.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  06.03.2024
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileUploadDto {

    private Long id;
    private String name;
    private String extension;
    private byte[] smallPreviewPhotoData;
    private Long faceInfoId;
}

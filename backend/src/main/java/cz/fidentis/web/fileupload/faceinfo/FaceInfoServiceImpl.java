package cz.fidentis.web.fileupload.faceinfo;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.FileUploadRepo;
import cz.fidentis.web.humanface.service.HumanFaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
/**
 * @author Petr Hendrych
 * @since 27.03.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class FaceInfoServiceImpl implements FaceInfoService {

    private final HumanFaceService humanFaceService;
    private final FaceInfoRepo faceInfoRepo;
    private final FileUploadRepo fileUploadRepo;

    @Override
    public FaceInfo getFaceInfoById(Long id) {
        return faceInfoRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Face info with id: %s does not exist".formatted(id)));
    }

    @Override
    public FaceInfo computeFaceInfoGeometry(Long id, Long uploadId) throws IOException {
        FileUpload file = fileUploadRepo.findFileUploadById(uploadId)
                .orElseThrow(() -> new EntityNotFoundException("File upload with id: %s does not exist".formatted(uploadId)));
        HumanFace face = humanFaceService.loadHumanFaceFromFile(file.getFileData().getData(), null);
        FaceInfo info = faceInfoRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Face info with id: %s does not exist".formatted(id)));

        info.setNumberOfFacets(face.getMeshModel().getFacets().size());
        info.setNumberOfVertices(face.getMeshModel().getNumVertices());

        return faceInfoRepo.save(info);
    }
}

package cz.fidentis.web.async;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author Ondřej Bazala
 * @since 03.10.2023
 */
@Configuration
@EnableAsync
@Profile("!test")
public class AsyncConfig {

}

package cz.fidentis.web.async;

import com.nimbusds.jose.shaded.gson.JsonObject;
import com.nimbusds.jose.shaded.gson.JsonParser;
import jakarta.websocket.OnMessage;
import jakarta.websocket.OnOpen;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Patrik Tomov
 * @since  07.04.2024
 */
public class WebSocketProgressHandler extends TextWebSocketHandler {
    private static final Map<String, WebSocketSession> SESSIONS = new ConcurrentHashMap<>();
    private static final Map<String, Boolean> INTERRUPT_FLAGS = new ConcurrentHashMap<>();

    @Override
    @OnOpen
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {
        SESSIONS.put(session.getId(), session);
        session.sendMessage(new TextMessage("{\"type\": \"sessionId\", \"sessionId\": \"" + session.getId() + "\"}"));
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        SESSIONS.remove(session.getId());
    }

    @OnMessage
    public void handleTextMessage(WebSocketSession session, TextMessage message) {
        String payload = message.getPayload();
        JsonObject jsonMessage = JsonParser.parseString(payload).getAsJsonObject();
        String action = jsonMessage.get("action").getAsString();

        if ("interrupt".equals(action)) {
            String sessionId = session.getId();
            INTERRUPT_FLAGS.put(sessionId, true);
        }
    }

    public boolean isInterrupted(String sessionId) {
        return INTERRUPT_FLAGS.getOrDefault(sessionId, false);
    }

    public void clearInterrupt(String sessionId) {
        INTERRUPT_FLAGS.remove(sessionId);
    }

    public void updateProgress(String sessionId, double progress) throws IOException {
        WebSocketSession session = SESSIONS.get(sessionId);
        if (session != null && session.isOpen()) {
            session.sendMessage(new TextMessage("{\"type\": \"progress\", \"progress\": " + progress + "}"));
        }
    }
}

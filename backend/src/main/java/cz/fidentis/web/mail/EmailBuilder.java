package cz.fidentis.web.mail;

import org.springframework.stereotype.Component;

/**
 * @author Petr Hendrych
 * @since 17.03.2023
 */
@Component
public class EmailBuilder {

    public String createdUserEmail(String username, String link, String websiteUrl) {
        String emailString = "";

        emailString += "<p>Hello %s,</p>".formatted(username);
        emailString += "<p>An account has been created in: <a href=\"" + websiteUrl + "\">www.fidentis.cz</a>. You have 15 minutes to set your password via link below.</p>";
        emailString += "<a href=\"https://" + link + "\">Link to create password page</a>";
        emailString += "<p>If you miss this time span go to: <a href=\"https://" + websiteUrl + "\">" + websiteUrl + "/login</a> and press \"Forgot password\". Enter your email and you will get new token for resetting password.</p>";

        return emailString;
    }

    public String forgotPasswordEmail(String username, String link) {
        String emailString = "";

        emailString += "<p>Hello %s,</p>".formatted(username);
        emailString += "<p>New token has been generated to reset your password. Token will expire after 15 minutes. Click on the link below to visit reset password page that contains reset token necessary for this action.</p>";
        emailString += "<a href=\"https://" + link + "\">Link to reset password page</a>";

        return emailString;
    }

    public String enabledUserEmail(String username) {
        String emailString = "";

        emailString += "<p>Hello %s,</p>".formatted(username);
        emailString += "<p>Your account has been enabled by Administrator.";

        return emailString;
    }

    public String disabledUserEmail(String username) {
        String emailString = "";

        emailString += "<p>Hello %s,</p>".formatted(username);
        emailString += "<p>Your account has been disabled by Administrator.";

        return emailString;
    }
}

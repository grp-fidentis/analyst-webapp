package cz.fidentis.web.mail;

/**
 * @author Petr Hendrych
 * @since 16.03.2023
 */
public interface EmailService {
    void send(String to, String email, String subject);
}

package cz.fidentis.web.analyticaltask.sampling;

import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.Function;

import static cz.fidentis.analyst.data.mesh.MeshTriangle.Smoothing.NORMAL;
import static cz.fidentis.analyst.data.mesh.MeshTriangle.Smoothing.SHAPE;

/**
 * @author Ondřej Bazala
 * @since 26.09.2023
 */
@Getter
@AllArgsConstructor
public enum PointSamplingStrategy {
    NONE(strength -> new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, strength)),
    RANDOM_SAMPLING(strength -> new PointSamplingConfig(PointSamplingConfig.Method.RANDOM, strength)),
    SHAPE_POISSON_DISK_SUB_SAMPLING(strength -> new PointSamplingConfig(PointSamplingConfig.Method.POISSON, strength, SHAPE)),
    NORMAL_POISSON_DISK_SUB_SAMPLING(strength -> new PointSamplingConfig(PointSamplingConfig.Method.POISSON, strength, NORMAL)),
    GAUSSIAN_CURVATURE(strength -> new PointSamplingConfig(PointSamplingConfig.Method.CURVATURE_GAUSSIAN, strength)),
    MEAN_CURVATURE(strength -> new PointSamplingConfig(PointSamplingConfig.Method.CURVATURE_MEAN, strength)),
    MAX_CURVATURE(strength -> new PointSamplingConfig(PointSamplingConfig.Method.CURVATURE_MAX, strength)),
    MIN_CURVATURE(strength -> new PointSamplingConfig(PointSamplingConfig.Method.CURVATURE_MIN, strength)),
    UNIFORM_SPACE_SAMPLING(strength -> new PointSamplingConfig(PointSamplingConfig.Method.UNIFORM_SPACE, strength)),
    UNIFORM_MESH_SAMPLING(strength -> new PointSamplingConfig(PointSamplingConfig.Method.UNIFORM_SURFACE, strength));

    private final Function<Integer, PointSamplingConfig> pointSampling;
}

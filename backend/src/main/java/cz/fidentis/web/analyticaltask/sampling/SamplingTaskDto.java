package cz.fidentis.web.analyticaltask.sampling;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SamplingTaskDto {

    private Long taskId;

    @NotNull
    private PointSamplingStrategy samplingStrategy = PointSamplingStrategy.NONE;

    @NotNull
    @Min(10)
    @Max(5000)
    private Integer samplingStrength;
}

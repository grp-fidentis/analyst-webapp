package cz.fidentis.web.analyticaltask.cuttingplanes;

import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlaneDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlanes2DProjectionDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlanes2DProjectionResponseDto;

import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
public interface CuttingPlanesTaskService {

    /**
     * Creates cutting planes for render
     *
     * @param taskId of task
     * @return cutting planes
     */
    List<CuttingPlaneDto> createCuttingPlanes(Long taskId);

    /**
     * Calculates 2D projection of intersection of faces and active cutting planes
     * In case of PairTask also calculates hausdorffDistances
     *
     * @param dto with cutting planes state
     * @return points of projection (intersection line, feature points)
     */
    CuttingPlanes2DProjectionResponseDto calculate2DProjection(CuttingPlanes2DProjectionDto dto);

    /**
     * Exports ZIP file with configuration.json and csv files of 2d projection curves of intersections
     *
     * @param dto with cutting planes state
     * @return ZIP file
     */
    byte[] exportProjectionsAsZip(CuttingPlanes2DProjectionDto dto);
}

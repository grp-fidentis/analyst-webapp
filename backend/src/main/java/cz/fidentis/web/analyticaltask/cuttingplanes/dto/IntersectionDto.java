package cz.fidentis.web.analyticaltask.cuttingplanes.dto;

import cz.fidentis.analyst.data.shapes.CrossSection2D;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.vecmath.Point2d;
import java.util.ArrayList;
import java.util.List;

import static cz.fidentis.web.humanface.HumanFaceUtil.roundToFourDigits;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@Getter
@NoArgsConstructor
public class IntersectionDto {

    private final List<CuttingPlaneIntersection> cuttingPlanesIntersections = new ArrayList<>();

    public IntersectionDto(boolean isPrimaryFace, int reducedVertices, List<CuttingPlaneDto> planesToProject, List<CrossSection2D> curves) {
        for (int i = 0; i < planesToProject.size(); i++) {
            CrossSection2D curve2d = curves.get(i);
            if (curve2d == null) {
                continue;
            }

            List<List<Point2d>> curveSegments = new ArrayList<>();
            if (curve2d.getCurveSegments() != null && !curve2d.getCurveSegments().isEmpty()) {
                curveSegments = curve2d.subSample(reducedVertices).getCurveSegments();
            }

            CuttingPlaneDto cuttingPlaneDto = planesToProject.get(i);
            CuttingPlaneIntersection intersection = new CuttingPlaneIntersection(cuttingPlaneDto.getCuttingPlaneId(),
                    isPrimaryFace, cuttingPlaneDto.getOrder(), curveSegments, curve2d.getFeaturePoints(), curve2d.getNormals());

            this.cuttingPlanesIntersections.add(intersection);
        }
    }

    /**
     * @author  Ondřej Bazala
     * @since  11.11.2023
     */
    @Getter
    @Setter
    public static class CuttingPlaneIntersection {

        private String cuttingPlaneId;

        private Boolean isPrimaryFace;

        private Integer order;

        private List<List<Point2D>> curveSegments;

        private List<Point2D> featurePoints;

        private List<Line2D> normals;

        public CuttingPlaneIntersection(String cuttingPlaneId, Boolean isPrimaryFace, Integer order,
                                        List<List<Point2d>> curveSegments,
                                        List<Point2d> featurePoints, List<java.awt.geom.Line2D> normals) {
            this.cuttingPlaneId = cuttingPlaneId;
            this.isPrimaryFace = isPrimaryFace;
            this.order = order;
            this.curveSegments = curveSegments.stream()
                    .map(s -> s.stream()
                            .map(p -> new Point2D(p.x, p.y))
                            .toList())
                    .toList();
            this.featurePoints = featurePoints.stream()
                    .map(p -> new Point2D(p.x, p.y))
                    .toList();
            this.normals = normals.stream()
                    .map(Line2D::new)
                    .toList();
        }

        /**
         * @author  Ondřej Bazala
         * @since  11.11.2023
         */
        @Getter
        @Setter
        public static class Point2D {

            private float x;

            private float y;

            public Point2D(double x, double y) {
                this.x = roundToFourDigits(x);
                this.y = roundToFourDigits(y);
            }
        }

        /**
         * @author  Ondřej Bazala
         * @since  11.11.2023
         */
        @Getter
        @Setter
        public static class Line2D {

            private Point2D start;

            private Point2D end;

            public Line2D(java.awt.geom.Line2D line) {
                this.start = new Point2D(line.getP1().getX(), line.getP1().getY());
                this.end = new Point2D(line.getP2().getX(), line.getP2().getY());
            }
        }
    }
}

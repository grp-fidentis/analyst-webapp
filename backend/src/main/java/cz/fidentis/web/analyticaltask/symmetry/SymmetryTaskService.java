package cz.fidentis.web.analyticaltask.symmetry;

import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryResponseTaskDto;
import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryTaskDto;

import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
public interface SymmetryTaskService {

    /**
     * Method that calculates symmetry plane and precisions
     *
     * @param dto with symmetry parameters
     * @return {@code SymmetryResponseTaskDto} dto with symmetry planes and precision results
     */
    List<SymmetryResponseTaskDto> calculate(SymmetryTaskDto dto);
}

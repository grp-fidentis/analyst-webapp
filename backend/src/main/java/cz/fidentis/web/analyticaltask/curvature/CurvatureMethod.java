package cz.fidentis.web.analyticaltask.curvature;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.function.Function;

/**
 * @author Ondřej Bazala
 * @since 27.11.2023
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum CurvatureMethod {
    MIN(point -> point.getCurvature().minPrincipal()),
    MAX(point -> point.getCurvature().maxPrincipal()),
    MEAN(point -> point.getCurvature().mean()),
    GAUSSIAN(point -> point.getCurvature().gaussian());
    
    private Function<MeshPoint, Double> value;
}

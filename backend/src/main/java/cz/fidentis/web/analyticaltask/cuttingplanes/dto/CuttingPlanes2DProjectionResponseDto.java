package cz.fidentis.web.analyticaltask.cuttingplanes.dto;

import cz.fidentis.analyst.engines.face.CuttingPlanesUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.IntStream;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@Getter
@NoArgsConstructor
public class CuttingPlanes2DProjectionResponseDto {

    private List<IntersectionDto> intersectionPointsDtos;

    private List<HausdorffDistance> hausdorffDistances;

    private CuttingPlanesUtils.BoundingBox2D boundingBoxDto;

    public CuttingPlanes2DProjectionResponseDto(List<IntersectionDto> intersectionPointsDtos,
                                                List<Double> distances,
                                                CuttingPlanesUtils.BoundingBox2D boundingBoxDto) {
        this.intersectionPointsDtos = intersectionPointsDtos;
        this.boundingBoxDto = boundingBoxDto;

        if (distances != null) {
            this.hausdorffDistances = IntStream.range(0, distances.size())
                    .mapToObj(i -> new HausdorffDistance(i + 1, distances.get(i)))
                    .toList();
        }
    }
}

package cz.fidentis.web.analyticaltask.distance.enums;

/**
 * @author Ondřej Bazala
 * @since 14.10.2023
 */
public enum ExportType {
    DISTANCE,
    WEIGHTED_DISTANCE,
}

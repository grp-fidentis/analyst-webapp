package cz.fidentis.web.analyticaltask.distance.dto;

import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.web.analyticaltask.distance.enums.ExportType;
import cz.fidentis.web.analyticaltask.distance.enums.HeatmapRenderMode;
import cz.fidentis.web.humanface.dto.update.TransformationDto;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  30.09.2023
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DistanceTaskDto {

    @NotNull
    private Long taskId;

    @NotNull
    private MeshDistanceConfig.Method distanceStrategy = MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS;

    @NotNull
    private Boolean crop = true;

    @NotNull
    private Boolean useRelativeDistance = false;

    @NotNull
    private HeatmapRenderMode heatmapRenderMode = HeatmapRenderMode.STANDARD;

    private List<FeaturePointSize> featurePointSizes = new ArrayList<>();

    private TransformationDto transformationDto;

    private ExportType exportType;

    @NotNull
    private Boolean recomputeDistances = false;

    /**
     * @author  Ondřej Bazala
     * @since  16.10.2023
     */
    @AllArgsConstructor
    @Data
    public static class FeaturePointSize {

        private String name;
        private Double size;
    }
}

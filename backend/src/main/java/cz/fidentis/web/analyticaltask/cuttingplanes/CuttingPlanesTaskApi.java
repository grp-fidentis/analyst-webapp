package cz.fidentis.web.analyticaltask.cuttingplanes;

import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlaneDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlanes2DProjectionDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlanes2DProjectionResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@RestController
@Tag(name = "Cutting Planes Analytical Task API", description = "API for computing cutting planes")
@RequestMapping("/api/analytical-tasks/cutting-planes")
@RequiredArgsConstructor
@Slf4j
public class CuttingPlanesTaskApi {

    private final CuttingPlanesTaskService cuttingPlanesTaskService;

    @PostMapping("/task/{taskId}")
    @Operation(summary = "Retrieves cutting planes for render")
    @PreAuthorize("isOwner(#taskId, 'TASK')")
    public ResponseEntity<List<CuttingPlaneDto>> createCuttingPlanes(@PathVariable Long taskId) {
        return ResponseEntity.ok(cuttingPlanesTaskService.createCuttingPlanes(taskId));
    }

    @PostMapping("/calculate-2D-projection")
    @Operation(summary = "Calculates 2D projection of intersection of faces and cutting planes")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<CuttingPlanes2DProjectionResponseDto> calculate2DProjection(
            @Valid @RequestBody CuttingPlanes2DProjectionDto dto) {
        return ResponseEntity.ok(cuttingPlanesTaskService.calculate2DProjection(dto));
    }

    @PostMapping(value = "/export", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Operation(summary = "Export projection curves as ZIP file")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<byte[]> exportProjectionsAsZip(@Valid @RequestBody CuttingPlanes2DProjectionDto dto) {
        String fileName = "task_" + dto.getTaskId() + "_";
        fileName += "cutting_planes_2D_projection";
        fileName += ".zip";

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
                .body(cuttingPlanesTaskService.exportProjectionsAsZip(dto));
    }
}

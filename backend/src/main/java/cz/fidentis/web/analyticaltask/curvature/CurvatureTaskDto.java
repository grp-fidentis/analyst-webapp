package cz.fidentis.web.analyticaltask.curvature;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author  Ondřej Bazala
 * @since  27.11.2023
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CurvatureTaskDto {

    @NotNull
    private Long taskId;

    @NotNull
    private CurvatureMethod curvatureMethod;
}

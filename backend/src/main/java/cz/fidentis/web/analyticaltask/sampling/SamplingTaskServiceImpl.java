package cz.fidentis.web.analyticaltask.sampling;

import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingServices;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.HumanFaceSamplesDto;
import cz.fidentis.web.humanface.service.HumanFaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static cz.fidentis.web.humanface.HumanFaceUtil.getSecondaryFace;
import static cz.fidentis.web.task.TaskUtil.isPairTask;

/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SamplingTaskServiceImpl implements SamplingTaskService {

    private final HumanFaceService humanFaceService;

    @Override
    @Transactional(readOnly = true)
    public HumanFaceSamplesDto calculate(SamplingTaskDto dto) {
        List<HumanFaceEntity> faces = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());

        HumanFaceEntity face = isPairTask(faces) ? getSecondaryFace(faces) : faces.get(0);

        return calculate(dto, face);
    }

    @Override
    @Transactional(readOnly = true)
    public HumanFaceSamplesDto calculate(SamplingTaskDto dto, HumanFaceEntity face) {
        var strategy = dto.getSamplingStrategy();

        if (strategy == PointSamplingStrategy.NONE) {
            return new HumanFaceSamplesDto(face.getId(), null);
        }

        PointSamplingConfig pointSampling = strategy.getPointSampling().apply(dto.getSamplingStrength());
        return new HumanFaceSamplesDto(
                face.getId(),
                PointSamplingServices.sample(face.getHumanFace().getMeshModel(), pointSampling)
        );
    }
}

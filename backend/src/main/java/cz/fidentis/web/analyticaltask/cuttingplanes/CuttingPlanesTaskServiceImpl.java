package cz.fidentis.web.analyticaltask.cuttingplanes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.shapes.CrossSection2D;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.face.CuttingPlanesUtils;
import cz.fidentis.analyst.engines.face.FaceCuttingServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.*;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.IntersectionDto.CuttingPlaneIntersection.Point2D;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesDirection;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesType;
import cz.fidentis.web.exceptions.exceptions.ExportException;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.service.HumanFaceService;
import cz.fidentis.web.task.TaskUtil;
import cz.fidentis.web.zip.ZipBuilder;
import cz.fidentis.web.zip.ZipBuilder.ZipFile;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.vecmath.Point2d;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.IntStream;

import static cz.fidentis.analyst.engines.face.CuttingPlanesUtils.*;
import static cz.fidentis.web.analyticaltask.cuttingplanes.dto.IntersectionDto.CuttingPlaneIntersection;
import static cz.fidentis.web.humanface.HumanFaceUtil.getPrimaryFace;
import static cz.fidentis.web.humanface.HumanFaceUtil.getSecondaryFace;
import static cz.fidentis.web.task.TaskUtil.isPairTask;
import static cz.fidentis.web.task.TaskUtil.isSingleFaceTask;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class CuttingPlanesTaskServiceImpl implements CuttingPlanesTaskService {

    private static final double FEATURE_POINT_CLOSENESS = 1.5;

    private final HumanFaceService humanFaceService;

    @Override
    public List<CuttingPlaneDto> createCuttingPlanes(Long taskId) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(taskId);
        HumanFaceEntity face = isPairTask(facesOfTask) ? getPrimaryFace(facesOfTask) : facesOfTask.get(0);

        List<CuttingPlaneDto> allCuttingPlanes = createCuttingPlanesOfType(face, CuttingPlanesType.BOUNDING_BOX_PLANE);

        if (face.getHumanFace().hasSymmetryPlane()) {
            allCuttingPlanes.addAll(createCuttingPlanesOfType(face, CuttingPlanesType.SYMMETRY_PLANE));
        }

        face.getHelperHumanFace().setCuttingPlaneDtos(allCuttingPlanes);
        humanFaceService.save(taskId, face);

        return allCuttingPlanes;
    }

    private List<CuttingPlaneDto> createCuttingPlanesOfType(HumanFaceEntity humanFaceEntity, CuttingPlanesType type) {
        List<CuttingPlaneDto> cuttingPlaneDtos = new ArrayList<>();
        HumanFace face = humanFaceEntity.getHumanFace();
        FaceStateServices.updateBoundingBox(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        for (CuttingPlanesDirection direction : CuttingPlanesDirection.values()) {
            for (int order = 1; order <= 3; order++) {
                Plane plane = type.equals(CuttingPlanesType.BOUNDING_BOX_PLANE)
                        ? FaceCuttingServices.fromBoundingBox(face, direction.getVectorDirection())
                        : FaceCuttingServices.fromSymmetryPlane(face, direction.getVectorDirection());
                cuttingPlaneDtos.add(new CuttingPlaneDto(humanFaceEntity, plane, type, direction, order));
            }
        }
        return cuttingPlaneDtos;
    }

    @Override
    public CuttingPlanes2DProjectionResponseDto calculate2DProjection(CuttingPlanes2DProjectionDto dto) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());
        HumanFaceEntity face = isPairTask(facesOfTask) ? getPrimaryFace(facesOfTask) : facesOfTask.get(0);

        List<CuttingPlaneDto> planesToProject = face.getHelperHumanFace().getCuttingPlaneDtos().stream()
                .filter(c -> dto.getCuttingPlaneStates().stream()
                        .anyMatch(s -> s.getCuttingPlaneId().equals(c.getCuttingPlaneId())))
                .sorted(Comparator.comparing(CuttingPlaneDto::getOrder))
                .toList();
        List<List<CrossSection2D>> curvesOfFaces = computeCrossSections(facesOfTask, dto, planesToProject);

        List<IntersectionDto> intersectionDtos = IntStream.range(0, curvesOfFaces.size())
                .mapToObj(i -> new IntersectionDto(
                        i == 0, dto.getReducedVertices(), planesToProject, curvesOfFaces.get(i)))
                .toList();

        List<Double> hausdorffDistances = isSingleFaceTask(facesOfTask) ? null :
                getHausdorffDistances(dto.getSamplingStrength(), curvesOfFaces.get(0), curvesOfFaces.get(1));

        FaceStateServices.updateBoundingBox(face.getHumanFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        BoundingBox2D boundingBoxDto = getBoundingBox2D(dto.getCanvasWidth(),
                dto.getCanvasHeight(), face.getHumanFace().getBoundingBox(), dto.getDirection().getVectorDirection());

        return new CuttingPlanes2DProjectionResponseDto(intersectionDtos, hausdorffDistances, boundingBoxDto);
    }

    private List<List<CrossSection2D>> computeCrossSections(List<HumanFaceEntity> facesOfTask, CuttingPlanes2DProjectionDto dto,
                                                            List<CuttingPlaneDto> cuttingPlaneDtos) {
        HumanFace primaryFace = facesOfTask.get(0).getHumanFace();
        HumanFace secondaryFace = null;
        if (TaskUtil.isPairTask(facesOfTask)) {
            primaryFace = getPrimaryFace(facesOfTask).getHumanFace();
            secondaryFace = getSecondaryFace(facesOfTask).getHumanFace();
        }

        List<CrossSectionPlane> crossSectionPlanes = cuttingPlaneDtos.stream()
                .map(c -> {
                    CuttingPlaneFrontendState cuttingPlaneState = dto.getCuttingPlaneStates().stream()
                            .filter(s -> s.getCuttingPlaneId().equals(c.getCuttingPlaneId()))
                            .findAny().orElseThrow();
                    return new CrossSectionPlane(cuttingPlaneState.getIsVisible(), c.getDirection().getVectorDirection(),
                            cuttingPlaneState.transformCuttingPlane(c.getPlane()));
                })
                .toList();

        return CuttingPlanesUtils.computeCrossSections(crossSectionPlanes, primaryFace, secondaryFace, FEATURE_POINT_CLOSENESS);
    }

    @Override
    public byte[] exportProjectionsAsZip(CuttingPlanes2DProjectionDto dto) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());

        List<CuttingPlaneIntersection> planeIntersections = calculate2DProjection(dto).getIntersectionPointsDtos()
                .stream()
                .flatMap(s -> s.getCuttingPlanesIntersections().stream())
                .toList();

        ZipBuilder zipBuilder = new ZipBuilder(new ZipFile("config.json", exportConfig(dto)));
        for (CuttingPlaneIntersection intersection : planeIntersections) {
            CuttingPlaneFrontendState frontendState = dto.getCuttingPlaneStates().stream()
                    .filter(s -> s.getCuttingPlaneId().equals(intersection.getCuttingPlaneId()))
                    .findAny().orElseThrow();
            String pairFace = intersection.getIsPrimaryFace() ? "primary_" : "secondary_";
            String filename = isPairTask(facesOfTask) ? pairFace : "";
            filename += "face_" + frontendState.getType().getExportName() + "_" +
                    frontendState.getDirection().getExportName() + "_" + frontendState.getOrder() + ".csv";

            zipBuilder.addZipFile(filename, exportCurve(intersection.getCurveSegments()));
        }

        try {
            return zipBuilder.build();
        } catch (IOException e) {
            throw new ExportException("Error while exporting cutting planes 2D projection zip");
        }
    }

    private byte[] exportConfig(CuttingPlanes2DProjectionDto dto) {
        List<CuttingPlaneConfig> cuttingPlaneConfigs = dto.getCuttingPlaneStates().stream()
                .map(state -> new CuttingPlaneConfig(state.getType().getConfigExportLabel(), state.getDirection().getVectorDirection(),
                        state.getIsVisible(), (int) (state.getSliderValue() * 100))).toList();

        Map<String, Object> cuttingPlanesConfig = getConfigExport(dto.getSamplingStrength(),
                dto.getDirection().getVectorDirection(), cuttingPlaneConfigs);

        try {
            return new ObjectMapper().writeValueAsBytes(cuttingPlanesConfig);
        } catch (JsonProcessingException e) {
            throw new ExportException("Error while exporting config.json");
        }
    }

    private byte[] exportCurve(List<List<Point2D>> curveSegments) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (PrintWriter writer = new PrintWriter(byteArrayOutputStream)) {
            List<Point2d> points = curveSegments.stream()
                    .flatMap(Collection::stream)
                    .map(l -> new Point2d(l.getX(), l.getY()))
                    .toList();
            writeCurveExport(points, writer);
        }
        return byteArrayOutputStream.toByteArray();
    }
}

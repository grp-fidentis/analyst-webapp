package cz.fidentis.web.analyticaltask.cuttingplanes.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.vecmath.Vector3d;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@Getter
@AllArgsConstructor
public enum CuttingPlanesDirection {
    DIRECTION_VERTICAL(new Vector3d(1, 0, 0), "vertical"),
    DIRECTION_HORIZONTAL(new Vector3d(0, 1, 0), "horizontal"),
    DIRECTION_FRONT_BACK(new Vector3d(0, 0, 1), "front_back"),;

    private final Vector3d vectorDirection;
    private final String exportName;
}

package cz.fidentis.web.analyticaltask.symmetry;

import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.function.BiFunction;

/**
 * @author Ondřej Bazala
 * @since 25.10.2023
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum SymmetryMethod {
    FAST_MESH_SURFACE((samplingStrategy, ignored) -> new SymmetryConfig(SymmetryConfig.Method.OLD_MESH, samplingStrategy)),
    ROBUST_MESH_SURFACE((samplingStrategy, ignored) -> new SymmetryConfig(SymmetryConfig.Method.ROBUST_MESH, samplingStrategy)),
    ROBUST_MESH_VERTICES((samplingStrategy, ignored) -> new SymmetryConfig(SymmetryConfig.Method.ROBUST_POINT_CLOUD, samplingStrategy)),
    FEATURE_POINTS;

    private BiFunction<PointSamplingConfig, PointSamplingConfig, SymmetryConfig> symmetryEstimator;
}

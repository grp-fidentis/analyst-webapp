package cz.fidentis.web.analyticaltask.distance.dto;

import lombok.*;

import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  30.09.2023
 */
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class DistanceTaskResponseDto {

    private final Float meanDistance;

    private final Float weightedMeanDistance;

    private final List<FeaturePointWeight> featurePointWeights;

    private List<Integer> colorsOfHeatmap;

    /**
     * @author  Ondřej Bazala
     * @since  11.10.2023
     */
    @AllArgsConstructor
    @Data
    public static class FeaturePointWeight {

        private String featurePointName;
        private Double weight;
    }
}

package cz.fidentis.web.analyticaltask.curvature;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author  Ondřej Bazala
 * @since  27.11.2023
 */
@RestController
@Tag(name = "Curvature Analytical Task API", description = "API for curvature calculations")
@RequestMapping("/api/analytical-tasks/curvature")
@RequiredArgsConstructor
@Slf4j
public class CurvatureTaskApi {

    private final CurvatureTaskService curvatureTaskService;

    @PostMapping("/calculate-heatmap")
    @Operation(summary = "Calculates heatmap of curvature of face")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<CurvatureTaskResponseDto> calculateHeatmap(@Valid @RequestBody CurvatureTaskDto dto) {
        return ResponseEntity.ok(curvatureTaskService.calculateHeatmap(dto));
    }
}

package cz.fidentis.web.analyticaltask.cuttingplanes.dto;

import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesDirection;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CuttingPlanes2DProjectionDto {

    @NotNull
    private Long taskId;

    @NotNull
    private CuttingPlanesDirection direction;

    @NotEmpty
    @Size(min = 3, max = 3)
    private List<CuttingPlaneFrontendState> cuttingPlaneStates;

    @NotNull
    @Min(0)
    @Max(100)
    private Integer samplingStrength;

    @NotNull
    @Min(0)
    @Max(100)
    private Integer reducedVertices;

    private Integer canvasWidth = 400;

    private Integer canvasHeight = 400;
}

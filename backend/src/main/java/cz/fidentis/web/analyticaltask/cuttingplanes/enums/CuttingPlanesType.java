package cz.fidentis.web.analyticaltask.cuttingplanes.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@Getter
@AllArgsConstructor
public enum CuttingPlanesType {
    SYMMETRY_PLANE("from symmetry (primary face)", "symmetry"),
    BOUNDING_BOX_PLANE("from bounding box", "bounding_box");

    final String configExportLabel;
    final String exportName;
}

package cz.fidentis.web.analyticaltask.symmetry;

import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryResponseTaskDto;
import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryTaskDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
@RestController
@Tag(name = "Symmetry Analytical Task API", description = "API for calculating symmetry")
@RequestMapping("/api/analytical-tasks/symmetry")
@RequiredArgsConstructor
@Slf4j
public class SymmetryTaskApi {

    private final SymmetryTaskService symmetryService;

    @PostMapping("/calculate")
    @Operation(summary = "Calculates symmetry plane and precision")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<List<SymmetryResponseTaskDto>> calculate(@Valid @RequestBody SymmetryTaskDto dto) {
        return ResponseEntity.ok(symmetryService.calculate(dto));
    }

}

package cz.fidentis.web.analyticaltask.registration;

import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.web.analyticaltask.distance.DistanceTaskService;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto;
import cz.fidentis.web.analyticaltask.sampling.SamplingTaskService;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.*;
import cz.fidentis.web.humanface.service.HumanFaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static cz.fidentis.web.humanface.HumanFaceUtil.getPrimaryFace;
import static cz.fidentis.web.humanface.HumanFaceUtil.getSecondaryFace;

/**
 * @author  Ondřej Bazala
 * @since  29.08.2023
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class RegistrationTaskServiceImpl implements RegistrationTaskService {

    private final HumanFaceService humanFaceService;

    private final DistanceTaskService distanceTaskService;

    private final SamplingTaskService samplingTaskService;

    @Override
    @Transactional
    public HumanFaceDto execute(RegistrationTaskDto dto) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());
        HumanFaceEntity primaryFace = getPrimaryFace(facesOfTask);
        HumanFaceEntity secondaryFace = getSecondaryFace(facesOfTask);

        boolean wasExecuted = executeRegistration(dto, primaryFace, secondaryFace);

        if (!wasExecuted) {
            return null;
        }

        DistanceTaskResponseDto distances = distanceTaskService.calculateDistance(dto.getDistanceTaskDto(), facesOfTask,
                false, true);
        HumanFaceSamplesDto samples = samplingTaskService.calculate(dto.getSamplingTaskDto(), secondaryFace);

        humanFaceService.save(dto.getTaskId(), secondaryFace);

        return HumanFaceDto.builder()
                .id(secondaryFace.getId())
                .geometry(new HumanFaceGeometryDto(secondaryFace, true))
                .featurePoints(secondaryFace.getHumanFace().hasLandmarks() ?
                        new HumanFaceFeaturePointsDto(secondaryFace) : null)
                .symmetryPlane(secondaryFace.getHumanFace().hasSymmetryPlane() ?
                        new HumanFaceSymmetryPlaneDto(secondaryFace) : null)
                .distances(distances)
                .samples(samples)
                .build();
    }

    private boolean executeRegistration(RegistrationTaskDto dto, HumanFaceEntity primaryFace, HumanFaceEntity secondaryFace) {
        return switch (dto.getRegistrationMethod()) {
            case ICP -> {
                int strength = dto.getSamplingTaskDto().getSamplingStrength();
                PointSamplingConfig samplingStrategy = dto.getSamplingTaskDto().getSamplingStrategy()
                        .getPointSampling().apply(strength);

                FaceRegistrationServices.alignMeshes(
                        secondaryFace.getHumanFace(),
                        new IcpConfig(
                            primaryFace.getHumanFace().getMeshModel(),
                            dto.getIcpMaxIter(),
                            dto.getScale(),
                            dto.getIcpMinError(),
                            samplingStrategy,
                            dto.getIcpAutoCrop() ? 0 : -1));
                yield true;
            }
            case FEATURE_POINTS -> FaceRegistrationServices.alignFeaturePoints(
                    primaryFace.getHumanFace(),
                    secondaryFace.getHumanFace(),
                    dto.getScale()) != null;
            case SYMMETRY -> {
                FaceRegistrationServices.alignSymmetryPlanes(
                        primaryFace.getHumanFace(),
                        secondaryFace.getHumanFace(),
                        false);
                yield true;
            }
        };
    }
}

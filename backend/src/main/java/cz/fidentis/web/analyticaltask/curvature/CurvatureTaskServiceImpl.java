package cz.fidentis.web.analyticaltask.curvature;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.service.HumanFaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.fidentis.web.humanface.HumanFaceUtil.getValuesFromHeatmap3D;

/**
 * @author  Ondřej Bazala
 * @since  27.11.2023
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CurvatureTaskServiceImpl implements CurvatureTaskService {

    private final HumanFaceService humanFaceService;

    @Override
    public CurvatureTaskResponseDto calculateHeatmap(CurvatureTaskDto dto) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());
        HumanFaceEntity humanFaceEntity = facesOfTask.get(0);
        HumanFace face = humanFaceEntity.getHumanFace();

        FaceStateServices.updateCurvature(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);

        Map<MeshFacet, List<Double>> heatmapValues = new HashMap<>();
        for (MeshFacet facet : face.getMeshModel().getFacets()) {
            List<Double> values = facet.getVertices().stream()
                    .map(point -> dto.getCurvatureMethod().getValue().apply(point))
                    .toList();
            heatmapValues.put(facet, values);
        }
        HeatMap3D heatMap = new HeatMap3D(heatmapValues, null);

        return new CurvatureTaskResponseDto(humanFaceEntity.getId(), getValuesFromHeatmap3D(face, heatMap));
    }
}

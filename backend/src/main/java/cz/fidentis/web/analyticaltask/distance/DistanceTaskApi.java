package cz.fidentis.web.analyticaltask.distance;

import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskDto;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto;
import cz.fidentis.web.analyticaltask.distance.enums.ExportType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

/**
 * @author  Ondřej Bazala
 * @since  30.09.2023
 */
@RestController
@Tag(name = "Distance Analytical Task API", description = "API for calculating distance")
@RequestMapping("/api/analytical-tasks/distance")
@RequiredArgsConstructor
@Slf4j
public class DistanceTaskApi {

    private final DistanceTaskService distanceTaskService;

    @PostMapping("/calculate-heatmap")
    @Operation(summary = "Calculates heatmap of secondary face")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<DistanceTaskResponseDto> calculateHeatmap(@Valid @RequestBody DistanceTaskDto dto) {
        return ResponseEntity.ok(distanceTaskService.calculateHeatmap(dto));
    }

    @PostMapping("/calculate-distance")
    @Operation(summary = "Calculates distance of secondary and primary face")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<DistanceTaskResponseDto> calculateDistance(@Valid @RequestBody DistanceTaskDto dto) {
        return ResponseEntity.ok(distanceTaskService.calculateDistance(dto));
    }

    @PostMapping(value = "/export-distance", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Operation(summary = "Export distance as CSV file")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<byte[]> exportDistance(@Valid @RequestBody DistanceTaskDto dto) {
        String fileName = "task_" + dto.getTaskId() + "_";
        fileName += dto.getExportType() == ExportType.DISTANCE ? "distance" : "weighted_distance";
        fileName += ".csv";

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
                .body(distanceTaskService.exportDistance(dto));
    }
}

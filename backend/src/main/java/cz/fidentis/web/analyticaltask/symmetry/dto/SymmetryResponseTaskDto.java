package cz.fidentis.web.analyticaltask.symmetry.dto;

import cz.fidentis.web.humanface.dto.HumanFaceSymmetryPlaneDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
@Getter
@Setter
@AllArgsConstructor
@Builder
public class SymmetryResponseTaskDto {

    private Long humanFaceId;

    private HumanFaceSymmetryPlaneDto symmetryPlane;
}

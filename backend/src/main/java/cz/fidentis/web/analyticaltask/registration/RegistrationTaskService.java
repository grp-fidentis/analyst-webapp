package cz.fidentis.web.analyticaltask.registration;

import cz.fidentis.web.humanface.dto.HumanFaceDto;

/**
 * @author  Ondřej Bazala
 * @since  29.08.2023
 */
public interface RegistrationTaskService {

    /**
     * Method that executes registration on primary and secondary face of task
     *
     * @param dto Object containing IDs of primary and secondary human faces
     *            and various parameters for configuring calculation
     * @return {@code HumanFaceGeometry} geometry dto object registered on primary face or null if
     *          registration was not executed
     */
    HumanFaceDto execute(RegistrationTaskDto dto);
}

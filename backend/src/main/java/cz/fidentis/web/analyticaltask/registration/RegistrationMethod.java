package cz.fidentis.web.analyticaltask.registration;

/**
 * @author Ondřej Bazala
 * @since 26.09.2023
 */
public enum RegistrationMethod {
    ICP,
    FEATURE_POINTS,
    SYMMETRY
}

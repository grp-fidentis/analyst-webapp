package cz.fidentis.web.analyticaltask.distance;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.engines.face.FaceDistanceServices;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskDto;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto.FeaturePointWeight;
import cz.fidentis.web.analyticaltask.distance.enums.ExportType;
import cz.fidentis.web.analyticaltask.distance.enums.HeatmapRenderMode;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.update.HumanFaceUpdateDto;
import cz.fidentis.web.humanface.dto.update.TransformationDto;
import cz.fidentis.web.humanface.service.HumanFaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cz.fidentis.web.humanface.HumanFaceUtil.*;

/**
 * @author  Ondřej Bazala
 * @since  30.09.2023
 */
@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class DistanceTaskServiceImpl implements DistanceTaskService {

    private final HumanFaceService humanFaceService;

    @Override
    public DistanceTaskResponseDto calculateHeatmap(DistanceTaskDto dto) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());

        return calculateDistance(dto, facesOfTask, true, false);
    }

    @Override
    public byte[] exportDistance(DistanceTaskDto dto) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());

        MeshDistances meshDistances = getDistance(dto, facesOfTask, false);

        List<Double> values = dto.getExportType() == ExportType.WEIGHTED_DISTANCE
                ? meshDistances.streamOfWeightedDistances().boxed().toList()
                : meshDistances.streamOfRawDistances().boxed().toList();

        StringBuilder builder = new StringBuilder();
        for (double value: values) {
            builder.append(roundToFourDigits(value));
            builder.append("\n");
        }
        return builder.toString().getBytes();
    }

    @Override
    public DistanceTaskResponseDto calculateDistance(DistanceTaskDto dto) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());
        HumanFaceEntity secondaryFaceEntity = getSecondaryFace(facesOfTask);

        HumanFaceUpdateDto.assignDataToHumanFace(secondaryFaceEntity, dto.getTransformationDto());

        return calculateDistance(dto, facesOfTask, false, true);
    }

    @Override
    public DistanceTaskResponseDto calculateDistance(DistanceTaskDto dto, List<HumanFaceEntity> facesOfTask,
                                                     boolean withHeatmap, boolean recomputeDistances) {
        HumanFace secondaryFace = getSecondaryFace(facesOfTask).getHumanFace();

        MeshDistances meshDistances = getDistance(dto, facesOfTask, recomputeDistances);

        float meanDistance = (float) meshDistances.getDistanceStats().getAverage();
        float weightedMeanDistance = (float) meshDistances.getWeightedStats().getAverage();

        List<FeaturePointWeight> featurePointWeights = null;
        List<Integer> colorsOfHeatmap = null;
        if (withHeatmap) {
            var spheresMask = getLandmarkRadii(dto, secondaryFace);
            featurePointWeights = getFeaturePointWeights(meshDistances, spheresMask);
            colorsOfHeatmap = calculateColorsOfHeatmap(dto, secondaryFace, meshDistances);
        }

        return new DistanceTaskResponseDto(meanDistance, weightedMeanDistance, featurePointWeights, colorsOfHeatmap);
    }

    private MeshDistances getDistance(DistanceTaskDto dto, List<HumanFaceEntity> facesOfTask,
                                      boolean recomputeDistances) {
        HumanFaceEntity primaryFaceEntity = getPrimaryFace(facesOfTask);
        HumanFaceEntity secondaryFaceEntity = getSecondaryFace(facesOfTask);
        HumanFace primaryFace = primaryFaceEntity.getHumanFace();
        HumanFace secondaryFace = secondaryFaceEntity.getHumanFace();

        boolean missingOcTree = primaryFace.getOctree() == null;
        boolean missingKdTree = primaryFace.getKdTree() == null;

        MeshDistances meshDistances = secondaryFaceEntity.getHelperHumanFace().getMeshDistances();

        recomputeDistances = recomputeDistances || dto.getRecomputeDistances() ||
                distanceIsNotCalculated(secondaryFaceEntity);

        if (recomputeDistances) {
            meshDistances = FaceDistanceServices.calculateDistance(
                    primaryFace,
                    secondaryFace,
                    dto.getUseRelativeDistance(),
                    dto.getCrop(),
                    dto.getDistanceStrategy(),
                    null);
        }

        var spheresMask = getLandmarkRadii(dto, secondaryFace);
        FaceDistanceServices.setPrioritySphereMask(meshDistances, spheresMask);

        if ((missingOcTree && primaryFace.getOctree() != null) || (missingKdTree && primaryFace.getKdTree() != null)) {
            humanFaceService.save(dto.getTaskId(), primaryFaceEntity);
        }

        if (recomputeDistances) {
            if (isTransformation(dto.getTransformationDto())) {
                TransformationDto reversedDto = TransformationDto.getReversed(dto.getTransformationDto());
                HumanFaceUpdateDto.assignDataToHumanFace(secondaryFaceEntity, reversedDto);
            }

            secondaryFaceEntity.getHelperHumanFace().setMeshDistances(meshDistances);
            humanFaceService.save(dto.getTaskId(), secondaryFaceEntity);
        }

        return meshDistances;
    }

    private boolean distanceIsNotCalculated(HumanFaceEntity humanFaceEntity) {
        MeshDistances meshDistances = humanFaceEntity.getHelperHumanFace().getMeshDistances();

        if (meshDistances == null) {
            return true;
        }

        List<MeshFacet> facets = humanFaceEntity.getHumanFace().getMeshModel().getFacets();
        return facets.stream().anyMatch(f -> meshDistances.getFacetMeasurement(f) == null);
    }

    private Map<Landmark, Double> getLandmarkRadii(DistanceTaskDto dto, HumanFace secondaryFace) {
        return dto.getFeaturePointSizes().stream()
                .collect(Collectors.toMap(
                        f -> secondaryFace.getAllLandmarks().stream()
                                .filter(fp -> fp.getName().equals(f.getName()))
                                .findAny().orElse(null),
                        DistanceTaskDto.FeaturePointSize::getSize));
    }

    private List<FeaturePointWeight> getFeaturePointWeights(
            MeshDistances meshDistances, Map<Landmark, Double> featurePointsRadii) {

        var featurePointWeights = FaceDistanceServices.getFeaturePointWeights(meshDistances, featurePointsRadii);
        return featurePointWeights.entrySet().stream()
                .map(e -> new FeaturePointWeight(e.getKey().getName(), e.getValue()))
                .toList();
    }

    private List<Integer> calculateColorsOfHeatmap(DistanceTaskDto dto, HumanFace secondaryFace,
                                                   MeshDistances meshDistances) {

        var weights = dto.getHeatmapRenderMode() == HeatmapRenderMode.WEIGHTED ? meshDistances.weightsAsMap() : null;
        HeatMap3D heatMap = new HeatMap3D(meshDistances.distancesAsMap(), weights);

        return getValuesFromHeatmap3D(secondaryFace, heatMap);
    }
}

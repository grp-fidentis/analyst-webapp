package cz.fidentis.web.analyticaltask.sampling;

import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.HumanFaceSamplesDto;

/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
public interface SamplingTaskService {

    /**
     * {@code face} defaults to only face / secondary face of task fetched from database by {@code taskId}
     *
     * @see #calculate(SamplingTaskDto, HumanFaceEntity)
     */
    HumanFaceSamplesDto calculate(SamplingTaskDto dto);

    /**
     * Method that gets samples of secondary face (or only face) by {@code PointSamplingStrategy}
     *
     * @param dto with strength and strategy of sampling
     * @param face to be sampled
     * @return {@code HumanFaceSamplesDto} samples dto object
     */
    HumanFaceSamplesDto calculate(SamplingTaskDto dto, HumanFaceEntity face);
}

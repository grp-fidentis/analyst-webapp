package cz.fidentis.web.analyticaltask.distance.enums;

/**
 * @author Ondřej Bazala
 * @since 26.09.2023
 */
public enum HeatmapRenderMode {
    STANDARD,
    WEIGHTED,
    NONE
}

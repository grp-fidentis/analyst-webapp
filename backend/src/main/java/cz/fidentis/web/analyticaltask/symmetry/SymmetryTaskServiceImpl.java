package cz.fidentis.web.analyticaltask.symmetry;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.web.analyticaltask.sampling.SamplingTaskDto;
import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryResponseTaskDto;
import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryTaskDto;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.HumanFaceSymmetryPlaneDto;
import cz.fidentis.web.humanface.service.HumanFaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static cz.fidentis.web.humanface.HumanFaceUtil.getPrimaryFace;
import static cz.fidentis.web.humanface.HumanFaceUtil.getSecondaryFace;
import static cz.fidentis.web.task.TaskUtil.isPairTask;

/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SymmetryTaskServiceImpl implements SymmetryTaskService {

    private final HumanFaceService humanFaceService;

    @Override
    @Transactional
    public List<SymmetryResponseTaskDto> calculate(SymmetryTaskDto dto) {
        List<HumanFaceEntity> facesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(dto.getTaskId());

        List<SymmetryResponseTaskDto> result = new ArrayList<>();
        if (isPairTask(facesOfTask)) {
            result.add(calculate(dto, getPrimaryFace(facesOfTask)));
            result.add(calculate(dto, getSecondaryFace(facesOfTask)));
        } else {
            result.add(calculate(dto, facesOfTask.getFirst()));
        }

        return result;
    }

    private SymmetryResponseTaskDto calculate(SymmetryTaskDto dto, HumanFaceEntity humanFaceEntity) {
        HumanFace face = humanFaceEntity.getHumanFace();

        if (dto.getSymmetryMethod() == SymmetryMethod.FEATURE_POINTS) {
            FaceStateServices.updateSymmetryPlane(face, FaceStateServices.Mode.COMPUTE_ALWAYS, null);
        } else {
            recomputeFromMesh(dto, face);
        }

        Double precision = face.hasSymmetryPlane() ? FaceStateServices.calculateSymmetryPlanePrecision(face, false) : null;
        humanFaceEntity.getHelperHumanFace().setSymmetryPlanePrecision(precision);

        humanFaceService.save(dto.getTaskId(), humanFaceEntity);

        return SymmetryResponseTaskDto.builder()
                .humanFaceId(humanFaceEntity.getId())
                .symmetryPlane(new HumanFaceSymmetryPlaneDto(humanFaceEntity))
                .build();
    }

    private void recomputeFromMesh(SymmetryTaskDto dto, HumanFace face) {
        SamplingTaskDto searchSamplingTask = dto.getCandidateSearchSampling();
        SamplingTaskDto pruningSamplingTask = dto.getCandidatePruningSampling();

        PointSamplingConfig searchSampling = searchSamplingTask.getSamplingStrategy()
                .getPointSampling().apply(searchSamplingTask.getSamplingStrength());
        PointSamplingConfig pruningSampling = pruningSamplingTask.getSamplingStrategy()
                .getPointSampling().apply(pruningSamplingTask.getSamplingStrength());

        SymmetryConfig symmetryStrategy = dto.getSymmetryMethod()
                .getSymmetryEstimator().apply(searchSampling, pruningSampling);

        FaceStateServices.updateCurvature(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        face.setSymmetryPlane(SymmetryServices.estimate(face.getMeshModel(), symmetryStrategy));
    }
}

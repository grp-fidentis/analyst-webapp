package cz.fidentis.web.analyticaltask.registration;

import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskDto;
import cz.fidentis.web.analyticaltask.sampling.SamplingTaskDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;

/**
 * @author  Ondřej Bazala
 * @since  29.08.2023
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationTaskDto {

    @NotNull
    private Long taskId;

    @NotNull
    private RegistrationMethod registrationMethod;

    @NotNull
    private Boolean scale = false;

    @NotNull
    @Positive
    private Integer icpMaxIter = 100;

    @NotNull
    @PositiveOrZero
    private Double icpMinError = 0.05;

    @NotNull
    private Boolean icpAutoCrop = true;

    @NotNull
    private SamplingTaskDto samplingTaskDto;

    @NotNull
    @Valid
    private DistanceTaskDto distanceTaskDto;
}

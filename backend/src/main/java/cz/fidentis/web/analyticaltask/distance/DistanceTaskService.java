package cz.fidentis.web.analyticaltask.distance;

import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskDto;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto;
import cz.fidentis.web.humanface.HumanFaceEntity;

import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  30.09.2023
 */
public interface DistanceTaskService {

    /**
     * Method that calculates heatmap of secondary face
     *
     * @param dto Object containing parameters for heatmap calculation
     * @return {@code DistanceTaskResponseDto} contains (normal/weighted) mean distance with heatMap colors
     */
    DistanceTaskResponseDto calculateHeatmap(DistanceTaskDto dto);

    /**
     * Method that calculates distance of secondary and primary face
     *
     * @param dto Object containing taskId and parameters needed for distance calculations
     * @param facesOfTask {@code HumanFaceEntities} of task
     * @param withHeatmap if true, calculates colors for vertices by distances
     * @param recomputeDistances if true, enforces re-computation of distances of faces
     * @return {@code DistanceTaskResponseDto} contains (normal/weighted) mean distance
     */
    DistanceTaskResponseDto calculateDistance(DistanceTaskDto dto, List<HumanFaceEntity> facesOfTask,
                                              boolean withHeatmap, boolean recomputeDistances);

    /**
     *{@code facesOfTask} defaults to faces of task fetched from database by {@code taskId}
     *
     * @see #calculateDistance(DistanceTaskDto, List, boolean, boolean)
     */
    DistanceTaskResponseDto calculateDistance(DistanceTaskDto dto);

    /**
     * Export distance as CSV file
     *
     * @param dto Object containing taskId and parameters needed for distance calculations
     * @return csv file of distances
     */
    byte[] exportDistance(DistanceTaskDto dto);
}

package cz.fidentis.web.analyticaltask.symmetry.dto;

import cz.fidentis.web.analyticaltask.sampling.SamplingTaskDto;
import cz.fidentis.web.analyticaltask.symmetry.SymmetryMethod;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.constraints.NotNull;

/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SymmetryTaskDto {

    @NotNull
    private Long taskId;

    @NotNull
    private SymmetryMethod symmetryMethod = SymmetryMethod.FAST_MESH_SURFACE;

    @NotNull
    private SamplingTaskDto candidateSearchSampling;

    @NotNull
    private SamplingTaskDto candidatePruningSampling;
}

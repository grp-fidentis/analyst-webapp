package cz.fidentis.web.analyticaltask.cuttingplanes.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Ondřej Bazala
 * @since 11.11.2023
 */
@Getter
@Setter
@AllArgsConstructor
public class HausdorffDistance {

    private Integer order;

    private Double distance;
}
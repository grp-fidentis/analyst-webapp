package cz.fidentis.web.analyticaltask.cuttingplanes.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesDirection;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesType;
import cz.fidentis.web.humanface.HumanFaceEntity;
import lombok.Getter;
import lombok.Setter;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@Getter
@Setter
public class CuttingPlaneDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String cuttingPlaneId = UUID.randomUUID().toString();

    private CuttingPlanesType type;

    private CuttingPlanesDirection direction;

    private Integer order;

    private List<Float> positions = new ArrayList<>();

    private List<Float> normals = new ArrayList<>();

    @JsonIgnore
    private Plane plane;

    public CuttingPlaneDto(HumanFaceEntity humanFaceEntity, Plane plane, CuttingPlanesType type,
                           CuttingPlanesDirection direction, Integer order) {
        FaceStateServices.updateBoundingBox(humanFaceEntity.getHumanFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        MeshFacet mesh = plane.getMesh(humanFaceEntity.getHumanFace().getBoundingBox());

        for (int v = 0; v < mesh.getCornerTable().getSize(); v++) {
            MeshPoint meshPoint = mesh.getVertices().get(mesh.getCornerTable().getRow(v).getVertexIndex());

            Point3d vert = meshPoint.getPosition();
            positions.add((float) vert.x);
            positions.add((float) vert.y);
            positions.add((float) vert.z);

            Vector3d normal = meshPoint.getNormal();
            normals.add((float) normal.x);
            normals.add((float) normal.y);
            normals.add((float) normal.z);
        }

        this.type = type;
        this.direction = direction;
        this.order = order;
        this.plane = plane;
    }
}

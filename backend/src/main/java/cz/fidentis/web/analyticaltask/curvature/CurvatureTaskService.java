package cz.fidentis.web.analyticaltask.curvature;

/**
 * @author  Ondřej Bazala
 * @since  27.11.2023
 */
public interface CurvatureTaskService {

    /**
     * Method that calculates heatmap of face
     *
     * @param dto Parameters for heatmap calculation
     *
     * @return {@link CurvatureTaskResponseDto} with face id and heatmap integers
     */
    CurvatureTaskResponseDto calculateHeatmap(CurvatureTaskDto dto);
}

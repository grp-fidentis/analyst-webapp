package cz.fidentis.web.analyticaltask.registration;

import cz.fidentis.web.humanface.dto.HumanFaceDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

/**
 * @author  Ondřej Bazala
 * @since  29.08.2023
 */
@RestController
@Tag(name = "Registration Analytical Task API", description = "API for executing registration")
@RequestMapping("/api/analytical-tasks/registration")
@RequiredArgsConstructor
@Slf4j
public class RegistrationTaskApi {

    private final RegistrationTaskService registrationService;

    @PostMapping("/execute")
    @Operation(summary = "Executes registration of two faces")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<HumanFaceDto> execute(@Valid @RequestBody RegistrationTaskDto dto) {
        return ResponseEntity.ok(registrationService.execute(dto));
    }
}

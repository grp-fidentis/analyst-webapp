package cz.fidentis.web.analyticaltask.cuttingplanes.dto;

import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.analyst.math.Quaternion;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesDirection;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesType;
import cz.fidentis.web.humanface.dto.Point;
import cz.fidentis.web.humanface.dto.update.TransformationDto;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.vecmath.Vector3d;

/**
 * @author  Ondřej Bazala
 * @since  11.11.2023
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CuttingPlaneFrontendState {

    @NotBlank
    private String cuttingPlaneId;

    @NotNull
    private Boolean isVisible;

    @NotNull
    private Double sliderValue;

    @NotNull
    private CuttingPlanesType type;

    @NotNull
    private CuttingPlanesDirection direction;

    @NotNull
    private Integer order;

    @NotNull
    private TransformationDto transformationDto;

    public Plane transformCuttingPlane(Plane plane) {
        Point rotation = transformationDto.getRotation();
        Point translation = transformationDto.getTranslation();

        Quaternion rotationQuaternion = new Quaternion(rotation.getX(), rotation.getY(), rotation.getZ(), 1.0);
        Vector3d translationVector = new Vector3d(translation.toPoint3d());

        return FaceRegistrationServices.transformPlane(plane, rotationQuaternion, translationVector, 1);
    }
}

package cz.fidentis.web.analyticaltask.curvature;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author  Ondřej Bazala
 * @since  27.11.2023
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CurvatureTaskResponseDto {

    private Long faceId;

    private List<Integer> heatmap;
}

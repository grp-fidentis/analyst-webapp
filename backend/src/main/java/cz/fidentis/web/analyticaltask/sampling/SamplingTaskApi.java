package cz.fidentis.web.analyticaltask.sampling;

import cz.fidentis.web.humanface.dto.HumanFaceSamplesDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

/**
 * @author  Ondřej Bazala
 * @since  25.10.2023
 */
@RestController
@Tag(name = "Sampling Analytical Task API", description = "API for sampling")
@RequestMapping("/api/analytical-tasks/sampling")
@RequiredArgsConstructor
@Slf4j
public class SamplingTaskApi {

    private final SamplingTaskService samplingTaskService;

    @PostMapping("/calculate")
    @Operation(summary = "Gets samples of secondary face by sampling strategy")
    @PreAuthorize("isOwner(#dto.taskId, 'TASK')")
    public ResponseEntity<HumanFaceSamplesDto> calculate(@Valid @RequestBody SamplingTaskDto dto) {
        return ResponseEntity.ok(samplingTaskService.calculate(dto));
    }
}

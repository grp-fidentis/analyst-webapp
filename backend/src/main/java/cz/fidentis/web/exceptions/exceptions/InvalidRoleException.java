package cz.fidentis.web.exceptions.exceptions;

/**
 *
 * Exception.
 *
 * @author Adam Majzlik
 */
public class InvalidRoleException extends RuntimeException {

    public InvalidRoleException(String msg) {
        super(msg);
    }

}

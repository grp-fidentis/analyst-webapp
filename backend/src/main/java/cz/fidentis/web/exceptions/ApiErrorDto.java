package cz.fidentis.web.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

/**
 * @author Petr Hendrych
 * @since 19.02.2023
 */
@Data
@AllArgsConstructor
public class ApiErrorDto {

    public ApiErrorDto(Exception exception, HttpStatus status, String message) {
        this.timestamp = LocalDateTime.now();
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.exception = exception.getClass().getCanonicalName();
        this.message = message;
    }

    private LocalDateTime timestamp;
    private Integer status;
    private String error;
    private String exception;
    private String message;
}

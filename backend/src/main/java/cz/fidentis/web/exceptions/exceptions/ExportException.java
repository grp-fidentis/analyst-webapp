package cz.fidentis.web.exceptions.exceptions;

/**
 * @author Ondřej Bazala
 * @since 16.09.2023
 */
public class ExportException extends RuntimeException {

    public ExportException(String msg) {
        super(msg);
    }
}

package cz.fidentis.web.exceptions.exceptions;

/**
 * @author Petr Hendrych
 * @since 19.02.2023
 */
public class EntityNotFoundException extends jakarta.persistence.EntityNotFoundException {

    public EntityNotFoundException(String message) {
        super(message);
    }
}

package cz.fidentis.web.exceptions;

import com.auth0.jwt.exceptions.TokenExpiredException;
import cz.fidentis.web.auth.exception.EntityAlreadyExistsException;
import cz.fidentis.web.auth.exception.TokenException;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.fileupload.exception.InvalidAdditionalFileTypeException;
import cz.fidentis.web.fileupload.exception.UserHasNoPermissionException;
import cz.fidentis.web.user.PasswordValidationException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import jakarta.validation.ConstraintViolation;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;


/**
 * @author Petr Hendrych
 * Since: 19.02.2023
 */
@RestControllerAdvice(basePackages = "cz.fidentis")
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final Map<Class<? extends Exception>, HttpStatus> statusMap;

    public GlobalExceptionHandler() {
        statusMap = new HashMap<>();
        statusMap.put(EntityNotFoundException.class, NOT_FOUND);
        statusMap.put(IllegalArgumentException.class, BAD_REQUEST);
        statusMap.put(UserHasNoPermissionException.class, FORBIDDEN);
        statusMap.put(EntityAlreadyExistsException.class, CONFLICT);
        statusMap.put(TokenException.class, BAD_REQUEST);
        statusMap.put(TokenExpiredException.class, UNAUTHORIZED);
        statusMap.put(InvalidAdditionalFileTypeException.class, BAD_REQUEST);
        statusMap.put(PasswordValidationException.class, BAD_REQUEST);

        statusMap.put(ConstraintViolationException.class, CONFLICT);

        statusMap.put(DataIntegrityViolationException.class, CONFLICT);

        statusMap.put(AccessDeniedException.class, FORBIDDEN);

        statusMap.put(jakarta.validation.ConstraintViolationException.class, BAD_REQUEST);
    }

    @ExceptionHandler({
            EntityNotFoundException.class,
            IllegalArgumentException.class,
            UserHasNoPermissionException.class,
            EntityAlreadyExistsException.class,
            TokenException.class,
            TokenExpiredException.class,
            InvalidAdditionalFileTypeException.class,
            PasswordValidationException.class
    })
    public ResponseEntity<ApiErrorDto> handleExceptions(Exception ex) {
        return createErrorResponseEntity(ex);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiErrorDto> handleDatabaseConstraintViolationException(ConstraintViolationException ex) {
        String customMessage = "Violation of set up constraint in database";
        if (ex.getConstraintName() != null) {
            customMessage += ": Error in " + ex.getConstraintName();
        }
        return createErrorResponseEntity(ex, customMessage);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ApiErrorDto> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        String customMessage = "Violation of set up constraint in database";
        Throwable rootCause = ex.getRootCause();
        if (rootCause != null) {
            customMessage += ": " + rootCause.getMessage();
        }
        return createErrorResponseEntity(ex, customMessage);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ApiErrorDto> handleAccessDeniedException(AccessDeniedException ex) {
        return createErrorResponseEntity(ex, "You don't have permissions to execute this action");
    }

    @ExceptionHandler(jakarta.validation.ConstraintViolationException.class)
    public ResponseEntity<ApiErrorDto> handleConstraintViolationException(jakarta.validation.ConstraintViolationException ex) {
        return createErrorResponseEntity(ex, getConstraintViolationMessage(ex));
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatusCode statusCode, WebRequest request) {
        var status = HttpStatus.valueOf(statusCode.value());
        if (INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, RequestAttributes.SCOPE_REQUEST);
            return new ResponseEntity<>(body, headers, status);
        }

        String message = ex.getMessage();
        if (ex instanceof MethodArgumentNotValidException convertedEx) {
            message = getMethodArgumentNotValidMessage(convertedEx);
        }

        return new ResponseEntity<>(new ApiErrorDto(ex, status, message), headers, status);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorDto> handleNotRecognizedException(Exception ex) {
        log.error("Not specified exception in GlobalExceptionHandler: ", ex);
        return createErrorResponseEntity(ex);
    }

    private ResponseEntity<ApiErrorDto> createErrorResponseEntity(Exception ex) {
        return createErrorResponseEntity(ex, ex.getMessage());
    }

    private ResponseEntity<ApiErrorDto> createErrorResponseEntity(Exception ex, String customMessage) {
        HttpStatus status = statusMap.getOrDefault(ex.getClass(), INTERNAL_SERVER_ERROR);
        ApiErrorDto apiErrorDto = new ApiErrorDto(ex, status, customMessage);
        return new ResponseEntity<>(apiErrorDto, status);
    }

    private String getConstraintViolationMessage(jakarta.validation.ConstraintViolationException ex) {
        StringBuilder message = new StringBuilder();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            String[] pathParts = violation.getPropertyPath().toString().split("\\.");
            message.append(pathParts[pathParts.length - 1])
                    .append(": ")
                    .append(violation.getMessage())
                    .append("\n");
        }
        return message.toString();
    }

    private String getMethodArgumentNotValidMessage(MethodArgumentNotValidException ex) {
        StringBuilder message = new StringBuilder();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            message.append(fieldError.getField())
                    .append(": ")
                    .append(fieldError.getDefaultMessage())
                    .append("\n");
        }
        return message.toString();
    }
}

package cz.fidentis.web.auth.passwordresettoken;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fidentis.web.user.User;
import cz.fidentis.web.base.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Petr Hendrych
 * @since 16.03.2023
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "password_reset_token")
public class PasswordResetToken extends BaseEntity {

    private String token;

    private LocalDateTime expiryDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "app_user_id", referencedColumnName = "id")
    private User user;

    public PasswordResetToken(String token, LocalDateTime expiryDate, User user) {
        this.token = token;
        this.expiryDate = expiryDate;
        this.user = user;
    }
}

package cz.fidentis.web.auth.exception;

import org.springframework.mail.MailException;

/**
 * @author Petr Hendrych
 * @since 26.03.2023
 */
public class EmailException extends MailException {

    public EmailException(String msg) {
        super(msg);
    }
}

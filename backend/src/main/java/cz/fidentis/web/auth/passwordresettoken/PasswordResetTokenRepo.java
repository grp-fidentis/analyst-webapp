package cz.fidentis.web.auth.passwordresettoken;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Petr Hendrych
 * @since 16.03.2023
 */
public interface PasswordResetTokenRepo extends JpaRepository<PasswordResetToken, Long> {

    Optional<PasswordResetToken> findByToken(String token);
}

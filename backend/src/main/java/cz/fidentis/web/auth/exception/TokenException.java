package cz.fidentis.web.auth.exception;

/**
 * @author Ondřej Bazala
 * @since 16.09.2023
 */
public class TokenException extends RuntimeException {

    public TokenException(String msg) {
        super(msg);
    }
}

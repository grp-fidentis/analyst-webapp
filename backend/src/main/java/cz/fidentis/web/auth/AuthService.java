package cz.fidentis.web.auth;

import cz.fidentis.web.user.User;
import cz.fidentis.web.user.dto.UserDetailDto;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.mail.MailException;

import java.util.Map;

/**
 * @author Petr Hendrych
 * @since 07.12.2022
 */
public interface AuthService {

    /**
     * Service to handle registering new users. Some validations are proceeded on frontend
     * (should implement also on BE with @Valid etc. annotations for handling registration
     * outside UI form). Users can create only admins. After creating new user email will
     * be sent to the new user with unique token to create his password. Token lasts 15
     * minutes after that time he has to use 'Forget password' on login page of application
     * to get new token.
     *
     * @param url string representation of host URL
     */
    void register(String url, UserDetailDto dto) throws MailException;

    /**
     * Service responding to send user new access token when
     *
     * @param authorizationHeader header with user`s valid refresh token. Token should be
     *                            prefixed with 'Bearer ' to say that we are using JWT tokens
     * @param requestUrl url which was accessed to refresh token
     *
     * @return Return JSON object with 2 attributes: 'Access-Token' which should contain
     * new valid access token for user and 'Refresh-Token' which should be same token
     * with which user requested this
     * @throws Exception When user isn't found exception is thrown (should implement custom
     * ExceptionHandler later)
     */
    Map<String, String> refreshUserToken(String authorizationHeader, String requestUrl) throws Exception;


    /**
     * Service that handles sending email with unique token to reset user password
     *
     * @param url string representation of host URL
     * @param email email of {@code User} where will be sent token
     */
    void forgotPassword(String url, String email);

    /**
     * Service that sets new password and check if token is valid and not expired
     *
     * @param password New password value
     * @param token UUID string token sent in email
     */
    void resetPassword(String password, String token);

    /**
     * Service that creates user after first OAuth2/OIDC login or updates user after other logins
     *
     * @param dto User to be created or updated
     */
    User loginOAuth2User(UserDetailDto dto);

    /**
     * Service that deletes cookies when user logs out of the application.
     *
     * @param request request made by user when logging out
     * @param response response which returns to the client
     */
    void logout(HttpServletRequest request, HttpServletResponse response);
}

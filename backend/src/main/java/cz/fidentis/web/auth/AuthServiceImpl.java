package cz.fidentis.web.auth;

import com.auth0.jwt.exceptions.TokenExpiredException;
import cz.fidentis.web.auth.exception.EmailException;
import cz.fidentis.web.auth.exception.EntityAlreadyExistsException;
import cz.fidentis.web.auth.exception.TokenException;
import cz.fidentis.web.auth.passwordresettoken.PasswordResetToken;
import cz.fidentis.web.auth.passwordresettoken.PasswordResetTokenRepo;
import cz.fidentis.web.auth.passwordresettoken.PasswordResetTokenService;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.mail.EmailBuilder;
import cz.fidentis.web.mail.EmailService;
import cz.fidentis.web.role.Role;
import cz.fidentis.web.role.RoleRepo;
import cz.fidentis.web.role.Roles;
import cz.fidentis.web.security.jwt.JwtConfig;
import cz.fidentis.web.security.jwt.JwtUtils;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.UserRepo;
import cz.fidentis.web.user.dto.UserDetailDto;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.mail.MailException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Petr Hendrych
 * @since 07.12.2022
 */
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class AuthServiceImpl implements AuthService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final JwtUtils jwtUtils;
    private final JwtConfig jwtConfig;
    private final EmailService emailService;
    private final PasswordResetTokenService passwordResetTokenService;
    private final EmailBuilder emailBuilder;
    private final PasswordResetTokenRepo passwordResetTokenRepo;

    @Override
    public Map<String, String> refreshUserToken(String authorizationHeader, String requestUrl) {
        if (authorizationHeader == null || !authorizationHeader.startsWith(jwtConfig.getTokenPrefix())) {
            throw new TokenException("Refresh token is missing");
        }

        try {
            String refreshToken = authorizationHeader.substring(jwtConfig.getTokenPrefix().length());
            String username = jwtUtils.getSubjectFromJwt(refreshToken);

            User user = userRepo.findByUsername(username)
                    .orElseThrow(() -> new NoSuchElementException("User: %s does not exist".formatted(username)));
            String accessToken = jwtUtils.generateToken(user, requestUrl, true);

            Map<String, String> tokens = new HashMap<>();
            tokens.put("Access-Token", accessToken);
            tokens.put("Refresh-Token", refreshToken);

            return tokens;
        } catch (TokenExpiredException ex) {
            throw new TokenExpiredException(ex.getMessage(), ex.getExpiredOn());
        }
    }

    @Override
    public void register(String url, UserDetailDto dto) throws MailException {
        User newUser = new User(
                dto.getFirstName(),
                dto.getLastName(),
                dto.getUsername(),
                dto.getEmail(),
                LocalDateTime.now(),
                dto.getEnabled(),
                dto.getLocked(),
                "LOCAL",
                dto.getRoles()
        );

        try {
            User user = userRepo.save(newUser);

            String token = UUID.randomUUID().toString();
            passwordResetTokenService.createResetPasswordToken(user, token);

            String link = url + "/reset-password?token=" + token;
            String emailString = emailBuilder.createdUserEmail(user.getUsername(), link, url);

            emailService.send(dto.getEmail(), emailString, "Created new user");
        } catch (ConstraintViolationException e) {
            throw new EntityAlreadyExistsException("User with username: %s already exists".formatted(dto.getUsername()));
        } catch (MailException e) {
            throw new EmailException("Couldn't connect to mail server");
        }
    }

    @Override
    public void forgotPassword(String url, String email) {
        User user = userRepo.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with email: %s does not exist".formatted(email)));

        try {
            String token = UUID.randomUUID().toString();
            String link = url + "/reset-password?token=" + token;
            String emailString = emailBuilder.forgotPasswordEmail(user.getUsername(), link);

            passwordResetTokenService.createResetPasswordToken(user, token);
            emailService.send(email, emailString, "Forgot password");
        } catch (MailException ex) {
            throw new EmailException("Couldn't connect to mail server");
        }

    }

    @Override
    public void resetPassword(String password, String token) {
        User user = getUserFromResetPasswordToken(token);
        user.setPassword(passwordEncoder.encode(password));

        if (!user.getEnabled()) {
            user.setEnabled(true);
            user.setDateEnabled(LocalDateTime.now());
        }

        userRepo.save(user);
    }

    @Override
    public User loginOAuth2User(UserDetailDto dto) {
        Optional<User> user = userRepo.findByUsername(dto.getUsername());
        if (user.isEmpty()) {
            return createUser(dto);
        }
        return updateUser(user.get(), dto);
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                cookie.setValue("");
                cookie.setPath("/");
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
        }
    }

    private User createUser(UserDetailDto dto) {
        User user = new User();
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setUsername(dto.getUsername());
        user.setDateCreated(LocalDateTime.now());
        user.setEnabled(false);
        user.setLocked(false);
        user.setLoginType(dto.getLoginType());
        Optional<Role> role = roleRepo.findByName(Roles.ROLE_USER.name());
        role.ifPresent(value -> user.setRoles(List.of(value)));
        return userRepo.save(user);
    }

    private User updateUser(User user, UserDetailDto dto) {
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setUsername(dto.getUsername());
        return userRepo.save(user);
    }

    private User getUserFromResetPasswordToken(String token) {
        PasswordResetToken passwordResetToken = passwordResetTokenRepo.findByToken(token)
                .orElseThrow(() -> new EntityNotFoundException("Token: '%s' does not exists".formatted(token)));

        if (passwordResetToken.getExpiryDate().isBefore(LocalDateTime.now())) {
            throw new TokenException("Token has expired!");
        }

        return passwordResetToken.getUser();
    }
}

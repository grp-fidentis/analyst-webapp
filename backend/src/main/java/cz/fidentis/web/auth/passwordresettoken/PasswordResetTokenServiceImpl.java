package cz.fidentis.web.auth.passwordresettoken;

import cz.fidentis.web.user.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * @author Ondřej Bazala
 * @since 29.08.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {

    private final PasswordResetTokenRepo  passwordResetTokenRepo;

    @Override
    public void createResetPasswordToken(User user, String token) {
        PasswordResetToken resetToken = new PasswordResetToken(token, LocalDateTime.now().plusMinutes(15), user);
        passwordResetTokenRepo.save(resetToken);
    }
}

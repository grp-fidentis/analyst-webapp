package cz.fidentis.web.auth.exception;

/**
 * @author Petr Hendrych
 * @since 19.02.2023
 */
public class EntityAlreadyExistsException extends RuntimeException {

    public EntityAlreadyExistsException(String message) {
        super(message);
    }
}

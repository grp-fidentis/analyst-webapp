package cz.fidentis.web.auth.passwordresettoken;

import cz.fidentis.web.user.User;

/**
 * @author Ondřej Bazala
 * @since 29.08.2023
 */
public interface PasswordResetTokenService {

    /**
     * Service to create new reset password token and save it to database with 15 min duration
     *
     * @param user {@code User} entity for which user token is made
     * @param token UUID token value
     */
    void createResetPasswordToken(User user, String token);
}

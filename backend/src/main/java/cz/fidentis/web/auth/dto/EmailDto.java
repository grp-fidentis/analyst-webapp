package cz.fidentis.web.auth.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

/**
 * @author Petr Hendrych
 * @param email email
 */
public record EmailDto(@NotBlank @Email String email) {}

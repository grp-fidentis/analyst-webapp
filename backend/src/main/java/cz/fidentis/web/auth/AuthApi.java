package cz.fidentis.web.auth;

import cz.fidentis.web.user.dto.UserDetailDto;
import cz.fidentis.web.auth.dto.EmailDto;
import cz.fidentis.web.auth.dto.ResetPasswordDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.net.URI;
import java.util.Map;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.HOST;

/**
 * @author Petr Hendrych
 * @since 07.12.2022
 */
@RestController
@Tag(name = "Auth API", description = "API for authentication related operations")
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthApi {

    private final AuthService authService;

    @GetMapping("/refresh-token")
    @Operation(summary = "Refreshes user's token")
    public void refreshUserToken(@RequestHeader(AUTHORIZATION) String authorizationHeader, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String requestUrl = request.getRequestURL().toString();
        Map<String, String> tokens = authService.refreshUserToken(authorizationHeader, requestUrl);

        response.setHeader("Access-Token", tokens.get("Access-Token"));
        response.setHeader("Refresh-Token", tokens.get("Refresh-Token"));
    }

    @PostMapping("/register")
    @Operation(summary = "Registers new user")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Void> register(@RequestHeader(HOST) String hostUrl, @Valid @RequestBody UserDetailDto dto) throws MailException {
        authService.register(hostUrl, dto);

        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/auth/register").toUriString());
        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/forgot-password")
    @Operation(summary = "Sends email to user's email address with url and token for changing password")
    public ResponseEntity<Void> forgotPassword(@RequestHeader(HOST) String hostUrl, @Valid @RequestBody EmailDto dto) {
        authService.forgotPassword(hostUrl, dto.email());
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/reset-password")
    @Operation(summary = "Resets password for user with valid token")
    public ResponseEntity<Void> resetPassword(@Valid @RequestBody ResetPasswordDto dto) {
        authService.resetPassword(dto.getPassword(), dto.getToken());
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/logout")
    @Operation(summary = "Deletes cookies when user logs out of the application")
    public ResponseEntity<Void> logout(HttpServletRequest request, HttpServletResponse response) {
        authService.logout(request, response);
        return ResponseEntity.noContent().build();
    }
}

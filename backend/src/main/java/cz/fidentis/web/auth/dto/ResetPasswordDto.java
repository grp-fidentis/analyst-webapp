package cz.fidentis.web.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.NotBlank;

/**
 * @author Petr Hendrych
 * @since 17.03.2023
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResetPasswordDto {

    @NotBlank
    private String token;

    @NotBlank
    private String password;
}

package cz.fidentis.web.user;

import cz.fidentis.web.project.ProjectDto;
import cz.fidentis.web.user.dto.ChangePasswordDto;
import cz.fidentis.web.user.dto.EnableUserDto;
import cz.fidentis.web.user.dto.UserDetailDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Petr Hendrych
 * @since 28.11.2022
 */
@RestController
@Tag(name = "User API", description = "API for managing and retrieving users")
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserApi {

    private final UserService userService;

    @GetMapping
    @Operation(summary = "Gets users")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Gets user by id")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN') or isOwner(#id, 'USER')")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @PostMapping("/reset-password")
    @Operation(summary = "Resets password of current user")
    public ResponseEntity<Void> resetPassword(@Valid @RequestBody ChangePasswordDto changePasswordDto) {
        userService.changePassword(changePasswordDto);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    @Operation(summary = "Updates user by id")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN') or isOwner(#id, 'USER')")
    public ResponseEntity<User> updateUser(@Valid @RequestBody UserDetailDto user, @PathVariable Long id) {
        return ResponseEntity.ok(userService.updateUser(id, user));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Deletes user by id")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN') or isOwner(#id, 'USER')")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/enable/{id}")
    @Operation(summary = "Enables or disables user by id according to DTO in request body")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN') or isOwner(#id, 'USER')")
    public ResponseEntity<User> enableUser(@PathVariable Long id, @Valid @RequestBody EnableUserDto dto) {
        return ResponseEntity.ok(userService.enableUser(id, dto));
    }

    @GetMapping("/{userId}/projects")
    @Operation(summary = "Gets current user's projects with tasks")
    public ResponseEntity<List<ProjectDto>> getProjectsByUser(@PathVariable Long userId) {
        return ResponseEntity.ok(userService.getProjectsByUser(userId));
    }
}

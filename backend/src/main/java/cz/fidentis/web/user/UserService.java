package cz.fidentis.web.user;

import cz.fidentis.web.project.ProjectDto;
import cz.fidentis.web.user.dto.ChangePasswordDto;
import cz.fidentis.web.user.dto.EnableUserDto;
import cz.fidentis.web.user.dto.UserDetailDto;

import java.util.List;

/**
 * @author Petr Hendrych
 * @since 28.11.2022
 */
public interface UserService {

    /**
     * Service to get application user by it`s ID
     *
     * @param userId ID of user
     * @return User entity with it`s mapped roles
     */
    User getUserById(Long userId);

    /**
     * Service to get all application users. Can call only user with ROLE_ADMIN others get
     * 403 forbidden response
     *
     * @return List of all registered user in application
     */
    List<User> getUsers();

    /**
     * Service to get current authenticated user from SecurityContextHolder
     *
     * @return User entity
     */
    User getCurrentAuthenticatedUser();

    /**
     * Service to delete user. This action can be performed only by admin on user by himself.
     *
     * @param userId ID of user
     */
    void deleteUser(Long userId);

    /**
     * Service to update {@code User} entity with new data
     *
     * @param userId ID of user
     * @param user editable {@code User} attributes
     */
    User updateUser(Long userId, UserDetailDto user);

    /**
     * Service to get application user by it`s username. Username should be unique inside
     * application
     *
     * @param username Searched string value
     * @return User entity with it`s mapped roles
     */
    User getUserByUserName(String username);

    /**
     * Service to change user`s password. Performs also few basics validations and checking if
     * logged user is the one for who is changing the password
     *
     * @param dto Object containing old password, new password 2 times for validation
     */
    void changePassword(ChangePasswordDto dto);

    /**
     * Service to enable or disable user according to Boolean value in DTO. Also sends an email to user which informs whether
     * user was enabled or disabled.
     *
     * @param id ID of user
     * @param dto Contains boolean value whether the user should be enabled or disabled
     * @return Enabled/disabled user
     */
    User enableUser(Long id, EnableUserDto dto);

    /**
     * Service to get all projects for user
     *
     * @param userId ID of user
     * @return Project by user id
     */
    List<ProjectDto> getProjectsByUser(Long userId);

}

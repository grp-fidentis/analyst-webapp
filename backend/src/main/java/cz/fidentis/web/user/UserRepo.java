package cz.fidentis.web.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Petr Hendrych
 * @since 28.11.2022
 */
public interface UserRepo extends JpaRepository<User, Long> {

    /**
     * Method to fetch user by username from database
     *
     * @param username Searched string value
     * @return If user is found in db then inside optional is found user
     * else optional contain null value
     */
    Optional<User> findByUsername(String username);

    /**
     * Method to fetch user by email from database
     *
     * @param email Searched string value of email
     * @return {@code User} entity if email match user
     */
    Optional<User> findByEmail(String email);

    /**
     * Method to fetch user by id from database
     *
     * @param id ID of user
     * @return {@code User} entity found by id
     */
    Optional<User> findById(Long id);

    /**
     * Method to fetch all {@code User} ordered by id
     *
     * @return List of {@code User} entities ordered by ID ascending
     */
    List<User> findAllByOrderByIdAsc();
}

package cz.fidentis.web.user.dto;

import cz.fidentis.web.role.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 16.03.2023
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailDto {

    private String firstName;

    private String lastName;

    @NotBlank
    private String username;

    @NotBlank
    @Email
    private String email;

    @NotEmpty
    @Valid
    private List<Role> roles;

    private Boolean enabled;

    private Boolean locked;

    private String loginType;
}

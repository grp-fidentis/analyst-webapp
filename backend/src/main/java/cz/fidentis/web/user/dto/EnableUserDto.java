package cz.fidentis.web.user.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Adam Majzlik
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class EnableUserDto {

    @NotNull
    private Boolean enabled;

}

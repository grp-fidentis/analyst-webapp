package cz.fidentis.web.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import cz.fidentis.web.auth.passwordresettoken.PasswordResetToken;
import cz.fidentis.web.base.BaseEntity;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.role.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Petr Hendrych
 * @since 28.11.2022
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_user")
@Table(name = "app_user")
public class User extends BaseEntity implements UserDetails {

    private String firstName;

    private String lastName;

    private String username;

    @JsonIgnore
    private String password;

    private String email;

    private Boolean enabled;

    private Boolean locked;

    private String loginType;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime dateCreated;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime dateEnabled;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private List<Project> projects;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private List<PasswordResetToken> tokens;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "app_user_roles",
            joinColumns = @JoinColumn(name = "app_user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Collection<Role> roles = new ArrayList<>();

    public User(String firstName,
                String lastName,
                String username,
                String email,
                LocalDateTime dateCreated,
                Boolean enabled,
                Boolean locked,
                String loginType,
                Collection<Role> roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.dateCreated = dateCreated;
        this.enabled = enabled;
        this.locked = locked;
        this.loginType = loginType;
        this.roles = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();

        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public boolean hasAnyRole(String... list) {
        for (String role : list) {
            if (roles.stream().anyMatch(r -> Objects.equals(r.getName(), role))) {
                return true;
            }
        }

        return false;
    }
}

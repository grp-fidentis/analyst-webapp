package cz.fidentis.web.user;

/**
 * @author Ondřej Bazala
 * @since 13.09.2023
 */
public class PasswordValidationException extends RuntimeException {

    public PasswordValidationException() {
    }

    public PasswordValidationException(String message) {
        super(message);
    }

    public PasswordValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public PasswordValidationException(Throwable cause) {
        super(cause);
    }

    public PasswordValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

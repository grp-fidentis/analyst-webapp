package cz.fidentis.web.user;

import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.exceptions.exceptions.InvalidRoleException;
import cz.fidentis.web.mail.EmailBuilder;
import cz.fidentis.web.mail.EmailService;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.project.ProjectDto;
import cz.fidentis.web.project.ProjectRepo;
import cz.fidentis.web.role.Roles;
import cz.fidentis.web.user.dto.ChangePasswordDto;
import cz.fidentis.web.user.dto.EnableUserDto;
import cz.fidentis.web.user.dto.UserDetailDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Petr Hendrych
 * @since 28.11.2022
 */
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
@Primary
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepo userRepo;
    private final ProjectRepo projectRepo;

    private final PasswordEncoder passwordEncoder;  // has to stay here due SecurityConfig configuration of UserDetailsService
    private final EmailBuilder emailBuilder;
    private final EmailService emailService;
    private final ModelMapper mapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User: %s not found in the database".formatted(username)));
    }

    @Override
    public User getUserById(Long userId) {
        return userRepo.findById(userId)
                .orElseThrow(() -> new NoSuchElementException("User with id: %s not found in the database".formatted(userId)));
    }

    @Override
    public List<User> getUsers() {
        return userRepo.findAllByOrderByIdAsc();
    }

    @Override
    public User getCurrentAuthenticatedUser() {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepo.findByUsername(username)
                .orElseThrow(() -> new NoSuchElementException("User: %s does not exist".formatted(username)));
    }

    @Override
    public void deleteUser(Long userId) {
        log.info("Deleting user with id: {}", userId);
        userRepo.deleteById(userId);
    }

    @Override
    public User updateUser(Long userId, UserDetailDto userDto) {
        User authUser = getCurrentAuthenticatedUser();
        User savedUser = userRepo.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User with id: %s not found".formatted(userId)));

        savedUser.setUsername(userDto.getUsername());
        savedUser.setFirstName(userDto.getFirstName());
        savedUser.setLastName(userDto.getLastName());
        savedUser.setEmail(userDto.getEmail());

        if (authUser.hasAnyRole("ROLE_ADMIN", "ROLE_SUPER_ADMIN")) {
            savedUser.setEnabled(userDto.getEnabled());
            savedUser.setLocked(userDto.getLocked());
            savedUser.setRoles(userDto.getRoles());
        }

        return userRepo.save(savedUser);
    }

    @Override
    public User getUserByUserName(String username) {
        return userRepo.findByUsername(username)
                .orElseThrow(() -> new NoSuchElementException("User: %s not found in the database".formatted(username)));
    }

    @Override
    public void changePassword(ChangePasswordDto dto) {
        User user = getCurrentAuthenticatedUser();

        log.info("Changing password for user: %s".formatted(user.getUsername()));

        if (!Objects.equals(dto.getPassword(), dto.getPasswordAgain())) {
            throw new PasswordValidationException("Passwords does not match!");
        }

        if (!passwordEncoder.matches(dto.getOldPassword(), user.getPassword())) {
            throw new PasswordValidationException("Old password is incorrect!");
        }

        user.setPassword(passwordEncoder.encode(dto.getPassword()));

        userRepo.save(user);
    }

    @Override
    public User enableUser(Long id, EnableUserDto dto) {
        Optional<User> optionalUser = userRepo.findById(id);
        if (optionalUser.isEmpty()) {
            throw new EntityNotFoundException("User with id: %s not found".formatted(id));
        }
        User user = optionalUser.get();
        User authUser = getCurrentAuthenticatedUser();

        if (user.hasAnyRole(Roles.ROLE_SUPER_ADMIN.toString()) && !authUser.hasAnyRole(Roles.ROLE_SUPER_ADMIN.toString())) {
            throw new InvalidRoleException("SUPER ADMIN can be managed only by other SUPER ADMINs.");
        }

        user.setEnabled(dto.getEnabled());
        if (dto.getEnabled()) {
            user.setDateEnabled(LocalDateTime.now());
        }
        if (user.getEmail() == null) {
            return userRepo.save(user);
        }
        sendEnableUserEmail(user);
        return userRepo.save(user);
    }

    @Override
    public List<ProjectDto> getProjectsByUser(Long userId) {
        if (!userRepo.existsById(userId)) {
            throw new EntityNotFoundException("User with id: " + userId + " was not found.");
        }
        List<ProjectDto> projectDtoList = new ArrayList<>();
        List<Project> projectList = projectRepo.findByUserId(userId);
        projectList.forEach(project -> projectDtoList.add(mapper.map(project, ProjectDto.class)));
        return projectDtoList;
    }

    private void sendEnableUserEmail(User user) {
        if (user.getEnabled()) {
            String emailString = emailBuilder.enabledUserEmail(user.getUsername());
            emailService.send(user.getEmail(), emailString, "FIDENTIS Analyst II - Enabled account");
        } else {
            String emailString = emailBuilder.disabledUserEmail(user.getUsername());
            emailService.send(user.getEmail(), emailString, "FIDENTIS Analyst II - Disabled account");
        }
    }

}

package cz.fidentis.web.taskparameters.dto;

import cz.fidentis.web.batchprocessing.distance.enums.SimilarityMethod;
import cz.fidentis.web.batchprocessing.registration.enums.BatchAveragingMethod;
import cz.fidentis.web.batchprocessing.registration.enums.BatchRegistrationMethod;

/**
 * @author Patrik Tomov
 * @since 07.10.2024
 */
public interface TaskParametersDto {

    String getTemplateFaceForRegistration();
    String getTemplateFaceForDistance();
    BatchAveragingMethod getAveragingMethod();
    Short getMaxRegistrationIterations();
    Short getStabilizationThreshold();
    BatchRegistrationMethod getRegistrationMethod();
    Integer getRegistrationSubsampling();
    Boolean getScaleFacesDuringRegistration();
    SimilarityMethod getSimilarityMethod();
    String getDeviations();
    String getDistances();
}

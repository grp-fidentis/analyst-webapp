package cz.fidentis.web.taskparameters;

import cz.fidentis.web.taskparameters.dto.TaskParametersDistanceUpdateDto;
import cz.fidentis.web.taskparameters.dto.TaskParametersDto;
import cz.fidentis.web.taskparameters.dto.TaskParametersRegistrationUpdateDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Patrik Tomov
 * @since  16.10.2024
 */
@RestController
@Tag(name = "Task Parameters API", description = "API for managing task parameters")
@RequestMapping("/api/task-parameters")
@RequiredArgsConstructor
public class TaskParametersApi {
    private final TaskParametersService taskParametersService;

    @GetMapping("/{taskParametersId}")
    @Operation(summary = "Get task parameters by task id")
    public ResponseEntity<TaskParametersDto> getTaskParametersByTaskId(@PathVariable Long taskParametersId) {
        TaskParametersDto taskParametersDto = taskParametersService.getTaskParametersId(taskParametersId);
        return ResponseEntity.ok(taskParametersDto);
    }

    @PutMapping("/{taskId}/registration-update")
    @Operation(summary = "Update registration task parameters for task id")
    public ResponseEntity<Void> updateTaskParameters(
            @PathVariable Long taskId,
            @Valid @RequestBody TaskParametersRegistrationUpdateDto taskParametersDto) {
        taskParametersService.updateTaskParametersForRegistration(taskId, taskParametersDto);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{taskId}/distance-update")
    @Operation(summary = "Update distance task parameters for task id")
    public ResponseEntity<Void> updateTaskParameters(
            @PathVariable Long taskId,
            @Valid @RequestBody TaskParametersDistanceUpdateDto taskParametersDto) {
        taskParametersService.updateTaskParametersForDistance(taskId, taskParametersDto);
        return ResponseEntity.noContent().build();
    }
}

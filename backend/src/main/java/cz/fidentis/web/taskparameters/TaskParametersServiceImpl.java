package cz.fidentis.web.taskparameters;

import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.task.Task;
import cz.fidentis.web.taskparameters.dto.TaskParametersDistanceUpdateDto;
import cz.fidentis.web.taskparameters.dto.TaskParametersDto;
import cz.fidentis.web.taskparameters.dto.TaskParametersRegistrationUpdateDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Patrik Tomov
 * @since 07.10.2024
 */
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class TaskParametersServiceImpl implements TaskParametersService {

    private final TaskParametersRepo taskParametersRepo;

    @Override
    public TaskParametersDto getTaskParametersId(Long taskParametersId) {
        return taskParametersRepo.findTaskParametersDtoById(taskParametersId)
                .orElseThrow(() -> new EntityNotFoundException("Task parameters not found for task ID: " + taskParametersId));
    }

    @Override
    public void createTaskParameters(Task task, FileUpload fileUpload) {
        TaskParameters taskParameters = new TaskParameters();
        taskParameters.setTask(task);
        taskParameters.setTemplateFaceForRegistration(fileUpload.getName());
        taskParameters.setTemplateFaceForDistance(fileUpload.getName());
        TaskParameters savedTaskParameters = taskParametersRepo.save(taskParameters);
        task.setTaskParameters(savedTaskParameters);
    }

    @Override
    public void updateTaskParametersForRegistration(Long taskId, TaskParametersRegistrationUpdateDto taskParametersDto) {
        TaskParameters taskParameters = taskParametersRepo.findTaskParametersByTaskId(taskId);

        taskParameters.setTemplateFaceForRegistration(taskParametersDto.getTemplateFaceForRegistration());
        taskParameters.setAveragingMethod(taskParametersDto.getAveragingMethod());
        taskParameters.setMaxRegistrationIterations(taskParametersDto.getMaxRegistrationIterations());
        taskParameters.setStabilizationThreshold(taskParametersDto.getStabilizationThreshold());
        taskParameters.setRegistrationMethod(taskParametersDto.getRegistrationMethod());
        taskParameters.setRegistrationSubsampling(taskParametersDto.getRegistrationSubsampling());
        taskParameters.setScaleFacesDuringRegistration(taskParametersDto.getScaleFacesDuringRegistration());
    }

    @Override
    public void updateTaskParametersForDistance(Long taskId, TaskParametersDistanceUpdateDto taskParametersDto) {
        TaskParameters taskParameters = taskParametersRepo.findTaskParametersByTaskId(taskId);

        taskParameters.setTemplateFaceForDistance(taskParametersDto.getTemplateFaceForDistance());
        taskParameters.setSimilarityMethod(taskParametersDto.getSimilarityMethod());
        taskParameters.setDeviations(taskParametersDto.getDeviations());
        taskParameters.setDistances(taskParametersDto.getDistances());
    }
}

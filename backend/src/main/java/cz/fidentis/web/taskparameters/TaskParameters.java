package cz.fidentis.web.taskparameters;

import cz.fidentis.web.base.BaseEntity;
import cz.fidentis.web.batchprocessing.distance.enums.SimilarityMethod;
import cz.fidentis.web.batchprocessing.registration.enums.BatchAveragingMethod;
import cz.fidentis.web.batchprocessing.registration.enums.BatchRegistrationMethod;
import cz.fidentis.web.task.Task;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

/**
 * @author Patrik Tomov
 * @since 06.10.2024
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "task_parameters")
@DynamicInsert
public class TaskParameters extends BaseEntity {

    @OneToOne(mappedBy = "taskParameters")
    private Task task;

    private String templateFaceForRegistration;

    private String templateFaceForDistance;

    @Enumerated(EnumType.STRING)
    private BatchAveragingMethod averagingMethod;

    private Short  maxRegistrationIterations;

    private Short  stabilizationThreshold;

    @Enumerated(EnumType.STRING)
    private BatchRegistrationMethod registrationMethod;

    private Integer registrationSubsampling;

    private Boolean scaleFacesDuringRegistration;

    @Enumerated(EnumType.STRING)
    private SimilarityMethod similarityMethod;

    private String deviations;

    private String distances;
}

package cz.fidentis.web.taskparameters.dto;

import cz.fidentis.web.batchprocessing.registration.enums.BatchAveragingMethod;
import cz.fidentis.web.batchprocessing.registration.enums.BatchRegistrationMethod;
import lombok.*;

/**
 * @author Patrik Tomov
 * @since  16.10.2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TaskParametersRegistrationUpdateDto {
    private String templateFaceForRegistration;
    private BatchAveragingMethod averagingMethod;
    private Short maxRegistrationIterations;
    private Short stabilizationThreshold;
    private BatchRegistrationMethod registrationMethod;
    private Integer registrationSubsampling;
    private Boolean scaleFacesDuringRegistration;
}

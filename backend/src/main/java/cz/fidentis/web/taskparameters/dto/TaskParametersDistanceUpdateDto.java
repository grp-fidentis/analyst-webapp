package cz.fidentis.web.taskparameters.dto;

import cz.fidentis.web.batchprocessing.distance.enums.SimilarityMethod;
import lombok.*;

/**
 * @author Patrik Tomov
 * @since  16.10.2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TaskParametersDistanceUpdateDto {
    private String templateFaceForDistance;
    private SimilarityMethod similarityMethod;
    private String deviations;
    private String distances;
}

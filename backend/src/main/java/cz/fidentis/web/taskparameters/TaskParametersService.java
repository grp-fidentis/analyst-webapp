package cz.fidentis.web.taskparameters;

import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.task.Task;
import cz.fidentis.web.taskparameters.dto.TaskParametersDistanceUpdateDto;
import cz.fidentis.web.taskparameters.dto.TaskParametersDto;
import cz.fidentis.web.taskparameters.dto.TaskParametersRegistrationUpdateDto;

/**
 * @author Patrik Tomov
 * @since 07.10.2024
 */
public interface TaskParametersService {

    /**
     * Retrieves Task Parameters by the Task ID.
     *
     * @param taskParametersId The ID of the task parameters to retrieve
     * @return The TaskParametersDto containing the task parameters
     */
    TaskParametersDto getTaskParametersId(Long taskParametersId);

    /**
     * Creates new Task Parameters for the given Task ID.
     *
     * @param task task to create parameters for
     * @param fileUpload first file upload to show in the template face
     */
    void createTaskParameters(Task task, FileUpload fileUpload);

    /**
     * Updates existing registration task parameters for the given Task ID.
     *
     * @param taskParametersId The ID of the task parameters to update
     * @param taskParametersDto The DTO containing updated task parameters data for registration
     */
    void updateTaskParametersForRegistration(Long taskParametersId, TaskParametersRegistrationUpdateDto taskParametersDto);

    /**
     * Updates existing distance task parameters for the given Task ID.
     *
     * @param taskParametersId The ID of the task parameters to update
     * @param taskParametersDto The DTO containing updated task parameters data for distance
     */
    void updateTaskParametersForDistance(Long taskParametersId, TaskParametersDistanceUpdateDto taskParametersDto);
}

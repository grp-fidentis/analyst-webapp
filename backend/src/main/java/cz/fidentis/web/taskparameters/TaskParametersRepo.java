package cz.fidentis.web.taskparameters;

import cz.fidentis.web.taskparameters.dto.TaskParametersDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Patrik Tomov
 * @since 07.10.2024
 */
@Repository
public interface TaskParametersRepo extends JpaRepository<TaskParameters, Long> {

    Optional<TaskParametersDto> findTaskParametersDtoById(Long taskParametersId);

    TaskParameters findTaskParametersByTaskId(Long taskId);
}

package cz.fidentis.web.task;

import cz.fidentis.web.task.dto.CreateTaskDto;
import cz.fidentis.web.task.dto.TaskDto;

import java.util.List;

/**
 * @author Petr Hendrych
 * @since 09.02.2023
 */
public interface TaskService {

   /**
    * Service to get all tasks for selected project.
    *
    * @param projectId ID of project
    * @return List of tasks (can be optimized further by writing query to skip blob data from task file)
    */
   List<TaskDto> getProjectTasks(Long projectId);

   /**
    * Service to create new task. This action will also create new {@code HumanFaceEntity} which is
    * basically {@code HumanFace} object with its {@code MeshModel} dumped as byte array (serialized)
    * and stored in database.
    *
    * @param dto Object with all needed properties to create new task and file upload copies
    * @return Task entity with file uploads entities copies
    */
   TaskDto createTask(CreateTaskDto dto);

   /**
    * Service to delete task.
    *
    * @param id ID of task
    */
   void deleteTask(Long id);

   /**
    * Service to get task dto by id.
    *
    * @param id ID of task
    * @return TaskDto entity
    */
   TaskDto getTaskDtoById(Long id);

   /**
    * Service to get task by id.
    *
    * @param id ID of task
    * @return {@code Task} entity
    */
   Task getTaskById(Long id);

   /**
    * Service to update {@code Task} name
    *
    * @param name New {@code Task} name
    * @param id ID of {@code Task}
    * @return Updated Task entity
    */
   TaskDto updateTaskName(String name, Long id);

    /**
     * Service to find task by taskName.
     *
     * @param taskName Name of task
     * @return Task entity
     */
    TaskDto findTaskByName(String taskName);
}

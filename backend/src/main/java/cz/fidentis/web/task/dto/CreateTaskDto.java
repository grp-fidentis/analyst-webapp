package cz.fidentis.web.task.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 09.02.2023
 */
@Data
@AllArgsConstructor
public class CreateTaskDto {

    @NotNull
    private Long projectId;

    @NotEmpty
    private List<@NotNull Long> fileUploadIds; // first id in list is determining primary face (if PAIR COMPARISON task)
}

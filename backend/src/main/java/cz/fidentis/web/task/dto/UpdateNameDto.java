package cz.fidentis.web.task.dto;


import jakarta.validation.constraints.NotBlank;

/**
 * @author Petr Hendrych
 */
public record UpdateNameDto(
        @NotBlank String name) {

}
package cz.fidentis.web.task.dto;


import cz.fidentis.web.task.TaskType;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Ondřej Bazala
 * @since 13.10.2023
 */
public interface TaskDto {

    Long getId();
    @Value("#{target.project.id}")
    Long getProjectId();
    @Value("#{target.taskParameters != null ? target.taskParameters.id : null}")
    Long getTaskParametersId();
    String getName();
    TaskType getTaskType();
}

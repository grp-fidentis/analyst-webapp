package cz.fidentis.web.task;

import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.FileUploadRepo;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.service.HumanFaceService;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.project.ProjectRepo;
import cz.fidentis.web.task.dto.CreateTaskDto;
import cz.fidentis.web.task.dto.TaskDto;
import cz.fidentis.web.taskparameters.TaskParametersService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static cz.fidentis.web.cache.CacheConfig.TASK_FACES;
import static cz.fidentis.web.fileupload.FileUploadServiceImpl.AVERAGE_FACE_NAME;

/**
 * @author Petr Hendrych
 * @since 09.02.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class TaskServiceImpl implements TaskService {

    private final HumanFaceService humanFaceService;
    private final FileUploadRepo fileUploadRepo;
    private final ProjectRepo projectsRepo;
    private final TaskRepo taskRepo;
    private final TaskParametersService taskParametersService;

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TaskDto> getProjectTasks(Long projectId) {
        return taskRepo.findAllByProjectId(projectId);
    }

    @CacheEvict(value = TASK_FACES, key = "@cacheService.tokenId")
    @Override
    public TaskDto createTask(CreateTaskDto dto) {
        List<Long> fileUploadIds = dto.getFileUploadIds();
        Project project = projectsRepo.findById(dto.getProjectId())
                    .orElseThrow(() -> new EntityNotFoundException("Project with id: %s does not exist".formatted(dto.getProjectId())));

        Task task = new Task();
        project.addTask(task);
        task.setTaskType(TaskType.getTaskTypeForNumberOfFaces(fileUploadIds.size()));

        Task savedTask = taskRepo.save(task);

        List<FileUpload> fileUploads = fileUploadRepo.findAllById(fileUploadIds);

        if (task.getTaskType().equals(TaskType.BATCH_PROCESSING)) {
            Optional<FileUpload> averageFaceFile = fileUploads.stream()
                    .filter(file -> file.getName().equals(AVERAGE_FACE_NAME))
                    .findFirst();
            if (averageFaceFile.isPresent()) {
                createDefaultTaskParameters(savedTask, averageFaceFile.get());
            } else {
                createDefaultTaskParameters(savedTask, fileUploads.getFirst());
            }
            addFilesToTaskInBackground(savedTask, fileUploads);
            fileUploadRepo.findById(fileUploadIds.getFirst()).ifPresent(fileUpload -> {
                HumanFaceEntity humanFace = humanFaceService.createAndSaveHumanFaceEntity(fileUpload, savedTask);
                savedTask.addHumanFace(humanFace);
            });
        } else {
            savedTask.setTaskParameters(null);
            List<HumanFaceEntity> humanFaces = createHumanFaces(fileUploads, fileUploadIds, savedTask);
            savedTask.addHumanFaces(humanFaces);
        }

        task.setName(getName(task, fileUploadIds));

        return taskRepo.findTaskById(savedTask.getId())
                .orElseThrow(() -> new EntityNotFoundException("Task with id %s does not exist".formatted(savedTask.getId())));
    }

    @Async
    public void createDefaultTaskParameters(Task task, FileUpload fileUpload) {
        taskParametersService.createTaskParameters(task, fileUpload);
    }

    @Async
    public void addFilesToTaskInBackground(Task task, List<FileUpload> fileUploads) {
        fileUploads.forEach(file -> file.addTask(task));
    }

    private String getName(Task task, List<Long> ids) {
        switch (task.getTaskType()) {
            case SINGLE_FACE_ANALYSIS -> {
                return task.getFileUploads().stream()
                        .findAny().orElseThrow().getFaceInfo().getGeometryFileName().split(".obj")[0];
            }
            case PAIR_COMPARISON -> {
                var primaryFaceInfo = task.getFileUploads().stream()
                        .filter(f -> f.getId().equals(ids.get(0))).findAny().orElseThrow().getFaceInfo();
                var secondaryFaceInfo = task.getFileUploads().stream()
                        .filter(f -> f.getId().equals(ids.get(1))).findAny().orElseThrow().getFaceInfo();
                return primaryFaceInfo.getGeometryFileName().split(".obj")[0] + ":" +
                        secondaryFaceInfo.getGeometryFileName().split(".obj")[0];
            }
            case BATCH_PROCESSING -> {
                return "Task " + task.getId() + ": " + ids.size() + " faces";
            }
            default -> throw new IllegalStateException("Task must have one of three TaskTypes");
        }
    }

    @Override
    @Async
    public void deleteTask(Long id) {
        log.info("Deleting task with id: {}", id);
        Task task = taskRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Task with id: %s does not exist".formatted(id)));
        taskRepo.delete(task);
    }

    @Override
    public TaskDto getTaskDtoById(Long id) {
        return taskRepo.findTaskById(id)
                .orElseThrow(() -> new EntityNotFoundException("Task with id: %s does not exist".formatted(id)));
    }

    @Override
    public Task getTaskById(Long id) {
        return taskRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Task with id: %s does not exist".formatted(id)));
    }

    @Override
    public TaskDto updateTaskName(String name, Long id) {
        Task task = taskRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Task with id: %s does not exist".formatted(id)));

        task.setName(name);
        taskRepo.save(task);

        return taskRepo.findTaskById(id)
                .orElse(null);
    }

    @Override
    public TaskDto findTaskByName(String taskName) {
        return taskRepo.findTaskByName(taskName)
                .orElse(null);
    }

    private List<HumanFaceEntity> createHumanFaces(List<FileUpload> fileUploads, List<Long> ids, Task task) {
        return fileUploads.stream()
                .map(fileUpload -> {
                    HumanFaceEntity humanFace = humanFaceService.createAndSaveHumanFaceEntity(fileUpload, task);
                    fileUpload.addTask(task);
                    if (task.getTaskType().equals(TaskType.PAIR_COMPARISON) && fileUpload.getId().equals(ids.get(0))) {
                        humanFace.setIsPrimaryFace(true);
                    }
                    return humanFace;
                })
                .toList();
    }
}

package cz.fidentis.web.task;

import cz.fidentis.web.task.dto.CreateTaskDto;
import cz.fidentis.web.task.dto.TaskDto;
import cz.fidentis.web.task.dto.UpdateNameDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 09.02.2023
 */
@RestController
@Tag(name = "Task API", description = "API for managing and retrieving tasks")
@RequestMapping("/api/tasks")
@RequiredArgsConstructor
public class TaskApi {

    private final TaskService taskService;

    @GetMapping
    @Operation(summary = "Gets tasks by projectId")
    @PreAuthorize("isOwner(#projectId, 'PROJECT')")
    public ResponseEntity<List<TaskDto>> getUserTasksOnProject(@RequestParam Long projectId) {
        return ResponseEntity.ok(taskService.getProjectTasks(projectId));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Gets task by id")
    @PreAuthorize("isOwner(#id, 'TASK')")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable Long id) {
        return ResponseEntity.ok(taskService.getTaskDtoById(id));
    }

    @PostMapping
    @Operation(summary = "Creates task")
    @PreAuthorize("isOwner(#dto.projectId, 'PROJECT') and isOwner(#dto.fileUploadIds, 'FILE_UPLOAD')")
    public ResponseEntity<TaskDto> createTask(@Valid @RequestBody CreateTaskDto dto) {
        return ResponseEntity.ok(taskService.createTask(dto));
    }

    @PutMapping("/{id}")
    @Operation(summary = "Updates task name")
    @PreAuthorize("isOwner(#id, 'TASK')")
    public ResponseEntity<TaskDto> updateTaskName(@Valid @RequestBody UpdateNameDto dto, @PathVariable Long id) {
        return ResponseEntity.ok(taskService.updateTaskName(dto.name(), id));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Deletes task by id")
    @PreAuthorize("isOwner(#id, 'TASK') or hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<Void> deleteTask(@PathVariable Long id) {
        taskService.deleteTask(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/check")
    @Operation(summary = "Checks if a task exists with the given taskName")
    public ResponseEntity<TaskDto> checkTaskExists(@RequestParam String taskName) {
        TaskDto task = taskService.findTaskByName(taskName);
        return ResponseEntity.ok(task);
    }
}


package cz.fidentis.web.task;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fidentis.web.base.BaseEntity;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.taskparameters.TaskParameters;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 08.02.2023
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "task")
public class Task extends BaseEntity {

    private String name;

    @Enumerated(EnumType.STRING)
    private TaskType taskType;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "task", fetch = FetchType.LAZY)
    private List<HumanFaceEntity> humanFaceEntities = new ArrayList<>();

    @JsonIgnore
    @JoinTable(
            name = "file_upload_tasks",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "file_upload_id"))
    @ManyToMany(fetch = FetchType.LAZY)
    private List<FileUpload> fileUploads = new ArrayList<>();

    public Task(Project project, List<HumanFaceEntity> humanFaceEntities, String name) {
        this.project = project;
        this.name = name;
        this.humanFaceEntities = humanFaceEntities;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "task_parameters_id", referencedColumnName = "id")
    @JsonIgnore
    private TaskParameters taskParameters;

    public void addHumanFaces(List<HumanFaceEntity> humanFaces) {
        this.humanFaceEntities.addAll(humanFaces);
    }

    public void addHumanFace(HumanFaceEntity humanFace) {
        this.humanFaceEntities.add(humanFace);
    }
}

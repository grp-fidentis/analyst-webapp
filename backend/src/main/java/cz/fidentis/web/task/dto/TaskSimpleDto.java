package cz.fidentis.web.task.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Adam Majzlik
 * @since 26.03.2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TaskSimpleDto {

    private Long id;
    private String name;

}

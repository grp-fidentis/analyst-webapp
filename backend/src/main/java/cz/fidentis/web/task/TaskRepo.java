package cz.fidentis.web.task;

import cz.fidentis.web.task.dto.TaskDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Petr Hendrych
 * @since 09.02.2023
 */
@Repository
public interface TaskRepo extends JpaRepository<Task, Long> {

    /**
     * Method to fetch task ids on selected project
     *
     * @param projectId ID of project
     * @return List of {@code Task} ids for desired project
     */
    List<TaskDto> findAllByProjectId(Long projectId);

    /**
     * Method to fetch certain columns of {@code Task} by its id
     *
     * @param id ID of {@code Task}
     * @return Stripped {@code Task} entity containing only id and name
     */
    Optional<TaskDto> findTaskById(Long id);


    /**
     * Method to fetch  {@code Task} by its name
     *
     * @param taskName Name of {@code Task}
     * @return {@code Task} entity
     */
    Optional<TaskDto> findTaskByName(String taskName);
}

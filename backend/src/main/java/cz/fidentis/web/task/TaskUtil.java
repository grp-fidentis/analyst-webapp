package cz.fidentis.web.task;

import cz.fidentis.web.humanface.HumanFaceEntity;

import java.util.List;

/**
 * @author Ondřej Bazala
 * @since 29.10.2023
 */
public final class TaskUtil {

    private TaskUtil() {}

    public static boolean isSingleFaceTask(List<HumanFaceEntity> facesOfTask) {
        return facesOfTask.size() == 1;
    }

    public static boolean isPairTask(List<HumanFaceEntity> facesOfTask) {
        return facesOfTask.size() == 2;
    }

    public static boolean isBatchTask(List<HumanFaceEntity> facesOfTask) {
        return facesOfTask.size() == 2;
    }
}

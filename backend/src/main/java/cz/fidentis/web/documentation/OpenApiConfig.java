package cz.fidentis.web.documentation;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ondřej Bazala
 * @since 13.09.2023
 */
@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        Info info = new Info()
                .title("FIDENTIS Analyst Web Application")
                .summary("Web application for visualizing and analysing 3D human faces")
                .description("""
                        Web application of [FIDENTIS Analyst II](https://gitlab.fi.muni.cz/grp-fidentis/analyst2) desktop software which is re-implementation of [FIDENTIS Analyst](https://github.com/Fidentis/Analyst).
                        This project is being developed at Masaryk University, Brno, as collaborative project between the [Lasaris](https://lasaris.fi.muni.cz/?lang=en)
                        Laboratory at Faculty of Informatics and the Department of Anthropology at Faculty of Science.
                        """)
                .version("0.1.0");

        return new OpenAPI().info(info);
    }

    @Bean
    GroupedOpenApi analyticalTasksApis() {
        return GroupedOpenApi.builder()
                .group("Analytical Tasks")
                .pathsToMatch("/api/analytical-tasks/**")
                .build();
    }

    @Bean
    GroupedOpenApi authApis() {
        return GroupedOpenApi.builder()
                .group("Auth")
                .pathsToMatch("/api/auth/**")
                .build();
    }

    @Bean
    GroupedOpenApi managementApis() {
        return GroupedOpenApi.builder()
                .group("Management")
                .pathsToMatch("/api/**")
                .pathsToExclude("/api/analytical-tasks/**",
                        "/api/auth/**")
                .build();
    }
}

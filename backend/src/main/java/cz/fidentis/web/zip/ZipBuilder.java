package cz.fidentis.web.zip;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author  Ondřej Bazala
 * @since  30.09.2023
 */
@NoArgsConstructor
public class ZipBuilder {

    private final List<ZipFile> zipFiles = new ArrayList<>();

    public ZipBuilder(ZipFile zipFile) {
        this.zipFiles.add(zipFile);
    }

    public ZipBuilder(List<ZipFile> zipFiles) {
        this.zipFiles.addAll(zipFiles);
    }

    public byte[] build() throws IOException {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream)) {
            for (var fileForZip : zipFiles) {
                writeToZip(fileForZip, zipOutputStream);
            }
            zipOutputStream.finish();

            return byteArrayOutputStream.toByteArray();
        }
    }

    private void writeToZip(ZipFile zipFile, ZipOutputStream zipOutputStream) throws IOException {
        zipOutputStream.putNextEntry(new ZipEntry(zipFile.filename));
        zipOutputStream.write(zipFile.data);
        zipOutputStream.closeEntry();
    }

    public void addZipFile(ZipFile zipFile) {
        zipFiles.add(zipFile);
    }

    public void addZipFile(String filename, byte[] data) {
        zipFiles.add(new ZipFile(filename, data));
    }

    /**
     * @author  Ondřej Bazala
     * @since  30.09.2023
     */
    @Getter
    @AllArgsConstructor
    public static class ZipFile {

        private String filename;

        private byte[] data;
    }
}

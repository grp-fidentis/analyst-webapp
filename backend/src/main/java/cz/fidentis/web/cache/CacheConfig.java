package cz.fidentis.web.cache;

import com.google.common.cache.CacheBuilder;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author Ondřej Bazala
 * @since 03.10.2023
 */
@Configuration
@EnableCaching
public class CacheConfig {

    public static final String TASK_FACES = "TASK_FACES";

    @Bean
    public CacheManager cacheManager() {
        return new TtlConcurrentMapCacheManager();
    }

    /**
     * @author Ondřej Bazala
     * @since 03.10.2023
     */
    private static class TtlConcurrentMapCacheManager extends ConcurrentMapCacheManager {

        @Override
        protected Cache createConcurrentMapCache(final String name) {
            return new ConcurrentMapCache(name,
                    CacheBuilder.newBuilder()
                            .expireAfterAccess(15, TimeUnit.MINUTES)
                            .maximumSize(100)
                            .build()
                            .asMap(), false);
        }
    }
}

package cz.fidentis.web.cache;

import com.auth0.jwt.interfaces.Claim;
import cz.fidentis.web.security.jwt.JwtUtils;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Ondřej Bazala
 * @since 03.10.2023
 */
@Component
@RequiredArgsConstructor
public class CacheService {

    private HttpServletRequest request;

    private final JwtUtils jwtUtils;

    public String getTokenId() {
        String token = request.getHeader("Authorization").split("Bearer ")[1];
        Claim id = jwtUtils.getClaimFromJwt(token, "id");
        return id.asString();
    }

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
}

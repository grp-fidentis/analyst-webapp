package cz.fidentis.web.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.fidentis.web.user.User;
import cz.fidentis.web.security.JwtAuthenticationFailureHandler;
import cz.fidentis.web.security.jwt.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Petr Hendrych
 * @since 30.11.2022
 */
@Slf4j
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final ObjectMapper objectMapper;

    public AuthenticationFilter(AuthenticationManager authenticationManager, ApplicationContext context, ObjectMapper mapper) {
        super.setAuthenticationFailureHandler(new JwtAuthenticationFailureHandler(mapper));
        this.objectMapper = mapper;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = context.getBean(JwtUtils.class);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String username = this.obtainUsername(request);
        String password = this.obtainPassword(request);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException {
        String access_token = jwtUtils.generateToken(authentication, request, true);
        String refresh_token = jwtUtils.generateToken(authentication, request, false);
        User user = (User) authentication.getPrincipal();

        response.setHeader("Access-Token", access_token);
        response.setHeader("Refresh-Token", refresh_token);
        objectMapper.writeValue(response.getOutputStream(), user);
    }
}

package cz.fidentis.web.security.permission;

import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.user.User;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.task.Task;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Petr Hendrych
 * @since 21.03.2023
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class EntityPermissionService {

    private final EntityManager em;

    private final Map<Class<?>, Function<Object, User>> entityToOwnerMapper = Map.of(
            User.class, User.class::cast,
            Project.class, entity -> ((Project) entity).getUser(),
            Task.class, entity -> ((Task) entity).getProject().getUser(),
            FileUpload.class, entity -> ((FileUpload) entity).getUser(),
            FaceInfo.class, entity -> ((FaceInfo) entity).getFileUploads().stream().findAny().orElseThrow().getUser(),
            HumanFaceEntity.class, entity -> ((HumanFaceEntity) entity).getTask().getProject().getUser());

    @Transactional
    public <T> boolean isOwnerOfEntities(Class<T> entityClass, List<Long> ids, User currentUser) {
        List<T> entities = safeFindAllByIds(entityClass, ids, currentUser.getId());

        return entities.stream().allMatch(entity -> {
            Function<Object, User> getUser = entityToOwnerMapper.getOrDefault(entityClass, null);
            if (getUser == null) {
                throw new IllegalStateException("Forbidden state with not recognized entity in Permission evaluation");
            }
            return getUser.apply(entity).equals(currentUser);
            });
    }

    private <T> List<T> safeFindAllByIds(Class<T> entityClass, List<Long> ids, Long authenticateUserId) {
        String idsInString = ids.stream()
                .map(Object::toString)
                .collect(Collectors.joining(", "));

        if (entityClass == null || ids.isEmpty()) {
            throw new IllegalArgumentException("Cannot find entities of class " + entityClass + " with primary keys: " + idsInString);
        }

        CriteriaQuery<T> criteriaQuery = em.getCriteriaBuilder().createQuery(entityClass);
        Root<T> root = criteriaQuery.from(entityClass);
        criteriaQuery.where(root.get("id").in(ids));
        List<T> entities = em.createQuery(criteriaQuery).getResultList();

        if (entities == null || entities.isEmpty()) {
            log.error("Cannot find instances of " + entityClass.getName() + " with ids: " + ids + " for user with id: " + authenticateUserId);
            throw new EntityNotFoundException("Cannot find " + entityClass.getName() + " entities with id: " + idsInString);
        }

        return entities;
    }
}

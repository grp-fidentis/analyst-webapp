package cz.fidentis.web.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.fidentis.web.security.jwt.JwtConfig;
import cz.fidentis.web.security.jwt.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Petr Hendrych
 * @since 30.11.2022
 */
@Slf4j
public class AuthorizationFilter extends OncePerRequestFilter {

    private final JwtUtils jwtUtils;
    private final JwtConfig jwtConfig;
    private final ObjectMapper objectMapper;

    public AuthorizationFilter(ApplicationContext context, ObjectMapper mapper) {
        this.jwtUtils = context.getBean(JwtUtils.class);
        this.objectMapper = mapper;
        this.jwtConfig = context.getBean(JwtConfig.class);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getServletPath().equals("/api/auth/login") || request.getServletPath().equals("/api/auth/refresh-token")) {
            // TODO: check if /reset-password and /forgot-password pass this also
            filterChain.doFilter(request, response);
        } else {
            String authorizationHeader = request.getHeader(AUTHORIZATION);
            if (authorizationHeader != null && authorizationHeader.startsWith(jwtConfig.getTokenPrefix())) {
                try {
                    String token = authorizationHeader.substring(jwtConfig.getTokenPrefix().length());
                    String username = jwtUtils.getSubjectFromJwt(token);
                    String[] roles = jwtUtils.getClaimFromJwt(token, "roles").asArray(String.class);

                    Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
                    Arrays.stream(roles).forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));

                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, null, authorities);
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);

                    filterChain.doFilter(request, response);
                } catch (Exception exception) {
                    handleException(response, exception);
                }
            } else {
                filterChain.doFilter(request, response);
            }
        }
    }

    private void handleException(HttpServletResponse response, Exception e) throws IOException {
        Map<String, String> error = new HashMap<>();
        error.put("message", e.getMessage());

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(APPLICATION_JSON_VALUE);
        objectMapper.writeValue(response.getOutputStream(), error);
    }
}

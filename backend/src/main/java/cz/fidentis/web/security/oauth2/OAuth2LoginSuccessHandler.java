package cz.fidentis.web.security.oauth2;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.fidentis.web.auth.AuthService;
import cz.fidentis.web.security.jwt.JwtUtils;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.dto.UserDetailDto;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Base64;
import java.util.Collections;
import java.util.Map;

/**
 * @author Adam Majzlik
 */
@Component
public class OAuth2LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    private JwtUtils tokenProvider;

    @Autowired
    private AuthService authService;

    @Autowired
    private ObjectMapper objectMapper;

    private final String redirectUri = "/oauth2/redirect";

    @Override
    protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (!(authentication instanceof OAuth2AuthenticationToken)) {
            return;
        }

        // getting info from token obtained from oauth2/oidc provider
        OAuth2AuthenticationToken oAuth2Token = (OAuth2AuthenticationToken) authentication;
        String provider = oAuth2Token.getAuthorizedClientRegistrationId();
        UserDetailDto dto = getUserDetailDto(authentication, provider);

        // creating or updating user who wants to login
        User user = authService.loginOAuth2User(dto);

        // generating tokens
        String requestUrl = request.getRequestURL().toString();
        String accessToken = tokenProvider.generateToken(user, requestUrl, true);
        String refreshToken = tokenProvider.generateToken(user, requestUrl, false);

        // adding tokens and user to response
        addTokensToResponse(response, accessToken, refreshToken);
        addEncodedUserToResponse(response, user);

        // redirect to frontend
        getRedirectStrategy().sendRedirect(request, response, redirectUri);
    }

    private void addTokensToResponse(HttpServletResponse response, String accessToken, String refreshToken) {
        Cookie accessTokenCookie = new Cookie("Access-Token", accessToken);
        accessTokenCookie.setPath("/");
        accessTokenCookie.setSecure(true);
        response.addCookie(accessTokenCookie);

        Cookie refreshTokenCookie = new Cookie("Refresh-Token", refreshToken);
        refreshTokenCookie.setPath("/");
        refreshTokenCookie.setSecure(true);
        response.addCookie(refreshTokenCookie);
    }

    private void addEncodedUserToResponse(HttpServletResponse response, User user) throws IOException {
        String encodedString = Base64.getEncoder().encodeToString(objectMapper.writeValueAsString(user).getBytes());
        Cookie userCookie = new Cookie("User", encodedString);
        userCookie.setPath("/");
        userCookie.setSecure(true);
        response.addCookie(userCookie);
    }

    private UserDetailDto getUserDetailDto(Authentication authentication, String provider) {
        DefaultOAuth2User principal = (DefaultOAuth2User) authentication.getPrincipal();
        Map<String, Object> attributes = Collections.unmodifiableMap(principal.getAttributes());
        // For adding another OAuth2/OIDC provider add condition
        if (provider.equals(LoginProviders.GITHUB.name().toLowerCase())) {
            return getGithubUserDetailDto(attributes);
        }
        // returns MUNI user detail dto
        return getMuniUserDetailDto(attributes);
    }

    private UserDetailDto getGithubUserDetailDto(Map<String, Object> attributes) {
        UserDetailDto dto = new UserDetailDto();
        dto.setUsername(attributes.get("login").toString());
        dto.setLoginType(LoginProviders.GITHUB.name().toUpperCase());
        return dto;
    }

    private UserDetailDto getMuniUserDetailDto(Map<String, Object> attributes) {
        UserDetailDto dto = new UserDetailDto();
        dto.setFirstName(attributes.get("given_name").toString());
        dto.setLastName(attributes.get("family_name").toString());
        dto.setEmail(attributes.get("email").toString());
        dto.setUsername(attributes.get("name").toString());
        dto.setLoginType(LoginProviders.MUNI.name().toUpperCase());
        return dto;
    }
}

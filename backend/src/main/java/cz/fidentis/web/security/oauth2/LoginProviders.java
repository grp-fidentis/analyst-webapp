package cz.fidentis.web.security.oauth2;

/**
 * @author Adam Majzlik
 */
public enum LoginProviders {

    GITHUB,
    MUNI;

}

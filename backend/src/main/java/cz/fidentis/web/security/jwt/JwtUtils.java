package cz.fidentis.web.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import cz.fidentis.web.user.User;
import cz.fidentis.web.role.Role;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.UUID;

/**
 * @author Petr Hendrych
 * @since 30.11.2022
 */
@Component
@AllArgsConstructor
public class JwtUtils {

    private final JwtConfig jwtConfig;

    /**
     * @see #generateToken(User, String, Boolean)
     *
     * User is from Authentication and requestURL from HttpServletRequest
     */
    public String generateToken(Authentication authentication, HttpServletRequest request, Boolean isAccessToken) {
        User user = (User) authentication.getPrincipal();
        String url = request.getRequestURL().toString();
        return generateToken(user, url, isAccessToken);
    }

    /**
     * Generates token.
     *
     * @param user logged user
     * @param requestUrl URL
     * @param isAccessToken decides if isAccessToken or refreshToken
     * @return Access/Refresh token
     */
    public String generateToken(User user, String requestUrl, Boolean isAccessToken) {
        JWTCreator.Builder jwt = JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + (isAccessToken ? jwtConfig.getAccessTokenExpirationInMs() : jwtConfig.getRefreshTokenExpirationInMs())))
                .withIssuer(requestUrl);

        if (isAccessToken) {
            jwt.withClaim("roles", user.getRoles().stream().map(Role::getName).toList())
                    .withClaim("id", UUID.randomUUID().toString());
        }

        return jwt.sign(jwtConfig.getSecretKey());
    }

    public String getSubjectFromJwt(String token) {
        DecodedJWT decodedJWT = getDecodedJWT(token);
        return decodedJWT.getSubject();
    }

    public Claim getClaimFromJwt(String token, String claim) {
        DecodedJWT decodedJWT = getDecodedJWT(token);
        return decodedJWT.getClaim(claim);
    }

    private DecodedJWT getDecodedJWT(String token) {
        JWTVerifier verifier = JWT.require(jwtConfig.getSecretKey()).build();
        return verifier.verify(JWT.decode(token));
    }
}

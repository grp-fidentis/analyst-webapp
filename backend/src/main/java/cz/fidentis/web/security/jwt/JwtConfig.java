package cz.fidentis.web.security.jwt;

import com.auth0.jwt.algorithms.Algorithm;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @author Petr Hendrych
 * @since 01.12.2022
 */
@Data
@NoArgsConstructor
@ConfigurationProperties(prefix = "application.jwt")
public class JwtConfig {

    private String secretKey;
    private String tokenPrefix;
    private Integer accessTokenExpirationInMs;
    private Integer refreshTokenExpirationInMs;

    @Bean
    public Algorithm getSecretKey() {
        return Algorithm.HMAC256(secretKey.getBytes());
    }
}

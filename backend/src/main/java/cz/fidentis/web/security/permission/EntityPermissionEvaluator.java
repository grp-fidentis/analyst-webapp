package cz.fidentis.web.security.permission;

import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.UserService;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.task.Task;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

import jakarta.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Petr Hendrych
 * @since 21.03.2023
 */
@SuppressWarnings("unchecked")
@RequiredArgsConstructor
public class EntityPermissionEvaluator implements PermissionEvaluator {

    private final EntityPermissionService securityService;
    private final UserService userService;

    private final Map<String, Class<?>> targetTypeToClass = Map.of(
            "USER", User.class,
            "PROJECT", Project.class,
            "FILE_UPLOAD", FileUpload.class,
            "TASK", Task.class,
            "HUMAN_FACE", HumanFaceEntity.class,
            "FACE_INFO", FaceInfo.class);

    @Override
    @Transactional
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        String username = (String) authentication.getPrincipal();
        User authUser = userService.getUserByUserName(username);
        List<Long> ids = targetId instanceof Long ? List.of((long) targetId) : (List<Long>) targetId;
        Class<?> entityClass = targetTypeToClass.get(targetType);

        if (permission.equals("isOwner")) {
            return securityService.isOwnerOfEntities(entityClass, ids, authUser);
        }

        return true;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        return false;
    }
}

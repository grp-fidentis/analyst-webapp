package cz.fidentis.web.humanface.dto;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.web.humanface.HumanFaceEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ondřej Bazala
 * @since 08.10.2023
 */
@Data
@NoArgsConstructor
public class HumanFaceSymmetryPlaneDto {

    private List<Float> positions = new ArrayList<>();

    private List<Float> normals = new ArrayList<>();

    private Double precision;

    public HumanFaceSymmetryPlaneDto(HumanFaceEntity humanFaceEntity) {
        Plane plane = humanFaceEntity.getHumanFace().getSymmetryPlane();

        if (!humanFaceEntity.getHumanFace().hasSymmetryPlane()) {
            return;
        }

        FaceStateServices.updateBoundingBox(humanFaceEntity.getHumanFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        MeshFacet mesh = plane.getMesh(humanFaceEntity.getHumanFace().getBoundingBox());
        for (int v = 0; v < mesh.getCornerTable().getSize(); v++) {
            MeshPoint meshPoint = mesh.getVertices().get(mesh.getCornerTable().getRow(v).getVertexIndex());

            Point3d vertex = meshPoint.getPosition();
            positions.add((float) vertex.x);
            positions.add((float) vertex.y);
            positions.add((float) vertex.z);

            Vector3d normal = meshPoint.getNormal();
            normals.add((float) normal.x);
            normals.add((float) normal.y);
            normals.add((float) normal.z);
        }

        precision = humanFaceEntity.getHelperHumanFace().getSymmetryPlanePrecision();
    }
}

package cz.fidentis.web.humanface;

import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlaneDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author Ondřej Bazala
 * @since 29.10.2023
 */
@Getter
@Setter
@NoArgsConstructor
public class HelperHumanFace implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private TaskFaceInfo taskFaceInfo;

    private Double symmetryPlanePrecision;

    private MeshDistances meshDistances;

    private List<CuttingPlaneDto> cuttingPlaneDtos;

    public HelperHumanFace(TaskFaceInfo taskFaceInfo) {
        this.taskFaceInfo = taskFaceInfo;
    }
}

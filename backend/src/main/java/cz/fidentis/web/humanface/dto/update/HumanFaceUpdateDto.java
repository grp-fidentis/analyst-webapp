package cz.fidentis.web.humanface.dto.update;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.web.humanface.HumanFaceEntity;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.List;

import static cz.fidentis.web.humanface.HumanFaceUtil.*;

/**
 * @author Ondřej Bazala
 * @since 08.10.2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HumanFaceUpdateDto {

    @NotNull
    private Long taskId;

    private TransformationDto transformationDto;

    private HumanFaceFeaturePointsUpdateDto featurePointsUpdate;

    private HumanFaceSymmetryPlaneUpdateDto symmetryPlaneUpdate;

    public void assignDataToHumanFace(HumanFaceEntity humanFaceEntity) {
        assignDataToHumanFace(humanFaceEntity, transformationDto);
        if (featurePointsUpdate != null) {
            featurePointsUpdate.assignDataToHumanFace(humanFaceEntity);
        }
        if (symmetryPlaneUpdate != null) {
            symmetryPlaneUpdate.assignDataToHumanFace(humanFaceEntity);
        }
    }

    public static void assignDataToHumanFace(HumanFaceEntity humanFaceEntity, TransformationDto transitionDto) {
        List<MeshFacet> meshFacets = humanFaceEntity.getHumanFace().getMeshModel().getFacets();

        for (MeshFacet meshFacet : meshFacets) {
            for (MeshPoint vertex : meshFacet.getVertices()) {
                if (isTransformation(transitionDto)) {
                    Point3d position = vertex.getPosition();
                    Point3d newPosition = transformPoint(position, transitionDto);

                    position.x = newPosition.x;
                    position.y = newPosition.y;
                    position.z = newPosition.z;
                }

                if (isRotation(transitionDto)) {
                    Vector3d normal = vertex.getNormal();
                    Vector3d newNormal = transformNormal(normal, transitionDto);

                    normal.x = newNormal.x;
                    normal.y = newNormal.y;
                    normal.z = newNormal.z;
                }
            }
        }
    }
}

package cz.fidentis.web.humanface.dto;

import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.TaskFaceInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Petr Hendrych
 * @since 13.04.2023
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HumanFaceDto {

    private Long id;
    private Boolean isAverageFace;
    private Boolean isPrimaryFace;
    private Boolean hasSymmetryPlane;
    private Boolean hasFeaturePoints;
    private TaskFaceInfo info;

    private byte[] texture;

    private DistanceTaskResponseDto distances;
    private HumanFaceSamplesDto samples;
    private HumanFaceGeometryDto geometry;
    private HumanFaceSymmetryPlaneDto symmetryPlane;
    private HumanFaceFeaturePointsDto featurePoints;
    private HumanFaceBoundingBox boundingBox;

    public HumanFaceDto(HumanFaceEntity humanFaceEntity) {
        this.id = humanFaceEntity.getId();
        this.isPrimaryFace = humanFaceEntity.getIsPrimaryFace();
        this.isAverageFace = humanFaceEntity.getIsAverageFace();
        this.hasFeaturePoints = humanFaceEntity.getHumanFace().hasLandmarks();
        this.hasSymmetryPlane = humanFaceEntity.getHumanFace().hasSymmetryPlane();
        this.info = humanFaceEntity.getHelperHumanFace().getTaskFaceInfo();

        this.texture = humanFaceEntity.getTexture();

        this.geometry = new HumanFaceGeometryDto(humanFaceEntity);
        this.boundingBox = new HumanFaceBoundingBox(humanFaceEntity);
        this.featurePoints = hasFeaturePoints ? new HumanFaceFeaturePointsDto(humanFaceEntity) : null;
        this.symmetryPlane = hasSymmetryPlane ? new HumanFaceSymmetryPlaneDto(humanFaceEntity) : null;
    }
}

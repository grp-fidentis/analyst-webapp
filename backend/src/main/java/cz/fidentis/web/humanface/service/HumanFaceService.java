package cz.fidentis.web.humanface.service;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.humanface.dto.update.HumanFaceUpdateDto;
import cz.fidentis.web.task.Task;

import java.io.IOException;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 25.02.2023
 */
public interface HumanFaceService {

    /**
     * Method that by given ID and task ID returns {@link HumanFaceEntity} with deserialized {@link HumanFace} as
     * {@code @transient} and {@code @JsonIgnored} attribute
     *
     * @param faceId ID of HumanFace
     * @param taskId ID of task
     * @return {@code HumanFaceEntity} with recreated {@code HumanFace} as attribute
     */
    HumanFaceEntity getHumanFaceEntityByIdAndTaskId(Long faceId, Long taskId);

    /**
     * Method that fetch all {@code HumanFace} entities on selected task.
     *
     * @param taskId ID of task
     * @return List of {@code HumanFace} entities
     */
    List<HumanFaceEntity> getHumanFaceEntitiesOfTask(Long taskId);

    /**
     * Method that fetch all {@code HumanFace} entities on selected task. These entities are
     * mapped to Dto for FE rendering.
     *
     * @param taskId ID of task
     * @return List of {@code HumanFace} entities mapped into dto for FE
     */
    List<HumanFaceDto> getHumanFaceDtosOfTask(Long taskId);

    /**
     * Method that converts {@code FileUpload} entity (its data) into {@code HumanFace} entity.
     * This entity is simplified and contains {@code CornerTable} with indices
     *
     * @param geometryData {@code FileUpload} geometry data
     * @param featurePointsData {@code FileUpload} feature points data
     * @return New {@code HumanFace} entity from desktop application
     * @throws IOException When working with files fails
     */
    HumanFace loadHumanFaceFromFile(byte[] geometryData, byte[] featurePointsData) throws IOException;

    /**
     * Saves {@link HumanFaceEntity} and if {@link HumanFaceEntity#getHumanFace()} is not null,
     * it will try to deserialize it and save as {@link HumanFaceEntity#getHumanFaceDump()}
     *
     * @param taskId ID of task
     * @param humanFaceEntity entity to be saved
     */
    void save(Long taskId, HumanFaceEntity humanFaceEntity);

    /**
     * Updates selected {@link HumanFaceEntity} with new points and normals
     *
     * @param id ID of HumanFaceEntity to update
     * @param faceUpdateDto dto with face updates
     */
    void saveGeometry(Long id, HumanFaceUpdateDto faceUpdateDto);

    /**
     * Method that fetches {@link HumanFaceDto} by file upload ID and task ID
     *
     * @param fileUploadId ID of FileUpload
     * @param taskId ID of Task
     * @return {@link HumanFaceDto}
     */
    HumanFaceDto getHumanFaceDtoByFileUploadIdAndTaskId(Long fileUploadId, Long taskId);

    /**
     * Method that fetches {@link HumanFaceEntity} by its ID
     *
     * @param id ID of HumanFaceEntity
     * @return {@link HumanFaceEntity} with selected ID
     */
    HumanFaceEntity getHumanFaceEntityById(Long id);

    /**
     * Method that fetches {@link HumanFaceEntity} by fileUpload ID and task ID
     *
     * @param fileUploadId ID of FileUpload
     * @param taskId ID of task
     * @param withFaceDump if true, {@link HumanFaceEntity#getHumanFaceDump()} will be deserialized
     * @return {@link HumanFaceEntity}
     */
    HumanFaceEntity getHumanFaceEntityByFileUploadIdAndTaskId(Long fileUploadId, Long taskId, boolean withFaceDump);

    /**
     * Method that creates new {@link HumanFaceEntity} and returns it
     *
     * @param fileUpload file upload with data
     * @param task task to which the face belongs
     * @return new {@link HumanFaceEntity}
     */
    HumanFaceEntity createAndSaveHumanFaceEntity(FileUpload fileUpload, Task task);

    /**
     * Method that creates new {@link HumanFaceEntity} and returns its {@link HumanFace}
     *
     * @param fileUpload file upload with data
     * @param task task to which the face belongs
     * @return new {@link HumanFace}
     */
    HumanFace createAndReturnHumanFace(FileUpload fileUpload, Task task);

    /**
     * Method that fetches human faces by their name
     *
     * @param taskId ID of task
     * @param rowFaceName name of first face
     * @param columnFaceName name of second face
     * @return list of {@link HumanFaceDto} with human faces
     */
    List<HumanFaceDto> getHumanFacesPair(Long taskId, String rowFaceName, String columnFaceName);
}

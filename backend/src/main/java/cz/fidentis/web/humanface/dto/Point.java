package cz.fidentis.web.humanface.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;

/**
 * @author Ondřej Bazala
 * @since 08.10.2023
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Point {

    private float x;
    private float y;
    private float z;

    public Point(double x, double y, double z) {
        this.x = (float) x;
        this.y = (float) y;
        this.z = (float) z;
    }

    public Point(Tuple3d tuple) {
        this.x = (float) tuple.x;
        this.y = (float) tuple.y;
        this.z = (float) tuple.z;
    }

    public Point3d toPoint3d() {
        return new Point3d(x, y, z);
    }
}

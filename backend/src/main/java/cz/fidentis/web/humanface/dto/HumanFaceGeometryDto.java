package cz.fidentis.web.humanface.dto;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.web.humanface.HumanFaceEntity;
import lombok.Data;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.List;

import static cz.fidentis.web.humanface.HumanFaceUtil.roundToFourDigits;

/**
 * @author Ondřej Bazala
 * @since 09.09.2023
 */
@Data
public class HumanFaceGeometryDto {
    private static final int INDICES_OF_TRIANGLE = 3;
    private static final int COORDINATES_OF_VERTEX = 3;
    private static final int UV_COORDINATES_OF_VERTEX = 2;

    private int numberOfFacets;

    private int[] indices;
    private float[] positions;
    private float[] normals;
    private float[] uvs;

    public HumanFaceGeometryDto(HumanFaceEntity humanFaceEntity) {
        this(humanFaceEntity, false);
    }

    public HumanFaceGeometryDto(HumanFaceEntity humanFaceEntity, boolean positionsAndNormalsOnly) {
        List<MeshFacet> facets = humanFaceEntity.getHumanFace().getMeshModel().getFacets();
        this.numberOfFacets = facets.size();

        int indicesArraySize;
        indices = new int[0];
        int uvsArraySize;
        uvs = new float[0];
        if (!positionsAndNormalsOnly) {
            indicesArraySize = facets.stream()
                    .mapToInt(f -> f.getTriangles().size()).sum() * INDICES_OF_TRIANGLE;
            indices = new int[indicesArraySize];

            uvsArraySize = facets.stream().mapToInt(f -> f.getVertices().size()).sum() * UV_COORDINATES_OF_VERTEX;
            uvs = new float[uvsArraySize];
        }

        int verticesArraySize = facets.stream()
                .mapToInt(f -> f.getVertices().size()).sum() * COORDINATES_OF_VERTEX;
        positions = new float[verticesArraySize];
        normals = new float[verticesArraySize];

        int indicesCounter = 0;
        int positionCounter = 0;
        int normalsCounter = 0;
        int uvsCounter = 0;
        for (MeshFacet facet : facets) {
            if (!positionsAndNormalsOnly) {
                for (MeshTriangle points : facet.getTriangles()) {
                    indices[indicesCounter++] = points.getIndex1();
                    indices[indicesCounter++] = points.getIndex2();
                    indices[indicesCounter++] = points.getIndex3();
                }
            }

            for (MeshPoint meshPoint : facet.getVertices()) {
                Point3d position = meshPoint.getPosition();
                positions[positionCounter++] = roundToFourDigits(position.x);
                positions[positionCounter++] = roundToFourDigits(position.y);
                positions[positionCounter++] = roundToFourDigits(position.z);

                Vector3d normal = meshPoint.getNormal();
                normals[normalsCounter++] = roundToFourDigits(normal.x);
                normals[normalsCounter++] = roundToFourDigits(normal.y);
                normals[normalsCounter++] = roundToFourDigits(normal.z);

                if (!positionsAndNormalsOnly) {
                    Vector3d texCoord = meshPoint.getTexCoord();
                    if (texCoord != null) {
                        uvs[uvsCounter++] = roundToFourDigits(texCoord.x);
                        uvs[uvsCounter++] = roundToFourDigits(texCoord.y);
                    }
                }
            }
        }
    }
}

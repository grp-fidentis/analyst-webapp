package cz.fidentis.web.humanface.dto.update;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.analyst.math.Quaternion;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.Point;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.vecmath.Vector3d;

/**
 * @author Ondřej Bazala
 * @since 08.10.2023
 */
@Data
@NoArgsConstructor
public class HumanFaceSymmetryPlaneUpdateDto {

    private TransformationDto symmetryTransformationDto;

    public void assignDataToHumanFace(HumanFaceEntity humanFaceEntity) {
        HumanFace face = humanFaceEntity.getHumanFace();
        if (face.hasSymmetryPlane()) {
            Point rotation = symmetryTransformationDto.getRotation();
            Point translation = symmetryTransformationDto.getTranslation();

            Vector3d translationVector = new Vector3d(translation.toPoint3d());
            Quaternion rotationQuaternion = new Quaternion(rotation.getX(), rotation.getY(), rotation.getZ(), 1.0);

            Plane newPlane = FaceRegistrationServices.transformPlane(
                    face.getSymmetryPlane(),
                    rotationQuaternion,
                    translationVector,
                    1);
            face.setSymmetryPlane(newPlane);
        }
    }
}

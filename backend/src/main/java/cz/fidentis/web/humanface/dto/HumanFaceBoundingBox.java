package cz.fidentis.web.humanface.dto;

import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.web.humanface.HumanFaceEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Ondřej Bazala
 * @since 11.11.2023
 */
@Getter
@Setter
@AllArgsConstructor
public class HumanFaceBoundingBox {

    private Point min;

    private Point max;

    private Point mid;

    private double diagonalLength;

    public HumanFaceBoundingBox(HumanFaceEntity humanFaceEntity) {
        FaceStateServices.updateBoundingBox(humanFaceEntity.getHumanFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        Box box = humanFaceEntity.getHumanFace().getBoundingBox();

        min = new Point(box.minPoint());
        max = new Point(box.maxPoint());
        mid = new Point(box.midPoint());
        diagonalLength = box.diagonalLength();
    }
}
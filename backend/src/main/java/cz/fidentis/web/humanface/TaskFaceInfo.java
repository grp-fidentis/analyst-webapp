package cz.fidentis.web.humanface;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author Ondřej Bazala
 * @since 26.11.2023
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskFaceInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String geometryFileName;

    private Long geometryFileSize;

    private String featurePointsFileName;

    private String previewPhotoFileName;

    private String textureFileName;

    private byte[] previewPhotoData;

    private Long numberOfVertices;

    private Integer numberOfFacets;

    private List<String> featurePointsNames;

    public TaskFaceInfo(FaceInfo faceInfo, HumanFace face) {
        this.geometryFileName = faceInfo.getGeometryFileName();
        this.geometryFileSize = faceInfo.getGeometryFileSize();
        this.featurePointsFileName = faceInfo.getFeaturePointsFileName();
        this.previewPhotoFileName = faceInfo.getPreviewPhotoFileName();
        this.textureFileName = faceInfo.getTextureFileName();
        this.previewPhotoData = faceInfo.getPreviewPhotoData();
        this.numberOfVertices = faceInfo.getNumberOfVertices();
        this.numberOfFacets = faceInfo.getNumberOfFacets();
        this.featurePointsNames = face.hasLandmarks()
                ? face.getAllLandmarks().stream().map(f -> f.getName()).toList()
                : null;
    }
}

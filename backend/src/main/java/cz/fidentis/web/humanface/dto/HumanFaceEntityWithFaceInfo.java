package cz.fidentis.web.humanface.dto;

import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.humanface.HumanFaceEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Patrik Tomov
 * @since  04.09.2024
 */
@Getter
@Setter
@AllArgsConstructor
public class HumanFaceEntityWithFaceInfo {
    private final HumanFaceEntity humanFaceEntity;
    private final FaceInfo faceInfo;
}

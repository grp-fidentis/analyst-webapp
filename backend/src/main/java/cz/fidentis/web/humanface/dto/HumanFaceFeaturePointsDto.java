package cz.fidentis.web.humanface.dto;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;
import cz.fidentis.web.humanface.HumanFaceEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ondřej Bazala
 * @since 08.10.2023
 */
@Data
public class HumanFaceFeaturePointsDto {

    private static final double DEFAULT_FEATURE_POINT_SIZE = 20;

    private List<FeaturePointDto> featurePointDtos = new ArrayList<>();

    public HumanFaceFeaturePointsDto(HumanFaceEntity humanFaceEntity) {
        List<Landmark> featurePoints = humanFaceEntity.getHumanFace().getAllLandmarks();
        featurePoints.forEach(f -> {
            FeaturePointDto featurePointDto = new FeaturePointDto();

            featurePointDto.setPoint(new Point(f.getPosition()));
            featurePointDto.setFeaturePointType(f.getType());
            featurePointDto.setName(f.getName());
            featurePointDto.setDescription(f.getDescription());
            featurePointDto.setSize(DEFAULT_FEATURE_POINT_SIZE);

            MeshVicinity relation = f.getMeshVicinity();
            if (relation != null) {
                Point nearestPoint = new Point(relation.nearestPoint());
                featurePointDto.setRelationToMesh(new FeaturePointDto.RelationToMeshDto(nearestPoint, relation.distance()));
            }

            featurePointDtos.add(featurePointDto);
        });
    }

    /**
     * @author Ondřej Bazala
     * @since 08.10.2023
     */
    @Data
    private static class FeaturePointDto {

        private Point point;

        private Integer featurePointType;

        private String name;

        private String description;

        private RelationToMeshDto relationToMesh;

        private Double size;

        /**
         * @author Ondřej Bazala
         * @since 08.10.2023
         */
        @Data
        @AllArgsConstructor
        private static class RelationToMeshDto {

            private Point nearestPoint;

            private double distance;
        }
    }
}

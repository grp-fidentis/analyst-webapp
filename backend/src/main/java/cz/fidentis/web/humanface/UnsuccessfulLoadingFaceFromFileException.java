package cz.fidentis.web.humanface;

/**
 * @author Ondřej Bazala
 * @since 04.09.2023
 */
public class UnsuccessfulLoadingFaceFromFileException extends RuntimeException {

    public UnsuccessfulLoadingFaceFromFileException() {
    }

    public UnsuccessfulLoadingFaceFromFileException(String message) {
        super(message);
    }

    public UnsuccessfulLoadingFaceFromFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsuccessfulLoadingFaceFromFileException(Throwable cause) {
        super(cause);
    }

    public UnsuccessfulLoadingFaceFromFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

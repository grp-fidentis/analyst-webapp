package cz.fidentis.web.humanface.dto.update;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.web.humanface.HumanFaceEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.vecmath.Point3d;
import java.util.List;

/**
 * @author Ondřej Bazala
 * @since 08.10.2023
 */
@Data
@NoArgsConstructor
public class HumanFaceFeaturePointsUpdateDto {

    private float[] positions = new float[0];

    private float[] nearestPointsPositions = new float[0];

    public void assignDataToHumanFace(HumanFaceEntity humanFaceEntity) {
        List<Landmark> featurePoints = humanFaceEntity.getHumanFace().getAllLandmarks();

        int positionCounter = 0;
        int nearestPointCounter = 0;
        for (Landmark featurePoint : featurePoints) {
            if (positions.length > 0) {
                Point3d position = featurePoint.getPosition();
                position.x = positions[positionCounter++];
                position.y = positions[positionCounter++];
                position.z = positions[positionCounter++];

                if (featurePoint.getMeshVicinity() != null) {
                    Point3d nearestPointsPosition = featurePoint.getMeshVicinity().nearestPoint();
                    nearestPointsPosition.x = nearestPointsPositions[nearestPointCounter++];
                    nearestPointsPosition.y = nearestPointsPositions[nearestPointCounter++];
                    nearestPointsPosition.z = nearestPointsPositions[nearestPointCounter++];
                }
            }
        }
    }
}

package cz.fidentis.web.humanface.dto;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.vecmath.Point3d;
import java.util.List;

import static cz.fidentis.web.humanface.HumanFaceUtil.roundToFourDigits;

/**
 * @author Ondřej Bazala
 * @since 09.09.2023
 */
@Data
@NoArgsConstructor
public class HumanFaceSamplesDto {

    private static final int COORDINATES_OF_VERTEX = 3;

    private Long humanFaceId;

    private float[] positions;

    public HumanFaceSamplesDto(Long humanFaceId, List<MeshPoint> points) {
        this.humanFaceId = humanFaceId;

        if (points == null) {
            return;
        }

        positions = new float[points.size() * COORDINATES_OF_VERTEX];

        int positionCounter = 0;
        for (MeshPoint meshPoint : points) {
            Point3d position = meshPoint.getPosition();
            positions[positionCounter++] = roundToFourDigits(position.x);
            positions[positionCounter++] = roundToFourDigits(position.y);
            positions[positionCounter++] = roundToFourDigits(position.z);
        }
    }
}

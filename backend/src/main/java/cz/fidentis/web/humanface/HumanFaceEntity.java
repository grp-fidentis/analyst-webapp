package cz.fidentis.web.humanface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.web.base.BaseEntity;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.task.Task;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.SerializationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Petr Hendrych
 * @since 03.03.2023
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "human_face_entity")
public class HumanFaceEntity extends BaseEntity {

    public HumanFaceEntity(Task task, byte[] humanFaceDump, byte[] helperHumanFaceDump, byte[] texture) {
        this.task = task;
        this.humanFaceDump = humanFaceDump;
        this.helperHumanFaceDump = helperHumanFaceDump;
        this.texture = texture;
    }

    public HumanFaceEntity(Task task, HumanFace humanFace, HelperHumanFace helperHumanFace) {
        this.humanFace = humanFace;
        this.humanFaceDump = SerializationUtils.serialize(humanFace);
        this.helperHumanFace = helperHumanFace;
        this.helperHumanFaceDump = SerializationUtils.serialize(helperHumanFace);
        this.task = task;
    }

    @Lob
    @JsonIgnore
    private byte[] humanFaceDump;

    @Transient
    @JsonIgnore
    private HumanFace humanFace;

    @Lob
    private byte[] helperHumanFaceDump;

    @Transient
    @JsonIgnore
    private HelperHumanFace helperHumanFace = new HelperHumanFace();

    @Lob
    private byte[] texture;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    private Task task;

    private Boolean isPrimaryFace = false;

    private Boolean isAverageFace = false;

    @JsonIgnore
    @JoinTable(
            name = "file_upload_human_face_entities",
            joinColumns = @JoinColumn(name = "human_face_entity_id"),
            inverseJoinColumns = @JoinColumn(name = "file_upload_id"))
    @ManyToMany(fetch = FetchType.LAZY)
    private List<FileUpload> fileUploads = new ArrayList<>();
}

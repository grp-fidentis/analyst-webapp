package cz.fidentis.web.humanface;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Petr Hendrych
 * @since 03.03.2023
 */
@Repository
public interface HumanFaceRepo extends JpaRepository<HumanFaceEntity, Long> {

    /**
     * Method to fetch all {@code HumanFaceEntities} on task ordered by ID
     *
     * @param taskId ID of task
     * @return List of {@code HumanFaceEntity} for selected task
     */
    List<HumanFaceEntity> findHumanFaceEntitiesByTaskIdOrderById(Long taskId);

    /**
     * Method to fetch {@code HumanFaceEntity} by file upload id and task id
     *
     * @param fileUploadId ID of {@code FileUpload}
     * @param taskId ID of task
     * @return {@code HumanFaceEntity} for selected file upload and task
     */
    @EntityGraph(attributePaths = "fileUploads")
    @Query("SELECT h FROM HumanFaceEntity h JOIN h.fileUploads f WHERE f.id = :fileUploadId AND h.task.id = :taskId")
    Optional<HumanFaceEntity> findHumanFaceEntityByFileUploadIdAndTaskId(Long fileUploadId, Long taskId);

    /**
     * Method to fetch {@code HumanFaceEntity} by file upload names and task id
     *
     * @param rowFaceName Name of first file name
     * @param columnFaceName Name of second file name
     * @param taskId ID of task
     * @return {@code HumanFaceEntity} for selected file upload names and task
     */
    @EntityGraph(attributePaths = "fileUploads")
    @Query("SELECT h FROM HumanFaceEntity h JOIN h.fileUploads f WHERE f.name IN (:rowFaceName, :columnFaceName) AND h.task.id = :taskId")
    List<HumanFaceEntity> findHumanFaceEntitiesByHumanFaceNames(Long taskId, String rowFaceName, String columnFaceName);

    /**
     * Method to fetch average {@code HumanFaceEntity} by task id and file upload name
     *
     * @param taskId ID of task
     * @return {@code HumanFaceEntity} for selected task and file upload name
     */
    @EntityGraph(attributePaths = "fileUploads")
    @Query("SELECT h FROM HumanFaceEntity h JOIN h.fileUploads f WHERE h.task.id = :taskId AND f.isAverageFace = true")
    Optional<HumanFaceEntity> findAverageFaceEntityByTaskId(Long taskId);

    /**
     * Method to fetch first {@code HumanFaceEntity} by task id
     *
     * @param taskId ID of task
     * @return {@code HumanFaceEntity} for selected task
     */
    Optional<HumanFaceEntity> findFirstByTaskId(Long taskId);
}

package cz.fidentis.web.humanface.dto.update;

import cz.fidentis.web.humanface.dto.Point;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Ondřej Bazala
 * @since 11.11.2023
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TransformationDto {

    private Point translation;

    private Point rotation;

    public static TransformationDto getReversed(TransformationDto dto) {
        Point trans = dto.getTranslation();
        Point rot = dto.getRotation();

        Point reversedTrans = new Point(trans.getX() * -1, trans.getY() * -1, trans.getZ() * -1);
        Point reversedRot = new Point(rot.getX() * -1, rot.getY() * -1, rot.getZ() * -1);

        return new TransformationDto(reversedTrans, reversedRot);
    }
}

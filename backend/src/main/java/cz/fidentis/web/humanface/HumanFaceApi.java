package cz.fidentis.web.humanface;

import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.humanface.dto.update.HumanFaceUpdateDto;
import cz.fidentis.web.humanface.service.HumanFaceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Petr Hendrych
 * @since 25.02.2023
 */
@RestController
@Tag(name = "Human Face API", description = "API for managing and retrieving human faces")
@RequestMapping("/api/human-faces")
@RequiredArgsConstructor
public class HumanFaceApi {

    private final HumanFaceService humanFaceService;

    @GetMapping("/task/{taskId}/file-upload/{fileUploadId}")
    @Operation(summary = "Gets human face by file upload id and task id")
    @PreAuthorize("isOwner(#taskId, 'TASK')")
    public ResponseEntity<HumanFaceDto> getHumanFaceByFileUploadIdAndTaskId(@PathVariable Long taskId, @PathVariable Long fileUploadId) {
        return ResponseEntity.ok(humanFaceService.getHumanFaceDtoByFileUploadIdAndTaskId(fileUploadId, taskId));
    }

    @GetMapping("/task/{taskId}")
    @Operation(summary = "Gets human faces of selected task")
    @PreAuthorize("isOwner(#taskId, 'TASK')")
    public ResponseEntity<List<HumanFaceDto>> getHumanFaceDtosOfTask(@PathVariable Long taskId) {
        return ResponseEntity.ok(humanFaceService.getHumanFaceDtosOfTask(taskId));
    }

    @PutMapping("/{id}/geometry")
    @Operation(summary = "Updates human face geometry with new positions and normals")
    @PreAuthorize("isOwner(#id, 'HUMAN_FACE')")
    public ResponseEntity<Void> saveGeometry(@PathVariable Long id, @Valid @RequestBody HumanFaceUpdateDto faceUpdateDto) {
        humanFaceService.saveGeometry(id, faceUpdateDto);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/task/{taskId}/get-pair")
    @Operation(summary = "Gets human faces pair by their names")
    public ResponseEntity<List<HumanFaceDto>> getHumanFaceByFileUploadIdAndTaskId(
            @PathVariable Long taskId,
            @RequestParam @NotBlank String rowFaceName,
            String columnFaceName) {
        return ResponseEntity.ok(humanFaceService.getHumanFacesPair(taskId, rowFaceName, columnFaceName));
    }
}

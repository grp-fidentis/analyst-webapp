package cz.fidentis.web.humanface.service;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceFactory;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.FileUploadRepo;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.humanface.*;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.humanface.dto.update.HumanFaceUpdateDto;
import cz.fidentis.web.task.Task;
import cz.fidentis.web.task.TaskRepo;
import cz.fidentis.web.task.TaskType;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static cz.fidentis.web.fileupload.FileExtensions.CSV_EXTENSION;
import static cz.fidentis.web.fileupload.FileExtensions.TEXTURE_EXTENSION;

/**
 * @author Petr Hendrych
 * @since 25.02.2023
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class HumanFaceServiceImpl implements HumanFaceService {

    @PersistenceContext
    private EntityManager em;

    private final HumanFaceServiceImplHelper humanFaceServiceImplHelper;
    private final FileUploadRepo fileUploadRepo;
    private final HumanFaceRepo humanFaceRepo;
    private final TaskRepo taskRepo;

    @Override
    public HumanFaceEntity getHumanFaceEntityByIdAndTaskId(Long faceId, Long taskId) {
        List<HumanFaceEntity> facesOfTask = getHumanFaceEntitiesOfTask(taskId);

        return facesOfTask.stream()
                .filter(f -> f.getId().equals(faceId)).findAny()
                .orElseThrow(() -> new EntityNotFoundException("HumanFace with id: %s does not exist".formatted(faceId)));
    }

    @Override
    public List<HumanFaceEntity> getHumanFaceEntitiesOfTask(Long taskId) {
        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new EntityNotFoundException("Task with id: %s does not exist".formatted(taskId)));
        List<HumanFaceEntity> faces = null;

        if (task.getTaskType().equals(TaskType.BATCH_PROCESSING)) {
            faces = humanFaceServiceImplHelper.getHumanFaceEntitiesForBatchProcessingTask(taskId);
        } else {
            faces = humanFaceServiceImplHelper.getHumanFaceEntitiesOfTask(taskId);
            if (!faces.getFirst().getTask().getId().equals(taskId)) {
                humanFaceServiceImplHelper.deleteCache();
                faces = humanFaceServiceImplHelper.getHumanFaceEntitiesOfTask(taskId);
            }
        }

        return faces;
    }

    @Override
    public List<HumanFaceDto> getHumanFaceDtosOfTask(Long taskId) {
        return getHumanFaceEntitiesOfTask(taskId).stream()
                .map(HumanFaceDto::new)
                .toList();
    }

    @Override
    public HumanFace loadHumanFaceFromFile(byte[] geometryData, byte[] featurePointsData) throws IOException {
        File geometryFile = Files.createTempFile("geometry", ".obj").toFile();

        File featurePointsFile = null;
        if (featurePointsData != null) {
            Path featurePointsPath = Path.of(geometryFile.getPath().split(".obj")[0] + "_landmarks.csv");
            featurePointsFile = Files.createFile(featurePointsPath).toFile();
        }

        HumanFace face;
        try (
                BufferedOutputStream geometryStream = new BufferedOutputStream(new FileOutputStream(geometryFile));
                BufferedOutputStream featurePointsStream = featurePointsFile != null ?
                        new BufferedOutputStream(new FileOutputStream(featurePointsFile)) : null
        ) {
            geometryStream.write(geometryData);
            geometryStream.flush();

            if (featurePointsStream != null) {
                featurePointsStream.write(featurePointsData);
                featurePointsStream.flush();
            }

            face = HumanFaceFactory.create(geometryFile);
        } finally {
            Files.delete(geometryFile.toPath());
            if (featurePointsFile != null) {
                Files.delete(featurePointsFile.toPath());
            }
        }

        return face;
    }

    @Override
    public void save(Long taskId, HumanFaceEntity updatedEntity) {
        List<HumanFaceEntity> facesOfTask = getHumanFaceEntitiesOfTask(taskId);
        humanFaceServiceImplHelper.saveCache(updatedEntity, facesOfTask);
        humanFaceServiceImplHelper.save(updatedEntity);
    }

    @Override
    public void saveGeometry(Long id, HumanFaceUpdateDto faceUpdateDto) {
        HumanFaceEntity humanFaceEntity = getHumanFaceEntityByIdAndTaskId(id, faceUpdateDto.getTaskId());

        faceUpdateDto.assignDataToHumanFace(humanFaceEntity);

        save(faceUpdateDto.getTaskId(), humanFaceEntity);
    }

    @Override
    @Transactional
    public HumanFaceDto getHumanFaceDtoByFileUploadIdAndTaskId(Long fileUploadId, Long taskId) {
        return humanFaceServiceImplHelper.getHumanFaceEntityByFileUploadIdAndTaskId(fileUploadId, taskId)
                .map(humanFace -> {
                    humanFace.setHumanFace(SerializationUtils.deserialize(humanFace.getHumanFaceDump()));
                    humanFace.setHelperHumanFace(SerializationUtils.deserialize(humanFace.getHelperHumanFaceDump()));
                    return humanFace;
                })
                .map(HumanFaceDto::new)
                .orElseGet(() -> {
                    FileUpload fileUpload = fileUploadRepo.findFileUploadById(fileUploadId)
                            .orElseThrow(() -> new EntityNotFoundException("FileUpload with id: %s does not exist".formatted(fileUploadId)));
                    Task task = taskRepo.findById(taskId)
                            .orElseThrow(() -> new EntityNotFoundException("Task with id: %s does not exist".formatted(taskId)));
                    HumanFaceEntity newHumanFace = createAndSaveHumanFaceEntity(fileUpload, task);
                    newHumanFace.setHumanFace(SerializationUtils.deserialize(newHumanFace.getHumanFaceDump()));
                    newHumanFace.setHelperHumanFace(SerializationUtils.deserialize(newHumanFace.getHelperHumanFaceDump()));
                    humanFaceRepo.save(newHumanFace);
                    return new HumanFaceDto(newHumanFace);
                });
    }

    @Override
    public HumanFaceEntity getHumanFaceEntityById(Long id) {
        return humanFaceServiceImplHelper.getHumanFaceEntityById(id)
                .orElseThrow(() -> new EntityNotFoundException("Human face with id: %s does not exist".formatted(id)));
    }

    @Override
    public HumanFaceEntity getHumanFaceEntityByFileUploadIdAndTaskId(Long fileUploadId, Long taskId, boolean withFaceDump) {
        return humanFaceServiceImplHelper.getHumanFaceEntityByFileUploadIdAndTaskId(fileUploadId, taskId)
                .map(humanFace -> {
                    if (withFaceDump) {
                        humanFace.setHumanFace(SerializationUtils.deserialize(humanFace.getHumanFaceDump()));
                    }
                    humanFace.setHelperHumanFace(SerializationUtils.deserialize(humanFace.getHelperHumanFaceDump()));
                    return humanFace;
                })
                .orElseThrow(() -> new EntityNotFoundException("Human face with fileUploadId: %s and taskId: %s does not exist".formatted(fileUploadId, taskId)));
    }

    @Override
    @Transactional
    public HumanFaceEntity createAndSaveHumanFaceEntity(FileUpload fileUpload, Task task) {
        HumanFace face = createHumanFace(fileUpload, task);
        return saveHumanFaceEntity(fileUpload, task, face);
    }

    @Override
    @Transactional
    public HumanFace createAndReturnHumanFace(FileUpload fileUpload, Task task) {
        HumanFace face = createHumanFace(fileUpload, task);
        saveHumanFaceEntity(fileUpload, task, face);
        em.flush();

        return face;
    }

    @Override
    @Transactional(readOnly = true)
    public List<HumanFaceDto> getHumanFacesPair(Long taskId, String rowFaceName, String columnFaceName) {
        List<HumanFaceEntity> humanFaceEntities = humanFaceRepo.findHumanFaceEntitiesByHumanFaceNames(taskId, rowFaceName, columnFaceName);

        humanFaceEntities.forEach(entity -> {
            entity.setHumanFace(SerializationUtils.deserialize(entity.getHumanFaceDump()));
            entity.setHelperHumanFace(SerializationUtils.deserialize(entity.getHelperHumanFaceDump()));
        });

        return humanFaceEntities.stream()
                .map(HumanFaceDto::new)
                .toList();
    }

    private void setFaceInfoGeometryInfo(FaceInfo faceInfo, HumanFace face) {
        if (faceInfo.getNumberOfFacets() == null) {
            faceInfo.setNumberOfFacets(face.getMeshModel().getFacets().size());
        }
        if (faceInfo.getNumberOfVertices() == null) {
            faceInfo.setNumberOfVertices(face.getMeshModel().getNumVertices());
        }
    }


    private HumanFace createHumanFace(FileUpload fileUpload, Task task) {
        FaceInfo faceInfo = fileUpload.getFaceInfo();
        byte[] featurePoints = loadFeaturePoints(fileUpload, faceInfo);

        HumanFace face;
        try {
            face = loadHumanFaceFromFile(fileUpload.getFileData().getData(), featurePoints);
        } catch (IOException e) {
            throw new UnsuccessfulLoadingFaceFromFileException("Error while loading HumanFace from file", e);
        }

        return face;
    }

    private HumanFaceEntity saveHumanFaceEntity(FileUpload fileUpload, Task task, HumanFace face) {
        FaceInfo faceInfo = fileUpload.getFaceInfo();
        byte[] texture = loadTexture(fileUpload, faceInfo);
        setFaceInfoGeometryInfo(faceInfo, face);

        HelperHumanFace helperHumanFace = new HelperHumanFace(new TaskFaceInfo(faceInfo, face));

        HumanFaceEntity humanFaceEntity = new HumanFaceEntity(
                task,
                SerializationUtils.serialize(face),
                SerializationUtils.serialize(helperHumanFace),
                texture
        );

        fileUpload.addHumanFaceEntity(humanFaceEntity);
        return humanFaceRepo.save(humanFaceEntity);
    }

    private byte[] loadTexture(FileUpload fileUpload, FaceInfo faceInfo) {
        return fileUploadRepo.findFileUploadByNameAndExtensionAndFaceInfoId(
                        fileUpload.getName(), TEXTURE_EXTENSION, faceInfo.getId())
                .map(textureFile -> textureFile.getFileData().getData())
                .orElse(null);
    }


    private byte[] loadFeaturePoints(FileUpload fileUpload, FaceInfo faceInfo) {
        return fileUploadRepo.findFileUploadByNameAndExtensionAndFaceInfoId(
                        fileUpload.getName() + "_landmarks", CSV_EXTENSION, faceInfo.getId())
                .map(featurePointsFile -> featurePointsFile.getFileData().getData())
                .orElse(null);
    }
}

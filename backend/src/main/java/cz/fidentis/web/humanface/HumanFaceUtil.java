package cz.fidentis.web.humanface;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.math.Quaternion;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.humanface.dto.Point;
import cz.fidentis.web.humanface.dto.update.TransformationDto;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ondřej Bazala
 * @since 03.10.2023
 */
public final class HumanFaceUtil {

    private HumanFaceUtil() {}

    public static HumanFaceEntity getPrimaryFace(List<HumanFaceEntity> facesOfTask) {
        return facesOfTask.stream().filter(HumanFaceEntity::getIsPrimaryFace).findAny()
                .orElseThrow(() -> new EntityNotFoundException("No primary face"));
    }

    public static HumanFaceEntity getSecondaryFace(List<HumanFaceEntity> facesOfTask) {
        return facesOfTask.stream().filter(face -> !face.getIsPrimaryFace()).findAny()
                .orElseThrow(() -> new EntityNotFoundException("No secondary face"));
    }

    public static float roundToFourDigits(double numberToRound) {
        return (float) Math.floor(numberToRound * 10000) / 10000;
    }

    public static boolean isTransformation(TransformationDto transformationDto) {
        return transformationDto != null && (isRotation(transformationDto) || isTranslation(transformationDto));
    }

    public static boolean isRotation(TransformationDto transformationDto) {
        Point rotation = transformationDto.getRotation();
        return rotation.getX() != 0 || rotation.getY() != 0 || rotation.getZ() != 0;
    }

    private static boolean isTranslation(TransformationDto transformationDto) {
        Point translation = transformationDto.getTranslation();
        return translation.getX() != 0 || translation.getY() != 0 || translation.getZ() != 0;
    }

    public static Point3d transformPoint(Point3d point, TransformationDto transformationDto) {
        if (point == null) {
            return null;
        }

        Vector3d translation = new Vector3d(transformationDto.getTranslation().toPoint3d());
        Quaternion rotation = new Quaternion(transformationDto.getRotation().getX(), transformationDto.getRotation().getY(),
                transformationDto.getRotation().getZ(), 1.0);

        Quaternion rotQuat = new Quaternion(point.x, point.y, point.z, 1);
        Quaternion rotationCopy = Quaternion.multiply(rotQuat, rotation.getConjugate());
        rotQuat = Quaternion.multiply(rotation, rotationCopy);

        return new Point3d(
                rotQuat.x + translation.x,
                rotQuat.y + translation.y,
                rotQuat.z + translation.z
        );
    }

    public static Vector3d transformNormal(Vector3d normal, TransformationDto transformationDto) {
        if (normal == null) {
            return null;
        }

        Point rotation = transformationDto.getRotation();
        Quaternion rotationQuaternion = new Quaternion(rotation.getX(), rotation.getY(), rotation.getZ(), 1.0);

        Quaternion rotQuat = new Quaternion(normal.x, normal.y, normal.z, 1);
        Quaternion rotationCopy = Quaternion.multiply(rotQuat, rotationQuaternion.getConjugate());
        rotQuat = Quaternion.multiply(rotationQuaternion, rotationCopy);

        return new Vector3d(rotQuat.x, rotQuat.y, rotQuat.z);
    }

    public static List<Integer> getValuesFromHeatmap3D(HumanFace face, HeatMap3D heatMap) {
        List<Integer> colors = new ArrayList<>();
        for (MeshFacet facet : face.getMeshModel().getFacets()) {
            if (heatMap.getFacets().contains(facet)) {
                for (int v = 0; v < facet.getVertices().size(); v++) {
                    Color color = heatMap.getColor(facet, v);
                    colors.add(color.getRed());
                    colors.add(color.getGreen());
                    colors.add(color.getBlue());
                }
            }
        }
        return colors;
    }
}

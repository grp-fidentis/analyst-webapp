package cz.fidentis.web.humanface.service;

import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.HumanFaceRepo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cz.fidentis.web.cache.CacheConfig.TASK_FACES;

/**
 * @author Ondřej Bazala
 * @since 03.10.2023
 */
@Service
@RequiredArgsConstructor
public class HumanFaceServiceImplHelper {

    private final HumanFaceRepo humanFaceRepo;

    @Transactional(readOnly = true)
    @Cacheable(value = TASK_FACES, key = "@cacheService.tokenId")
    public List<HumanFaceEntity> getHumanFaceEntitiesOfTask(Long taskId) {
        List<HumanFaceEntity> humanFaceEntities = humanFaceRepo.findHumanFaceEntitiesByTaskIdOrderById(taskId);

        humanFaceEntities.forEach(entity -> {
            entity.setHumanFace(SerializationUtils.deserialize(entity.getHumanFaceDump()));
            entity.setHelperHumanFace(SerializationUtils.deserialize(entity.getHelperHumanFaceDump()));
        });

        return humanFaceEntities;
    }

    @CachePut(value = TASK_FACES, key = "@cacheService.tokenId")
    public List<HumanFaceEntity> saveCache(HumanFaceEntity humanFaceEntity, List<HumanFaceEntity> facesOfTask) {
        facesOfTask.remove(humanFaceEntity);
        facesOfTask.add(humanFaceEntity);

        return facesOfTask;
    }

    @CacheEvict(value = TASK_FACES, key = "@cacheService.tokenId")
    public void deleteCache() { /* Deletes TASK_FACES cache */ }

    @Transactional
    public void save(HumanFaceEntity humanFaceEntity) {
        if (humanFaceEntity.getHumanFace() != null) {
            byte[] faceDump = SerializationUtils.serialize(humanFaceEntity.getHumanFace());
            humanFaceEntity.setHumanFaceDump(faceDump);

            byte[] helperFaceDump = SerializationUtils.serialize(humanFaceEntity.getHelperHumanFace());
            humanFaceEntity.setHelperHumanFaceDump(helperFaceDump);
        }

        humanFaceRepo.save(humanFaceEntity);
    }

    @Transactional(readOnly = true)
    public Optional<HumanFaceEntity> getHumanFaceEntityById(Long id) {
        Optional<HumanFaceEntity> humanFaceEntity = humanFaceRepo.findById(id);
        humanFaceEntity.ifPresent(entity -> {
            entity.setHumanFace(SerializationUtils.deserialize(entity.getHumanFaceDump()));
            entity.setHelperHumanFace(SerializationUtils.deserialize(entity.getHelperHumanFaceDump()));
        });
        return humanFaceEntity;
    }

    @Transactional(readOnly = true)
    public Optional<HumanFaceEntity> getHumanFaceEntityByFileUploadIdAndTaskId(Long fileUploadId, Long taskId) {
        return humanFaceRepo.findHumanFaceEntityByFileUploadIdAndTaskId(fileUploadId, taskId);
    }

    @Transactional(readOnly = true)
    public List<HumanFaceEntity> getHumanFaceEntitiesForBatchProcessingTask(Long taskId) {
        Optional<HumanFaceEntity> firstEntity = humanFaceRepo.findFirstByTaskId(taskId);
        Optional<HumanFaceEntity> averageFaceEntity = humanFaceRepo.findAverageFaceEntityByTaskId(taskId);


        List<HumanFaceEntity> result = new ArrayList<>();
        firstEntity.ifPresent(result::add);
        averageFaceEntity.ifPresent(entity -> {
            if (!result.contains(entity)) {
                result.add(entity);
            }
        });

        result.forEach(entity -> {
            entity.setHumanFace(SerializationUtils.deserialize(entity.getHumanFaceDump()));
            entity.setHelperHumanFace(SerializationUtils.deserialize(entity.getHelperHumanFaceDump()));
        });

        return result;
    }
}

package cz.fidentis.web.compression;

import net.jpountz.lz4.LZ4FrameOutputStream;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.WriteListener;
import jakarta.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

/**
 * @author Ondřej Bazala
 * @since 11.09.2023
 */
public class CompressionResponseStream extends ServletOutputStream {

    private final HttpServletResponse httpServletResponse;
    private final ServletOutputStream servletOutputStream;
    private final ByteArrayOutputStream byteArrayOutputStream;
    private final FilterOutputStream lz4FrameOutputStream;
    private boolean closed = false;

    public CompressionResponseStream(HttpServletResponse httpServletResponse) throws IOException {
        this.httpServletResponse = httpServletResponse;
        this.servletOutputStream = httpServletResponse.getOutputStream();
        this.byteArrayOutputStream = new ByteArrayOutputStream();
        this.lz4FrameOutputStream = new LZ4FrameOutputStream(byteArrayOutputStream);
    }

    @Override
    public void close() throws IOException {
        if (closed) {
            throw new IOException("This output stream has already been closed");
        }
        byte[] bytes = byteArrayOutputStream.toByteArray();
        lz4FrameOutputStream.close();

        httpServletResponse.addHeader("Content-Length", Integer.toString(bytes.length));
        httpServletResponse.addHeader("Custom-Content-Encoding", "true");
        servletOutputStream.write(bytes);
        servletOutputStream.flush();
        servletOutputStream.close();
        closed = true;
    }

    @Override
    public void flush() throws IOException {
        if (closed) {
            throw new IOException("Cannot flush a closed output stream");
        }
        lz4FrameOutputStream.flush();
    }

    @Override
    public void write(int b) throws IOException {
        if (closed) {
            throw new IOException("Cannot write to a closed output stream");
        }
        lz4FrameOutputStream.write((byte) b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        write(b, 0, b.length);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        if (closed) {
            throw new IOException("Cannot write to a closed output stream");
        }
        lz4FrameOutputStream.write(b, off, len);
    }

    @Override
    public boolean isReady() {
        return !closed;
    }

    @Override
    public void setWriteListener(WriteListener listener) {}
}

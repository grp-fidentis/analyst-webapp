package cz.fidentis.web.compression;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * @author Ondřej Bazala
 * @since 11.09.2023
 */
public class CompressionResponseWrapper extends HttpServletResponseWrapper {

    private final HttpServletResponse originalResponse;
    private ServletOutputStream servletOutputStream;
    private PrintWriter writer;

    public CompressionResponseWrapper(HttpServletResponse response) {
        super(response);
        this.originalResponse = response;
    }

    public ServletOutputStream createOutputStream() throws IOException {
        return new CompressionResponseStream(originalResponse);
    }

    public void finishResponse() throws IOException {
        if (writer != null) {
            writer.close();
        } else {
            if (servletOutputStream != null) {
                servletOutputStream.close();
            }
        }
    }

    @Override
    public void flushBuffer() throws IOException {
        servletOutputStream.flush();
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        if (writer != null) {
            throw new IllegalStateException("getWriter() has already been called!");
        }

        if (servletOutputStream == null) {
            servletOutputStream = createOutputStream();
        }
        return (servletOutputStream);
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        if (writer != null) {
            return (writer);
        }

        if (servletOutputStream != null) {
            throw new IllegalStateException("getOutputStream() has already been called!");
        }

        servletOutputStream = createOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(servletOutputStream, StandardCharsets.UTF_8));
        return writer;
    }

    @Override
    public void setContentLength(int length) { /* CompressionResponseStream handles setting content length */ }
}

package cz.fidentis.web.compression;

import org.springframework.web.filter.OncePerRequestFilter;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/**
 * @author Ondřej Bazala
 * @since 11.09.2023
 */
public class CompressionFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String acceptEncoding = request.getHeader("accept-custom-encoding");
        if (acceptEncoding != null && acceptEncoding.contains("true")) {
            CompressionResponseWrapper compressionWrapper = new CompressionResponseWrapper(response);
            filterChain.doFilter(request, compressionWrapper);
            compressionWrapper.finishResponse();
            return;
        }
        filterChain.doFilter(request, response);
    }
}

package cz.fidentis.web.config.dto;

import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SimpleFileUploadDetail implements FileUploadDetail {

    private Long id;
    private String name;
    private String extension;
    private Boolean isAverageFace;
    private byte[] smallPreviewPhotoData;
    private Long faceInfoId;
    private Collection<TaskDetail> tasks;
}

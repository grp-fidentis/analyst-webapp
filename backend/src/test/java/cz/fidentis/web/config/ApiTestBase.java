package cz.fidentis.web.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskService;
import cz.fidentis.web.analyticaltask.cuttingplanes.CuttingPlanesTaskService;
import cz.fidentis.web.analyticaltask.distance.DistanceTaskService;
import cz.fidentis.web.analyticaltask.registration.RegistrationTaskService;
import cz.fidentis.web.analyticaltask.sampling.SamplingTaskService;
import cz.fidentis.web.analyticaltask.symmetry.SymmetryTaskService;
import cz.fidentis.web.auth.AuthService;
import cz.fidentis.web.auth.passwordresettoken.PasswordResetTokenService;
import cz.fidentis.web.batchprocessing.registration.BatchRegistrationService;
import cz.fidentis.web.exceptions.GlobalExceptionHandler;
import cz.fidentis.web.fileupload.FileUploadService;
import cz.fidentis.web.fileupload.faceinfo.FaceInfoService;
import cz.fidentis.web.humanface.service.HumanFaceService;
import cz.fidentis.web.mail.EmailService;
import cz.fidentis.web.project.ProjectService;
import cz.fidentis.web.role.RoleService;
import cz.fidentis.web.security.SecurityConfig;
import cz.fidentis.web.security.jwt.JwtConfig;
import cz.fidentis.web.security.jwt.JwtUtils;
import cz.fidentis.web.task.TaskService;
import cz.fidentis.web.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @author Ondřej Bazala
 * @since 03.11.2023
 */
@WebMvcTest
@ActiveProfiles("test")
@WithMockUser
@Import(SecurityConfig.class)
@ContextConfiguration(classes = {GlobalExceptionHandler.class})
public abstract class ApiTestBase {

    protected static final byte[] EMPTY_BODY = new byte[0];

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected ObjectMapper objectMapper;


    @MockBean
    protected UserDetailsService userDetailsService;

    @MockBean
    protected PasswordEncoder passwordEncoder;

    @MockBean
    protected JwtUtils jwtUtils;

    @MockBean
    protected JwtConfig jwtConfig;


    // SERVICES

    @MockBean
    protected UserService userService;

    @MockBean
    protected HumanFaceService humanFaceService;

    @MockBean
    protected TaskService taskService;

    @MockBean
    protected FileUploadService fileUploadService;

    @MockBean
    protected FaceInfoService faceInfoService;

    @MockBean
    protected ProjectService projectService;

    @MockBean
    protected RegistrationTaskService registrationTaskService;

    @MockBean
    protected DistanceTaskService distanceTaskService;

    @MockBean
    protected SamplingTaskService samplingTaskService;

    @MockBean
    protected SymmetryTaskService symmetryTaskService;

    @MockBean
    protected PasswordResetTokenService passwordResetTokenService;

    @MockBean
    protected RoleService roleService;

    @MockBean
    protected AuthService authService;

    @MockBean
    protected EmailService emailService;

    @MockBean
    protected CuttingPlanesTaskService cuttingPlanesTaskService;

    @MockBean
    protected CurvatureTaskService curvatureTaskService;

    @MockBean
    protected BatchRegistrationService batchRegistrationService;
}

package cz.fidentis.web.config;

import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskService;
import cz.fidentis.web.analyticaltask.cuttingplanes.CuttingPlanesTaskService;
import cz.fidentis.web.analyticaltask.distance.DistanceTaskService;
import cz.fidentis.web.analyticaltask.registration.RegistrationTaskService;
import cz.fidentis.web.analyticaltask.sampling.SamplingTaskService;
import cz.fidentis.web.analyticaltask.symmetry.SymmetryTaskService;
import cz.fidentis.web.async.AsyncConfig;
import cz.fidentis.web.auth.AuthService;
import cz.fidentis.web.auth.passwordresettoken.PasswordResetTokenRepo;
import cz.fidentis.web.auth.passwordresettoken.PasswordResetTokenService;
import cz.fidentis.web.batchprocessing.registration.BatchRegistrationService;
import cz.fidentis.web.cache.CacheService;
import cz.fidentis.web.fileupload.FileUploadRepo;
import cz.fidentis.web.fileupload.FileUploadService;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.fileupload.faceinfo.FaceInfoRepo;
import cz.fidentis.web.fileupload.faceinfo.FaceInfoService;
import cz.fidentis.web.humanface.HumanFaceRepo;
import cz.fidentis.web.humanface.service.HumanFaceService;
import cz.fidentis.web.mail.EmailService;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.project.ProjectRepo;
import cz.fidentis.web.project.ProjectService;
import cz.fidentis.web.role.RoleRepo;
import cz.fidentis.web.role.RoleService;
import cz.fidentis.web.security.jwt.JwtConfig;
import cz.fidentis.web.security.jwt.JwtUtils;
import cz.fidentis.web.task.TaskRepo;
import cz.fidentis.web.task.TaskService;
import cz.fidentis.web.task.dto.CreateTaskDto;
import cz.fidentis.web.task.dto.TaskDto;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.UserRepo;
import cz.fidentis.web.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

/**
 * @author Petr Hendrych
 * @since 12.05.2023
 */
@SpringBootTest
@ActiveProfiles("test")
@Transactional
public abstract class ServiceTestBase {

    @BeforeEach
    protected void parentBeforeEach() {
        setAdminAsCurrentUser();

        when(cacheService.getTokenId()).thenReturn("id");
    }

    protected Project createProject() {
        project = projectService.createProject("Project");
        return project;
    }

    protected void setAdminAsCurrentUser() {
        User admin = userRepo.findByUsername("admin")
                .orElseThrow(() -> new IllegalStateException("Admin user is not inserted by liquibase"));
        currentUser = admin;
        doReturn(admin).when(userService).getCurrentAuthenticatedUser();
    }

    protected FileUploadDetail saveProjectAndObjFile() {
        project = projectService.createProject("Project");

        fileUploadDetail = saveObjFile("01.obj");
        return fileUploadDetail;
    }

    protected FileUploadDetail saveObjFile(String fileName) {
        try {
            File file = ResourceUtils.getFile("classpath:" + fileName);
            byte[] objContent = Files.readAllBytes(file.toPath());

            MultipartFile multipartFile = new MockMultipartFile(fileName, fileName,
                    "application/octet-stream", objContent);
            fileUploadService.saveFile(multipartFile, project.getId());
            return fileUploadService.getFilesOnProject(project.getId()).stream()
                    .filter(f -> fileName.contains(f.getName()) && fileName.contains(f.getExtension()))
                    .findAny().get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected TaskDto createTaskWithFaces(List<Long> fileUploadIds) {
        return taskService.createTask(new CreateTaskDto(project.getId(), fileUploadIds));
    }

    protected Project project;

    protected FileUploadDetail fileUploadDetail;

    protected User currentUser;

    @SpyBean
    protected UserService userService;

    @MockBean
    protected JavaMailSender javaMailSender;

    @MockBean
    protected AsyncConfig asyncConfig;

    @MockBean
    protected CacheService cacheService;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    @Autowired
    protected JwtConfig jwtConfig;

    @Autowired
    protected HumanFaceService humanFaceService;

    @Autowired
    protected HumanFaceRepo humanFaceRepo;

    @Autowired
    protected JwtUtils jwtUtils;

    @Autowired
    protected UserRepo userRepo;

    @Autowired
    protected TaskService taskService;

    @Autowired
    protected TaskRepo taskRepo;

    @Autowired
    protected FileUploadService fileUploadService;

    @Autowired
    protected FileUploadRepo fileUploadRepo;

    @Autowired
    protected FaceInfoService faceInfoService;

    @Autowired
    protected FaceInfoRepo faceInfoRepo;

    @Autowired
    protected ProjectService projectService;

    @Autowired
    protected ProjectRepo projectRepo;

    @Autowired
    protected RoleRepo roleRepo;

    @Autowired
    protected RoleService roleService;

    @Autowired
    protected RegistrationTaskService registrationTaskService;

    @Autowired
    protected DistanceTaskService distanceTaskService;

    @Autowired
    protected SymmetryTaskService symmetryTaskService;

    @Autowired
    protected SamplingTaskService samplingTaskService;

    @Autowired
    protected EmailService emailService;

    @Autowired
    protected PasswordResetTokenService passwordResetTokenService;

    @Autowired
    protected PasswordResetTokenRepo passwordResetTokenRepo;

    @Autowired
    protected AuthService authService;

    @Autowired
    protected CuttingPlanesTaskService cuttingPlanesTaskService;

    @Autowired
    protected CurvatureTaskService curvatureTaskService;

    @Autowired
    protected BatchRegistrationService batchRegistrationService;
}

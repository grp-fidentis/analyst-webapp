package cz.fidentis.web.config.dto;

import cz.fidentis.web.task.TaskType;
import cz.fidentis.web.task.dto.TaskDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SimpleTaskDto implements TaskDto {

    private Long id;
    private Long projectId;
    private Long taskParametersId;
    private String name;
    private TaskType taskType;
}

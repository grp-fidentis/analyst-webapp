package cz.fidentis.web.config;

import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.web.analyticaltask.curvature.CurvatureMethod;
import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskDto;
import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskResponseDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlaneDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlaneFrontendState;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlanes2DProjectionDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlanes2DProjectionResponseDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesDirection;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesType;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskDto;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto;
import cz.fidentis.web.analyticaltask.distance.enums.ExportType;
import cz.fidentis.web.analyticaltask.distance.enums.HeatmapRenderMode;
import cz.fidentis.web.analyticaltask.registration.RegistrationMethod;
import cz.fidentis.web.analyticaltask.registration.RegistrationTaskDto;
import cz.fidentis.web.analyticaltask.sampling.PointSamplingStrategy;
import cz.fidentis.web.analyticaltask.sampling.SamplingTaskDto;
import cz.fidentis.web.analyticaltask.symmetry.SymmetryMethod;
import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryTaskDto;
import cz.fidentis.web.batchprocessing.registration.dto.RegistrationOptionsTaskDto;
import cz.fidentis.web.batchprocessing.registration.enums.BatchAveragingMethod;
import cz.fidentis.web.batchprocessing.registration.dto.AveragingOptionsTaskDto;
import cz.fidentis.web.batchprocessing.registration.dto.BatchRegistrationTaskDto;
import cz.fidentis.web.batchprocessing.registration.enums.BatchRegistrationMethod;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.fileupload.dto.FileUploadDto;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.humanface.TaskFaceInfo;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.humanface.dto.HumanFaceSamplesDto;
import cz.fidentis.web.humanface.dto.Point;
import cz.fidentis.web.humanface.dto.update.HumanFaceUpdateDto;
import cz.fidentis.web.humanface.dto.update.TransformationDto;
import cz.fidentis.web.role.Role;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.dto.UserDetailDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cz.fidentis.web.fileupload.FileUploadServiceImpl.AVERAGE_FACE_NAME;

public class TestUtils {

    public static User getUser() {
        return new User("John", "Doe", "john191", "john.doe@email.com",
                LocalDateTime.now(), true, false, "LOCAL", Arrays.asList(new Role("USER")));
    }

    public static UserDetailDto getUserDetailDto() {
        return new UserDetailDto("John", "Doe", "john191", "john.doe@email.com",
                Arrays.asList(new Role("USER")), true, false, "LOCAL");
    }

    public static HumanFaceUpdateDto getHumanFaceUpdateDto() {
        return new HumanFaceUpdateDto(1L, null, null, null);
    }

    public static SamplingTaskDto getSamplingTaskDto(Long taskId) {
        return new SamplingTaskDto(taskId, PointSamplingStrategy.RANDOM_SAMPLING, 100);
    }

    public static HumanFaceSamplesDto getHumanFaceSamplesDto() {
        return new HumanFaceSamplesDto(1L, List.of());
    }

    public static RegistrationTaskDto getRegistrationTaskDto(Long taskId) {
        return new RegistrationTaskDto(taskId, RegistrationMethod.ICP, true, 50,
                0.5, true, getSamplingTaskDto(taskId), getDistanceTaskDto(taskId));
    }

    public static AveragingOptionsTaskDto getAveragingOptionsTaskDto() {
        return new AveragingOptionsTaskDto(BatchAveragingMethod.NEAREST_NEIGHBOURS, 3, 0);
    }

    public static RegistrationOptionsTaskDto getRegistrationOptionsTaskDto() {
        return new RegistrationOptionsTaskDto(BatchRegistrationMethod.MESH_BASED_ICP, 1000, false);
    }

    public static FileUploadDto createSelectedFileUpload(FileUploadDetail fileUploadDetail) {
        return new FileUploadDto(
                fileUploadDetail.getId(),
                fileUploadDetail.getName(),
                fileUploadDetail.getExtension(),
                fileUploadDetail.getSmallPreviewPhotoData(),
                fileUploadDetail.getFaceInfoId());
    }

    public static BatchRegistrationTaskDto getBatchRegistrationTaskDto(Long taskId, Long projectId, FileUploadDetail fileUploadDetail) {
       FileUploadDto fileUploadDto = createSelectedFileUpload(fileUploadDetail);
        return new BatchRegistrationTaskDto(taskId, projectId, getAveragingOptionsTaskDto(), getRegistrationOptionsTaskDto(),
               fileUploadDto, List.of(fileUploadDto.getId()), "sessionId", 50);
    }

    public static CurvatureTaskDto getCurvatureTaskDto(Long taskId) {
        return new CurvatureTaskDto(taskId, CurvatureMethod.MEAN);
    }

    public static CurvatureTaskResponseDto getCurvatureTaskResponseDto() {
        return new CurvatureTaskResponseDto(1L, List.of(1, 2, 3));
    }

    public static DistanceTaskResponseDto getDistanceTaskResponseDto() {
        return new DistanceTaskResponseDto(1f, 1f, List.of());
    }

    public static SymmetryTaskDto getSymmetryTaskDto(Long taskId) {
        return new SymmetryTaskDto(taskId, SymmetryMethod.FAST_MESH_SURFACE, getSamplingTaskDto(taskId), getSamplingTaskDto(taskId));
    }

    public static DistanceTaskDto getDistanceTaskDto(Long taskId) {
        return new DistanceTaskDto(taskId, MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, true,
                true, HeatmapRenderMode.STANDARD, List.of(),
                new TransformationDto(new Point(5, 0, 0), new Point(0, 0, 0)),
                ExportType.DISTANCE, true );
    }

    public static CuttingPlanes2DProjectionDto getCuttingPlanes2DProjectionDto(Long taskId) {
        List<CuttingPlaneFrontendState> cuttingPlaneFrontendStates = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            cuttingPlaneFrontendStates.add(new CuttingPlaneFrontendState("id", true, 0D,
                    CuttingPlanesType.SYMMETRY_PLANE, CuttingPlanesDirection.DIRECTION_VERTICAL, i, new TransformationDto()));
        }
        return new CuttingPlanes2DProjectionDto(taskId, CuttingPlanesDirection.DIRECTION_VERTICAL, cuttingPlaneFrontendStates,
                50, 100, 400, 400 );
    }

    public static CuttingPlanes2DProjectionResponseDto getCuttingPlanes2DProjectionResponseDto(Long taskId) {
        return new CuttingPlanes2DProjectionResponseDto(List.of(), List.of(), null);
    }

    public static List<CuttingPlaneDto> getCuttingPlaneDtos() {
        return List.of();
    }

    public static HumanFaceDto getHumanFaceDto() {
        return new HumanFaceDto(1L, false, false, false, false, new TaskFaceInfo(), new byte[0],
                getDistanceTaskResponseDto(), null, null, null, null, null);
    }

    public static HumanFaceDto getAverageHumanFaceDto() {
        TaskFaceInfo taskFaceInfo = new TaskFaceInfo();
        taskFaceInfo.setGeometryFileSize(0L);
        taskFaceInfo.setGeometryFileName(AVERAGE_FACE_NAME);
        return new HumanFaceDto(1L, true, false, false, false, taskFaceInfo, new byte[0],
                getDistanceTaskResponseDto(), null, null, null, null, null);
    }



    public static FaceInfo getFaceInfo() {
        return new FaceInfo("geometryName", 10L, "fpName", "previewName",
                "textureName", new byte[10], 100L, 50, List.of());
    }
}

package cz.fidentis.web.service;

import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.role.Role;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Ondřej Bazala
 * @since 10.11.2023
 */
class RoleServiceTest extends ServiceTestBase {

    @Test
    void saveNewRoleShouldPersistRole() {
        Role role = new Role("TEST_ROLE");

        Role savedRole = roleService.saveRole(role);
        savedRole = roleRepo.findById(savedRole.getId()).orElse(null);

        assertThat(savedRole).isNotNull();
        assertThat(savedRole.getId()).isNotNull();
        assertThat(savedRole.getName()).isEqualTo(role.getName());
    }

    @Test
    void getRolesShouldReturnNonEmptyListWhenRolesInDatabase() {
        roleRepo.save(new Role("ROLE_NEW"));

        List<Role> roles = roleService.getRoles();

        assertThat(roles)
                .isNotEmpty()
                .hasSize(4)
                .extracting("name")
                .contains("ROLE_ADMIN", "ROLE_SUPER_ADMIN", "ROLE_USER", "ROLE_NEW");
    }
}

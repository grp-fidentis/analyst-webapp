package cz.fidentis.web.service.analyticaltask;

import cz.fidentis.web.analyticaltask.cuttingplanes.dto.*;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesDirection;
import cz.fidentis.web.analyticaltask.cuttingplanes.enums.CuttingPlanesType;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.humanface.dto.Point;
import cz.fidentis.web.humanface.dto.update.TransformationDto;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;

class CuttingPlanesTaskServiceTest extends ServiceTestBase {

    @Test
    void whenCreateCuttingPlanes_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));

        List<CuttingPlaneDto> cuttingPlanes = cuttingPlanesTaskService.createCuttingPlanes(taskWithFaces.getId());

        assertThat(cuttingPlanes).isNotNull().isNotEmpty().hasSize(9)
                .usingRecursiveFieldByFieldElementComparatorOnFields("cuttingPlaneId").isNotNull();
    }

    @Test
    void whenCalculate2DProjection_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));
        CuttingPlanes2DProjectionDto dto = getCuttingPlanes2DProjectionDto(taskWithFaces.getId());

        List<CuttingPlaneDto> cuttingPlanes = cuttingPlanesTaskService.createCuttingPlanes(taskWithFaces.getId());
        List<CuttingPlaneFrontendState> cuttingPlaneFrontendState = cuttingPlanes.stream()
                .filter(c -> c.getDirection() == CuttingPlanesDirection.DIRECTION_VERTICAL)
                .map(c -> new CuttingPlaneFrontendState(c.getCuttingPlaneId(), true, 0D, c.getType(),
                        c.getDirection(), c.getOrder(), new TransformationDto(new Point(0, 0, 0), new Point(0, 0, 0))))
                .toList();
        dto.setCuttingPlaneStates(cuttingPlaneFrontendState);
        CuttingPlanes2DProjectionResponseDto responseDto = cuttingPlanesTaskService.calculate2DProjection(dto);

        assertThat(responseDto).isNotNull();
        assertThat(responseDto.getBoundingBoxDto()).isNotNull();
        assertThat(responseDto.getHausdorffDistances()).hasSize(3);
        assertThat(responseDto.getIntersectionPointsDtos()).hasSize(2);
        assertThat(responseDto.getIntersectionPointsDtos().stream()
                .flatMap(i -> i.getCuttingPlanesIntersections().stream()).toList()).hasSize(6);
    }

    @Test
    void whenExportProjectionsAsZip_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));
        CuttingPlanes2DProjectionDto dto = getCuttingPlanes2DProjectionDto(taskWithFaces.getId());

        List<CuttingPlaneDto> cuttingPlanes = cuttingPlanesTaskService.createCuttingPlanes(taskWithFaces.getId());
        List<CuttingPlaneFrontendState> cuttingPlaneFrontendState = cuttingPlanes.stream()
                .filter(c -> c.getDirection().equals(CuttingPlanesDirection.DIRECTION_VERTICAL) &&
                        c.getType() == CuttingPlanesType.BOUNDING_BOX_PLANE)
                .map(c -> new CuttingPlaneFrontendState(c.getCuttingPlaneId(), true, 0D, c.getType(),
                        c.getDirection(), c.getOrder(), new TransformationDto(new Point(0, 0, 0), new Point(0, 0, 0))))
                .toList();
        dto.setCuttingPlaneStates(cuttingPlaneFrontendState);
        byte[] bytes = cuttingPlanesTaskService.exportProjectionsAsZip(dto);

        assertThat(bytes).isNotNull().isNotEmpty().hasSizeGreaterThan(10);
    }
}

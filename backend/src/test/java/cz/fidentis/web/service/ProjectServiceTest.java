package cz.fidentis.web.service;

import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.project.ProjectSimpleDto;
import cz.fidentis.web.project.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Petr Hendrych
 * @since 10.05.2023
 */
class ProjectServiceTest extends ServiceTestBase {

    @Test
    void givenAdminUserProjectName_whenCreateNewProject_thenNewProjectForUserIsCreated() {
        Project project = projectService.createProject("Project");

        assertThat(project.getUser()).isEqualTo(currentUser);
        assertThat(project.getName()).isEqualTo("Project");
        assertThat(projectRepo.findAllByUserId(currentUser.getId())).hasSize(1).contains(
                new ProjectSimpleDto(project.getId(), project.getName(), project.getIsOpened())
        );
    }

    @Test
    void givenAdminUserAndProjects_whenGettingOpenedProjects_thenReturnAllOpenedProjectByUser() {
        projectService.createProject("Project");
        projectService.createProject("Project 2");

        List<ProjectSimpleDto> projects = projectService.getProjects(true);

        assertThat(projects).hasSize(2);
        assertThat(projects.stream().allMatch(ProjectSimpleDto::isOpened)).isTrue();
    }

    @Test
    void givenAdminUserAndProjects_whenChangingProjectStatus_thenCheckAllProjectsByOpenStatus() {
        Project project1 = projectService.createProject("Project");
        projectService.createProject("Project 2");

        projectService.setStatusToProject(project1.getId(), false);

        assertThat(projectService.getProjects(true)).hasSize(1);
        assertThat(projectService.getProjects(false)).hasSize(1);
        assertThat(projectService.getProjects(null)).hasSize(2);
    }

    @Test
    void givenAdminUserAndProjects_whenDeletingProject_thenProjectIsDeleted() {
        Project project = projectService.createProject("Project");
        ProjectSimpleDto simpleDto = new ProjectSimpleDto(project.getId(), project.getName(), project.getIsOpened());

        assertThat(projectRepo.findAllByUserId(currentUser.getId())).hasSize(1).contains(simpleDto);

        projectService.deleteProject(project.getId());

        assertThat(projectRepo.findAllByUserId(currentUser.getId())).isEmpty();
    }
}

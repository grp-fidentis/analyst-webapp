package cz.fidentis.web.service;

import cz.fidentis.web.auth.exception.EmailException;
import cz.fidentis.web.auth.exception.TokenException;
import cz.fidentis.web.auth.passwordresettoken.PasswordResetToken;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.dto.UserDetailDto;
import jakarta.mail.Session;
import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static cz.fidentis.web.config.TestUtils.getUserDetailDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class AuthServiceTest extends ServiceTestBase {

    @Test
    void whenRefreshUserToken_thenGenerateNewTokens() throws Exception {
        String requestUrl = "http://localhost:8080/api/auth/refresh-token";
        String refreshToken = jwtUtils.generateToken(currentUser, requestUrl, false);
        String authorizationHeader = "Bearer " + refreshToken;

        var tokens = authService.refreshUserToken(authorizationHeader, requestUrl);

        String oldRefreshToken = authorizationHeader.substring(jwtConfig.getTokenPrefix().length());
        assertThat(tokens).containsKey("Access-Token").containsKey("Refresh-Token");
        assertThat(tokens.get("Access-Token")).isNotBlank();
        assertThat(tokens).containsEntry("Refresh-Token", oldRefreshToken);
    }

    @Test
    void whenRegister_thenSendEmailAndCreateResetToken() throws EmailException {
        UserDetailDto userDto = getUserDetailDto();
        userDto.setRoles(List.of(roleRepo.findByName("ROLE_USER").get()));
        String url = "http://localhost:8080";

        when(javaMailSender.createMimeMessage()).thenReturn(new MimeMessage((Session)null));

        authService.register(url, userDto);

        assertThat(userRepo.findAll()).hasSize(2);
        assertThat(passwordResetTokenRepo.findAll()).hasSize(1);
    }

    @Test
    void whenForgotPassword_thenSendEmailAndCreateResetToken() throws EmailException {
        String url = "http://localhost:8080";

        when(javaMailSender.createMimeMessage()).thenReturn(new MimeMessage((Session)null));

        authService.forgotPassword(url, currentUser.getEmail());

        assertThat(passwordResetTokenRepo.findAll()).hasSize(1);
        verify(javaMailSender).createMimeMessage();
    }

    @Test
    void whenResetPassword_thenResetPasswordAndEnableUser() {
        currentUser.setEnabled(false);
        userRepo.save(currentUser);
        String oldPassword = currentUser.getPassword();
        String password = "newPassword";
        String token = UUID.randomUUID().toString();
        passwordResetTokenRepo.save(new PasswordResetToken(token, LocalDateTime.now().plusMinutes(15), currentUser));

        authService.resetPassword(password, token);

        User userAfterReset = userRepo.findById(currentUser.getId()).get();
        assertThat(userAfterReset.getPassword()).isNotEqualTo(oldPassword);
        assertThat(userAfterReset.getEnabled()).isTrue();
    }

    @Test
    void givenExpiredToken_whenResetPassword_thenThrowTokenException() {
        String expiredToken = UUID.randomUUID().toString();
        passwordResetTokenRepo.save(new PasswordResetToken(expiredToken, LocalDateTime.now().minusMinutes(15), currentUser));

        assertThrows(TokenException.class, () -> authService.resetPassword("newPassword", expiredToken));
    }
}

package cz.fidentis.web.service;

import cz.fidentis.web.auth.passwordresettoken.PasswordResetToken;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.user.User;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class PasswordResetTokenServiceTest extends ServiceTestBase {

    @Test
    void whenCreateResetPasswordToken_thenTokenIsPersisted() {
        User user = currentUser;
        String token = "resetToken123";

        passwordResetTokenService.createResetPasswordToken(user, token);

        PasswordResetToken savedToken = passwordResetTokenRepo.findByToken(token).orElse(null);
        assertThat(savedToken).isNotNull();
        assertThat(savedToken.getUser()).isEqualTo(user);
        assertThat(savedToken.getExpiryDate()).isBetween(LocalDateTime.now().plusMinutes(13), LocalDateTime.now().plusMinutes(17));
    }
}

package cz.fidentis.web.service;

import cz.fidentis.web.config.ServiceTestBase;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class EmailServiceTest extends ServiceTestBase {

    @Captor
    ArgumentCaptor<MimeMessage> mimeMessageCaptor;

    @Test
    void whenSendEmail_thenSendEmailWithCorrectParameters() throws MessagingException, IOException {
        String to = "test@example.com";
        String email = "Test email content";
        String subject = "Test Subject";

        when(javaMailSender.createMimeMessage()).thenReturn(new MimeMessage((Session)null));

        emailService.send(to, email, subject);

        verify(javaMailSender).send(mimeMessageCaptor.capture());
        MimeMessage createdEmail = mimeMessageCaptor.getValue();
        assertThat(createdEmail).isNotNull();
        assertThat(createdEmail.getSubject()).isEqualTo(subject);
        assertThat(createdEmail.getContent()).isEqualTo(email);
        assertThat(createdEmail.getAllRecipients()).hasSize(1).contains(new InternetAddress(to));
        assertThat(createdEmail.getFrom()).hasSize(1).contains(new InternetAddress("noreply@fidentis.cz"));
    }
}

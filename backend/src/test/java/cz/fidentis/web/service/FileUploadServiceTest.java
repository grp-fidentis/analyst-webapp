package cz.fidentis.web.service;

import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.fileupload.exception.InvalidAdditionalFileTypeException;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.project.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static cz.fidentis.web.fileupload.FileExtensions.OBJ_EXTENSION;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Petr Hendrych
 * @since 12.05.2023
 */
class FileUploadServiceTest extends ServiceTestBase {

    private Project project;

    @BeforeEach
    void setup() {
        project = projectService.createProject("Project");
    }

    @Test
    void givenAdminUserAndProject_whenUploadOBJFile_thenNewFileUploadAndFaceInfoIsCreated() throws IOException {
        MultipartFile file = new MockMultipartFile("01.obj", "01.obj", "application/octet-stream", "content".getBytes());

        fileUploadService.saveFile(file, project.getId());
        List<FileUploadDetail> fileUploads = fileUploadService.getFilesOnProject(project.getId());
        List<FaceInfo> faceInfos = faceInfoRepo.findAll();

        FileUploadDetail dbFile = fileUploads.get(0);
        FaceInfo faceInfo = faceInfos.get(0);

        assertThat(fileUploads).hasSize(1);
        assertThat(faceInfos).hasSize(1);
        assertThat(dbFile.getName()).isEqualTo("01");
        assertThat(dbFile.getExtension()).isEqualTo(OBJ_EXTENSION);
        assertThat(faceInfo.getGeometryFileName()).isEqualTo(file.getName());
    }

    @Test
    void givenTwoFilesJPGAndOBJ_whenGettingFilesForTable_thenReturnOnlyOBJFiles() throws IOException {
        MultipartFile objFile = new MockMultipartFile("01.obj", "01.obj", "application/octet-stream", "content".getBytes());
        MultipartFile jpgFile = new MockMultipartFile("01.jpg", "01.jpg", "application/octet-stream", "content".getBytes());

        fileUploadService.saveFile(objFile, project.getId());

        List<FileUploadDetail> fileUploads = fileUploadService.getFilesOnProject(project.getId());

        fileUploadService.uploadAdditionalFile(jpgFile, project.getId(), fileUploads.get(0).getFaceInfoId(), "texture");

        List<FileUploadDetail> allObjFiles = fileUploadService.getFilesOnProject(project.getId());
        List<FileUpload> allFiles = fileUploadRepo.findAll();

        assertThat(allObjFiles).hasSize(1);
        assertThat(allFiles).hasSize(2);
    }

    @Test
    void givenThreeObjFiles_whenDeletingTwoFile_thenReturnListHasSizeOne() throws IOException {
        MultipartFile file = new MockMultipartFile("01.obj", "01.obj", "application/octet-stream", "content".getBytes());
        MultipartFile file2 = new MockMultipartFile("02.obj", "02.obj", "application/octet-stream", "content".getBytes());
        MultipartFile file3 = new MockMultipartFile("03.obj", "03.obj", "application/octet-stream", "content".getBytes());

        fileUploadService.saveFile(file, project.getId());
        fileUploadService.saveFile(file2, project.getId());
        fileUploadService.saveFile(file3, project.getId());

        List<FileUploadDetail> fileUploads = fileUploadService.getFilesOnProject(project.getId());
        List<FaceInfo> faceInfos = faceInfoRepo.findAll();

        assertThat(fileUploads).hasSize(3);
        assertThat(faceInfos).hasSize(3);

        fileUploadService.deleteFiles(List.of(fileUploads.get(0).getId(), fileUploads.get(1).getId()));

        List<FileUploadDetail> fileUploadsAfterDelete = fileUploadService.getFilesOnProject(project.getId());
        List<FaceInfo> faceInfosAfterDelete = faceInfoRepo.findAll();

        assertThat(fileUploadsAfterDelete).hasSize(1);
        assertThat(faceInfosAfterDelete).hasSize(1);
        assertThat(faceInfosAfterDelete.get(0).getGeometryFileName())
                .isEqualTo(fileUploadsAfterDelete.get(0).getName() + "." + fileUploadsAfterDelete.get(0).getExtension());
    }

    @Test
    void givenUploadObjFile_whenUploadSmallPreviewPhoto_thenFileUploadContainsSmallPreviewPhotoData() throws IOException {
        MultipartFile objFile = new MockMultipartFile("01.obj", "01.obj", "application/octet-stream", "content".getBytes());
        MultipartFile previewPhoto = new MockMultipartFile("01_preview_small.png", "01.obj", "application/octet-stream", "content".getBytes());

        fileUploadService.saveFile(objFile, project.getId());
        List<FileUploadDetail> fileUploads = fileUploadService.getFilesOnProject(project.getId());

        FileUpload file = fileUploadService.uploadSmallPreviewPhoto(fileUploads.get(0).getId(), previewPhoto);

        assertThat(file.getSmallPreviewPhotoData()).isEqualTo(previewPhoto.getBytes());
    }

    @Test
    void givenFileUpload_whenUploadAllAdditionalFiles_thenPointsTexturePreviewAreUploaded() throws IOException {
        MultipartFile objFile = new MockMultipartFile("01.obj", "01.obj", "application/octet-stream", "content".getBytes());
        MultipartFile textureFile = new MockMultipartFile("01.jpg", "01.jpg", "application/octet-stream", "content".getBytes());
        MultipartFile pointsFile = new MockMultipartFile("01_landmarks.csv", "01_landmarks.csv", "application/octet-stream", "content".getBytes());
        MultipartFile previewFile = new MockMultipartFile("01_preview.jpg", "01_preview.jpg", "application/octet-stream", "content".getBytes());

        fileUploadService.saveFile(objFile, project.getId());
        List<FileUploadDetail> fileUploads = fileUploadService.getFilesOnProject(project.getId());

        Long faceInfoId = fileUploads.get(0).getFaceInfoId();
        fileUploadService.uploadAdditionalFile(textureFile, project.getId(), faceInfoId, "texture");
        fileUploadService.uploadAdditionalFile(pointsFile, project.getId(), faceInfoId, "points");
        fileUploadService.uploadAdditionalFile(previewFile, project.getId(), faceInfoId, "preview");

        FaceInfo info = faceInfoRepo.findById(faceInfoId).get();
        List<FileUpload> allFiles = fileUploadRepo.findAll();

        assertThat(info.getGeometryFileName()).isEqualTo(objFile.getName());
        assertThat(info.getPreviewPhotoFileName()).isEqualTo(previewFile.getName());
        assertThat(info.getTextureFileName()).isEqualTo(textureFile.getName());
        assertThat(info.getFeaturePointsFileName()).isEqualTo(pointsFile.getName());
        assertThat(allFiles).hasSize(4);
    }

    @Test
    void givenFileUpload_whenDeleteAdditionalFile_thenAdditionalFilesAreDeleted() throws IOException {
        MultipartFile objFile = new MockMultipartFile("01.obj", "01.obj", "application/octet-stream", "content".getBytes());
        MultipartFile textureFile = new MockMultipartFile("01.jpg", "01.jpg", "application/octet-stream", "content".getBytes());
        MultipartFile pointsFile = new MockMultipartFile("01_landmarks.csv", "01_landmarks.csv", "application/octet-stream", "content".getBytes());
        MultipartFile previewFile = new MockMultipartFile("01_preview.jpg", "01_preview.jpg", "application/octet-stream", "content".getBytes());

        fileUploadService.saveFile(objFile, project.getId());
        List<FileUploadDetail> fileUploads = fileUploadService.getFilesOnProject(project.getId());

        Long faceInfoId = fileUploads.get(0).getFaceInfoId();
        fileUploadService.uploadAdditionalFile(textureFile, project.getId(), faceInfoId, "texture");
        fileUploadService.uploadAdditionalFile(pointsFile, project.getId(), faceInfoId, "points");
        fileUploadService.uploadAdditionalFile(previewFile, project.getId(), faceInfoId, "preview");

        assertThat(fileUploadRepo.findAll()).hasSize(4);

        fileUploadService.deleteAdditionalFile("01", "jpg", "texture", faceInfoId);
        fileUploadService.deleteAdditionalFile("01_landmarks", "csv", "points", faceInfoId);
        fileUploadService.deleteAdditionalFile("01_preview", "jpg", "preview", faceInfoId);

        assertThat(fileUploadRepo.findAll()).hasSize(1);
    }

    @Test
    void givenFileUpload_whenUploadAdditionFileWithBadType_thenThrowInvalidAdditionalFileTypeException() throws IOException {
        MultipartFile objFile = new MockMultipartFile("01.obj", "01.obj", "application/octet-stream", "content".getBytes());
        MultipartFile textureFile = new MockMultipartFile("01.jpg", "01.jpg", "application/octet-stream", "content".getBytes());

        fileUploadService.saveFile(objFile, project.getId());
        List<FileUploadDetail> fileUploads = fileUploadService.getFilesOnProject(project.getId());

        Long faceInfoId = fileUploads.get(0).getFaceInfoId();
        assertThrows(
                InvalidAdditionalFileTypeException.class,
                () -> fileUploadService.uploadAdditionalFile(textureFile, project.getId(), faceInfoId, "random")
        );
    }
}

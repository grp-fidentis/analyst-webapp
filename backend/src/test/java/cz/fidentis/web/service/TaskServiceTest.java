package cz.fidentis.web.service;

import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.task.Task;
import cz.fidentis.web.task.dto.CreateTaskDto;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Petr Hendrych
 * @since 13.05.2023
 */
class TaskServiceTest extends ServiceTestBase {

    @BeforeEach
    void beforeEach() {
        saveProjectAndObjFile();
    }

    @Test
    void givenFileUpload_whenCreatingTask_thenTaskAndHumanFaceIsCreated() {
        TaskDto taskDto = taskService.createTask(getCreateTaskDto());

        List<HumanFaceDto> humanFaceDtos = humanFaceService.getHumanFaceDtosOfTask(taskDto.getId());

        assertThat(taskDto.getName()).isNotNull();
        assertThat(taskService.getProjectTasks(project.getId())).hasSize(1);
        assertThat(humanFaceDtos).hasSize(1);
    }

    @Test
    void givenTask_whenUpdateTaskName_thenTaskNameIsChanged() {
        String taskName = "New task name";
        Task task = saveTask(taskName);

        TaskDto updatedTask = taskService.updateTaskName(taskName, task.getId());

        assertThat(updatedTask.getName()).isEqualTo(taskName);
        assertThat(updatedTask.getId()).isEqualTo(task.getId());
    }

    @Test
    void givenTask_whenDeleteTask_thenTaskAndHumanFacesAreDeleted() {
        Task task = saveTask("name");

        taskService.deleteTask(task.getId());

        assertThat(taskService.getProjectTasks(project.getId())).isEmpty();
        assertThat(humanFaceRepo.findAll()).isEmpty();
    }

    @Test
    void whenGetTaskById_thenValidResult() {
        Task task = taskRepo.save(new Task(project, List.of(), "name"));

        TaskDto taskDto = taskService.getTaskDtoById(task.getId());

        assertThat(taskDto).isNotNull();
        assertThat(taskDto.getName()).isEqualTo(task.getName());
    }

    @Test
    void whenGetProjectTasks_thenValidResult() {
        Task task = taskRepo.save(new Task(project, List.of(), "name"));

        List<TaskDto> projectTasks = taskService.getProjectTasks(project.getId());

        assertThat(projectTasks).hasSize(1);
    }

    private CreateTaskDto getCreateTaskDto() {
        return new CreateTaskDto(project.getId(), List.of(fileUploadDetail.getId()));
    }

    private Task saveTask(String name) {
        Task task = new Task(project, List.of(), name);
        return taskRepo.save(task);
    }
}

package cz.fidentis.web.service;

import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.role.Role;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.dto.ChangePasswordDto;
import cz.fidentis.web.user.dto.UserDetailDto;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static cz.fidentis.web.config.TestUtils.getUser;
import static cz.fidentis.web.config.TestUtils.getUserDetailDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class UserServiceTest extends ServiceTestBase {

    @Test
    void whenGetUserById_thenReturnUserIfExists() {
        User result = userService.getUserById(currentUser.getId());

        assertThat(result).isSameAs(currentUser);
    }

    @Test
    void whenGetNotExistingUserById_thenThrowNoSuchElementException() {
        Long userId = 99L;

        assertThrows(NoSuchElementException.class, () -> userService.getUserById(userId));
    }

    @Test
    void whenGetUsers_thenReturnListOfUsers() {
        User firstUser = getUser();
        firstUser.setUsername("first");
        roleRepo.save(firstUser.getRoles().stream().findAny().get());
        User secondUser = getUser();
        secondUser.setUsername("second");
        roleRepo.save(secondUser.getRoles().stream().findAny().get());
        List<User> users = List.of(firstUser, secondUser);
        userRepo.saveAll(users);

        List<User> result = userService.getUsers();

        assertThat(result).hasSize(3);
    }

    @Test
    void whenDeleteUser_thenDeleteUserIfExists() {
        assertThat(userRepo.findAll()).hasSize(1);

        userService.deleteUser(currentUser.getId());

        assertThat(userRepo.findAll()).isEmpty();
    }

    @Test
    void whenUpdateUser_thenUpdateUserIfExists() {
        User user = new User();
        user.setUsername("loda797");
        user.setFirstName("hmm");
        user.setLastName("poopopo");
        user.setEmail("fsfsf@gsg.cz");
        userRepo.save(user);
        UserDetailDto userDto = new UserDetailDto();
        userDto.setUsername("john191");
        userDto.setFirstName("John");
        userDto.setLastName("Doe");
        userDto.setEmail("john.doe@email.com");

        User result = userService.updateUser(user.getId(), userDto);

        assertThat(result).isNotNull();
        assertThat(result.getUsername()).isSameAs(userDto.getUsername());
        assertThat(result.getEmail()).isSameAs(userDto.getEmail());
        assertThat(result.getFirstName()).isSameAs(userDto.getFirstName());
        assertThat(result.getLastName()).isSameAs(userDto.getLastName());
    }

    @Test
    void whenGetUserByUserName_thenReturnUserIfExists() {
        User result = userService.getUserByUserName(currentUser.getUsername());

        assertThat(result).isSameAs(currentUser);
    }

    @Test
    void whenChangePassword_thenChangePasswordForAuthenticatedUser() {
        currentUser.setPassword(passwordEncoder.encode("oldPassword"));
        userRepo.save(currentUser);
        ChangePasswordDto dto = new ChangePasswordDto("oldPassword", "newPassword", "newPassword");

        userService.changePassword(dto);

        currentUser = userRepo.findById(currentUser.getId()).get();
        assertThat(passwordEncoder.matches("newPassword", currentUser.getPassword())).isTrue();
    }
}

package cz.fidentis.web.service.analyticaltask;

import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskDto;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;

class DistanceTaskServiceTest extends ServiceTestBase {

    @Test
    void whenCalculateDistance_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));
        DistanceTaskDto dto = getDistanceTaskDto(taskWithFaces.getId());

        DistanceTaskResponseDto responseDto = distanceTaskService.calculateDistance(dto);

        assertThat(responseDto).isNotNull();
        assertThat(responseDto.getMeanDistance()).isNotNegative();
        assertThat(responseDto.getWeightedMeanDistance()).isNotNegative();
    }

    @Test
    void whenCalculateHeatmap_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));
        DistanceTaskDto dto = getDistanceTaskDto(taskWithFaces.getId());

        DistanceTaskResponseDto responseDto = distanceTaskService.calculateHeatmap(dto);

        assertThat(responseDto).isNotNull();
        assertThat(responseDto.getMeanDistance()).isNotNegative();
        assertThat(responseDto.getWeightedMeanDistance()).isNotNegative();
        assertThat(responseDto.getColorsOfHeatmap()).isNotEmpty();
        assertThat(responseDto.getFeaturePointWeights()).isEmpty();
    }

    @Test
    void whenExportDistance_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));
        DistanceTaskDto dto = getDistanceTaskDto(taskWithFaces.getId());

        byte[] bytes = distanceTaskService.exportDistance(dto);

        assertThat(bytes).isNotNull().isNotEmpty();
    }
}

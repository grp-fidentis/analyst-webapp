package cz.fidentis.web.service.analyticaltask;

import cz.fidentis.web.analyticaltask.registration.RegistrationTaskDto;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getRegistrationTaskDto;
import static org.assertj.core.api.Assertions.assertThat;

class RegistrationTaskServiceTest extends ServiceTestBase {

    @Test
    void whenExecuteRegistration_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));
        RegistrationTaskDto dto = getRegistrationTaskDto(taskWithFaces.getId());

        HumanFaceDto humanFaceDto = registrationTaskService.execute(dto);

        assertThat(humanFaceDto).isNotNull();
        assertThat(humanFaceDto.getId()).isNotNull();
        assertThat(humanFaceDto.getGeometry()).isNotNull();
        assertThat(humanFaceDto.getGeometry().getPositions()).isNotEmpty();
        assertThat(humanFaceDto.getDistances()).isNotNull();
        assertThat(humanFaceDto.getGeometry().getPositions()).isNotEmpty();
        assertThat(humanFaceDto.getSamples()).isNotNull();
        assertThat(humanFaceDto.getSamples().getPositions()).isNotEmpty();
    }
}

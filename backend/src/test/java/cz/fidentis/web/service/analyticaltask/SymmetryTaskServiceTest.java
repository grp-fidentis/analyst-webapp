package cz.fidentis.web.service.analyticaltask;

import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryResponseTaskDto;
import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryTaskDto;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getSymmetryTaskDto;
import static org.assertj.core.api.Assertions.assertThat;

class SymmetryTaskServiceTest extends ServiceTestBase {

    @Test
    void givenTwoObjFiles_whenCalculateSymmetryPlane_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));
        SymmetryTaskDto dto = getSymmetryTaskDto(taskWithFaces.getId());

        List<SymmetryResponseTaskDto> symmetryResponseTaskDtos = symmetryTaskService.calculate(dto);

        var firstSymmetryResponseTaskDto = symmetryResponseTaskDtos.get(0);
        assertThat(firstSymmetryResponseTaskDto).isNotNull();
        assertThat(firstSymmetryResponseTaskDto.getHumanFaceId()).isNotNull();
        assertThat(firstSymmetryResponseTaskDto.getSymmetryPlane()).isNotNull();
        assertThat(firstSymmetryResponseTaskDto.getSymmetryPlane().getPrecision()).isPositive();
        assertThat(firstSymmetryResponseTaskDto.getSymmetryPlane().getPositions()).isNotEmpty();

        var secondSymmetryResponseTaskDto = symmetryResponseTaskDtos.get(1);
        assertThat(secondSymmetryResponseTaskDto).isNotNull();
        assertThat(secondSymmetryResponseTaskDto.getHumanFaceId()).isNotNull();
        assertThat(secondSymmetryResponseTaskDto.getSymmetryPlane()).isNotNull();
        assertThat(secondSymmetryResponseTaskDto.getSymmetryPlane().getPrecision()).isPositive();
        assertThat(secondSymmetryResponseTaskDto.getSymmetryPlane().getPositions()).isNotEmpty();
    }

    @Test
    void givenOneObjFiles_whenCalculateSymmetryPlane_thenReturnValidResult() {
        saveProjectAndObjFile();
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail.getId()));
        SymmetryTaskDto dto = getSymmetryTaskDto(taskWithFaces.getId());

        List<SymmetryResponseTaskDto> symmetryResponseTaskDtos = symmetryTaskService.calculate(dto);

        var firstSymmetryResponseTaskDto = symmetryResponseTaskDtos.get(0);
        assertThat(firstSymmetryResponseTaskDto).isNotNull();
        assertThat(firstSymmetryResponseTaskDto.getHumanFaceId()).isNotNull();
        assertThat(firstSymmetryResponseTaskDto.getSymmetryPlane()).isNotNull();
        assertThat(firstSymmetryResponseTaskDto.getSymmetryPlane().getPrecision()).isPositive();
        assertThat(firstSymmetryResponseTaskDto.getSymmetryPlane().getPositions()).isNotEmpty();
    }
}

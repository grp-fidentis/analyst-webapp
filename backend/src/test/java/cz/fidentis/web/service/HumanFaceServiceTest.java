package cz.fidentis.web.service;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.humanface.dto.HumanFaceGeometryDto;
import cz.fidentis.web.humanface.dto.Point;
import cz.fidentis.web.humanface.dto.update.HumanFaceUpdateDto;
import cz.fidentis.web.humanface.dto.update.TransformationDto;
import cz.fidentis.web.task.dto.CreateTaskDto;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Petr Hendrych
 * @since 13.05.2023
 */
class HumanFaceServiceTest extends ServiceTestBase {

    @BeforeEach
    void beforeEach() {
        saveProjectAndObjFile();
    }

    @Test
    void givenHumanFace_whenGettingHumanFace_thenGetTransformedToDto() throws IOException {
        CreateTaskDto createTaskDto = new CreateTaskDto(project.getId(), List.of(fileUploadDetail.getId()));
        TaskDto taskDto = taskService.createTask(createTaskDto);
        FaceInfo faceInfo = faceInfoService.computeFaceInfoGeometry(fileUploadDetail.getFaceInfoId(), fileUploadDetail.getId());

        List<HumanFaceDto> humanFaceDtos = humanFaceService.getHumanFaceDtosOfTask(taskDto.getId());

        HumanFaceGeometryDto humanFaceDto = humanFaceDtos.get(0).getGeometry();
        assertThat(humanFaceDtos).hasSize(1);
        assertThat(humanFaceDto.getNumberOfFacets()).isEqualTo(faceInfo.getNumberOfFacets());
        assertThat(humanFaceDto.getPositions()).hasSize((int) (faceInfo.getNumberOfVertices() * 3));
        assertThat(humanFaceDto.getNormals()).hasSize((int) (faceInfo.getNumberOfVertices() * 3));
        assertThat(humanFaceDto.getUvs()).hasSize((int) (faceInfo.getNumberOfVertices() * 2));
    }

    @Test
    void whenGetHumanFaceEntityById_thenReturnEntity() {
        CreateTaskDto createTaskDto = new CreateTaskDto(project.getId(), List.of(fileUploadDetail.getId()));
        TaskDto taskDto = taskService.createTask(createTaskDto);
        List<HumanFaceEntity> humanFaceEntities = humanFaceRepo.findAll();

        HumanFaceEntity humanFaceEntity = humanFaceService.getHumanFaceEntityByIdAndTaskId(humanFaceEntities.get(0).getId(), taskDto.getId());

        HumanFaceEntity same = humanFaceEntity.equals(humanFaceEntities.get(0)) ? humanFaceEntities.get(0) :
                humanFaceEntities.get(1);
        assertThat(humanFaceEntity).isNotNull().isEqualTo(same);
    }

    @Test
    void whenGetHumanFaceEntitiesOfTask_thenReturnAllEntitiesOfTask() {
        CreateTaskDto createTaskDto = new CreateTaskDto(project.getId(), List.of(fileUploadDetail.getId()));
        TaskDto taskDto = taskService.createTask(createTaskDto);

        List<HumanFaceEntity> humanFaceEntitiesOfTask = humanFaceService.getHumanFaceEntitiesOfTask(taskDto.getId());

        assertThat(humanFaceEntitiesOfTask).isNotNull().hasSize(1);
    }

    @Test
    void whenLoadHumanFaceFromFile_thenCorrectlyCreateNewHumanFace() throws IOException {
        File file = ResourceUtils.getFile("classpath:01.obj");
        byte[] objContent = Files.readAllBytes(file.toPath());

        HumanFace humanFace = humanFaceService.loadHumanFaceFromFile(objContent, null);

        assertThat(humanFace).isNotNull();
        assertThat(humanFace.getMeshModel()).isNotNull();
        assertThat(humanFace.getMeshModel().getNumVertices()).isEqualTo(2500);
    }

    @Test
    void whenSave_thenCorrectlyUpdatedChanges() {
        CreateTaskDto createTaskDto = new CreateTaskDto(project.getId(), List.of(fileUploadDetail.getId()));
        TaskDto taskDto = taskService.createTask(createTaskDto);
        HumanFaceEntity humanFaceEntity = humanFaceRepo.findAll().get(0);
        boolean oldIsPrimaryFace = humanFaceEntity.getIsPrimaryFace();
        humanFaceEntity.setIsPrimaryFace(!humanFaceEntity.getIsPrimaryFace());

        humanFaceService.save(taskDto.getId(), humanFaceEntity);

        HumanFaceEntity updatedHumanFace = humanFaceRepo.findById(humanFaceEntity.getId()).get();
        assertThat(updatedHumanFace).isNotNull();
        assertThat(updatedHumanFace.getIsPrimaryFace()).isNotEqualTo(oldIsPrimaryFace);
    }

    @Test
    void whenSaveGeometry_thenCorrectlyUpdatedChangesToGeometry() {
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        CreateTaskDto createTaskDto = new CreateTaskDto(project.getId(), List.of(
                fileUploadDetail.getId(), fileUploadDetail2.getId()));
        TaskDto taskDto = taskService.createTask(createTaskDto);
        List<HumanFaceEntity> humanFaceEntities = humanFaceService.getHumanFaceEntitiesOfTask(taskDto.getId());
        HumanFaceEntity toChange = humanFaceEntities.get(0);
        MeshPoint oldMeshPoint = toChange.getHumanFace().getMeshModel().getFacets().get(0).getVertices().get(0);
        double oldPositionX = oldMeshPoint.getPosition().x;
        double oldNormalX = oldMeshPoint.getNormal().x;
        HumanFaceUpdateDto humanFaceUpdateDto = new HumanFaceUpdateDto();
        humanFaceUpdateDto.setTaskId(taskDto.getId());
        humanFaceUpdateDto.setTransformationDto(new TransformationDto(new Point(5, 0, 0), new Point(0, 0, 0)));

        humanFaceService.saveGeometry(toChange.getId(), humanFaceUpdateDto);

        HumanFaceEntity updatedHumanFace = humanFaceRepo.findById(toChange.getId()).get();
        MeshPoint meshPoint = updatedHumanFace.getHumanFace().getMeshModel().getFacets().get(0).getVertices().get(0);
        assertThat(meshPoint.getPosition().x).isEqualTo(oldPositionX + 5);
        assertThat(meshPoint.getNormal().x).isEqualTo(oldNormalX);
    }
}

package cz.fidentis.web.service.analyticaltask;

import cz.fidentis.web.analyticaltask.sampling.SamplingTaskDto;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.humanface.dto.HumanFaceSamplesDto;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getSamplingTaskDto;
import static org.assertj.core.api.Assertions.assertThat;

class SamplesTaskServiceTest extends ServiceTestBase {

    @Test
    void whenCalculateSamples_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));
        SamplingTaskDto dto = getSamplingTaskDto(taskWithFaces.getId());

        HumanFaceSamplesDto samplesDto = samplingTaskService.calculate(dto);

        assertThat(samplesDto).isNotNull();
        assertThat(samplesDto.getHumanFaceId()).isNotNull();
        assertThat(samplesDto.getPositions()).isNotEmpty();
    }
}

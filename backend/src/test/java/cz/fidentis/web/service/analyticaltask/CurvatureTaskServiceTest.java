package cz.fidentis.web.service.analyticaltask;

import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskDto;
import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskResponseDto;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getCurvatureTaskDto;
import static org.assertj.core.api.Assertions.assertThat;

class CurvatureTaskServiceTest extends ServiceTestBase {

    @Test
    void whenCalculateHeatmap_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId()));

        CurvatureTaskDto dto = getCurvatureTaskDto(taskWithFaces.getId());

        CurvatureTaskResponseDto responseDto = curvatureTaskService.calculateHeatmap(dto);

        assertThat(responseDto).isNotNull();
        assertThat(responseDto.getFaceId()).isNotNull();
        assertThat(responseDto.getHeatmap()).isNotNull().isNotEmpty();
    }
}

package cz.fidentis.web.service.batchprocessing;

import cz.fidentis.web.batchprocessing.registration.dto.BatchRegistrationTaskDto;
import cz.fidentis.web.batchprocessing.registration.dto.RegistrationResponseDto;
import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.humanface.HumanFaceEntity;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.task.dto.TaskDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getBatchRegistrationTaskDto;
import static org.assertj.core.api.Assertions.assertThat;

public class BatchRegistrationServiceTest extends ServiceTestBase {

    @Test
    void whenExecuteBatchRegistration_thenReturnValidResult() {
        Project project = createProject();
        FileUploadDetail fileUploadDetail1 = saveObjFile("01.obj");
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        FileUploadDetail fileUploadDetail3 = saveObjFile("03.obj");
        TaskDto taskWithFaces = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId(), fileUploadDetail3.getId()));

        BatchRegistrationTaskDto dto = getBatchRegistrationTaskDto(taskWithFaces.getId(), project.getId(), fileUploadDetail1);

        RegistrationResponseDto registrationResponseDto = batchRegistrationService.execute(dto);

        assertThat(registrationResponseDto.getHumanFace()).isNotNull();
        assertThat(registrationResponseDto.getHumanFace().getIsAverageFace()).isTrue();
        assertThat(registrationResponseDto.getHumanFace().getInfo().getGeometryFileName()).isEqualTo("Average Face");
        assertThat(registrationResponseDto.getHumanFace().getId()).isNotNull();
        assertThat(registrationResponseDto.getHumanFace().getGeometry()).isNotNull();
        assertThat(registrationResponseDto.getHumanFace().getGeometry().getPositions()).isNotEmpty();

        assertThat(registrationResponseDto.isCompletedSuccessfully()).isTrue();
    }

    @Test
    void whenExportAverageFace_thenReturnValidResult() {
        FileUploadDetail fileUploadDetail1 = saveProjectAndObjFile();
        FileUploadDetail fileUploadDetail2 = saveObjFile("02.obj");
        FileUploadDetail fileUploadDetail3 = saveObjFile("03.obj");
        TaskDto taskDto = createTaskWithFaces(List.of(fileUploadDetail1.getId(), fileUploadDetail2.getId(), fileUploadDetail3.getId()));
        HumanFaceEntity humanFaceEntity = humanFaceService.getHumanFaceEntityByFileUploadIdAndTaskId(fileUploadDetail1.getId(), taskDto.getId(), false);

        byte[] bytes = batchRegistrationService.exportAverageFace(humanFaceEntity.getId());

        assertThat(bytes).isNotNull().isNotEmpty();
    }
}

package cz.fidentis.web.service;

import cz.fidentis.web.config.ServiceTestBase;
import cz.fidentis.web.exceptions.exceptions.EntityNotFoundException;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static cz.fidentis.web.config.TestUtils.getFaceInfo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FaceInfoServiceTest extends ServiceTestBase {

    @Test
    void whenGetFaceInfoById_thenReturnFaceInfoIfExists() {
        FaceInfo faceInfo = getFaceInfo();
        faceInfo = faceInfoRepo.save(faceInfo);

        FaceInfo result = faceInfoService.getFaceInfoById(faceInfo.getId());

        assertThat(result).isSameAs(faceInfo);
    }

    @Test
    void whenGetNotSavedFaceInfoById_thenThrowEntityNotFoundException() {
        Long faceInfoId = 8L;

        assertThrows(EntityNotFoundException.class, () -> faceInfoService.getFaceInfoById(faceInfoId));
    }

    @Test
    void whenComputeFaceInfoGeometry_thenUpdateFaceInfoWithGeometryData() throws IOException {
        saveProjectAndObjFile();

        FileUpload fileUpload = fileUploadRepo.findAll().get(0);
        Long uploadId = fileUpload.getId();
        FaceInfo faceInfo = faceInfoRepo.findAll().get(0);
        Long faceInfoId = faceInfo.getId();

        FaceInfo result = faceInfoService.computeFaceInfoGeometry(faceInfoId, uploadId);

        assertThat(result.getNumberOfVertices()).isEqualTo(2500);
        assertThat(result.getNumberOfFacets()).isEqualTo(1);
    }
}

package cz.fidentis.web.api;

import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.user.User;
import cz.fidentis.web.user.UserApi;
import cz.fidentis.web.user.dto.ChangePasswordDto;
import cz.fidentis.web.user.dto.UserDetailDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getUser;
import static cz.fidentis.web.config.TestUtils.getUserDetailDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {UserApi.class})
class UserApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/users";

    @Test
    void getUsers() throws Exception {
        List<User> users = List.of(getUser(), getUser());

        when(userService.getUsers()).thenReturn(users);

        ResultActions result = mvc.perform(get(API_PREFIX));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(users)));
    }

    @Test
    void getUserById() throws Exception {
        Long userId = 1L;
        User user = getUser();

        when(userService.getUserById(userId)).thenReturn(user);

        ResultActions result = mvc.perform(get(API_PREFIX + "/{id}", userId));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(user)));
    }

    @Test
    void resetPassword() throws Exception {
        ChangePasswordDto changePasswordDto = new ChangePasswordDto("oldPassword",
                "newPassword", "newPassword");

        ResultActions result = mvc.perform(post(API_PREFIX + "/reset-password")
                .content(objectMapper.writeValueAsString(changePasswordDto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));;
    }

    @Test
    void updateUser() throws Exception {
        Long userId = 1L;
        UserDetailDto userDetailDto = getUserDetailDto();
        User updatedUser = getUser();

        when(userService.updateUser(any(), any())).thenReturn(updatedUser);

        ResultActions result = mvc.perform(put(API_PREFIX + "/{id}", userId)
                .content(objectMapper.writeValueAsString(userDetailDto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(updatedUser)));
    }

    @Test
    void deleteUser() throws Exception {
        Long userId = 1L;

        ResultActions result = mvc.perform(delete(API_PREFIX + "/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));;
    }
}


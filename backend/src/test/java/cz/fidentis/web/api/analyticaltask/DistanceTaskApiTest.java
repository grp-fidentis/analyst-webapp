package cz.fidentis.web.api.analyticaltask;

import cz.fidentis.web.analyticaltask.distance.DistanceTaskApi;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskDto;
import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskResponseDto;
import cz.fidentis.web.config.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getDistanceTaskDto;
import static cz.fidentis.web.config.TestUtils.getDistanceTaskResponseDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {DistanceTaskApi.class})
class DistanceTaskApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/analytical-tasks/distance";

    @Test
    void calculateHeatmap() throws Exception {
        DistanceTaskDto dto = getDistanceTaskDto(1L);
        DistanceTaskResponseDto responseDto = getDistanceTaskResponseDto();

        when(distanceTaskService.calculateHeatmap(any())).thenReturn(responseDto);

        ResultActions result = mvc.perform(post(API_PREFIX + "/calculate-heatmap")
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(responseDto)));
    }

    @Test
    void calculateDistance() throws Exception {
        DistanceTaskDto dto = getDistanceTaskDto(1L);
        DistanceTaskResponseDto responseDto = getDistanceTaskResponseDto();

        when(distanceTaskService.calculateDistance(any())).thenReturn(responseDto);

        ResultActions result = mvc.perform(post(API_PREFIX + "/calculate-distance")
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(responseDto)));
    }

    @Test
    void exportDistance() throws Exception {
        DistanceTaskDto dto = getDistanceTaskDto(1L);
        byte[] exportCsv = new byte[10];

        when(distanceTaskService.exportDistance(any())).thenReturn(exportCsv);

        ResultActions result = mvc.perform(post(API_PREFIX + "/export-distance")
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(content().bytes(exportCsv))
                .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=task_" + dto.getTaskId() + "_distance.csv"));
    }
}

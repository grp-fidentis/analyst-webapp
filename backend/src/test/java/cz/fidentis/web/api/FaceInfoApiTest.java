package cz.fidentis.web.api;

import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.fileupload.faceinfo.FaceInfoApi;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {FaceInfoApi.class})
class FaceInfoApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/face-info";

    @Test
    void getFaceInfoByFileUploadId() throws Exception {
        Long id = 1L;
        FaceInfo faceInfo = new FaceInfo();

        when(faceInfoService.getFaceInfoById(id)).thenReturn(faceInfo);

        ResultActions result = mvc.perform(get(API_PREFIX + "/{id}", id));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(faceInfo)));
    }

    @Test
    void computeFaceInfoGeometry() throws Exception {
        Long id = 1L;
        Long uploadId = 2L;
        FaceInfo faceInfo = new FaceInfo();

        when(faceInfoService.computeFaceInfoGeometry(id, uploadId)).thenReturn(faceInfo);

        ResultActions result = mvc.perform(post(API_PREFIX + "/{id}/compute-geometry-info/{uploadId}", id, uploadId));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(faceInfo)));
    }
}

package cz.fidentis.web.api;

import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.role.Role;
import cz.fidentis.web.role.RoleApi;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {RoleApi.class})
class RoleApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/roles";

    @Test
    void getRoles() throws Exception {
        List<Role> roles = List.of(new Role("admin"), new Role("user"));

        when(roleService.getRoles()).thenReturn(roles);

        ResultActions result = mvc.perform(get(API_PREFIX)
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(roles)));
    }

    @Test
    void createRole() throws Exception {
        Role role = new Role("user");

        when(roleService.saveRole(any())).thenReturn(role);

        ResultActions result = mvc.perform(post(API_PREFIX)
                .content(objectMapper.writeValueAsString(role))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(role)));
    }
}

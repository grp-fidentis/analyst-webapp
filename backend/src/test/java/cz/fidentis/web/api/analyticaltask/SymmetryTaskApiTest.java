package cz.fidentis.web.api.analyticaltask;

import cz.fidentis.web.analyticaltask.symmetry.SymmetryMethod;
import cz.fidentis.web.analyticaltask.symmetry.SymmetryTaskApi;
import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryResponseTaskDto;
import cz.fidentis.web.analyticaltask.symmetry.dto.SymmetryTaskDto;
import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.humanface.dto.HumanFaceSymmetryPlaneDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getSymmetryTaskDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {SymmetryTaskApi.class})
class SymmetryTaskApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/analytical-tasks/symmetry";

    @Test
    void calculate() throws Exception {
        SymmetryTaskDto symmetryTaskDto = getSymmetryTaskDto(1L);
        List<SymmetryResponseTaskDto> responseDtoList = List.of(
                new SymmetryResponseTaskDto(1L, new HumanFaceSymmetryPlaneDto()));

        when(symmetryTaskService.calculate(any())).thenReturn(responseDtoList);

        ResultActions result = mvc.perform(post(API_PREFIX + "/calculate")
                .content(objectMapper.writeValueAsString(symmetryTaskDto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(responseDtoList)));
    }
}

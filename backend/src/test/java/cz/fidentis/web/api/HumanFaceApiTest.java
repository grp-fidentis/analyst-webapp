package cz.fidentis.web.api;

import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.humanface.HumanFaceApi;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import cz.fidentis.web.humanface.dto.update.HumanFaceUpdateDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.getHumanFaceUpdateDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {HumanFaceApi.class})
class HumanFaceApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/human-faces";

    @Test
    void getHumanFaceDtosOfTask() throws Exception {
        Long taskId = 1L;
        List<HumanFaceDto> humanFaceDtos = List.of(new HumanFaceDto(), new HumanFaceDto());

        when(humanFaceService.getHumanFaceDtosOfTask(taskId)).thenReturn(humanFaceDtos);

        ResultActions result = mvc.perform(get(API_PREFIX + "/task/{taskId}", taskId));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(humanFaceDtos)));
    }

    @Test
    void saveGeometry() throws Exception {
        Long humanFaceId = 1L;
        HumanFaceUpdateDto faceUpdateDto = getHumanFaceUpdateDto();

        ResultActions result = mvc.perform(put(API_PREFIX + "/{id}/geometry", humanFaceId)
                .content(objectMapper.writeValueAsString(faceUpdateDto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));
    }
}

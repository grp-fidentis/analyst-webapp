package cz.fidentis.web.api.analyticaltask;

import cz.fidentis.web.analyticaltask.sampling.SamplingTaskApi;
import cz.fidentis.web.analyticaltask.sampling.SamplingTaskDto;
import cz.fidentis.web.humanface.dto.HumanFaceSamplesDto;
import cz.fidentis.web.config.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import static cz.fidentis.web.config.TestUtils.getHumanFaceSamplesDto;
import static cz.fidentis.web.config.TestUtils.getSamplingTaskDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {SamplingTaskApi.class})
class SamplingTaskApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/analytical-tasks/sampling";

    @Test
    void calculate() throws Exception {
        SamplingTaskDto dto = getSamplingTaskDto(1L);
        HumanFaceSamplesDto samplesDto = getHumanFaceSamplesDto();

        when(samplingTaskService.calculate(any())).thenReturn(samplesDto);

        ResultActions result = mvc.perform(post(API_PREFIX + "/calculate")
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(samplesDto)));
    }
}

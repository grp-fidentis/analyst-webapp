package cz.fidentis.web.api;

import cz.fidentis.web.auth.AuthApi;
import cz.fidentis.web.auth.dto.EmailDto;
import cz.fidentis.web.auth.dto.ResetPasswordDto;
import cz.fidentis.web.user.dto.UserDetailDto;
import cz.fidentis.web.config.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.HashMap;
import java.util.Map;

import static cz.fidentis.web.config.TestUtils.getUserDetailDto;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.HOST;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {AuthApi.class})
class AuthApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/auth";

    @Test
    void refreshUserToken() throws Exception {
        String authorizationHeaderValue = "Bearer <token>";
        String accessToken = "access";
        String refreshToken = "access";
        Map<String, String> resultTokens = new HashMap<>();
        resultTokens.put("Access-Token", accessToken);
        resultTokens.put("Refresh-Token", refreshToken);

        when(authService.refreshUserToken(anyString(), anyString())).thenReturn(resultTokens);
        when(jwtConfig.getTokenPrefix()).thenReturn("not real value to evade Filter logic");

        ResultActions result = mvc.perform(get(API_PREFIX + "/refresh-token")
                .header(AUTHORIZATION, authorizationHeaderValue));

        result.andExpect(status().isOk())
                .andExpect(header().exists("Access-Token"))
                .andExpect(header().string("Access-Token", accessToken))
                .andExpect(header().exists("Refresh-Token"))
                .andExpect(header().string("Refresh-Token", refreshToken));
    }

    @Test
    void register() throws Exception {
        String hostUrl = "localhost:8080";
        UserDetailDto userDetailDto = getUserDetailDto();

        ResultActions result = mvc.perform(post(API_PREFIX + "/register")
                .header("Host", hostUrl)
                .content(objectMapper.writeValueAsString(userDetailDto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isCreated());
    }

    @Test
    void forgotPassword() throws Exception {
        String hostUrl = "http://localhost:8080";
        EmailDto dto = new EmailDto("user@example.com");

        ResultActions result = mvc.perform(post(API_PREFIX + "/forgot-password")
                .header(HOST, hostUrl)
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));
    }

    @Test
    void resetPassword() throws Exception {
        ResetPasswordDto resetPasswordDto = new ResetPasswordDto("token", "password");

        ResultActions result = mvc.perform(post(API_PREFIX + "/reset-password")
                .content(objectMapper.writeValueAsString(resetPasswordDto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));
    }
}

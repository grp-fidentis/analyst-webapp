package cz.fidentis.web.api.analyticaltask;

import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskApi;
import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskDto;
import cz.fidentis.web.analyticaltask.curvature.CurvatureTaskResponseDto;
import cz.fidentis.web.config.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import static cz.fidentis.web.config.TestUtils.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = CurvatureTaskApi.class)
class CurvatureTaskApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/analytical-tasks/curvature";

    @Test
    void calculateHeatmap() throws Exception {
        CurvatureTaskDto dto = getCurvatureTaskDto(1L);
        CurvatureTaskResponseDto responseDto = getCurvatureTaskResponseDto();

        when(curvatureTaskService.calculateHeatmap(any())).thenReturn(responseDto);

        ResultActions result = mvc.perform(post(API_PREFIX + "/calculate-heatmap")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)));

        result.andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(responseDto)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

}

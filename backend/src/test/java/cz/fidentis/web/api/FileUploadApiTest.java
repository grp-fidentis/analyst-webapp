package cz.fidentis.web.api;

import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.config.dto.SimpleFileUploadDetail;
import cz.fidentis.web.fileupload.FileData;
import cz.fidentis.web.fileupload.FileUpload;
import cz.fidentis.web.fileupload.FileUploadApi;
import cz.fidentis.web.fileupload.dto.DeleteAdditionalFileDto;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.fileupload.faceinfo.FaceInfo;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.user.User;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {FileUploadApi.class})
class FileUploadApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/file-uploads";

    @Test
    void getFilesOfProject() throws Exception {
        Long projectId = 1L;
        List<FileUploadDetail> fileUploadDetails = List.of(new SimpleFileUploadDetail(), new SimpleFileUploadDetail());

        when(fileUploadService.getFilesOnProject(projectId)).thenReturn(fileUploadDetails);

        ResultActions result = mvc.perform(get(API_PREFIX + "/projects/{projectId}", projectId));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(fileUploadDetails)));
    }

    @Test
    void saveFiles() throws Exception {
        Long projectId = 1L;
        MockMultipartFile file1 = new MockMultipartFile("files", "file1.obj", MediaType.APPLICATION_OCTET_STREAM_VALUE, "file content".getBytes());
        MockMultipartFile file2 = new MockMultipartFile("files", "file2.obj", MediaType.APPLICATION_OCTET_STREAM_VALUE, "file content".getBytes());

        ResultActions result = mvc.perform(multipart(HttpMethod.POST, API_PREFIX + "/{projectId}", projectId)
                .file(file1)
                .file(file2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));

        verify(fileUploadService, times(1)).saveFile(file1, projectId);
        verify(fileUploadService, times(1)).saveFile(file2, projectId);

    }

    @Test
    void saveAdditionalFile() throws Exception {
        Long projectId = 1L;
        Long faceInfoId = 2L;
        String fileType = "preview";
        MockMultipartFile file = new MockMultipartFile("file", "preview.jpg", MediaType.IMAGE_JPEG_VALUE, "image content".getBytes());
        FaceInfo faceInfo = new FaceInfo();

        when(fileUploadService.uploadAdditionalFile(file, projectId, faceInfoId, fileType)).thenReturn(faceInfo);

        ResultActions result = mvc.perform(multipart(HttpMethod.POST, API_PREFIX + "/{projectId}/face-info/{faceInfoId}", projectId, faceInfoId)
                .file(file)
                .param("fileType", fileType)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(faceInfo)));
    }

    @Test
    void uploadSmallPreviewPhoto() throws Exception {
        Long id = 1L;
        MockMultipartFile file = new MockMultipartFile("file", "small_preview.jpg", MediaType.IMAGE_JPEG_VALUE, "image content".getBytes());
        FileUpload fileUpload = new FileUpload("file", ".jpg", new User(), new Project(), new FaceInfo(), new FileData());

        when(fileUploadService.uploadSmallPreviewPhoto(id, file)).thenReturn(fileUpload);

        ResultActions result = mvc.perform(multipart(HttpMethod.POST, API_PREFIX + "/{id}/preview-photo", id)
                .file(file)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(fileUpload)));
    }

    @Test
    void deleteFiles() throws Exception {
        List<Long> ids = List.of(1L, 2L, 3L);

        ResultActions result = mvc.perform(delete(API_PREFIX)
                .content(objectMapper.writeValueAsString(ids))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));
    }

    @Test
    void deleteAdditionalFile() throws Exception {
        Long faceInfoId = 1L;
        DeleteAdditionalFileDto dto = new DeleteAdditionalFileDto("file_name", "jpg", "preview");

        ResultActions result = mvc.perform(delete(API_PREFIX + "/face-info/{faceInfoId}", faceInfoId)
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));
    }
}

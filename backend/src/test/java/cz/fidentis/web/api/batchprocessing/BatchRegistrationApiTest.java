package cz.fidentis.web.api.batchprocessing;

import cz.fidentis.web.batchprocessing.registration.BatchRegistrationApi;
import cz.fidentis.web.batchprocessing.registration.dto.BatchRegistrationTaskDto;
import cz.fidentis.web.batchprocessing.registration.dto.RegistrationResponseDto;
import cz.fidentis.web.batchprocessing.registration.dto.RegistrationResultDto;
import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.config.dto.SimpleFileUploadDetail;
import cz.fidentis.web.fileupload.dto.FileUploadDetail;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import static cz.fidentis.web.config.TestUtils.getAverageHumanFaceDto;
import static cz.fidentis.web.config.TestUtils.getBatchRegistrationTaskDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = BatchRegistrationApi.class)
class BatchRegistrationApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/batch-processing/registration";

    @Test
    void execute() throws Exception {
        FileUploadDetail fileUploadDetail = new SimpleFileUploadDetail(1L, "file.obj","obj", false, new byte[10], 1L, null);
        BatchRegistrationTaskDto dto = getBatchRegistrationTaskDto(1L, 1L, fileUploadDetail);
        HumanFaceDto humanFaceDto = getAverageHumanFaceDto();
        RegistrationResponseDto registrationResponseDto = new RegistrationResponseDto();
        registrationResponseDto.setHumanFace(humanFaceDto);
        registrationResponseDto.setCompletedSuccessfully(true);
        when(batchRegistrationService.execute(any())).thenReturn(registrationResponseDto);

        ResultActions result = mvc.perform(post(API_PREFIX + "/execute")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)));

        result.andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(registrationResponseDto)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void exportAverageFace() throws Exception {
        long humanFaceEntityId = 1L;
        byte[] exportCsv = new byte[10];

        when(batchRegistrationService.exportAverageFace(any())).thenReturn(exportCsv);

        ResultActions result = mvc.perform(post(API_PREFIX + "/export-average-face/" + humanFaceEntityId)
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(content().bytes(exportCsv))
                .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + "Average Face.obj"));
    }
}

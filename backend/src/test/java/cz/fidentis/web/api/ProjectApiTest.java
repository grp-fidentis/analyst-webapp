package cz.fidentis.web.api;

import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.project.Project;
import cz.fidentis.web.project.ProjectApi;
import cz.fidentis.web.project.ProjectSimpleDto;
import cz.fidentis.web.user.User;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {ProjectApi.class})
class ProjectApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/projects";

    @Test
    void createProject() throws Exception {
        String projectName = "New Project";
        Project project = new Project(projectName, new User());

        when(projectService.createProject(projectName)).thenReturn(project);

        ResultActions result = mvc.perform(post(API_PREFIX)
                .queryParam("name", projectName));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(project)));
    }

    @Test
    void getUserProjects() throws Exception {
        List<ProjectSimpleDto> projectSimpleDtos = List.of(new ProjectSimpleDto(1L, "name", true));

        when(projectService.getProjects(any())).thenReturn(projectSimpleDtos);

        ResultActions result = mvc.perform(get(API_PREFIX)
                .queryParam("isOpen", "true"));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(projectSimpleDtos)));
    }

    @Test
    void setOpenToProjects() throws Exception {
        ResultActions result = mvc.perform(put(API_PREFIX + "/{id}/set-open-status", 1)
                .queryParam("open", "true"));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));
    }

    @Test
    void deleteProject() throws Exception {
        ResultActions result = mvc.perform(delete(API_PREFIX + "/{id}", "1"));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));
    }
}

package cz.fidentis.web.api.analyticaltask;

import cz.fidentis.web.analyticaltask.cuttingplanes.CuttingPlanesTaskApi;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlaneDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlanes2DProjectionDto;
import cz.fidentis.web.analyticaltask.cuttingplanes.dto.CuttingPlanes2DProjectionResponseDto;
import cz.fidentis.web.config.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static cz.fidentis.web.config.TestUtils.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = CuttingPlanesTaskApi.class)
class CuttingPlanesTaskApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/analytical-tasks/cutting-planes";

    @Test
    void createCuttingPlanes() throws Exception {
        List<CuttingPlaneDto> responseDto = getCuttingPlaneDtos();

        when(cuttingPlanesTaskService.createCuttingPlanes(any())).thenReturn(responseDto);

        ResultActions result = mvc.perform(post(API_PREFIX + "/task/{taskId}", 1L));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(responseDto)));
    }

    @Test
    void calculate2DProjection() throws Exception {
        Long taskId = 1L;
        CuttingPlanes2DProjectionDto dto = getCuttingPlanes2DProjectionDto(taskId);
        CuttingPlanes2DProjectionResponseDto responseDto = getCuttingPlanes2DProjectionResponseDto(taskId);

        when(cuttingPlanesTaskService.calculate2DProjection(any())).thenReturn(responseDto);

        ResultActions result = mvc.perform(post(API_PREFIX + "/calculate-2D-projection")
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(objectMapper.writeValueAsString(responseDto)));
    }

    @Test
    void exportProjectionsAsZip() throws Exception {
        CuttingPlanes2DProjectionDto dto = getCuttingPlanes2DProjectionDto(1L);
        byte[] exportZip = new byte[10];

        when(cuttingPlanesTaskService.exportProjectionsAsZip(any())).thenReturn(exportZip);

        String fileName = "task_1_cutting_planes_2D_projection.zip";

        ResultActions result = mvc.perform(post(API_PREFIX + "/export")
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(content().bytes(exportZip))
                .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName + ""));
    }

}

package cz.fidentis.web.api.analyticaltask;

import cz.fidentis.web.analyticaltask.distance.dto.DistanceTaskDto;
import cz.fidentis.web.analyticaltask.registration.RegistrationMethod;
import cz.fidentis.web.analyticaltask.registration.RegistrationTaskApi;
import cz.fidentis.web.analyticaltask.registration.RegistrationTaskDto;
import cz.fidentis.web.analyticaltask.sampling.SamplingTaskDto;
import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.humanface.dto.HumanFaceDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import static cz.fidentis.web.config.TestUtils.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = RegistrationTaskApi.class)
class RegistrationTaskApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/analytical-tasks/registration";

    @Test
    void execute() throws Exception {
        RegistrationTaskDto dto = getRegistrationTaskDto(1L);
        HumanFaceDto humanFaceDto = getHumanFaceDto();

        when(registrationTaskService.execute(any())).thenReturn(humanFaceDto);

        ResultActions result = mvc.perform(post(API_PREFIX + "/execute")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)));

        result.andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(humanFaceDto)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

}

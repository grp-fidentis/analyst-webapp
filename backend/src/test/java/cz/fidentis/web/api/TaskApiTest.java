package cz.fidentis.web.api;

import cz.fidentis.web.config.ApiTestBase;
import cz.fidentis.web.config.dto.SimpleTaskDto;
import cz.fidentis.web.task.TaskApi;
import cz.fidentis.web.task.TaskType;
import cz.fidentis.web.task.dto.CreateTaskDto;
import cz.fidentis.web.task.dto.TaskDto;
import cz.fidentis.web.task.dto.UpdateNameDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = {TaskApi.class})
class TaskApiTest extends ApiTestBase {

    private static final String API_PREFIX = "/api/tasks";

    @Test
    void getUserTasksOnProject() throws Exception {
        Long projectId = 1L;
        List<TaskDto> taskDtos = List.of(
                new SimpleTaskDto(1L, projectId, 1L, "name", TaskType.PAIR_COMPARISON),
                new SimpleTaskDto(1L, projectId, 1L,"name", TaskType.PAIR_COMPARISON));

        when(taskService.getProjectTasks(projectId)).thenReturn(taskDtos);

        ResultActions result = mvc.perform(get(API_PREFIX)
                .param("projectId", String.valueOf(projectId))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(taskDtos)));
    }

    @Test
    void getTaskById() throws Exception {
        Long taskId = 1L;
        TaskDto taskDto = new SimpleTaskDto(taskId, 1L, 1L, "name", TaskType.PAIR_COMPARISON);

        when(taskService.getTaskDtoById(taskId)).thenReturn(taskDto);

        ResultActions result = mvc.perform(get(API_PREFIX + "/{id}", taskId)
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(taskDto)));
    }

    @Test
    void createTask() throws Exception {
        CreateTaskDto createTaskDto = new CreateTaskDto(1L, List.of(1L, 2L));
        TaskDto taskDto = new SimpleTaskDto(1L, 1L, 1L, "name", TaskType.PAIR_COMPARISON);

        when(taskService.createTask(createTaskDto)).thenReturn(taskDto);

        ResultActions result = mvc.perform(post(API_PREFIX)
                .content(objectMapper.writeValueAsString(createTaskDto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(taskDto)));
    }

    @Test
    void updateTaskName() throws Exception {
        Long taskId = 1L;
        String updatedTaskName = "Updated Task";
        UpdateNameDto updateNameDto = new UpdateNameDto(updatedTaskName);
        TaskDto taskDto = new SimpleTaskDto(taskId, 1L, 1L, updatedTaskName, TaskType.PAIR_COMPARISON);

        when(taskService.updateTaskName(updateNameDto.name(), taskId)).thenReturn(taskDto);

        ResultActions result = mvc.perform(put(API_PREFIX + "/{id}", taskId)
                .content(objectMapper.writeValueAsString(updateNameDto))
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(taskDto)));
    }

    @Test
    void deleteTask() throws Exception {
        Long taskId = 1L;

        ResultActions result = mvc.perform(delete(API_PREFIX + "/{id}", taskId));

        result.andExpect(status().isNoContent())
                .andExpect(content().bytes(EMPTY_BODY));
    }
}

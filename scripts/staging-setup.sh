#!/bin/bash
#
# This script sets up a clear instalation of a Debian staging server by installing all required prerequisities.
#
# PREREQUISITES:
#   - A PostgreSQL database running at db.fi.muni.cz.
#
# MINIMAL REQUIREMENTS put on the staging server:
#   Linux (preferred Debian 11+)
#    8 GB RAM
#   10 GB HD
#    4 VCPUs
#
# MANUAL STEP to enable SSH access:
#   - Generate ssh key pair
#     - ssh-keygen -t rsa -C "GitLab SSH key"
#   - Configure gitlab project - add protected variables (Settings -> CI/CD -> Variables -> Expand -> Add Variable):
#     - SSH_STAGING_PRIVATE_KEY - the content of the private key
#     - SSH_STAGING_USER - name of the user on the remote server
#     - STAGING__IPADDRESS - IP address of the staging server
#   - Add public key to staging server
#     - Login as the same user which you have specified in SSH_USER GitLab's variable.
#     - Paste the public key into ~/.ssh/authorized_keys file.
#

#if [ "$#" -ne 2 ]; then
#  echo "use staging-setup.sh DB_NAME DB_PASSWORD"
#  exit 0;

#
# See project's GitLab variables for correct values of FIDENTIS database name PostgreSQL password
#
HOSTNAME="fidentis-stage.fi.muni.cz"
#DB_NAME=$1
#DB_PASSWORD=$2

#
# Install required SW
# 
# NOTE: On the staging server with Debian 11, Oracle JDK 21 has been installed manually 
#       (installing OpenJDK 21 requires too many manual steps).
#       Debian 12 should include OpenJDK 21 by default.
#
apt-get update
apt install -y postgresql maven openjdk-21-jdk

#
# Install NVM needed by the client code
#
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
source ~/.bashrc
nvm install 16.13.1
curl -qL https://www.npmjs.com/install.sh | sh

#
# OBSOLETE (see prerequisites): 
# Set up PostgreSQL, create the FIDENTIS database.
# Then, you can use 'psql -h localhost -p 5432 -U postgres -W fidentis' to connect the DB.
#
# sudo -i -u postgres psql -c "ALTER USER postgres PASSWORD '$DB_PASSWORD'; create database $DB_NAME"

#
# Prepare FIDENTIS Analyst folder
#
mkdir /opt/fidentis-analyst


#
# Enable FIDENTIS app service.
# Then, the app is available at https://$HOSTNAME:8080
#
echo -e "[Unit]\nDescription=FIDENTIS Analyst 2 Service\nAfter=syslog.target\n\n[Service]\nWorkingDirectory=/opt/fidentis-analyst/analyst-webapp/backend/\nExecStart=mvn spring-boot:run -Pprod\n\n[Install]\nWantedBy=multi-user.target\n" > /etc/systemd/system/fidentis.service
systemctl daemon-reload
systemctl enable fidentis.service
systemctl start fidentis.service

##############################################################################
# Let's Encrypt certificate. See https://gitlab.fi.muni.cz/unix/letsencrypt-fi
#
apt install -y openssl perl openssh-server apache2
pushd /opt/fidentis-analyst

rm -rf letsencrypt-fi
git clone https://gitlab.fi.muni.cz/unix/letsencrypt-fi.git
pushd letsencrypt-fi
./le-gen-csr $HOSTNAME
cp $HOSTNAME.key /etc/ssl/private/
cp $HOSTNAME.crt /etc/ssl/certs/
cp $HOSTNAME-bundle.crt /etc/ssl/certs/
cp $HOSTNAME-chain.crt /etc/ssl/certs/
chmod go= /etc/ssl/private/$HOSTNAME.key

cp le-renewed-cert /usr/local/sbin/
sed -i "s|/etc/pki/tls/certs|/etc/ssl/certs|" /usr/local/sbin/le-renewed-cert
cat letsencrypt-fi-authorized-keys >> /root/.ssh/authorized_keys
popd

#
# Run Apache for automated updates of the Let's Encrypt certificate
#
pushd /etc/apache2/sites-enabled
rm pushd /etc/apache2/sites-enabled
echo -e "<VirtualHost $HOSTNAME:80>
     ServerAdmin webmaster@localhost
     DocumentRoot /var/www/html
     ErrorLog \${APACHE_LOG_DIR}/error.log
     CustomLog \${APACHE_LOG_DIR}/access.log combined
     Redirect permanent / https://$HOSTNAME/
</VirtualHost>" > $HOSTNAME.conf
echo -e "<IfModule mod_ssl.c>
	<VirtualHost _default_:443>
		ServerAdmin webmaster@localhost
		DocumentRoot /var/www/html
		ErrorLog \${APACHE_LOG_DIR}/error.log
		CustomLog \${APACHE_LOG_DIR}/access.log combined
		SSLEngine on
		SSLCertificateFile	/etc/ssl/certs/fidentis-stage.fi.muni.cz-chain.crt
		SSLCertificateKeyFile /etc/ssl/private/fidentis-stage.fi.muni.cz.key
		<FilesMatch \"\.(cgi|shtml|phtml|php)\$\">
				SSLOptions +StdEnvVars
		</FilesMatch>
		<Directory /usr/lib/cgi-bin>
				SSLOptions +StdEnvVars
		</Directory>
		Header always set Strict-Transport-Security \"max-age=3600;\"
		Redirect temp /.well-known/acme-challenge/ https://fadmin.fi.muni.cz/.well-known/acme-challenge/
	</VirtualHost>
</IfModule>" > $HOSTNAME-ssl.conf
popd

pushd /etc/apache2/mods-enabled
ln -s ../mods-available/ssl.conf
ln -s ../mods-available/ssl.load
ln -s ../mods-available/socache_shmcb.load
ln -s ../mods-available/headers.load
popd

systemctl reload apache2.service

popd

popd /etc/cron.daily
wget https://fadmin.fi.muni.cz/noauth/sshkh/get_ssh_hosts.tar.gz
tar xvzf get_ssh_hosts.tar.gz
rm get_ssh_hosts.tar.gz 
popd




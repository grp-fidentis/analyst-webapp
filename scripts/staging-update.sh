#!/bin/bash
#
# This script is to be uploaded onto a staging server end invoked remotely via SSH
# by CI/CD from gitlab.
# The script updates and restarts FIDENTIS Analyst web app on the staging server.
#
if [ "$#" -ne 5 ]; then
  echo "use staging.sh DB_USER DB_PASSWORD SSL_PASSWORD MUNI_OIDC_CLIENT_ID MUNI_OIDC_CLIENT_SECRET"
  exit 1;
fi

#
# Configuration section
#
CONFIG_FILE="backend/src/main/resources/application-prod.yml"
CONFIG_FILE_MAIN="backend/src/main/resources/application.yml"
DB_USER=$1
DB_PASSWORD=$2
SSL_PASSWORD=$3
MUNI_OIDC_CLIENT_ID=$4
MUNI_OIDC_CLIENT_SECRET=$5

#
# Fail immediately with an error exit code when some command fails.
#
set -e

#
# Re-generate SSL certificate
#
pushd /etc/ssl/certs
openssl pkcs12 -export -in fidentis-stage.fi.muni.cz-chain.crt -out fidentis-ssl.p12 -name fidentis-ssl -inkey /etc/ssl/private/fidentis-stage.fi.muni.cz.key -passout pass:$SSL_PASSWORD
popd

#
# Compile FIDENTIS
#
pushd /opt/fidentis-analyst

rm -rf analyst2
git clone https://gitlab.fi.muni.cz/grp-fidentis/analyst2.git
pushd analyst2
mvn clean install -DskipTests
popd

rm -rf analyst-webapp
git clone https://gitlab.fi.muni.cz/grp-fidentis/analyst-webapp.git
pushd analyst-webapp
sed -i -e "s/DB_USER/$DB_USER/g" $CONFIG_FILE
sed -i -e "s/DB_PASSWORD/$DB_PASSWORD/g" $CONFIG_FILE
sed -i -e "s/MUNI_OIDC_CLIENT_ID/$MUNI_OIDC_CLIENT_ID/g" $CONFIG_FILE_MAIN
sed -i -e "s/MUNI_OIDC_CLIENT_SECRET/$MUNI_OIDC_CLIENT_SECRET/g" $CONFIG_FILE_MAIN
cp /etc/ssl/certs/fidentis-ssl.p12 ./backend/src/main/resources/
mvn clean install -DskipTests -Pprod
popd

popd

#
# Run FIDENTIS Web App as a system service
#
systemctl restart fidentis.service


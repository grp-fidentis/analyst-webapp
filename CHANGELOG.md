* 33422698 -- Update pom.xml version.
*   d32c20cd -- Merge branch '205-refactor-project-gui' into 'master'
|\  
| * 11b5d39c -- Resolve "Refactor project GUI"
|/  
*   a4febb9e -- Merge branch '204-refactor-gui-classes' into 'master'
|\  
| * d819d0ef -- Resolve "Refactor GUI classes"
|/  
*   e71cfc26 -- Merge branch '202-move-project-related-logic-into-the-project-module' into 'master'
|\  
| * a10d47f3 -- Resolve "Move project-related logic into the Project module"
|/  
*   3f5ff965 -- Merge branch '201-refactor-project-management' into 'master'
|\  
| * fa1cd230 -- Resolve "Refactor project management"
|/  
*   6909cabf -- Merge branch '200-move-project-management-into-a-toolbar' into 'master'
|\  
| * 185f8022 -- Resolve "Move project management into a toolbar"
|/  
* bc57dac5 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 4e90787a -- Update pom.xml version.
*   93eaf830 -- Merge branch '198-accelerate-octree-creation' into 'master'
|\  
| * 2e52b77d -- Resolve "Accelerate octree creation"
|/  
*   cf48d852 -- Merge branch '197-remove-dependency-of-gui-on-jogl' into 'master'
|\  
| * a07a91ca -- Resolve "Remove dependency of GUI on JOGL"
|/  
*   df507270 -- Merge branch '196-fix-readme' into 'master'
|\  
| * d4d31fbf -- Resolve "Fix README"
|/  
*   beb195cf -- Merge branch '108-upgrade-to-jdk-17-2' into 'master'
|\  
| * 7a771e38 -- Resolve "Upgrade to JDK 17"
|/  
*   6922bb8c -- Merge branch '191-fix-error-in-feature-points-rendering' into 'master'
|\  
| * 2883b90b -- Resolve "Fix error in feature points rendering"
|/  
*   1882da95 -- Merge branch '190-introduce-spacepartioning-module' into 'master'
|\  
| * 0c0b05c8 -- Resolve "Introduce SpacePartioning module"
|/  
*   fb401659 -- Merge branch '193-move-project-classes-to-project-package' into 'master'
|\  
| * 173c3b14 -- moved project files to separate package
|/  
*   60f0ac1d -- Merge branch 'issue-186/procrustes-documentation' into 'master'
|\  
| * 013ccf8f -- MR [#186] documentation
|/  
*   27c3d80f -- Merge branch '192-fix-rendering-of-fps-and-lights-when-texture-is-turned-on' into 'master'
|\  
| * 74f990e7 -- Resolve "Fix rendering of FPs and lights when texture is turned on"
|/  
* 5d6b4b8a -- Add LICENSE
*   90d401a7 -- Merge branch 'procrustes-refactor' into 'master'
|\  
| * 4a13944f -- refactor: Removing not need code from procrustes face model
|/  
*   48cd2542 -- Merge branch '181-edit-feature-points' into 'master'
|\  
| * 440e419d -- Resolve "Edit feature points"
|/  
*   e2b13218 -- Merge branch 'issue-123/procrustes-visitor' into 'master'
|\  
| * 4dbd9a33 -- Minor improvements before merge
| * 6f18f763 -- [#123] feat: rework of procrustes face model class and simplyfication of analysis
| * e33246e2 -- [#123] feat: Procrustes visitor has history of applied transformations
| * 63355ca1 -- [#123] feat: reworking procrustes visitor to extend HumanFaceVisitor
| *   796d7224 -- Merge branch 'issue-123/procrustes-visitor'
| |\  
| | * 4207346e -- [#123] rebase: rebase on master branch
| | * 58d6f682 -- [#123] test: adding tests and refactoring code
| | * a2616ddb -- [#123] feat: procrustes visitor superimposition of points
| | * c5d994f1 -- [123] feat: procrustes analysis with subset of feature points
| | * 2c5017f6 -- [#123] feat: visualisation of rotated featurepoints
| | * 9411d1ad -- [#123] feat: rotation issue
| | * e958e2df -- [#123] feat: procrustes visitor rework
| | * 1c2ba9ed -- [#123] feat: Procrustes Visitor
* | |   762be9ed -- Merge branch '184-move-useOctree-to-GUI' into 'master'
|\ \ \  
| * | | 7c26e69c -- Move useOctree variable to GUI and create it's own check box
|/ / /  
* | |   452054af -- Merge branch '182-indicate-faces-analyzed-in-multiple-tasks' into 'master'
|\ \ \  
| * | | a53cbdf4 -- Resolve "Indicate faces analyzed in multiple tasks"
|/ / /  
* | |   988dfb20 -- Merge branch '126-create-average-face-contructor-for-octrees' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | 8ef1afdd -- Compute Average Face
|/ /  
* |   5b39bd6c -- Merge branch '183-fix-material-serialization' into 'master'
|\ \  
| * | c1dd8f84 -- Resolve "Fix material serialization"
|/ /  
* |   16c9fcaa -- Merge branch '180-adaptive-downsampling-for-icp' into 'master'
|\ \  
| * | 5b791b26 -- Resolve "Adaptive downsampling for ICP"
|/ /  
* |   eacd7eef -- Merge branch '171-integrate-face-state-tab-into-task-views' into 'master'
|\ \  
| * | f1b9c1b6 -- Resolve "Integrate Face state tab into task views"
|/ /  
* |   f330a3c8 -- Merge branch 'ro-load-material-and-texture-together-with-face-geometry' into 'master'
|\ \  
| * | 989d8778 -- Ro load material and texture together with face geometry
* | |   457f5429 -- Merge branch '168-adapt-filter-tab' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | be46d007 -- Resolve "Adapt Filter tab"
|/ /  
* |   a1d037ac -- Merge branch '177-fix-error-in-cutting-plane-1-1' into 'master'
|\ \  
| |/  
|/|   
| * 64e2e116 -- Resolve "Fix error in cutting plane 1:1"
|/  
*   1d78362c -- Merge branch '178-fix-error-in-the-registration-gui' into 'master'
|\  
| * f40f7a6f -- Resolve "Fix error in the registration GUI"
|/  
*   00b599e2 -- Merge branch '174-introduce-a-uniform-space-sampling' into 'master'
|\  
| * 857af3dd -- Resolve "Introduce a uniform space sampling"
|/  
*   9713c5aa -- Merge branch '176-add-drawable-line' into 'master'
|\  
| * 94e0f2c8 -- Added drawable line
|/  
*   befdabbe -- Merge branch '167-adapt-face-info-tab' into 'master'
|\  
| * b381ba89 -- Resolve "Adapt Face info tab"
|/  
*   3a1a4371 -- Merge branch '161-165-fix-and-improve-project-workflow' into 'master'
|\  
| *   118ceedb -- Merge branch 'master' into 161-165-fix-and-improve-project-workflow
| |\  
| * | bf288f1a -- fixed scrolling of project tab
| * | 9a88a1aa -- fixed preview image loading
| * |   36bfff07 -- Merge branch 'master' into 161-165-fix-and-improve-project-workflow
| |\ \  
| * | | 3bf4e9be -- fixed saving project
| * | | 0aae829f -- fixed image preview, showing preview images
| * | | eeaacb1d -- changed the behavior of initial window
| * | | 4eb34529 -- fixed image preview
| * | | f70ed1cc -- added selection of the primary face in 1:1
| * | | 6183290e -- fix N:N tab opening
* | | |   2ff7d689 -- Merge branch '173-introduce-point-clouds' into 'master'
|\ \ \ \  
| * | | | b2969a87 -- Introduce point sampling strategies for ICP and symmetry"
|/ / / /  
* | | |   9d7cad76 -- Merge branch '172-visualize-bouding-box-for-cutting-plane' into 'master'
|\ \ \ \  
| |_|_|/  
|/| | |   
| * | | 68f736dd -- Resolve "Visualize bouding box for cutting plane"
|/ / /  
* | |   85a892f1 -- Merge branch '170-fix-showing-path-in-file-info-panel' into 'master'
|\ \ \  
| |_|/  
|/| |   
| * | 57093535 -- fixed showing path; changed font
|/ /  
* |   3f4435ae -- Merge branch '166-adapt-file-info-tab' into 'master'
|\ \  
| |/  
|/|   
| * b854fefa -- adapt file info tab
|/  
*   394cd0cd -- Merge branch '158-upgrade-project-tab-load-landmarks-open-close-tabs-from-menu-save-project-when-closing-app' into 'master'
|\  
| * f5315c9b -- Resolve "Upgrade project tab (opening and closing app changes; analyse models; opening preview image)"
|/  
*   4719829d -- Merge branch '160-accelerate-n-n' into 'master'
|\  
| * 578cc3bb -- Resolve "Accelerate N:N"
|/  
*   b57b6375 -- Merge branch '159-accelerate-swapping' into 'master'
|\  
| *   b3dc7133 -- Merge branch 'master' into '159-accelerate-swapping'
| |\  
| |/  
|/|   
* |   e24b2ea7 -- Merge branch '159-accelerate-swapping' into 'master'
|\ \  
| * | 0bc94b7a -- Resolve "Accelerate swapping"
|/ /  
| * 8d58415a -- N:N integrated into the project tab
| * 9267f7f2 -- Using Kryo with objenesis to accelerate serialization
|/  
*   d31bc855 -- Merge branch '157-fix-symmetry-plane-alignment' into 'master'
|\  
| * b35e2db9 -- Resolve "Fix symmetry plane alignment"
|/  
*   bd88d625 -- Merge branch '155-remove-eventbus-from-projectpanel' into 'master'
|\  
| * 7bedd802 -- Resolve "Remove EventBus from ProjectPanel"
|/  
*   028d627a -- Merge branch '156-fix-icp-scaling' into 'master'
|\  
| * 82026c27 -- Resolve "Fix ICP scaling"
|/  
*   0d5d4478 -- Merge branch '131-remove-crosssection-remain-crosssectionzigzag-only' into 'master'
|\  
| * 44f75a2a -- Resolve "Remove CrossSection, remain CrossSectionZigZag only"
|/  
*   d9f2884a -- Merge branch '154-introduce-humanfaceutils' into 'master'
|\  
| * 05bf4a11 -- Resolve "Introduce HumanFaceUtils"
|/  
*   86f9932c -- Merge branch '149-project-tab-improvements' into 'master'
|\  
| * 7a233e97 -- Resolve "Project tab improvements"
|/  
*   3793a05a -- Merge branch '152-improve-batch-processing' into 'master'
|\  
| * b63600f0 -- Resolve "Improve batch processing"
|/  
*   c1e4c798 -- Merge branch '151-compute-hd-batch-finalization-by-using-opencl' into 'master'
|\  
| * 3956ed7f -- Resolve "Compute HD batch finalization by using OpenCL"
|/  
*   759528af -- Merge branch '148-fix-batch-processing' into 'master'
|\  
| * 758bae20 -- Resolve "Fix batch processing"
|/  
*   a9500202 -- Merge branch '146-export-average-face' into 'master'
|\  
| * 4615002e -- Resolve "Export average face"
|/  
*   36c1cef3 -- Merge branch '127-add-basic-json-scheme-for-project' into 'master'
|\  
| * b3ad53f4 -- Resolve "Add basic JSON scheme for project"
|/  
*   6d5a67c6 -- Merge branch '145-fix-landmarks-initialization-from-file' into 'master'
|\  
| * ff5495ae -- Resolve "Fix landmarks initialization from file"
|/  
*   10d197ac -- Merge branch '144-fix-batch-mode-rendering-of-avg-face' into 'master'
|\  
| * 12b00022 -- Resolve "Fix batch mode rendering of AVG face"
|/  
*   b5cd9492 -- Merge branch '143-fix-the-scene' into 'master'
|\  
| * 1ea67138 -- Resolve "Fix the scene"
|/  
*   88316bf8 -- Merge branch '142-introduce-batch-processing-gui' into 'master'
|\  
| * d23bf126 -- Resolve "Introduce batch processing GUI"
|/  
*   53b5c786 -- Merge branch '111-create-octree' into 'master'
|\  
| * 20b29d73 -- Create Octree structure and create Visitor for Octree
|/  
*   411006ff -- Merge branch '141-create-batch-evaluation-tests' into 'master'
|\  
| * 47c5ce9e -- Resolve "Create batch evaluation tests"
|/  
*   6690a848 -- Merge branch '140-optimize-symmetry-plane-computation' into 'master'
|\  
| * 2cbe32cc -- Resolve "Optimize symmetry plane computation"
|/  
*   249c4bdd -- Merge branch '139-ground-truth-similarity-stats' into 'master'
|\  
| * da56c781 -- Resolve "Ground truth similarity stats"
|/  
*   d597fb18 -- Merge branch '138-optimize-weighted-hd' into 'master'
|\  
| * 40c9cea8 -- Resolve "Optimize weighted HD"
|/  
*   e7976cff -- Merge branch '137-symmetry-plane-precision-measurement' into 'master'
|\  
| * a6f30d31 -- Added symmetry plane precision measurment
|/  
*   e37267a9 -- Merge branch '136-compute-symmetry-plane-from-feature-points' into 'master'
|\  
| * 4cf89ef5 -- Resolve "Compute symmetry plane from feature points"
|/  
* 5e583ab5 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 023f290d -- Update pom.xml version.
* 3c1e0b70 -- CHANGELOG.md file updated with commits between the current and previous tag.
* ed1bad2b -- Update pom.xml version.
* 4cc13e1c -- Update .gitlab-ci.yml
* a1f16612 -- CHANGELOG.md file updated with commits between the current and previous tag.
* e061166e -- Update pom.xml version.
*   4ba0434e -- Merge branch 'master' of gitlab.fi.muni.cz:grp-fidentis/analyst2
|\  
| * afb2d982 -- Update .gitlab-ci.yml
* | dd5a5496 -- CHANGELOG.md file updated with commits between the current and previous tag.
* | 718dfc43 -- Update pom.xml version.
|/  
* 37c3f4b3 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 9f5d6521 -- Update pom.xml version.
*   2ea14287 -- Merge branch 'master' of gitlab.fi.muni.cz:grp-fidentis/analyst2
|\  
| *   53be5bf6 -- Merge branch '134-update-ci-pipeline' into 'master'
| |\  
| | * 42d46ca2 -- Resolve "Update CI pipeline"
| |/  
* | 445febb1 -- CHANGELOG.md file updated with commits between the current and previous tag.
* | 216d3b07 -- Update pom.xml version.
|/  
*   ec13b772 -- Merge branch '130-split-a-profile-curve-with-respect-to-feature-points' into 'master'
|\  
| * 5c67269f -- Resolve "Split a profile curve with respect to feature points"
|/  
*   55e33200 -- Merge branch '128-support-segmentation-of-a-profile-curve' into 'master'
|\  
| * ede1e5c3 -- Resolve "Support segmentation of a profile curve"
|/  
*   06678872 -- Merge branch '133-fix-gui-changes' into 'master'
|\  
| * 3fb8e0c9 -- Fixed previous dirty merge
|/  
*   02943644 -- Merge branch '132-fix-transparency-of-cutting-planes' into 'master'
|\  
| * d9623f67 -- Fixed
|/  
*   1a6a0aac -- Merge branch '118-introduce-view-toolbox' into 'master'
|\  
| * f3f3b606 -- Resolve "Introduce view toolbox"
|/  
*   80f08293 -- Merge branch '122-add-unit-tests-for-crosssection' into 'master'
|\  
| * 517dccd2 -- Add unit tests for CrossSection
|/  
*   6412ac23 -- Merge branch 'issue121' into 'master'
|\  
| * 1160ce6f -- cross section visitor optimised
|/  
*   86d571d4 -- Merge branch '120-set-transparency-of-cutting-planes' into 'master'
|\  
| * 0f6a22e6 -- Transparency of cutting planes
|/  
*   b1583cce -- Merge branch 'mirror' into 'master'
|\  
| * 2d1f6280 -- mirror cuts in both single and 1:1 views
|/  
*   512a1dfc -- Merge branch 'issue-38/procrustes-superimposition' into 'master'
|\  
| * 3eff067a -- Removed 3point... file resiuum
| * 21cb1809 -- Minor javadoc fixies
| * daacd3e3 -- [#38] refactor: deleted unnecessary files
| * 82cf0ac8 -- [#38] feat: check if amount of fp is more than 3
| * ca38f5a4 -- [#38] refactor: removing procrustes analysis utils class
| * 33d17160 -- [#38] refactor: removing procrustes analysis utils class
| * 8b3f663f -- [#38] documentation
| * 4dd01a3f -- [#38] feat: procrustes scaling
| *   2ed5c65e -- Merge branch 'master' into 'issue-38/procrustes-superimposition'
| |\  
| * | 4123f32e -- [#38] documentation: adding java docs
| * |   83ec7a7c -- Merge branch 'master' into issue-38/procrustes-superimposition
| |\ \  
| * | | 7428bd66 -- [#38] documentation: IPosition documantation
| * | | cda9762b -- [#38] refactor: adding documantation and deleting dead code
| * | | 447b13c8 -- [#38] feat: procrustes analysis working with matrix
| * | | fa57f8e8 -- [#38] feat: procrustes visualisation
| * | | 8bd53bfa -- [#38] feat: rotation on procrustes analysis
| * | | 02ca049a -- [#38] feat: procrustes analysis (work in progress: wrong transformation)
| * | | f7586beb -- [#38] feat: calculation of vertices rotation
| * | | 104198c5 -- [#38] feat: centering models on feature points centroid
* | | |   74aa7589 -- Merge branch '116-show-changes-from-humanface-in-list-of-models-via-eventbus' into 'master'
|\ \ \ \  
| |_|_|/  
|/| | |   
| * | | 0b418e8c -- Resolve "Show changes from HumanFace in list of models (via EventBus)"
|/ / /  
* | |   f51302ba -- Merge branch '117-bug-the-application-freezes-in-1-1-analysis' into 'master'
|\ \ \  
| |_|/  
|/| |   
| * | a7b96146 -- Resolve "BUG: The application freezes in 1:1 analysis"
|/ /  
* |   a216b006 -- Merge branch 'offset' into 'master'
|\ \  
| * | 0664170d -- working offset and alligning of faces made optional
|/ /  
* |   ccb34fd8 -- Merge branch '115-make-logging-into-the-outputwindow-available-from-low-level-modules' into 'master'
|\ \  
| * | a721e90e -- Resolve "Make logging into the OutputWindow available from low-level modules"
|/ /  
* |   42761201 -- Merge branch '113-introduce-new-features-on-list-of-faces-in-a-project' into 'master'
|\ \  
| * \   4e7d7f99 -- Merge branch 'master' into 113-introduce-new-features-on-list-of-faces-in-a-project
| |\ \  
| * | | f216d538 -- added filter and face state panels (without functionality)
| * | | f168c590 -- removed commented code
| * | | ede73147 -- created new table model class which observes HumanFace
| * | | 6c8f10cd -- HumanFace now extends Observable
| * | | 0d12a329 -- created new class DefaultModelsTableModel
| * | |   e20670fa -- Merge branch 'master' into 113-introduce-new-features-on-list-of-faces-in-a-project
| |\ \ \  
| | | |/  
| | |/|   
| * | | 0f7f9e0c -- added "has KD-tree" column to table and multiple model selection
* | | |   972a5551 -- Merge branch 'export_csv' into 'master'
|\ \ \ \  
| |_|_|/  
|/| | |   
| * | | 46b8ca5e -- Exporting polyline profiles
|/ / /  
* | |   dc929d34 -- Merge branch 'issue10' into 'master'
|\ \ \  
| |_|/  
|/| |   
| * | 4224b404 -- Polyline profiles in separate tab
|/ /  
* |   c0329df4 -- Merge branch 'issue-81/fp-io-formats' into 'master'
|\ \  
| |/  
|/|   
| * 4213b08a -- Final minor improvements
| * 8de43d65 -- [#81] test: tests generate tmp files
| * 27c5c426 -- [#81] direcotry: moving files
| * f41c4a32 -- [#81] fix: removed comparison procrustesAnalysis files in development from incorrect branch
| * b77711c4 -- [#81] feat: added procrustesAnalysis exception
| * b0f0ba96 -- [#81] documentation: added java docs and resolved lint wanrings
| * 04d278fb -- [#81] documentation: adding java docs
| *   a88d1df4 -- Merge branch 'master' into issue-81/fp-io-formats
| |\  
| * | 470099d3 -- [#81] documantation: added author
| * | 5de17b10 -- [#81] documantation: added documantation to classes
| * | 9980bca9 -- [issue81]: refactoring export import services
| * | 90e0dbe8 -- [#81] feat: import/export of .fp format
|  /  
* | 85415dbd -- CHANGELOG.md file updated with commits between the current and previous tag.
* | 43727377 -- Update pom.xml version.
|/  
*   ad05bcb5 -- Merge branch '109-fix-gl-canvas-size' into 'master'
|\  
| * 1d737ddc -- Resolve "Fix GL Canvas size"
|/  
*   1c9a6f3d -- Merge branch '112-introduce-a-message-window' into 'master'
|\  
| * ec85edec -- Added OuputWindow and testing output meaasges
|/  
*   c2339a54 -- Merge branch '103-list-of-faces-in-project' into 'master'
|\  
| * 0bcb3c98 -- removed bug when getting name of file throws exception
| * 8179a447 -- updated message when model with same name is loaded
| * 50b0f598 -- removed warnings
| * 96078e98 -- added javadocs
| * 7b65ab15 -- updated attributes, added javadocs
| * b738128e -- pick model from list of models and select analysis
| * 54537962 -- loading models to the project class (not only to the list of models)
| * 3dcc80d3 -- added getShortName method (name of the file without path)
| * 9a374c6f -- merged primaryFace and secondaryFaces to just one attribute, models
| *   813aeb3e -- Merge branch 'master' into 103-list-of-faces-in-project
| |\  
| |/  
|/|   
* |   e6d35d70 -- Merge branch '105-adjust-hd-control-panel' into 'master'
|\ \  
| * | 14d7ce92 -- Removed unchecked and public access warnings
| * | b7637929 -- Misleading parameter replaced in the method signature
| * | 2d77d1ff -- Javadoc added
| * | a314b781 -- More suitable method called to omit unused method parameters
| * | a581e9fd -- Public and private constants reordered
| * | 7a436b3b -- Private and final modifiers added to class attribute
| * | d9a5ca5f -- Recompute Hausdorff distance only if the feature point is selected
| * | 6adb0c77 -- Buttons added to the feature point sliders
| * | 96eaaee7 -- Unused import removed
| * | d69360f1 -- Method for addition of slider option line with buttons implemented
| * | aecd2f51 -- Method for addition of slider with buttons implemented
| * | 23badd6c -- Method for addition of formatted text field with buttons implemented
| * | dd0d8e0c -- Gridwidth moved out of the addButtonCustom method
| * | 4dc74873 -- Methods for addition of slider with value refactored to use new methods for text field and slider addition
| * | 876869f2 -- Method for addition of slider implemented
| * | b084b098 -- Method for addition of formatted text field implemented
| * | b12f8fb6 -- Value-range class for integer implemented
|/ /  
* | 49b0fabb -- Update README.md
* | 919df176 -- Update README.md
* | 3fce6531 -- Update README.md
* |   7b57c496 -- Merge branch '104-better-color-mapping-for-the-whd-heatmap' into 'master'
|\ \  
| * | 682bcfd1 -- Useless class attribute eliminated
| * | f1ddef1d -- Heatmap of weighted HD replaced with a saturated heatmap of regular HD
| * | 7e37cf33 -- Minor refactoring
| * |   eb5313cb -- Merge branch 'master' into 104-better-color-mapping-for-the-whd-heatmap
| |\ \  
| |/ /  
|/| |   
| * | 8f5c6295 -- Variable color saturation added to heatmap renderer
| * | d3a24cda -- Map of heatmap colors saturation added to the drawable face
| * | fdba90b8 -- Double objects replaced with primitive data types
| | * 17bf3917 -- added remove button functionality
| | * c6faaa70 -- added selection buttons functionality
| | *   cbce008e -- Merge branch 'master' into 103-list-of-faces-in-project
| | |\  
| |_|/  
|/| |   
* | |   692a363c -- Merge branch '105-adjust-hd-control-panel' into 'master'
|\ \ \  
| * | | eb5fcf5f -- Methods of slider's mouse listener reduced to MouseListener-class methods only
| * | | 4e9da8b2 -- Hasdorff distance recalculated immediately when feature point highlighted
| * | | efded212 -- Buttons for (de)selection of all feature points added
| * | | 8ca5c086 -- Simple buttons are aligned to the start of the line
| * | | 4def42ab -- More suitable method called
| * | | 589a13ed -- Hausdorff distance recomputed when slider is released
| * | | eebac547 -- Slider mouse events redirected to the text field
| * | | 5d8fb6a8 -- Action listener replaced with key binding
| * | | 091d27c5 -- Identity return replaced with method call
| * | | c35c8a18 -- Typo fixed
| * | | 1146c286 -- Heatmap check-boxes replaced with dropdown menu
| |/ /  
* | |   e81a9fd1 -- Merge branch '106-replace-average-with-weighted-average-function-in-the-computation-of-feature-point-s-weight' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | 493c71a2 -- Variable renamed
| * | 29b865b5 -- Javadoc corrected
| * | cd6496c7 -- Javadoc beautified
| * | 12421b53 -- Computation of feature point weights corrected (weighted average used)
| * | 0d5d262c -- Collector for computation of the weighted average implemented
| * | f5ee16b8 -- Tests modified for correct calculation of feature point weights
|/ /  
| *   6c48d42b -- Merge branch 'master' into 103-list-of-faces-in-project
| |\  
| |/  
|/|   
* | af107f3e -- Removed -n (newver files only) parameter from the mirror command in the publish stage
 /  
* 9e090d11 -- added checkbox to table and reduced name of models
* f93e2b4e -- added table with models
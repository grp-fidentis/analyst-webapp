# FIDENTIS Analyst Web Application

Web application of [FIDENTIS Analyst II](https://gitlab.fi.muni.cz/grp-fidentis/analyst2) desktop software which is re-implementation of [FIDENTIS Analyst](https://github.com/Fidentis/Analyst).
This project is being developed at Masaryk University, Brno, as collaborative project between the [Lasaris](https://lasaris.fi.muni.cz/?lang=en) 
Laboratory at Faculty of Informatics and the Department of Anthropology at Faculty of Science.

---

## Requirements

Modules of the [FIDENTIS Analyst II](https://gitlab.fi.muni.cz/grp-fidentis/analyst2) desktop application.

Backend dependencies:
* [Oracle JDK 21](https://www.oracle.com/java/technologies/downloads/#java21) (with all %JAVA_PATH% set up)
* [Maven](https://www.mkyong.com/maven/how-to-install-maven-in-windows/) (even there is folder `.mvn` and files `mvnw` and `mvnw.cmd` that should help run it without it but it's better to have installed)
* [PostgreSQL 15](https://www.postgresql.org/download/) - for database (setup is described in development guide)

Frontend dependencies:
* [Node](https://nodejs.org/en/) - version: `16.13.1` (the best way to install direct versions of node is via [nvm](https://github.com/nvm-sh/nvm))
* [npm](https://www.npmjs.com/package/npm) - version `8.1.2`

---

## Installation and running

To build whole module run `mvn clean install`. This command also copy frontend part of the application to output `.jar`. <br>
To run application from command line, you have to set your database (see developer\`s guide how to run it locally) and 
run command `mvn spring-boot:run {profile}` from within the `backend` folder.

The application supports 2 profiles:
* production/staging: `-Pprod`
* development: `-Pdev`

### SSL setup and the renewal of certificates

It is necessary to renew the self-signed SSL key every year. To generate this key, you have to run the command `keytool -genkeypair -alias fidentis-ssl -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore fidentis-ssl.p12 -validity 365`
in the command line. After running this command you will be asked to enter a password. The entered password MUST be `fidentis`. Then you will be asked to enter
other information such as name, organizational unit, organization, city, state and two-letter country code. Filling this information
is up to you but according to the truth. The generated key must be placed in `./backend/src/main/resources` folder.

---

## Developer\`s guide

Application is divided into the frontend application via [React](https://reactjs.org/) library, [THREE.js](https://threejs.org/) and other supplement packages.
Backend is written in [Spring Boot](https://spring.io/projects/spring-boot) (version 3.1.2).

### Frontend application

Can be found inside the `frontend` folder. Proxy servers for development are set in: `vite.config.ts`. For tooling is used [Vite](https://vitejs.dev/).

Main language is [Typescript](https://www.typescriptlang.org/) so everything has to be correctly typed (no usage of `any`). And UI component library is [MUI](https://mui.com/).

For styling purposes the packages installed are [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/). Configuration are taken from public packages by [Haaxor1689](https://github.com/Haaxor1689).

#### Folder system:
* `assets` - folder to place any extra `.scss` files and any image/icon that is used inside application
* `components` - folder for all custom components (may change in future if there will be too many)
* `hooks` - folder for all custom hooks
* `pages` - folder any separate view that will be used for routing
* `providers` - folder for all contexts
* `services` - folder for all files that handles backend calls (using [Axios](https://axios-http.com/)). Each file should contain only on `URL` variable that copy backend endpoints mapping
* `types` - folder for all general typings
* `utils`

#### Running frontend for development
First start with command: `npm install` inside folder `./frontend`. This command will install all needed packages that are mentioned inside `package.json`.
<br>
For development run command: `npm run dev` inside folder `./frontend`. This command starts development server with hot reload. This server runs on URL `http://127.0.0.1:5173`
and after every change and save content is reloaded.

#### Using Swagger UI for overview of REST API
You can access REST API documentation on staging server with enabled school vpn or 
you can run backend locally:

- staging server:
  - [swagger UI](http://172.26.2.223:8081/api/swagger-ui)
  - [JSON docs](http://172.26.2.223:8081/api/swagger-ui/docs)
  - [YAML docs](http://172.26.2.223:8081/api/swagger-ui/docs.yaml)
- localhost:
  - [localhost:8080/api/swagger-ui](localhost:8080/api/swagger-ui)
  - [localhost:8080/api/swagger-ui/docs](localhost:8080/api/swagger-ui/docs)
  - [localhost:8080/api/swagger-ui/docs.yaml](localhost:8080/api/swagger-ui/docs.yaml)

### Backend application

Firstly, we have to set up `application.yml`. More precisely the development properties that will match our local setup. Go inside folder: `./backend/src/main/resources`. Here open file: `application-dev.yml`.
This file should contain only your local values (aka postgres database name, username and password). So after creating local database fill these attributes correctly and **!DO NOT PUSH THIS FILE TO ORIGIN!**.

After this setup we can run command: `mvn clean install` if we didn't before for whole project. This will create runnable `.jar` file with frontend application bundled in.
<br>
To start server run command: `mvn spring-boot:run` (`-Pdev` profile is default profile set in `pom.xml` it will start it in development profile, so it will pick development properties).

#### Manipulating database

To create, update, delete tables/columns there is installed [liquibase](https://docs.liquibase.com/tools-integrations/springboot/springboot.html). This provides for developers easy way of writing simple 
scripts (changelogs) that will run at the start of application (only changelogs that didn't already run - there are records in 
database which changelogs already run). Scripts are written in `xml` and files can be found inside 
folder: `./backend/src/main/resources/db/changelog`.

- **changelog-root.xml** - changelog which automatically includes all other changelogs
- **changelog-constraints.xml** - changelog for additional constraints for already created tables
- **changelog-indexes.xml** - changelog for additional indexes for already created tables
- **changelog-inserts.xml** - changelog for SQL insertions
- **tables/XYZ-table_name.xml** - changelogs with definitions of tables
  - Each changelog should contain only one table
  - XYZ should be unique ID (incrementing by 1) and predetermines order of execution of changelog
  - table_name should be equal to table name
  
Each changelog can contain multiple `<changeSet>` tags where each have to have unique id starting with id of file.

#### Running tests

Tests are configured to run automatically on pushed new commit to gitlab via CI or can be run manually locally. 
There are mocked API tests, services integration tests with in-memory database and context initialization test.
- `ApiTestBase` serves as base class for all API tests.
- `ServiceTestBase` serves as base class for context initialization test and all service integration tests.

#### Mail service

To test mail service in development, install NPM package [maildev](https://www.npmjs.com/package/maildev) which provide simple GUI and mail server. All properties for Spring Boot application are already
implemented in `application-dev.yml` file. If you want to run mail server on different ports you have to change these settings also.

#### Package system:
- `analyticaltask` - Analytical task algorithms REST API
- `async` - Config for asynchronous methods
- `auth` - Authentication related classes dealing with passwords, registering, JWT tokens... 
- `base` - Base classes meant as parents to be extended
- `cache` - Config for caching of data
- `compression` - Classes handling compression of data
- `documentation` - Config for OpenApi/Swagger
- `exceptions` - GlobalExceptionHandler and shareable Exceptions
- `fileupload` - Entity fileUpload
- `humanface` - Entity HumanFace
- `mail` - Classes for mail service
- `project` - Entity Project
- `role` - Entity Role
- `security` - Security related classes like filters, permission evaluators, JWT utils...
- `task` - Entity Task
- `user` - Entity User
- `zip` - Classes handling creation of ZIP file

Every entity package usually consists of:
  - Entity class
  - DAO repository class
  - Business service interface and implementation class
  - REST API class
  - DTO classes
  - Exceptions classes related to entity
